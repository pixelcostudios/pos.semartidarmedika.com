<?php

namespace App\Http\Controllers\API;
use App\Cart;
use App\Cartdetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input as input;

class TransactionController extends Controller
{
    public $successStatus = 200;
    public function index()
    {
        if (Input::get('status') != '') {
            if(Input::get('status')== 'update')
            {
                $status    = 1;
            }
            elseif (Input::get('status') == 'process') {
                $status    = 2;
            }
            else
            {
                $status    = 0;
            }
        } else {
            $status    = 0;
        }
        if (Input::get('user_id') != '') {
            $user_id    = Input::get('user_id');
            $post_list  = Cart::where('user_id', '=', $user_id)->where('cart_status', '=', $status)->orderBy('cart_date', 'DESC')->get();
        } else {
            $user_id    = 0;
            $post_list  = array();
        }

        $data_array = array();
        foreach ($post_list as $key) {
            $post_sublist = Cartdetail::leftJoin('posts', 'posts.post_id', '=', 'cart_orders.post')->where('code', '=', $key->cart_code)->get();
            $data_subarray = array();
            foreach ($post_sublist as $key_value)
            {
                $data_subarray[] = array('item'=> $key_value->post_title, 'price' => $key_value->price, 'qty' => $key_value->qty, 'weight' => $key_value->weight);
            }
            $data_array[] = array('cart_id' => $key->cart_id,
            'cart_code' => $key->cart_code,
            'cart_date' => $key->cart_date,
            'cart_status' => $key->cart_status,
            'cart_cost' => $key->cart_cost,
            'cart_data' => $data_subarray);
        }
        return response()->json(['status' => 200, 'message' => 'List Transaction', 'data' => $data_array], $this->successStatus);
    }
}
