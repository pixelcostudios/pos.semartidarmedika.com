@extends('layouts.categories.categories_add')

@section('content')
@include('layouts.menu')
<div class="row wrapper border-bottom white-bg page-heading">
<div class="col-lg-10">
<h2>Merek Barang "{{$type}}"</h2>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="{{ url('home') }}">Home</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ url('categories') }}">Merek Barang</a>
    </li>
    <li class="breadcrumb-item active">
        <strong>All Merek Barang</strong>
    </li>
</ol>
</div>
<div class="col-lg-2">

</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
            <form name="myform" method="post" action="{{ url('categories/create') }}" enctype="multipart/form-data" class="form" data-async>
                @csrf
				<div class="row">
					<div class="col-lg-9 padding-none">
						<div class="ibox ">
							<div class="ibox-title">
								<h5>Merek Barang Editor</h5>
								<a href="{{ url('categories/add?type=') }}{{$type}}" class="btn btn-default pull-left">Add New </a>
								<button type="submit" name="Pixel_Save" class="btn btn-primary float-right">Save </button>
							</div>
							<div class="ibox-content">
								<div class="form-group">
									<label for="title"> Title </label>
                                    <input type="text" class="form-control" name="cat_title" placeholder="Title ID" required="required">
                                    <input type="hidden" class="form-control" name="cat_type" value="{{$type}}">
								</div>
								<div class="row">
									<div class="form-group col-lg-4 col-sm-4 col-xs-12">
										<label for="status">Parent</label>
										<select name="cat_up" class="form-control select2">
											<option value="0">Select Parent / No Parent</option>
											@foreach($cat_list as $p_row)
											<option value="{{$p_row['cat_id']}}">{{$p_row['cat_title']}} [ID#{{$p_row['cat_id']}}]
											</option>
											@endforeach
										</select>
									</div>
									<div class="form-group col-lg-4 col-sm-4 col-xs-12">
										<label for="status">Status</label>
										<select name="cat_status" class="form-control">
											<option value="1">Publish</option>
											<option value="0">Draft</option>
										</select>
									</div>
									<div class="form-group col-lg-4 col-sm-4 col-xs-12">
										<label for="date">Date</label>
										<div class="input-group">
											<input name="cat_date" class="form-control date-picker" type="text" data-date-format="yyyy-mm-dd hh:mm">
											<span class="input-group-addon tooltip-demo">
												<i class="fa fa-calendar" data-toggle="tooltip" data-placement="top" title="Calendar"></i>
											</span>
										</div>
									</div>
								</div>

								<div class="form-group clear">
									<label for="content">Content ID</label>
									<textarea id="content_id" name="cat_summary"></textarea>
								</div>

								<div class="form-group">
									<label for="title">SEO Key ID</label>
									<input type="text" class="form-control" name="cat_key" placeholder="SEO Key ID" value="">
								</div>
								<div class="form-group">
									<label for="title">SEO Desc ID</label>
									<textarea type="text" class="form-control" name="cat_desc" placeholder="SEO Desc ID"></textarea>
								</div>
								<div class="clear"></div>
							</div>
						</div>
					</div>
					<div class="col-lg-3 padding-right-0">
						<div class="bar-title white-bg">
							<div class="tabs-container">
								<div class="tabs-left">
									<ul class="nav nav-tabs tooltip-demo">
										<li><a data-toggle="tab" class="active show" href="#tab_images" aria-expanded="true"> <i class="fa fa-picture-o"
												 data-toggle="tooltip" data-placement="top" title="Images"></i></a></li>
										<li class=""><a data-toggle="tab" href="#tab_icon" aria-expanded="false"><i class="fa fa-dot-circle-o"
												 data-toggle="tooltip" data-placement="top" title="Icon"></i></a></li>
										<li class=""><a data-toggle="tab" href="#tab_thumb" aria-expanded="false"><i class="fa fa fa-camera"
												 data-toggle="tooltip" data-placement="top" title="Thumb"></i></a></li>
										<li class=""><a data-toggle="tab" href="#tab_background" aria-expanded="false"><i class="fa fa-th-large"
												 data-toggle="tooltip" data-placement="top" title="Background"></i></a></li>
										<li class=""><a data-toggle="tab" href="#tab_pdf" aria-expanded="false"><i class="fa fa-bookmark" data-toggle="tooltip"
												 data-placement="top" title="PDF"></i></a></li>
										<li class=""><a data-toggle="tab" href="#tab_files" aria-expanded="false"><i class="fa fa-file" data-toggle="tooltip"
												 data-placement="top" title="Files"></i></a></li>
									</ul>
									<div class="tab-content ">
										<div id="tab_images" class="tab-pane active">
											<div class="panel-body">
                                                <div class="input-file"><input type="file" name="upload_photo[]" multiple="multiple" id="input_images"></div>
											</div>
										</div>
										<div id="tab_icon" class="tab-pane">
											<div class="panel-body">
                                                <div class="input-file"><input type="file" name="upload_icon[]" multiple="multiple" id="input_icon"></div>
											</div>
										</div>
										<div id="tab_thumb" class="tab-pane">
											<div class="panel-body">
                                                <div class="input-file"><input type="file" name="upload_thumb[]" multiple="multiple" id="input_thumb"></div>
											</div>
										</div>
										<div id="tab_background" class="tab-pane">
											<div class="panel-body">
												<div class="input-file"><input type="file" name="upload_background[]" multiple="multiple" id="input_background"></div>
											</div>
										</div>
										<div id="tab_pdf" class="tab-pane">
											<div class="panel-body">
												<div class="input-file"><input type="file" name="upload_pdf[]" multiple="multiple" id="input_pdf"></div>
											</div>
										</div>
										<div id="tab_files" class="tab-pane">
											<div class="panel-body">
												<div class="input-file"><input type="file" name="upload_files[]" multiple="multiple" id="input_files"></div>
											</div>
										</div>
									</div>

								</div>

							</div>
							<!--End Tab-->

							<div class="clear"></div>
						</div>
					</div>
                </div>

            </form>
		</div>
<div class="footer">
<div class="float-right">
<strong>
    </strong>
</div>
<div>
<strong>Copyright</strong> {{$copyright}}
</div>
</div>
@endsection
