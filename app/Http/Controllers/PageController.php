<?php
namespace App\Http\Controllers;

use DB;
use Image;

use App\Page;
use App\Categories;
use App\Meta;
use App\Helpers\Base;
use App\Http\Controllers\Slug;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input as input;

use Illuminate\Http\Request;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        if (Input::get('show') != '') {
            $show = Input::get('show');
        } else {
            $show = 10;
        }

        $post_list = Page::where([['posts.post_type', '=', 'pages'], ['posts.post_up', '=', '0']])->orderBy('date', 'DESC')->paginate($show);

        if (isset($_POST['post-delete']) && $_POST['post-delete'] != "") {
            for ($i = 0; $i < count(@$_POST['post-check']); $i++) {
                $post_id = @$_POST['post-check'][$i];

                $post_type = ['images', 'icon', 'thumb', 'background', 'pdf', 'files'];
                foreach (Page::whereIn('post_type', $post_type)->where([['post_up', '=', $post_id]])->get() as $row_images) {
                    if ($row_images->post_type == 'pdf' or $row_images->post_type == 'files') {
                        @unlink('./upload/files/' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                    } else {
                        @unlink('./upload/' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                        @unlink('./upload/c_' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                        @unlink('./upload/c1_' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                    }
                    DB::table('posts')->where('post_id', '=', $row_images->post_id)->delete();
                }

                DB::table('posts')->where('post_id', '=', $post_id)->delete();
            }
            return redirect('/page/')->with('success', 'Post updated!');
        } elseif (isset($_POST['post-deleted']) && $_POST['post-deleted'] != "") {
            $post_id = $_POST['post_id'];
            $post_type = array('images', 'thumb', 'background', 'pdf', 'files', 'icon');

            $post_type = ['images', 'icon', 'thumb', 'background', 'pdf', 'files'];
            foreach (Page::whereIn('post_type', $post_type)->where([['post_up', '=', $post_id]])->get() as $row_images) {
                if ($row_images->post_type == 'pdf' or $row_images->post_type == 'files') {
                    @unlink('./upload/files/' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                } else {
                    @unlink('./upload/' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                    @unlink('./upload/c_' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                    @unlink('./upload/c1_' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                }
                DB::table('posts')->where('post_id', '=', $row_images->post_id)->delete();
            }

            DB::table('posts')->where('post_id', '=', $post_id)->delete();

            return redirect('/page/')->with('success', 'Post updated!');
        }

        return view('back/page', ['post_list' => $post_list, 'show' => $show, 'copyright' => copyright()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $controller = new SlugController;
        $user_id        =  $request->get('user_id');
        $post_up        = $request->get('post_up');
        $post_slug      = ($request->get('post_title') != '') ? $controller->PostSlug($request->get('post_title'),'-') : '';
        $post_title     = ($request->get('post_title') != '') ? $request->get('post_title') : '';
        $post_content   = ($request->get('post_content') != '') ? $request->get('post_content') : '';
        $post_summary   = ($request->get('post_summary') != '') ? $request->get('post_summary') : '';
        $post_price     = ($request->get('post_price') != '') ? $request->get('post_price') : 0;
        $post_key       = ($request->get('post_key') != '') ? $request->get('post_key') : '';
        $post_thumb     = ($request->get('post_thumb') != '') ? $request->get('post_thumb') : '';
        $post_desc      = ($request->get('post_desc') != '') ? $request->get('post_desc') : '';
        $post_comment   = $request->get('post_comment');
        $date           = ($request->get('date') != '') ? $request->get('date') : date('Y-m-d H:i:s');
        $status         = $request->get('status');

        Page::insert([
            'user_id' => $user_id,
            'post_type' => 'pages',
            'post_up' => $post_up,
            'post_slug' =>  $post_slug,
            'post_title' => $post_title,
            'post_content' => $post_content,
            'post_summary' => $post_summary,
            'post_price' => $post_price,
            'post_unit' => '0',
            'post_link' => '',
            'post_video' => '',
            'post_key' => $post_key,
            'post_desc' => $post_desc,
            'post_count' => '0',
            'post_mime_type' => '',
            'post_comment' => $post_comment,
            'post_thumb' => $post_thumb,
            'date' => $date,
            'status' => $status,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'deleted_at' => date('Y-m-d H:i:s')
        ]);

        $id = DB::getPdo()->lastInsertId();

        if ($request->get('categories') != '') {
            foreach ($request->get('categories') as $categories) {
                if (Meta::where([['meta_type', '=', 'categories'], ['meta_source', '=', $id], ['meta_dest', '=', $categories]])->count() == '0') {
                    $row_cats = Categories::where('cat_id', $categories)->first();
                    Meta::insert([
                        'meta_type'     => 'categories',
                        'meta_source'   => $id,
                        'meta_dest'     => $categories,
                        'meta_title'    => $row_cats->cat_title,
                        'meta_slug'     => $row_cats->cat_slug,
                        'meta_date'     => date('Y-m-d H:i:s'),
                        'meta_status'   => '1'
                    ]);
                }
            }

            if (count((array) $request->get('categories')) > 0) {
                $meta_list = Meta::where([['meta_type', '=', 'categories'], ['meta_source', '=', $id]])->whereNotIn('meta_dest', $request->get('categories'))->get();

                foreach ($meta_list as $key) {
                    Meta::where('meta_id', '=', $key->meta_id)->delete();
                }
            }
        } else {
            $meta_list = Meta::where([['meta_type', '=', 'categories'], ['meta_source', '=', $id]])->get();

            foreach ($meta_list as $key) {
                Meta::where('meta_id', '=', $key->meta_id)->delete();
            }
        }

        if ($request->hasfile('upload_photo')) {
            foreach ($request->file('upload_photo') as $image) {
                $original_name = $image->getClientOriginalName();
                $file_name = ext_name($original_name);
                $file_ext = ext_filename($original_name);

                $resize_image = Image::make($image->getRealPath());
                $resize_image->resize(150, 150, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(public_path() . '/upload/r_' . $controller->PostSlug($file_name) . '.' . $file_ext);

                $img_crop = Image::make($image->getRealPath());
                $img_crop->fit(300);
                // $img->crop($request->input('w'), $request->input('h'), $request->input('x1'), $request->input('y1'));
                $img_crop->save(public_path() . '/upload/c_' . $controller->PostSlug($file_name) . '.' . $file_ext);

                $image->move(public_path() . '/upload/', $controller->PostSlug($file_name) . '.' . $file_ext);
                // $data[] = $original_name;

                Page::insert([
                    'user_id' => $user_id,
                    'post_type' => 'images',
                    'post_up' => $id,
                    'post_slug' =>  $controller->PostSlug($file_name),
                    'post_title' => $file_name,
                    'post_content' => '',
                    'post_summary' => '',
                    'post_price' => '0',
                    'post_unit' => '0',
                    'post_link' => '',
                    'post_video' => '',
                    'post_key' => '',
                    'post_desc' => '',
                    'post_count' => '0',
                    'post_mime_type' => $file_ext,
                    'post_comment' => '1',
                    'date' => date('Y-m-d H:i:s'),
                    'status' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'deleted_at' => date('Y-m-d H:i:s')
                ]);
            }
        }
        if ($request->hasfile('upload_icon')) {
            foreach ($request->file('upload_icon') as $image) {
                $original_name = $image->getClientOriginalName();
                $file_name = ext_name($original_name);
                $file_ext = ext_filename($original_name);

                $image->move(public_path() . '/upload/', $controller->PostSlug($file_name) . '.' . $file_ext);

                Page::insert([
                    'user_id' => $user_id,
                    'post_type' => 'icon',
                    'post_up' => $id,
                    'post_slug' =>  $controller->PostSlug($file_name),
                    'post_title' => $file_name,
                    'post_content' => '',
                    'post_summary' => '',
                    'post_price' => '0',
                    'post_unit' => '0',
                    'post_link' => '',
                    'post_video' => '',
                    'post_key' => '',
                    'post_desc' => '',
                    'post_count' => '0',
                    'post_mime_type' => $file_ext,
                    'post_comment' => '1',
                    'date' => date('Y-m-d H:i:s'),
                    'status' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'deleted_at' => date('Y-m-d H:i:s')
                ]);
            }
        }
        if ($request->hasfile('upload_thumb')) {
            foreach ($request->file('upload_thumb') as $image) {
                $original_name = $image->getClientOriginalName();
                $file_name = ext_name($original_name);
                $file_ext = ext_filename($original_name);

                $image->move(public_path() . '/upload/', $controller->PostSlug($file_name) . '.' . $file_ext);

                Page::insert([
                    'user_id' => $user_id,
                    'post_type' => 'thumb',
                    'post_up' => $id,
                    'post_slug' =>  $controller->PostSlug($file_name),
                    'post_title' => $file_name,
                    'post_content' => '',
                    'post_summary' => '',
                    'post_price' => '0',
                    'post_unit' => '0',
                    'post_link' => '',
                    'post_video' => '',
                    'post_key' => '',
                    'post_desc' => '',
                    'post_count' => '0',
                    'post_mime_type' => $file_ext,
                    'post_comment' => '1',
                    'date' => date('Y-m-d H:i:s'),
                    'status' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'deleted_at' => date('Y-m-d H:i:s')
                ]);
            }
        }
        if ($request->hasfile('upload_background')) {
            foreach ($request->file('upload_background') as $image) {
                $original_name = $image->getClientOriginalName();
                $file_name = ext_name($original_name);
                $file_ext = ext_filename($original_name);

                $image->move(public_path() . '/upload/', $controller->PostSlug($file_name) . '.' . $file_ext);

                Page::insert([
                    'user_id' => $user_id,
                    'post_type' => 'background',
                    'post_up' => $id,
                    'post_slug' =>  $controller->PostSlug($file_name),
                    'post_title' => $file_name,
                    'post_content' => '',
                    'post_summary' => '',
                    'post_price' => '0',
                    'post_unit' => '0',
                    'post_link' => '',
                    'post_video' => '',
                    'post_key' => '',
                    'post_desc' => '',
                    'post_count' => '0',
                    'post_mime_type' => $file_ext,
                    'post_comment' => '1',
                    'date' => date('Y-m-d H:i:s'),
                    'status' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'deleted_at' => date('Y-m-d H:i:s')
                ]);
            }
        }
        if ($request->hasfile('upload_pdf')) {
            foreach ($request->file('upload_pdf') as $image) {
                $original_name = $image->getClientOriginalName();
                $file_name = ext_name($original_name);
                $file_ext = ext_filename($original_name);

                $image->move(public_path() . '/upload/', $controller->PostSlug($file_name) . '.' . $file_ext);

                Page::insert([
                    'user_id' => $user_id,
                    'post_type' => 'pdf',
                    'post_up' => $id,
                    'post_slug' =>  $controller->PostSlug($file_name),
                    'post_title' => $file_name,
                    'post_content' => '',
                    'post_summary' => '',
                    'post_price' => '0',
                    'post_unit' => '0',
                    'post_link' => '',
                    'post_video' => '',
                    'post_key' => '',
                    'post_desc' => '',
                    'post_count' => '0',
                    'post_mime_type' => $file_ext,
                    'post_comment' => '1',
                    'date' => date('Y-m-d H:i:s'),
                    'status' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'deleted_at' => date('Y-m-d H:i:s')
                ]);
            }
        }
        if ($request->hasfile('upload_files')) {
            foreach ($request->file('upload_files') as $image) {
                $original_name = $image->getClientOriginalName();
                $file_name = ext_name($original_name);
                $file_ext = ext_filename($original_name);

                $image->move(public_path() . '/upload/', $controller->PostSlug($file_name) . '.' . $file_ext);

                Page::insert([
                    'user_id' => $user_id,
                    'post_type' => 'files',
                    'post_up' => $id,
                    'post_slug' =>  $controller->PostSlug($file_name),
                    'post_title' => $file_name,
                    'post_content' => '',
                    'post_summary' => '',
                    'post_price' => '0',
                    'post_unit' => '0',
                    'post_link' => '',
                    'post_video' => '',
                    'post_key' => '',
                    'post_desc' => '',
                    'post_count' => '0',
                    'post_mime_type' => $file_ext,
                    'post_comment' => '1',
                    'date' => date('Y-m-d H:i:s'),
                    'status' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'deleted_at' => date('Y-m-d H:i:s')
                ]);
            }
        }
        return redirect('/page/edit/' . $id)->with('success', 'Post updated!');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page)
    {
        //
    }

    public function search()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        if (Input::get('show') != '') {
            $show = Input::get('show');
        } else {
            $show = 10;
        }
        if (Input::get('query') != '') {
            $query = Input::get('query');
        } else {
            $query = '';
        }

        $post_list = Page::where('posts.post_type', 'post')->where('post_title', 'like', '%' . $query . '%')->orWhere('post_content', 'like', '%' . $query . '%')->orWhere('post_summary', 'like', '%' . $query . '%')->orderBy('date', 'DESC')->paginate($show);

        if (isset($_POST['post-delete']) && $_POST['post-delete'] != "") {
            for ($i = 0; $i < count(@$_POST['post-check']); $i++) {
                $post_id = @$_POST['post-check'][$i];

                $post_type = ['images', 'icon', 'thumb', 'background', 'pdf', 'files'];
                foreach (Page::whereIn('post_type', $post_type)->where([['post_up', '=', $post_id]])->get() as $row_images) {
                    if ($row_images->post_type == 'pdf' or $row_images->post_type == 'files') {
                        @unlink('./upload/files/' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                    } else {
                        @unlink('./upload/' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                        @unlink('./upload/c_' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                        @unlink('./upload/c1_' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                    }
                    DB::table('posts')->where('post_id', '=', $row_images->post_id)->delete();
                }

                DB::table('posts')->where('post_id', '=', $post_id)->delete();
            }
            return redirect('/page/')->with('success', 'Post updated!');
        } elseif (isset($_POST['post-deleted']) && $_POST['post-deleted'] != "") {
            $post_id = $_POST['post_id'];
            $post_type = array('images', 'thumb', 'background', 'pdf', 'files', 'icon');

            $post_type = ['images', 'icon', 'thumb', 'background', 'pdf', 'files'];
            foreach (Page::whereIn('post_type', $post_type)->where([['post_up', '=', $post_id]])->get() as $row_images) {
                if ($row_images->post_type == 'pdf' or $row_images->post_type == 'files') {
                    @unlink('./upload/files/' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                } else {
                    @unlink('./upload/' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                    @unlink('./upload/c_' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                    @unlink('./upload/c1_' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                }
                DB::table('posts')->where('post_id', '=', $row_images->post_id)->delete();
            }

            DB::table('posts')->where('post_id', '=', $post_id)->delete();

            return redirect('/page/')->with('success', 'Post updated!');
        }

        return view('back/page_search', ['post_list' => $post_list, 'show' => $show, 'query' => $query, 'copyright' => copyright()]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        $post_edit      = Page::where('post_id', $id)->get();
        $post_list      = post_list_recrusive($post_type = 'pages', $post_up = 0, $depth = 5, $now = 0); //Page::where('post_type', 'post')->orderBy('date', 'DESC')->get();
        $user_list      = DB::table('users')->join('users_groups', 'users.id', '=', 'users_groups.user_id')->get();
        $images_list    = Page::where([['post_type', '=', 'images'], ['post_up', '=', $id]])->get();
        $icon_list      = Page::where([['post_type', '=', 'icon'], ['post_up', '=', $id]])->get();
        $thumb_list     = Page::where([['post_type', '=', 'thumb'], ['post_up', '=', $id]])->get();
        $background_list= Page::where([['post_type', '=', 'background'], ['post_up', '=', $id]])->get();
        $pdf_list       = Page::where([['post_type', '=', 'pdf'], ['post_up', '=', $id]])->get();
        $files_list     = Page::where([['post_type', '=', 'files'], ['post_up', '=', $id]])->get();

        return view('back/page_edit', [
            'post_edit' => $post_edit,
            'post_list' => $post_list,
            'user_list' => $user_list,
            'images_list' => $images_list,
            'icon_list' => $icon_list,
            'thumb_list' => $thumb_list,
            'background_list' => $background_list,
            'pdf_list' => $pdf_list,
            'files_list' => $files_list,
            'copyright' => copyright()
        ]);
    }
    public function add()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        $post_list      = post_list_recrusive($post_type = 'pages', $post_up = 0, $depth = 5, $now = 0); //Page::where('post_type', 'post')->get();
        $user_list      = DB::table('users')->join('users_groups', 'users.id', '=', 'users_groups.user_id')->get();

        return view('back/page_add', [
            'post_list' => $post_list,
            'user_list' => $user_list,
            'copyright' => copyright()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $controller = new SlugController;
        $post_data = Page::find($id);
        $post_data->user_id =  $request->get('user_id');
        $post_data->post_up = $request->get('post_up');
        $post_data->post_slug = ($request->get('post_title') != '') ? $controller->PostSlug($request->get('post_title'),$id) : '';
        //$controller->PostSlug(Page::find($request->get('post_up'))->value('post_slug).'-'.$request->get('post_title'));
        $post_data->post_title = ($request->get('post_title') != '') ? $request->get('post_title') : '';
        $post_data->post_content = ($request->get('post_content') != '') ? $request->get('post_content') : '';
        $post_data->post_summary = ($request->get('post_summary') != '') ? $request->get('post_summary') : '';
        $post_data->post_price = ($request->get('post_price') != '') ? $request->get('post_price') : '';
        $post_data->post_key = ($request->get('post_key') != '') ? $request->get('post_key') : '';
        $post_data->post_desc = ($request->get('post_desc') != '') ? $request->get('post_desc') : '';
        $post_data->post_comment = $request->get('post_comment');
        $post_data->post_thumb = $request->get('post_thumb');
        $post_data->date = ($request->get('date') != '') ? $request->get('date') : date('Y-m-d H:i:s');
        $post_data->status = $request->get('status');
        $post_data->save();

        if($request->get('Pixel_D_Images'))
        {
            if (is_array(@$_POST['post-check'])) {
                for ($i = 0; $i < count(@$_POST['post-check']); $i++) {
                    $i_name = explode(",", $_POST['post-check'][$i]);

                    if (file_exists(public_path() . '/upload/' . $i_name[1])) {
                        unlink(public_path() . "/upload/" . $i_name[1]);
                    }
                    if (file_exists(public_path() . '/upload/c_' . $i_name[1])) {
                        unlink(public_path() . "/upload/c_" . $i_name[1]);
                    }
                    if (file_exists(public_path() . '/upload/r_' . $i_name[1])) {
                        unlink(public_path() . "/upload/r_" . $i_name[1]);
                    }
                    DB::table('posts')->where('post_id', '=', $i_name[0])->delete();
                }
            }
        }
        if ($request->get('Pixel_D_PDF'))
        {
            if (is_array(@$_POST['pdf-check'])) {
                for ($i = 0; $i < count(@$_POST['pdf-check']); $i++) {
                    $i_name = explode(",", $_POST['pdf-check'][$i]);

                    if (file_exists(public_path() . '/upload/files/' . $i_name[1])) {
                        unlink(public_path() . "/upload/files/" . $i_name[1]);
                    }
                    DB::table('posts')->where('post_id', '=', $i_name[0])->delete();
                }
            }
        }
        if ($request->get('Pixel_D_Files'))
        {
            if (is_array(@$_POST['files-check'])) {
                for ($i = 0; $i < count(@$_POST['files-check']); $i++) {
                    $i_name = explode(",", $_POST['files-check'][$i]);

                    if (file_exists(public_path() . '/upload/files/' . $i_name[1])) {
                        unlink(public_path() . "/upload/files/" . $i_name[1]);
                    }
                    DB::table('posts')->where('post_id', '=', $i_name[0])->delete();
                }
            }
        }

        if ($request->get('categories') != '') {
            foreach ($request->get('categories') as $categories) {
                if (Meta::where([['meta_type', '=', 'categories'], ['meta_source', '=', $id], ['meta_dest', '=', $categories]])->count() == '0') {
                    $row_cats = Categories::where('cat_id', $categories)->first();
                    Meta::insert([
                        'meta_type'     => 'categories',
                        'meta_source'   => $id,
                        'meta_dest'     => $categories,
                        'meta_title'    => $row_cats->cat_title,
                        'meta_slug'     => $row_cats->cat_slug,
                        'meta_date'     => date('Y-m-d H:i:s'),
                        'meta_status'   => '1'
                    ]);
                }
            }

            if (count((array) $request->get('categories')) > 0) {
                $meta_list = Meta::where([['meta_type', '=', 'categories'], ['meta_source', '=', $id]])->whereNotIn('meta_dest', $request->get('categories'))->get();

                foreach ($meta_list as $key) {
                    Meta::where('meta_id', '=', $key->meta_id)->delete();
                }
            }
        } else {
            $meta_list = Meta::where([['meta_type', '=', 'categories'], ['meta_source', '=', $id]])->get();

            foreach ($meta_list as $key) {
                Meta::where('meta_id', '=', $key->meta_id)->delete();
            }
        }

        if ($request->hasfile('upload_photo')) {
            foreach ($request->file('upload_photo') as $image) {
                $original_name = $image->getClientOriginalName();
                $file_name = ext_name($original_name);
                $file_ext = ext_filename($original_name);

                $resize_image = Image::make($image->getRealPath());
                $resize_image->resize(150, 150, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(public_path() . '/upload/r_' . $controller->PostSlug($file_name) . '.' . $file_ext);

                $img_crop = Image::make($image->getRealPath());
                $img_crop->fit(300);
                // $img->crop($request->input('w'), $request->input('h'), $request->input('x1'), $request->input('y1'));
                $img_crop->save(public_path() . '/upload/c_' . $controller->PostSlug($file_name) . '.' . $file_ext);

                $image->move(public_path() . '/upload/', $controller->PostSlug($file_name) . '.' . $file_ext);
                // $data[] = $original_name; //Auth::id()

                Page::insert([
                    'user_id' => $request->get('user_id'),
                    'post_type' => 'images',
                    'post_up' => $id,
                    'post_slug' =>  $controller->PostSlug($file_name),
                    'post_title' => $file_name,
                    'post_content' => '',
                    'post_summary' => '',
                    'post_price' => '0',
                    'post_unit' => '0',
                    'post_link' => '',
                    'post_video' => '',
                    'post_key' => '',
                    'post_desc' => '',
                    'post_count' => '0',
                    'post_mime_type' => $file_ext,
                    'post_comment' => '1',
                    'date' => date('Y-m-d H:i:s'),
                    'status' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'deleted_at' => date('Y-m-d H:i:s')
                ]);
            }
        }
        if ($request->hasfile('upload_icon')) {
            foreach ($request->file('upload_icon') as $image) {
                $original_name = $image->getClientOriginalName();
                $file_name = ext_name($original_name);
                $file_ext = ext_filename($original_name);

                $image->move(public_path() . '/upload/', $controller->PostSlug($file_name) . '.' . $file_ext);

                Page::insert([
                    'user_id' => $request->get('user_id'),
                    'post_type' => 'icon',
                    'post_up' => $id,
                    'post_slug' =>  $controller->PostSlug($file_name),
                    'post_title' => $file_name,
                    'post_content' => '',
                    'post_summary' => '',
                    'post_price' => '0',
                    'post_unit' => '0',
                    'post_link' => '',
                    'post_video' => '',
                    'post_key' => '',
                    'post_desc' => '',
                    'post_count' => '0',
                    'post_mime_type' => $file_ext,
                    'post_comment' => '1',
                    'date' => date('Y-m-d H:i:s'),
                    'status' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'deleted_at' => date('Y-m-d H:i:s')
                ]);
            }
        }
        if ($request->hasfile('upload_thumb')) {
            foreach ($request->file('upload_thumb') as $image) {
                $original_name = $image->getClientOriginalName();
                $file_name = ext_name($original_name);
                $file_ext = ext_filename($original_name);

                $image->move(public_path() . '/upload/', $controller->PostSlug($file_name) . '.' . $file_ext);

                Page::insert([
                    'user_id' => $request->get('user_id'),
                    'post_type' => 'thumb',
                    'post_up' => $id,
                    'post_slug' =>  $controller->PostSlug($file_name),
                    'post_title' => $file_name,
                    'post_content' => '',
                    'post_summary' => '',
                    'post_price' => '0',
                    'post_unit' => '0',
                    'post_link' => '',
                    'post_video' => '',
                    'post_key' => '',
                    'post_desc' => '',
                    'post_count' => '0',
                    'post_mime_type' => $file_ext,
                    'post_comment' => '1',
                    'date' => date('Y-m-d H:i:s'),
                    'status' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'deleted_at' => date('Y-m-d H:i:s')
                ]);
            }
        }
        if ($request->hasfile('upload_background')) {
            foreach ($request->file('upload_background') as $image) {
                $original_name = $image->getClientOriginalName();
                $file_name = ext_name($original_name);
                $file_ext = ext_filename($original_name);

                $image->move(public_path() . '/upload/', $controller->PostSlug($file_name) . '.' . $file_ext);

                Page::insert([
                    'user_id' => $request->get('user_id'),
                    'post_type' => 'background',
                    'post_up' => $id,
                    'post_slug' =>  $controller->PostSlug($file_name),
                    'post_title' => $file_name,
                    'post_content' => '',
                    'post_summary' => '',
                    'post_price' => '0',
                    'post_unit' => '0',
                    'post_link' => '',
                    'post_video' => '',
                    'post_key' => '',
                    'post_desc' => '',
                    'post_count' => '0',
                    'post_mime_type' => $file_ext,
                    'post_comment' => '1',
                    'date' => date('Y-m-d H:i:s'),
                    'status' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'deleted_at' => date('Y-m-d H:i:s')
                ]);
            }
        }
        if ($request->hasfile('upload_pdf')) {
            foreach ($request->file('upload_pdf') as $image) {
                $original_name = $image->getClientOriginalName();
                $file_name = ext_name($original_name);
                $file_ext = ext_filename($original_name);

                $image->move(public_path() . '/upload/files/', $controller->PostSlug($file_name) . '.' . $file_ext);

                Page::insert([
                    'user_id' => $request->get('user_id'),
                    'post_type' => 'pdf',
                    'post_up' => $id,
                    'post_slug' =>  $controller->PostSlug($file_name),
                    'post_title' => $file_name,
                    'post_content' => '',
                    'post_summary' => '',
                    'post_price' => '0',
                    'post_unit' => '0',
                    'post_link' => '',
                    'post_video' => '',
                    'post_key' => '',
                    'post_desc' => '',
                    'post_count' => '0',
                    'post_mime_type' => $file_ext,
                    'post_comment' => '1',
                    'date' => date('Y-m-d H:i:s'),
                    'status' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'deleted_at' => date('Y-m-d H:i:s')
                ]);
            }
        }
        if ($request->hasfile('upload_files')) {
            foreach ($request->file('upload_files') as $image) {
                $original_name = $image->getClientOriginalName();
                $file_name = ext_name($original_name);
                $file_ext = ext_filename($original_name);

                $image->move(public_path() . '/upload/files/', $controller->PostSlug($file_name) . '.' . $file_ext);

                Page::insert([
                    'user_id' => $request->get('user_id'),
                    'post_type' => 'files',
                    'post_up' => $id,
                    'post_slug' =>  $controller->PostSlug($file_name),
                    'post_title' => $file_name,
                    'post_content' => '',
                    'post_summary' => '',
                    'post_price' => '0',
                    'post_unit' => '0',
                    'post_link' => '',
                    'post_video' => '',
                    'post_key' => '',
                    'post_desc' => '',
                    'post_count' => '0',
                    'post_mime_type' => $file_ext,
                    'post_comment' => '1',
                    'date' => date('Y-m-d H:i:s'),
                    'status' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'deleted_at' => date('Y-m-d H:i:s')
                ]);
            }
        }
        return redirect('/page/edit/' . $id)->with('success', 'Post updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page)
    {
        //
    }
}
