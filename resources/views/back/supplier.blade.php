@extends('layouts.post.post')

@section('content')
@include('layouts.menu')
<div class="row wrapper border-bottom white-bg page-heading">
<div class="col-lg-10">
<h2>Supplier</h2>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="{{url('/')}}">Home</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{url('supplier/')}}">Supplier</a>
    </li>
    <li class="breadcrumb-item active">
        <strong>All Supplier</strong>
    </li>
</ol>
</div>
<div class="col-lg-2">

</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
<div class="row">
<div class="col-lg-12">
    <div class="ibox ">

            <div class="ibox-title">
                <h5>Manage Supplier</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-wrench"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="col-sm-3">
                        <form method="get" action="{{ url('supplier/search') }}">
                    @csrf
                        <div class="input-group"><input type="text" name="query" placeholder="Search" class="input-sm form-control">
                            <span class="input-group-btn">
                                <button type="submit" name="Pixel_Search" class="btn btn-sm btn-primary"> Go!</button> </span></div>
                        </form>
                    </div>
                    <div class="col-sm-6 m-b-xs">
                        <div data-toggle="buttons" class="btn-group">
                            <label onclick="location.href='{{ url('/') }}/supplier/add'" class="btn btn-sm btn-primary">
                                <i class="fa fa-plus"></i> Tambah </label>
                            <!--<label class="btn btn-sm btn-white active"> <input type="radio" id="option2" name="options"> Week </label>
                                                    <label class="btn btn-sm btn-white"> <input type="radio" id="option3" name="options"> Month </label>-->
                        </div>
                    </div>
                    <div class="col-sm-3 m-b-xs">

                        <select id="select-show" onchange="window.location = jQuery('#select-show option:selected').val();" class="input-sm form-control input-s-sm inline float-right category-show">
                            <option  value="{{ url('/') }}/supplier/?show=10" @if($show=='10')selected @endif>10</option>
                            <option  value="{{ url('/') }}/supplier/?show=25" @if($show=='25')selected @endif>25</option>
                            <option  value="{{ url('/') }}/supplier/?show=50" @if($show=='50')selected @endif>50</option>
                            <option  value="{{ url('/') }}/supplier/?show=100" @if($show=='100')selected @endif>100</option>
                        </select>
                    </div>
                </div>
                <div class="table-responsive">
                    <form method="post">
                    @csrf
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th width="1%">
                                    <label>
                                        <input type="checkbox" class="colored-green i-checks checkall">
                                        <span class="text"></span>
                                    </label>
                                </th>
                                <th>Nama Supplier </th>
                                <th>Sales </th>
                                <th>Alamat </th>
                                <th>Phone</th>
                                <th>Rekening</th>
                                <th width="7%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($post_list as $p)
                            <tr>
                                <td>
                                    <label>
                                        <input type="checkbox" name="post-check[]" value="{{  $p->suplier_id }}" class="colored-green">
                                        <span class="text"></span>
                                    </label>
                                </td>
                                <td><a href="{{url('/')}}/supplier/edit/{{$p->suplier_id}}">{{$p->suplier_nama}}</a></td>
                                <td>
                                   {{$p->suplier_sales}}
                                </td>
                                <td>
                                   {{$p->suplier_alamat}}
                                </td>
                                <td>
                                   {{$p->suplier_notelp}}
                                </td>
                                <td>
                                   {{$p->suplier_rekening}}
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <a class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                            Action
                                        </a>
                                        <ul class="dropdown-menu drop-right">
                                            <li><a href="{{url('/')}}/supplier/edit/{{  $p->suplier_id }}">Edit</a></li>
                                            <li><a class="open-modal_deleted" data-toggle="modal" href="#" data-id="{{  $p->suplier_id }}"
                                                    data-target="#modal_deleted">Delete</a></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                        @if (count($post_list) > 0)
                        <tfoot>
                            <tr>
                                <td colspan="9" class="footable-visible">
                                    <div class="pull-left margin-top-20">
                                        <div class="btn-group">
                                            <div class="btn-group dropup">
                                                <button type="button" class="btn btn-default btn-custom button-sm dropdown-toggle" data-toggle="dropdown"
                                                    aria-expanded="false">
                                                    <i class="fa fa-ellipsis-horizontal"></i> Action
                                                </button>
                                                <ul class="dropdown-menu dropdown-archive">
                                                    <li>
                                                        <button type="button" data-toggle="modal" data-target="#modal_delete" class="btn btn-default btn-sm"><i
                                                                class="fa fa-trash-o"></i>
                                                            Delete</button>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                    @if ($post_list->lastPage() > 1)
                                            <ul class="pagination pull-right">
                                                <li class="page-item"><a href="#" class="page-link">{{ $post_list->total() }} Item</a></li>
                                                <li class="{{ ($post_list->currentPage() == 1) ? ' disabled' : '' }} page-item">
                                                    <a class=" page-link " href="{{ $post_list->url(1) }}&show={{$show}}" aria-label="Previous">
                                                        <span aria-hidden="true">&laquo;</span>
                                                        <span class="sr-only">Previous</span>
                                                    </a>
                                                </li>
                                                @if ( $post_list->currentPage() > 5 )
                                                <li class="page-item">
                                                    <a class="page-link" tabindex="-1">...</a>
                                                </li>
                                                @endif

                                                @for ($i = 1; $i <= $post_list->lastPage(); $i++)
                                                    @if ( ($i > ($post_list->currentPage() - 5)) && ($i < ($post_list->currentPage() + 5)) )
                                                    <li class="{{ ($post_list->currentPage() == $i) ? ' active' : '' }} page-item">
                                                        <a class=" page-link " href="{{ $post_list->url($i) }}&show={{$show}}">{{ $i }}</a>
                                                    </li>
                                                    @endif
                                                @endfor
                                                @if ( $post_list->currentPage() < ($post_list->lastPage() - 4) )
                                                <li class="page-item">
                                                    <a class="page-link" tabindex="-1">...</a>
                                                </li>
                                                @endif

                                                <li class="{{ ($post_list->currentPage() == $post_list->lastPage()) ? ' disabled' : '' }} page-item">
                                                    <a href="{{ $post_list->url($post_list->currentPage()+1) }}&show={{$show}}" class="page-link" aria-label="Next">
                                                        <span aria-hidden="true">&raquo;</span>
                                                        <span class="sr-only">Next</span>
                                                    </a>
                                                </li>
                                                <li class="page-item {{($post_list->currentPage()==$post_list->lastPage())?'disabled':''}}">
                                                    <a class="page-link" href="{{ $post_list->url($post_list->lastPage()) }}">
                                                        Last
                                                    </a>
                                                </li>
                                            </ul>
                                    @endif

                                </td>
                            </tr>
                        </tfoot>
                        @endif
                       </table>

            <div class="modal fade" id="modal_delete">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title">Anda yakin ?</h4>

                        </div>
                        <div class="modal-body">
                            <p>Apakah anda akan menghapus Supplier ini.</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" name="post-delete" value="delete" class="btn btn-danger">Delete</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="modal_deleted">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title">Anda Yakin ?</h4>

                        </div>
                        <div class="modal-body">
                            <p>Apakah anda akan menghapus Supplier ini.</p>
                            <input type="hidden" name="Supplier_id" id="Supplier_id" value="" />
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" name="post-deleted" value="delete" class="btn btn-danger">Delete</button>
                        </div>
                    </div>
                </div>
            </div>


        </form>
    </div>
</div>
</div>
</div>

</div>
</div>
<div class="footer">
<div class="float-right">
<strong>
    </strong>
</div>
<div>
<strong>Copyright</strong> {{$copyright}}
</div>
</div>
@endsection
