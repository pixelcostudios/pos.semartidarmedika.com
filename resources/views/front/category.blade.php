@extends('layouts.front.post')
@section('content')
@include('layouts.front.header_post')
<!-- ++++ Most Bold Title ++++ -->

<section class="banner  o-hidden banner-inner about-banner">
    <div class="container">
        <!--banner text-->
        <div class="banner-txt">
            <h1>{{$title}}</h1>
            <p class="semi-bold"></p>
        <!--end banner text-->
    </div>
</section>
<section class="bg-white o-hidden blog-p2 blog-content">
        <!--blog content row one-->
        <div class="container">
            <div class="row">
                @foreach ($post_list as $key)
                <!--blog content box-->
                <div class="col-xs-12 col-sm-4 margin-bottom-30">
                    <div class="box-content-with-img equalheight is-featured">
                        @foreach (App\Page::where('post_type', 'images')->where([['post_up', '=', $key->post_id]])->skip(0)->take(1)->get() as $p2)
                            <a href="{{url('/')}}/{{$key->post_slug}}"><img src="{{url('/')}}/thumb/crop/350/300/{{$p2->post_slug}}.{{$p2->post_mime_type}}"
                                alt="{{$key->post_title}}" class="img-responsive" width="100%"></a>
                            @endforeach

                        <div class="box-content-text">
                            <p class="gray-text"><span class="icon-calendar-full"></span>{{$key->date}}</p>
                            <h3 class="semi-bold"><a href="{{url('/')}}/{{$key->post_slug}}">{{$key->post_title}}</a></h3>
                            <p class="regular-text">{{substr(strip_tags($key->post_content),0,120)}}</p>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        <div class="row clear">
                    <div class="col-sm-12">
                        @if ($post_list->lastPage() > 1)
                    <ul class="pagination">
                        <li class="{{ ($post_list->currentPage() == 1) ? ' disabled' : '' }} page-item">
                            <a class=" page-link " href="{{ $post_list->url(1) }}" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                                <span class="sr-only">Previous</span>
                            </a>
                        </li>
                        @if ( $post_list->currentPage() > 5 )
                        <li class="page-item">
                            <a class="page-link" tabindex="-1">...</a>
                        </li>
                        @endif

                        @for ($i = 1; $i <= $post_list->lastPage(); $i++)
                            @if ( ($i > ($post_list->currentPage() - 5)) && ($i < ($post_list->currentPage() + 5)) )
                            <li class="{{ ($post_list->currentPage() == $i) ? ' active' : '' }} page-item">
                                <a class=" page-link " href="{{ $post_list->url($i) }}">{{ $i }}</a>
                            </li>
                            @endif
                        @endfor
                        @if ( $post_list->currentPage() < ($post_list->lastPage() - 4) )
                        <li class="page-item">
                            <a class="page-link" tabindex="-1">...</a>
                        </li>
                        @endif

                        <li class="{{ ($post_list->currentPage() == $post_list->lastPage()) ? ' disabled' : '' }} page-item">
                            <a href="{{ $post_list->url($post_list->currentPage()+1) }}" class="page-link" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                                <span class="sr-only">Next</span>
                            </a>
                        </li>
                        <li class="page-item {{($post_list->currentPage()==$post_list->lastPage())?'disabled':''}}">
                            <a class="page-link" href="{{ $post_list->url($post_list->lastPage()) }}">
                                &raquo;&raquo;
                            </a>
                        </li>
                    </ul>
            @endif
                    </div>
                </div>
        <!--end blog content row one-->
    </section>

@include('layouts.front.footer')
@endsection
