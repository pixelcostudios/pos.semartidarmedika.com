@extends('layouts.post.post_edit')

@section('content')
@include('layouts.menu')
<div class="row wrapper border-bottom white-bg page-heading">
<div class="col-lg-10">
<h2>Unit</h2>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="{{ url('home') }}">Home</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ url('item/unit') }}">Unit</a>
    </li>
    <li class="breadcrumb-item active">
        <strong>All Unit</strong>
    </li>
</ol>
</div>
<div class="col-lg-2">

</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    @foreach($post_edit as $p)
            <form name="myform" method="post" action="{{ url('item/unit_update') }}/{{$p->unit_id}}" enctype="multipart/form-data" class="form" data-async>
                @csrf
				<div class="row">
					<div class="col-lg-12 padding-none">
						<div class="ibox ">
							<div class="ibox-title">
								<h5>Unit Editor</h5>
								<button type="submit" name="Pixel_Save" class="btn btn-primary float-right">Save Unit</button>
							</div>
							<div class="ibox-content">
								<div class="form-group">
									<label for="title">Unit  </label>
									<input type="text" class="form-control" name="unit_nama" placeholder="Unit " value="{{$p->unit_nama}}" required="required">
                                </div>

								<div class="clear"></div>
							</div>
						</div>
					</div>
                </div>

            </form>
            @endforeach
		</div>
<div class="footer">
<div class="float-right">
<strong>
    </strong>
</div>
<div>
<strong>Copyright</strong> {{$copyright}}
</div>
</div>
@endsection
