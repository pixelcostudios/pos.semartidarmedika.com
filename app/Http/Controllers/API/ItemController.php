<?php

namespace App\Http\Controllers\API;

use DB;
use App\Item;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input as input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ItemController extends Controller
{
    public $successStatus = 200;
    // public function index()
    // {
    //     if (Input::get('user_id') != '') {
    //         $user_id = Input::get('user_id');
    //     } else {
    //         $user_id = null;
    //     }
    //     $post_list = Address::where('user_id', '=', $user_id)->orderBy('date', 'DESC')->get();

    //     // $data_array = array();
    //     // foreach ($post_list as $key) {
    //     //     $data_array[] = array('id' => $key->cmt_id, 'title' => $key->cmt_title, 'content' => $key->cmt_content);
    //     // }
    //     return response()->json(['status' => 200, 'message' => 'List Item', 'data' => $post_list], $this->successStatus);
    // }
    public function item()
    {
        if (Input::get('search') != '') {
            $search = Input::get('search');
        } else {
            $search = '';
        }
        $data_array = array();
        foreach (Item::selectRAW('barang_id,barang_nama')->orderBy('barang_id', 'ASC')->where('barang_nama', 'like', '%' . $search . '%')->get() as $key) {
            $data_array[] = array('id' => $key->barang_id, 'text' => $key->barang_nama.' | '. $key->barang_id);
        }
        return response()->json($data_array, 200);
    }

    public function item_detail()
    {
        if (Input::get('id') != '') {
            $search = Input::get('id');
        } else {
            $search = null;
        }
        $data_array = array();
        foreach (Item::where('barang_id', '=' , $search)->get() as $key) {
            $data_array = array('id' => $key->barang_id, 'text' => $key->barang_nama, 'unit' => $key->barang_satuan, 'price' => $key->barang_harjul, 'stock' => $key->barang_stok);
        }
        return response()->json($data_array, 200);
    }
    public function item_buying_detail()
    {
        if (Input::get('id') != '') {
            $search = Input::get('id');
        } else {
            $search = null;
        }
        $data_array = array();
        foreach (Item::where('barang_id', '=', $search)->get() as $key) {
            $data_array = array('id' => $key->barang_id, 'text' => $key->barang_nama, 'unit' => $key->barang_satuan, 'price' => $key->barang_harpok, 'stock' => $key->barang_stok);
        }
        return response()->json($data_array, 200);
    }
    public function item_detail_distributor()
    {
        if (Input::get('id') != '') {
            $search = Input::get('id');
        } else {
            $search = null;
        }
        $data_array = array();
        foreach (Item::where('barang_id', '=' , $search)->get() as $key) {
            $data_array = array('id' => $key->barang_id, 'text' => $key->barang_nama, 'unit' => $key->barang_satuan, 'price' => $key->barang_harjul_grosir, 'stock' => $key->barang_stok);
        }
        return response()->json($data_array, 200);
    }
}
