<?php

namespace App\Http\Controllers;

use DB;
use Image;

use App\Post;
use App\Banner;
use App\Categories;
use App\Meta;
use App\Helpers\Base;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input as input;

use Illuminate\Http\Request;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        if (Input::get('show') != '') {
            $show = Input::get('show');
        } else {
            $show = 10;
        }

        $post_list = Post::where([['posts.post_type', '=', 'banner'], ['posts.post_up', '=', '0']])->orderBy('date', 'DESC')->paginate($show);

        if (isset($_POST['post-delete']) && $_POST['post-delete'] != "") {
            for ($i = 0; $i < count(@$_POST['post-check']); $i++) {
                $post_id = @$_POST['post-check'][$i];

                // $post_type = ['images', 'icon', 'thumb', 'background', 'pdf', 'files'];
                // foreach (Post::whereIn('post_type', $post_type)->where([['post_up', '=', $post_id]])->get() as $row_images) {
                //     if ($row_images->post_type == 'pdf' or $row_images->post_type == 'files') {
                //         @unlink('./upload/files/' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                //     } else {
                //         @unlink('./upload/' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                //         @unlink('./upload/c_' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                //         @unlink('./upload/c1_' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                //     }
                //     DB::table('posts')->where('post_id', '=', $row_images->post_id)->delete();
                // }

                DB::table('posts')->where('post_id', '=', $post_id)->delete();
            }
            return redirect('/banner/')->with('success', 'Post updated!');
        } elseif (isset($_POST['post-deleted']) && $_POST['post-deleted'] != "") {
            $post_id = $_POST['post_id'];

            // $post_type = ['images', 'icon', 'thumb', 'background', 'pdf', 'files'];
            // foreach (Post::whereIn('post_type', $post_type)->where([['post_up', '=', $post_id]])->get() as $row_images) {
            //     if ($row_images->post_type == 'pdf' or $row_images->post_type == 'files') {
            //         @unlink('./upload/files/' . $row_images->post_slug . '.' . $row_images->post_mime_type);
            //     } else {
            //         @unlink('./upload/' . $row_images->post_slug . '.' . $row_images->post_mime_type);
            //         @unlink('./upload/c_' . $row_images->post_slug . '.' . $row_images->post_mime_type);
            //         @unlink('./upload/c1_' . $row_images->post_slug . '.' . $row_images->post_mime_type);
            //     }
            //     DB::table('posts')->where('post_id', '=', $row_images->post_id)->delete();
            // }

            DB::table('posts')->where('post_id', '=', $post_id)->delete();

            return redirect('/banner/')->with('success', 'Post updated!');
        }

        return view('back/banner', ['post_list' => $post_list, 'show' => $show, 'copyright' => copyright()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $user_id        = Auth::user()->id;
        $post_up        = '0';
        // $post_slug      = ($request->get('post_title') != '') ? strip_i_slug($request->get('post_title')) : '';
        $post_title     = ($request->get('post_title') != '') ? $request->get('post_title') : '';
        $post_link     = ($request->get('post_link') != '') ? $request->get('post_link') : '';
        $post_content   = ($request->get('post_content') != '') ? $request->get('post_content') : '';
        $date           = ($request->get('date') != '') ? $request->get('date') : date('Y-m-d H:i:s');
        $status         = $request->get('status');

        Post::insert([
            'user_id' => $user_id,
            'post_type' => 'banner',
            'post_up' => $post_up,
            'post_slug' =>  '',
            'post_title' => $post_title,
            'post_content' => $post_content,
            'post_summary' => '',
            'post_link' => $post_link,
            'post_video' => '',
            'post_key' => '',
            'post_desc' => '',
            'post_count' => '0',
            'post_mime_type' => '',
            'post_comment' => '0',
            'date' => $date,
            'status' => $status,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'deleted_at' => date('Y-m-d H:i:s')
        ]);

        $id = DB::getPdo()->lastInsertId();

        if ($request->get('categories') != '') {
            foreach ($request->get('categories') as $categories) {
                if (Meta::where([['meta_type', '=', 'categories'], ['meta_source', '=', $id], ['meta_dest', '=', $categories]])->count() == '0') {
                    $row_cats = Categories::where('cat_id', $categories)->first();
                    Meta::insert([
                        'meta_type'     => 'categories',
                        'meta_source'   => $id,
                        'meta_dest'     => $categories,
                        'meta_title'    => $row_cats->cat_title,
                        'meta_slug'     => $row_cats->cat_slug,
                        'meta_date'     => date('Y-m-d H:i:s'),
                        'meta_status'   => '1'
                    ]);
                }
            }

            if (count((array) $request->get('categories')) > 0) {
                $meta_list = Meta::where([['meta_type', '=', 'categories'], ['meta_source', '=', $id]])->whereNotIn('meta_dest', $request->get('categories'))->get();

                foreach ($meta_list as $key) {
                    Meta::where('meta_id', '=', $key->meta_id)->delete();
                }
            }
        } else {
            $meta_list = Meta::where([['meta_type', '=', 'categories'], ['meta_source', '=', $id]])->get();

            foreach ($meta_list as $key) {
                Meta::where('meta_id', '=', $key->meta_id)->delete();
            }
        }

        if ($request->hasfile('upload_photo')) {
            foreach ($request->file('upload_photo') as $image) {
                $original_name = $image->getClientOriginalName();
                $file_name = ext_name($original_name);
                $file_ext = ext_filename($original_name);

                $resize_image = Image::make($image->getRealPath());
                $resize_image->resize(150, 150, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(public_path() . '/upload/r_' . strip_i_slug($file_name) . '.' . $file_ext);

                $img_crop = Image::make($image->getRealPath());
                $img_crop->fit(300);
                // $img->crop($request->input('w'), $request->input('h'), $request->input('x1'), $request->input('y1'));
                $img_crop->save(public_path() . '/upload/c_' . strip_i_slug($file_name) . '.' . $file_ext);

                $image->move(public_path() . '/upload/', strip_i_slug($file_name) . '.' . $file_ext);
                // $data[] = $original_name;


                $post_data = Post::find($id);
                $post_data->post_slug = strip_i_slug($file_name);
                $post_data->post_mime_type = $file_ext;
                $post_data->save();
            }
        }
        return redirect('/banner/edit/' . $id)->with('success', 'Post updated!');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Banner $banner)
    {
        //
    }

    public function search()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        if (Input::get('show') != '') {
            $show = Input::get('show');
        } else {
            $show = 10;
        }
        if (Input::get('query') != '') {
            $search = Input::get('query');
        } else {
            $search = '';
        }

        $post_list = Post::where('posts.post_type', 'banner')->where('post_title', 'like', '%' . $search . '%')->orWhere('post_content', 'like', '%' . $search . '%')->orWhere('post_summary', 'like', '%' . $search . '%')->orderBy('date', 'DESC')->paginate($show);

        if (isset($_POST['post-delete']) && $_POST['post-delete'] != "") {
            for ($i = 0; $i < count(@$_POST['post-check']); $i++) {
                $post_id = @$_POST['post-check'][$i];

                // $post_type = ['images', 'icon', 'thumb', 'background', 'pdf', 'files'];
                // foreach (Post::whereIn('post_type', $post_type)->where([['post_up', '=', $post_id]])->get() as $row_images) {
                //     if ($row_images->post_type == 'pdf' or $row_images->post_type == 'files') {
                //         @unlink('./upload/files/' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                //     } else {
                //         @unlink('./upload/' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                //         @unlink('./upload/c_' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                //         @unlink('./upload/c1_' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                //     }
                //     DB::table('posts')->where('post_id', '=', $row_images->post_id)->delete();
                // }

                DB::table('posts')->where('post_id', '=', $post_id)->delete();
            }
            return redirect('/banner/')->with('success', 'Post updated!');
        } elseif (isset($_POST['post-deleted']) && $_POST['post-deleted'] != "") {
            $post_id = $_POST['post_id'];
            $post_type = array('images', 'thumb', 'background', 'pdf', 'files', 'icon');

            // $post_type = ['images', 'icon', 'thumb', 'background', 'pdf', 'files'];
            // foreach (Post::whereIn('post_type', $post_type)->where([['post_up', '=', $post_id]])->get() as $row_images) {
            //     if ($row_images->post_type == 'pdf' or $row_images->post_type == 'files') {
            //         @unlink('./upload/files/' . $row_images->post_slug . '.' . $row_images->post_mime_type);
            //     } else {
            //         @unlink('./upload/' . $row_images->post_slug . '.' . $row_images->post_mime_type);
            //         @unlink('./upload/c_' . $row_images->post_slug . '.' . $row_images->post_mime_type);
            //         @unlink('./upload/c1_' . $row_images->post_slug . '.' . $row_images->post_mime_type);
            //     }
            //     DB::table('posts')->where('post_id', '=', $row_images->post_id)->delete();
            // }

            DB::table('posts')->where('post_id', '=', $post_id)->delete();

            return redirect('/banner/')->with('success', 'Post updated!');
        }

        return view('back/banner_search', ['post_list' => $post_list, 'show' => $show, 'copyright' => copyright()]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        $post_edit      = Post::where('post_id', $id)->get();
        $cat_list       = cat_list_recrusive($cat_type = 'banner', $cat_up = 0, $depth = 5, $now = 0);
        $images_list    = Post::where([['post_type', '=', 'images'], ['post_up', '=', $id]])->get();
        $icon_list      = Post::where([['post_type', '=', 'icon'], ['post_up', '=', $id]])->get();
        $thumb_list     = Post::where([['post_type', '=', 'thumb'], ['post_up', '=', $id]])->get();
        $background_list = Post::where([['post_type', '=', 'background'], ['post_up', '=', $id]])->get();
        $pdf_list       = Post::where([['post_type', '=', 'pdf'], ['post_up', '=', $id]])->get();
        $files_list     = Post::where([['post_type', '=', 'files'], ['post_up', '=', $id]])->get();
        $cat_array      = Meta::where([['meta_type', '=', 'categories'], ['meta_source', '=', $id]])->get();

        $array_cat = array('0' => 0);
        foreach ($cat_array as $key_cat) {
            $array_cat[] = $key_cat->meta_dest;
        }

        return view('back/banner_edit', [
            'post_edit' => $post_edit,
            'cat_list' => $cat_list,
            'images_list' => $images_list,
            'icon_list' => $icon_list,
            'thumb_list' => $thumb_list,
            'background_list' => $background_list,
            'pdf_list' => $pdf_list,
            'files_list' => $files_list,
            'array_cat' => $array_cat,
            'copyright' => copyright()
        ]);
    }
    public function add()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        $cat_list       = cat_list_recrusive($cat_type = 'banner', $cat_up = 0, $depth = 5, $now = 0);

        return view('back/banner_add', [
            'cat_list' => $cat_list,
            'copyright' => copyright()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post_data = Post::find($id);
        $post_data->user_id = Auth::user()->id;
        $post_data->post_up = '0';
        $post_data->post_slug = ($request->get('post_title') != '') ? strip_i_slug($request->get('post_title')) : '';
        $post_data->post_title = ($request->get('post_title') != '') ? $request->get('post_title') : '';
        $post_data->post_link     = ($request->get('post_link') != '') ? $request->get('post_link') : '';
        $post_data->post_content = ($request->get('post_content') != '') ? $request->get('post_content') : '';
        $post_data->date = ($request->get('date') != '') ? $request->get('date') : date('Y-m-d H:i:s');
        $post_data->status = $request->get('status');
        $post_data->save();

        if (is_array(@$_POST['post-check'])) {
            for ($i = 0; $i < count(@$_POST['post-check']); $i++) {
                $i_name = explode(",", $_POST['post-check'][$i]);

                if (file_exists(public_path() . '/upload/' . $i_name[1])) {
                    unlink(public_path() . "/upload/" . $i_name[1]);
                }
                if (file_exists(public_path() . '/upload/c_' . $i_name[1])) {
                    unlink(public_path() . "/upload/c_" . $i_name[1]);
                }
                if (file_exists(public_path() . '/upload/r_' . $i_name[1])) {
                    unlink(public_path() . "/upload/r_" . $i_name[1]);
                }
                DB::table('posts')->where('post_id', '=', $i_name[0])->delete();
            }
        }

        if ($request->get('categories') != '') {
            foreach ($request->get('categories') as $categories) {
                if (Meta::where([['meta_type', '=', 'categories'], ['meta_source', '=', $id], ['meta_dest', '=', $categories]])->count() == '0') {
                    $row_cats = Categories::where('cat_id', $categories)->first();
                    Meta::insert([
                        'meta_type'     => 'categories',
                        'meta_source'   => $id,
                        'meta_dest'     => $categories,
                        'meta_title'    => $row_cats->cat_title,
                        'meta_slug'     => $row_cats->cat_slug,
                        'meta_date'     => date('Y-m-d H:i:s'),
                        'meta_status'   => '1'
                    ]);
                }
            }

            if (count((array) $request->get('categories')) > 0) {
                $meta_list = Meta::where([['meta_type', '=', 'categories'], ['meta_source', '=', $id]])->whereNotIn('meta_dest', $request->get('categories'))->get();

                foreach ($meta_list as $key) {
                    Meta::where('meta_id', '=', $key->meta_id)->delete();
                }
            }
        } else {
            $meta_list = Meta::where([['meta_type', '=', 'categories'], ['meta_source', '=', $id]])->get();

            foreach ($meta_list as $key) {
                Meta::where('meta_id', '=', $key->meta_id)->delete();
            }
        }

        if ($request->hasfile('upload_photo')) {
            foreach ($request->file('upload_photo') as $image) {
                $original_name = $image->getClientOriginalName();
                $file_name = ext_name($original_name);
                $file_ext = ext_filename($original_name);

                $resize_image = Image::make($image->getRealPath());
                $resize_image->resize(150, 150, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(public_path() . '/upload/r_' . strip_i_slug($file_name) . '.' . $file_ext);

                $img_crop = Image::make($image->getRealPath());
                $img_crop->fit(300);
                // $img->crop($request->input('w'), $request->input('h'), $request->input('x1'), $request->input('y1'));
                $img_crop->save(public_path() . '/upload/c_' . strip_i_slug($file_name) . '.' . $file_ext);

                $image->move(public_path() . '/upload/', strip_i_slug($file_name) . '.' . $file_ext);
                // $data[] = $original_name; //Auth::id()

                $post_data = Post::find($id);
                $post_data->post_slug = strip_i_slug($file_name);
                $post_data->post_mime_type = $file_ext;
                $post_data->save();
            }
        }
        return redirect('/banner/edit/' . $id)->with('success', 'Post updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function destroy(Banner $banner)
    {
        //
    }
}
