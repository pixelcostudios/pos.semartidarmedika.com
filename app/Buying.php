<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buying extends Model
{
    protected $primaryKey = 'beli_nofak';
    protected $table = "buys";
}
