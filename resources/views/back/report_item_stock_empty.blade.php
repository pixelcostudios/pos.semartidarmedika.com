@extends('layouts.post.post')

@section('content')
@include('layouts.menu')
<div class="row wrapper border-bottom white-bg page-heading printing">
<div class="col-lg-10">
<h2>Report Stok Barang Empty</h2>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="{{url('/')}}">Home</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{url('report/item_stock_empty/')}}">Report Stok Barang Empty</a>
    </li>
    <li class="breadcrumb-item active">
        <strong>All Report Stok Barang Empty</strong>
    </li>
</ol>
</div>
<div class="col-lg-2">

</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
<div class="row">
<div class="col-lg-12 padding-none">
    <div class="ibox ">

            <div class="ibox-title printing">
                <h5>Manage Report Stok Barang Empty</h5>
                <a href="{{url('/')}}/report/item_stock_empty_download" class="btn btn-default float-right download-xls"> <i class="fa fa-download"></i> Download </a>
                <button type="button" class="btn btn-default float-right btn-print"><i class="fa fa-print"></i> Print</button>
            </div>
            <div class="ibox-content">
                <div class="table-responsive" id="printarea">
                    <form method="post">
                    @csrf
                    <table class="table table-striped">
                        <tbody>
                            @foreach($post_list as $p)
                            <tr>
                                <th colspan="6">{{$p->cat_title}} </th>
                            </tr>
                            <tr>
                                <th width="1%">No</th>
                                <th width="10%">Kode Barang </th>
                                <th>Nama Barang </th>
                                <th class="text-center">Stok</th>
                            </tr>
                            @foreach (App\Item::where('barang_kategori_id','=',$p->cat_id)->whereRaw('barang_stok = 0')->get() as $value  => $key)
                            <tr>
                                <td>{{$value+1}} </td>
                                <td>{{$key->barang_id}} </td>
                                <td>{{$key->barang_nama}} </td>
                                <td class="text-center">{{ceil($key->barang_stok)}} </td>
                            </tr>
                            @endforeach
                            @endforeach

                        </tbody>
                    </table>
        </form>
    </div>
</div>
</div>
</div>

</div>
</div>
<div class="footer printing">
<div class="float-right">
<strong>
    </strong>
</div>
<div>
<strong>Copyright</strong> {{$copyright}}
</div>
</div>
@endsection
