<!doctype html>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>{{$title}} | {{ $setting[0] }}</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="{{ url('/') }}/themes/price/css/bootstrap.min.css" rel="stylesheet">
<link href="{{ url('/') }}/themes/price/font-awesome/css/font-awesome.css" rel="stylesheet">
<link href="{{ url('/') }}/themes/price/js/plugins/jQuery-Table-Mobilize/dist/theme-default.min.css" rel="stylesheet">
<link href="{{ url('/') }}/themes/price/css/plugins/select2/select2.min.css" rel="stylesheet">
<link href="{{ url('/') }}/themes/price/js/plugins/datetime/bootstrap-datetimepicker.css" rel="stylesheet" />
<link href="{{ url('/') }}/themes/price/js/plugins/datetime/bootstrap-datepicker3.min.css" rel="stylesheet" />
<link href="{{ url('/') }}/themes/price/css/animate.css" rel="stylesheet">
<link href="{{ url('/') }}/themes/price/css/style.css" rel="stylesheet">
</head>
<body style="background:none;">

@yield('content')

<!-- JS here -->
<script src="{{ url('/') }}/themes/price/js/jquery-3.1.1.min.js"></script>
<script src="{{ url('/') }}/themes/price/js/bootstrap.js"></script>
<script src="{{ url('/') }}/themes/price/js/index.js"></script>
<script src="{{ url('/') }}/themes/price/js/plugins/jQuery-Table-Mobilize/dist/jquery.tableMobilize.min.js"></script>
<script src="{{ url('/') }}/themes/price/js/plugins/select2/select2.full.min.js"></script>
<script src="{{ url('/') }}/themes/price/js/plugins/validation/jquery.form-validator.min.js"></script>
<script src="{{ url('/') }}/themes/price/js/plugins/datetime/bootstrap-datetimepicker.min.js"></script>
<script src="{{ url('/') }}/themes/price/js/plugins/datetime/bootstrap-datepicker.min.js"></script>
<script>
    $(document).ready(function () {
        $(".select_list").select2();
        $(".date-picker").datetimepicker({
            minView: 2,
            format: 'yyyy-mm-dd',
            autoclose: true,
        });
        $(".date-picker-month").datepicker({
            startView: 1,
            minViewMode: 1,
            autoclose: true
        });
    });
    $('table').tableMobilize();
</script>
</body>
</html>
