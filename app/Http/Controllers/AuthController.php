<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator, Redirect, Response;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Session;

class AuthController extends Controller
{

    public function index()
    {
        return view('auth/login');
    }

    public function registration()
    {
        return view('registration');
    }

    public function postLogin(Request $request)
    {
        request()->validate([
            'login' => 'required',
            'password' => 'required',
        ]);
        $login = request()->input('login');
        $password = request()->input('password');

        $fieldType = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';

        if($fieldType== 'email')
        {
            $credentials = $request->only('email', 'password');
            if (Auth::attempt(['email' => $login, 'password' => $password, 'active' => 1])) {
                // Authentication passed...
                return redirect()->intended('home');
            }
        }
        else
        {
            $credentials = $request->only('username', 'password');
            if (Auth::attempt(['username' => $login, 'password' => $password, 'active' => 1])) {
                // Authentication passed...
                return redirect()->intended('home');
            }
        }

        return redirect('/panel/login')->with('success', 'Oppes! You have entered invalid credentials');
    }

    public function postRegistration(Request $request)
    {
        request()->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
        ]);

        $data = $request->all();

        $check = $this->create($data);

        // return Redirect::to("dashboard")->withSuccess('Great! You have Successfully loggedin');
        return redirect('home')->with('success', 'Great! You have Successfully loggedin');
    }

    public function dashboard()
    {

        if (Auth::check()) {
            return view('dashboard');
        }
        // return Redirect::to("login")->withSuccess('Opps! You do not have access');
        return redirect('login')->with('success', 'Opps! You do not have access');
    }

    public function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password'])
        ]);
    }

    public function logout()
    {
        Session::flush();
        Auth::logout();
        return redirect('/panel/login');
    }
}
