@extends('layouts.front.post_detail')
@section('content')
@include('layouts.front.header_post')
<!-- ++++ Most Bold Title ++++ -->
@foreach ($post_list as $key)
<section class="banner  o-hidden banner-inner about-banner">
    <div class="container">
        <!--banner text-->
        <div class="banner-txt">
            <h1>{{$key->post_title}}</h1>
            <p class="semi-bold">{!!$key->post_summary!!}</p>
        <!--end banner text-->
    </div>
</section>

<!-- ++++ Most Bold Title ++++ -->
<!-- ++++ blog standard content ++++ -->
<section class="bg-white o-hidden common-form-section contact-form-wrapper more-about" id="more-about">
    <div class="container relative">
        <div class="row">
            <!-- Content -->
            <div class="col-sm-8 margin-auto">
                <!-- Post -->
                <!-- Image -->
                @if($key->post_thumb==1)
                <div class="box-thumb">
                @if(count($gallery)>1)
                    <div class="blog-media">
                        <ul class="clearlist content-slider">
                            @foreach ($gallery as $p2)
                            <li><img src="{{url('/')}}/upload/{{$p2->post_slug}}.{{$p2->post_mime_type}}"
                                alt="{{$key->post_title}}" ></li>
                            @endforeach
                        </ul>
                    </div>
                    @if($key->post_price!=0)
                    <div class="box-price">Rp. {{currency($key->post_price)}}</div>
                    @endif
                @else
                    <div class="blog-media">
                    @foreach ($gallery as $p2)
                    <img src="{{url('/')}}/upload/{{$p2->post_slug}}.{{$p2->post_mime_type}}"
                                alt="{{$key->post_title}}" width="100%">
                    @endforeach
                    </div>
                    @if($key->post_price!=0)
                    <div class="box-price">Rp. {{currency($key->post_price)}}</div>
                    @endif
                @endif
                </div>
                @endif
                <div class="blog-item-body">
                    {!!$key->post_content!!}
                </div>
                <!--blog share-->
                <div class="blog-post-share">
                    <ul class="social-links">
                        <li>Share this post:</li>
                        <li><a href="blog-details.html#"><span class="icon-facebook"></span></a></li>
                        <li><a href="blog-details.html#"><span class="icon-twitter"></span></a></li>
                        <li><a href="blog-details.html#"><span class="icon-instagram"></span></a></li>
                    </ul>
                </div>
            </div>
            <!-- End Content -->

        </div>
    </div>
</section>
@endforeach
@include('layouts.front.footer')
@endsection
