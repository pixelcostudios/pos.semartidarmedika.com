@extends('layouts.post.distributor_add')

@section('content')
@include('layouts.menu')
<div class="row wrapper border-bottom white-bg page-heading">
<div class="col-lg-10">
<h2>Penjualan (Grosir)</h2>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="{{ url('home') }}">Home</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ url('selling/distributor') }}">Penjualan (Grosir)</a>
    </li>
    <li class="breadcrumb-item active">
        <strong>All Penjualan (Grosir)</strong>
    </li>
</ol>
</div>
<div class="col-lg-2">

</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
            <form name="myform" method="post" action="{{ url('selling/distributor_create') }}" enctype="multipart/form-data" class="form" data-async>
                @csrf
				<div class="row">
					<div class="col-lg-12 padding-none">
						<div class="ibox ">
							<div class="ibox-title">
								<h5>Penjualan (Grosir) Editor</h5>
								<button type="submit" name="Pixel_Save" class="btn btn-primary float-right">Simpan</button>
							</div>
							<div class="ibox-content">
                                <div class="relative input_fields_wrap">
                                    <div class="text-center margin-buttton-10">
                                            <button class="btn btn-primary add_product_price add_field_button"><i class="fa fa-plus"></i></button>
                                            <button class="btn btn-primary add_product_price add_field_button_custom">Custom</button>
                                            <br><br>
                                    </div>
                                    <!--
                                    <div class="row item-select">
                                        <div class="form-group col-sm-3">
                                            <label>Item</label><br>
                                            <select class="form-control item_select" name="post_item[]" data-placeholder="Pilih Item">
                                            </select>
                                        </div>
                                        <div class="form-group col-sm-2">
                                            <label>Harga</label><br>
                                            <input type="text" class="form-control price" placeholder="Price " readonly>
                                        </div>
                                        <div class="form-group col-sm-1">
                                            <label>Satuan</label><br>
                                            <input type="text" class="form-control unit" placeholder="Satuan " readonly>
                                        </div>
                                        <div class="form-group col-sm-1">
                                            <label>Stock</label><br>
                                            <input type="text" class="form-control stock" placeholder="Stock " readonly>
                                        </div>
                                        <div class="form-group col-sm-2">
                                            <label>Discount</label><br>
                                            <input type="text" class="form-control discount" name="post_discount[]" placeholder="Discount" value="0">
                                        </div>
                                        <div class="form-group col-sm-1">
                                            <label>QTY</label><br>
                                            <input type="text" class="form-control qty" name="post_qty[]" placeholder="QTY" value="1" required="required">
                                        </div>
                                    </div>
                                    -->
                                </div>
								<div class="row">
                                    <div class="form-group col-sm-3">
                                        <label for="title">Customer  </label>
                                        <input type="text" class="form-control" name="customer">
                                    </div>
                                    <div class="form-group col-sm-3">
                                        <label for="title">Seller  </label>
                                        <input type="text" class="form-control" name="seller">
                                    </div>
                                    <div class="form-group col-sm-2">

                                    </div>
                                    <div class="form-group col-sm-4 pull-right">
                                        <label for="title">Total  </label>
                                        <input type="text" class="form-control total" name="total" readonly>
                                    </div>
                                </div>
                                <div class="row">

                                </div>
                                <div class="row">
                                    <div class="form-group col-sm-3">
                                        <label for="title">Address  </label>
                                        <input type="text" class="form-control" name="jual_address">
                                    </div>
                                    <div class="form-group col-sm-3">
                                        <label for="title">Phone  </label>
                                        <input type="text" class="form-control" name="jual_phone">
                                    </div>
                                    <div class="form-group col-sm-2 pull-right text-right">
                                        <label for="title">PPN  </label>
                                    </div>
                                    <div class="form-group col-sm-4 pull-right">
                                        <input type="text" class="form-control transport" name="transport" value="0" required="required" autocomplete="off">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-sm-8 pull-right text-right">
                                        <label for="title">Bayar  </label>
                                    </div>
                                    <div class="form-group col-sm-4 pull-right">
                                        <input type="text" class="form-control pay" name="pay" required="required" autocomplete="off">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-sm-8 pull-right text-right">
                                        <label for="title">Kembalian  </label>
                                    </div>
                                    <div class="form-group col-sm-4 pull-right">
                                        <input type="text" class="form-control return" name="kembalian" readonly>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-sm-8 pull-right text-right">
                                        <label for="title">Pembayaran  </label>
                                    </div>
                                    <div class="form-group col-sm-4 pull-right">
                                        <select name="jual_payment_type" class="form-control select_list">
                                            <option value="Cash">Cash</option>
											<option value="BCA">BCA</option>
											<option value="BNI">BNI</option>
                                            <option value="BRI">BRI</option>
										</select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-sm-8 pull-right text-right">
                                        <label for="title">Nomor Kartu  </label>
                                    </div>
                                    <div class="form-group col-sm-4 pull-right">
                                        <input type="text" class="form-control " name="jual_payment_account" autocomplete="off">
                                    </div>
                                </div>

								<div class="clear"></div>
							</div>
						</div>
					</div>
                </div>

            </form>
		</div>
<div class="footer">
<div class="float-right">
<strong>
    </strong>
</div>
<div>
<strong>Copyright</strong> {{$copyright}}
</div>
</div>
@endsection
