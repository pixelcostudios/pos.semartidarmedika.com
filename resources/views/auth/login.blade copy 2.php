@extends('layouts.login')

@section('content')
<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <!--
		<div>

			<h1 class="logo-name margin-bottom-20"><img src="img/logo.png" width="150"></h1>

		</div>
		-->
        <div class="ibox-content">
            <div class="row">
                <div class="col-sm-9 margin-auto">
                    <div class="row">
                        <div class="col-sm-5">
                            <!--<h3 class="text-left">Selamat datang </h3>-->
                            <img src="img/logo.png" class="logo" width="250">
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label class="username">{{ __('E-Mail Address') }}</label>
                            </div>
                            <div class="form-group">
                                <label class="password">{{ __('Password') }}</label>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <form class="m-t" method="POST" action="{{ route('login') }}">
                                <div class="form-group">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    <!--<input type="text" name="user_name" class="form-control" placeholder="Username" required="">-->
                                </div>
                                <div class="form-group">
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    <!--<input type="password" name="user_password" class="form-control" placeholder="Password" required="">-->
                                </div>
                                <div class="form-group" style="text-align: left;">
                                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                        <label class="form-check-label" for="remember">
                                            {{ __('Remember Me') }}
                                        </label>
                                </div>
                                <button type="submit" class="btn btn-primary block  m-b">Login</button>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

@endsection