<?php

namespace App\Http\Controllers;

use DB;
use App\Retur;
use App\Item;
use App\Helpers\Base;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input as input;
use Illuminate\Http\Request;

class ReturController extends Controller
{
    public function selling()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        if (Input::get('show') != '') {
            $show = Input::get('show');
        } else {
            $show = 10;
        }

        $post_list = Retur::where('retur_type', '=', 'penjualan')->orderby('retur_tanggal', 'DESC')->paginate($show);

        if (isset($_POST['post-delete']) && $_POST['post-delete'] != "") {
            for ($i = 0; $i < count(@$_POST['post-check']); $i++) {
                $post_id = @$_POST['post-check'][$i];

                Retur::where('retur_id', '=', $post_id)->delete();
            }
            return redirect('/retur/selling/')->with('success', 'Post updated!');
        } elseif (isset($_POST['post-deleted']) && $_POST['post-deleted'] != "") {
            $post_id = $_POST['post_id'];

            Retur::where('retur_id', '=', $post_id)->delete();

            return redirect('/retur/selling/')->with('success', 'Post updated!');
        }

        return view('back/retur_selling', ['post_list' => $post_list, 'show' => $show, 'copyright' => copyright()]);
    }
    public function selling_add()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        $item_list  = Item::get();

        return view('back/retur_selling_add', [
            'item_list' => $item_list,
            'copyright' => copyright()
        ]);
    }
    public function selling_create(Request $request)
    {
        if ($request->get('item') != '') {
            $p_note     = $request->get('item');
            $post_qty   = $request->get('qty');
            $post_price   = $request->get('price');
            $post_name   = $request->get('name');
            $post_unit   = $request->get('unit');
            $post_desc   = $request->get('desc');
            foreach ($p_note as $key => $n) {
                if ($n != '') {
                    $retur_type             = 'penjualan';

                    Retur::insert([
                        'retur_type' => $retur_type,
                        'retur_tanggal' => date('Y-m-d H:i:s'),
                        'retur_barang_id' => $n,
                        'retur_barang_nama' => $post_name[$key],
                        'retur_barang_satuan' => $post_unit[$key],
                        'retur_harjul' => $post_price[$key],
                        'retur_qty' => $post_qty[$key],
                        'retur_subtotal' => $post_price[$key] * $post_qty[$key],
                        'retur_keterangan' => $post_desc[$key],
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                        'deleted_at' => date('Y-m-d H:i:s')
                    ]);

                    $stock = Item::where('barang_id', '=', $n)->value('barang_stok');

                    Item::where('barang_id', '=', $n)->update(['barang_stok' => $stock + $post_qty[$key]]);
                }
            }
        }

        return redirect('/retur/selling')->with('success', 'Post updated!');
    }
    public function buying()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        if (Input::get('show') != '') {
            $show = Input::get('show');
        } else {
            $show = 10;
        }

        $post_list = Retur::where('retur_type', '=', 'pembelian')->orderby('retur_tanggal', 'DESC')->paginate($show);

        if (isset($_POST['post-delete']) && $_POST['post-delete'] != "") {
            for ($i = 0; $i < count(@$_POST['post-check']); $i++) {
                $post_id = @$_POST['post-check'][$i];

                Retur::where('retur_id', '=', $post_id)->delete();
            }
            return redirect('/retur/buying/')->with('success', 'Post updated!');
        } elseif (isset($_POST['post-deleted']) && $_POST['post-deleted'] != "") {
            $post_id = $_POST['post_id'];

            Retur::where('retur_id', '=', $post_id)->delete();

            return redirect('/retur/buying/')->with('success', 'Post updated!');
        }

        return view('back/retur_buying', ['post_list' => $post_list, 'show' => $show, 'copyright' => copyright()]);
    }
    public function buying_add()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        $item_list  = Item::get();

        return view('back/retur_buying_add', [
            'item_list' => $item_list,
            'copyright' => copyright()
        ]);
    }
    public function buying_create(Request $request)
    {
        if ($request->get('item') != '') {
            $p_note     = $request->get('item');
            $post_qty   = $request->get('qty');
            $post_price   = $request->get('price');
            $post_name   = $request->get('name');
            $post_unit   = $request->get('unit');
            $post_desc   = $request->get('desc');
            foreach ($p_note as $key => $n) {
                if ($n != '') {
                    $retur_type             = 'pembelian';

                    Retur::insert([
                        'retur_type' => $retur_type,
                        'retur_tanggal' => date('Y-m-d H:i:s'),
                        'retur_barang_id' => $n,
                        'retur_barang_nama' => $post_name[$key],
                        'retur_barang_satuan' => $post_unit[$key],
                        'retur_harjul' => $post_price[$key],
                        'retur_qty' => $post_qty[$key],
                        'retur_subtotal' => $post_price[$key] * $post_qty[$key],
                        'retur_keterangan' => $post_desc[$key],
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                        'deleted_at' => date('Y-m-d H:i:s')
                    ]);

                    $stock = Item::where('barang_id', '=', $n)->value('barang_stok');

                    Item::where('barang_id', '=', $n)->update(['barang_stok' => $stock - $post_qty[$key]]);
                }
            }
        }

        return redirect('/retur/buying')->with('success', 'Post updated!');
    }
}
