<?php

namespace App\Http\Controllers\API;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Member;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Validator;

class UserController extends Controller
{
    public $successStatus = 200;

    public function login(Request $request)
    {
        request()->validate([
            'login' => 'required',
            'password' => 'required',
        ]);
        $login = request()->input('login');
        $password = request()->input('password');

        $fieldType = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';

        if ($fieldType == 'email') {
            $credentials = $request->only('email', 'password');
            if (Auth::attempt(['email' => $login, 'password' => $password, 'active' => 1])) {
                $user = Auth::user();
                $user_detail = Member::where('email', '=', $login)->first();
                // $success['token'] =  $user->createToken('nApp')->accessToken;
                return response()->json(['status' => 200, 'message' => 'Succesfully Login', 'token' => $user->createToken('nApp')->accessToken, 'data' => $user_detail], $this->successStatus);
            } else {
                return response()->json(['status' => 401,'message' => 'Unauthorised'], 401);
            }
        // }
        // elseif($fieldType == 'username') {
        //     $credentials = $request->only('username', 'password');
        //     if (Auth::attempt(['username' => $login, 'password' => $password, 'active' => 1])) {
        //         $user = Auth::user();
        //         $user_detail = Member::where('email', '=', $request->input('email'))->first();
        //         // $success['token'] =  $user->createToken('nApp')->accessToken;
        //         return response()->json(['status' => 200, 'message' => 'Succesfully Login', 'token' => $user->createToken('nApp')->accessToken,'data'=>$user_detail], $this->successStatus);
        //     } else {
        //         return response()->json(['status' => 401, 'message' => 'Unauthorised'], 401);
        //     }
        } else {
            return response()->json(['status' => 401, 'message' => 'Unauthorised'], 401);
        }

        // if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
        //     $user = Auth::user();
        //     $success['token'] =  $user->createToken('nApp')->accessToken;
        //     return response()->json(['success' => $success], $this->successStatus);
        // } else {
        //     return response()->json(['error' => 'Unauthorised'], 401);
        // }
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'fullname' => 'required',
            'mobile' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            // 'c_password' => 'required|same:password',
        ]);

        $email_count = DB::table('users')->where('email', $request->input('email'))->count();

        if ($email_count > 0)
        {
            return response()->json(['status' => 401,'message' => 'Email Already'], 401);
        }
        else
        {
            if ($validator->fails()) {
                return response()->json(['status' => 401, 'message' => $validator->errors()], 401);
            }
            $user = User::create([
                'password'     => bcrypt($request->input('password')),
                'email'        => $request->input('email'),
                'first_name'   => $request->input('fullname'),
                'mobile'       => $request->input('mobile'),
                'active'       => 1,
                'added'        => date('Y-m-d H:i:s')
            ]);

            $id = $user->id;//DB::getPdo()->lastInsertId();
            $role = 2;

            // if (DB::table('users_groups')->where('user_id', $id)->count() == 0) {
            //     DB::table('users_groups')->insert(['user_id' => $id, 'group_id' => $role]);
            // } else {
            //     DB::table('users_groups')->where('user_id', '=', $id)->update(['group_id' => $role]);
            // }
            DB::table('users_groups')->insert(['user_id' => $id, 'group_id' => $role]);

            // if (Auth::attempt(['username' => $request->get('username'), 'password' => $request->get('password'), 'active' => 1])) {
            //     $user_detail = Auth::user();
            //     return response()->json(['message' => 'Register Successfully', 'token' => $user->createToken('nApp')->accessToken, 'data' => $user_detail], $this->successStatus);
            // } else {
            //     return response()->json(['message' => 'Unauthorised'], 401);
            // }
            $user_detail = Member::where('email','=', $request->input('email'))->first();

            return response()->json(['status' => 200,'message' => 'Register Successfully', 'token' => $user->createToken('nApp')->accessToken,'data' => $user_detail], $this->successStatus);
        }
    }

    public function details()
    {
        $user = Auth::user();
        return response()->json(['status'=> 200,'message' => 'Get User Detail', 'data' => $user], $this->successStatus);
    }
    public function update(Request $request, $id)
    {
        $post_data = Member::find($id);
        if (($request->get('password') != '')) {
            $post_data->password      = Hash::make($request->get('password'));
        }
        // $post_data->email             = ($request->get('email') != '') ? $request->get('email') : '';

        $post_data->first_name        = ($request->get('fullname') != '') ? $request->get('fullname') : '';
        $post_data->mobile            = ($request->get('mobile') != '') ? $request->get('mobile') : '';
        $post_data->address           = ($request->get('address') != '') ? $request->get('address') : '';
        $post_data->city              = ($request->get('city') != '') ? $request->get('city') : '';
        $post_data->province          = ($request->get('province') != '') ? $request->get('province') : '';
        $post_data->save();
        return response()->json(['status' => 200, 'message' => 'Update Profile Successfully'], $this->successStatus);
    }
    public function avatar(Request $request, $id)
    {
        if ($request->hasfile('avatar')) {
            foreach ($request->file('avatar') as $image) {
                $original_name = $image->getClientOriginalName();
                $file_name = ext_name($original_name);
                $file_ext = ext_filename($original_name);

                $image->move(public_path() . '/upload/avatar/', strip_i_slug($file_name) . '.' . $file_ext);
                $post_data = Member::find($id);

                $post_data->avatar          = strip_i_slug($file_name) . '.' . $file_ext;
                $post_data->save();
            }
        }
        return response()->json(['status' => 200, 'message' => 'Update Avatar Successfully'], $this->successStatus);
    }
    public function doctor()
    {
        $post_list  = Member::selectRaw('*, users.id as user_id')->leftJoin('users_groups', 'users_groups.user_id', '=', 'users.id')->leftJoin('groups', 'groups.id', '=', 'users_groups.group_id')->where('users_groups.group_id','=','3')->orderBy('created_at', 'DESC')->get();
        $data_array = array();
        foreach ($post_list as $key) {
            $data_array[] = array('id' => $key->user_id, 'fullname' => $key->first_name, 'mobile' => $key->mobile);
        }
        return response()->json(['status' => 200, 'message' => 'List Doctor', 'data' => $data_array], $this->successStatus);
    }
    public function ustad()
    {
        $post_list  = Member::selectRaw('*, users.id as user_id')->leftJoin('users_groups', 'users_groups.user_id', '=', 'users.id')->leftJoin('groups', 'groups.id', '=', 'users_groups.group_id')->where('users_groups.group_id', '=', '4')->orderBy('created_at', 'DESC')->get();
        $data_array = array();
        foreach ($post_list as $key) {
            $data_array[] = array('id' => $key->user_id, 'fullname' => $key->first_name, 'mobile' => $key->mobile);
        }
        return response()->json(['status' => 200, 'message' => 'List Doctor', 'data' => $data_array], $this->successStatus);
    }
}
