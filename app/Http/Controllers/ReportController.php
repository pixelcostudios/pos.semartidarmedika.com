<?php

namespace App\Http\Controllers;

// use DB;
use App\Categories;
use App\Item;
// use App\Unit;
use App\Selling;
use App\Buying;
use App\Retur;
use App\Helpers\Base;
use App\Exports\ReportItemExport;
use App\Exports\ReportItemStockExport;
use App\Exports\ReportItemStockEmptyExport;
use App\Exports\ProfitLossExport;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input as input;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function item()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }

        $post_list = Categories::where('cats.cat_type', 'product')->where('cats.cat_up', 0)->orderBy('cat_date', 'DESC')->get();

        return view('back/report_item', ['post_list' => $post_list, 'copyright' => copyright()]);
    }
    public function item_download()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }

        return Excel::download(new ReportItemExport, 'Report_Item.xlsx');
    }
    public function item_stock()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }

        $post_list = Categories::where('cats.cat_type', 'product')->where('cats.cat_up', 0)->orderBy('cat_date', 'DESC')->get();

        return view('back/report_item_stock', ['post_list' => $post_list, 'copyright' => copyright()]);
    }
    public function item_stock_download()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }

        return Excel::download(new ReportItemStockExport, 'Report_Stock_Item.xlsx');
    }
    public function item_stock_empty()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }

        // $post_list = Item::selectRAW('*')->whereRaw('barang_stok <= barang_min_stok')->orderBy('barang_id', 'ASC')->get();
        $post_list = Categories::where('cats.cat_type', 'product')->where('cats.cat_up', 0)->orderBy('cat_date', 'DESC')->get();

        // print_r($post_list);
        return view('back/report_item_stock_empty', ['post_list' => $post_list, 'copyright' => copyright()]);
    }
    public function item_stock_empty_download()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }

        return Excel::download(new ReportItemStockEmptyExport, 'Report_Stock_Item.xlsx');
    }
    public function selling()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        if (Input::get('start_date') != '') {
            $start_date = Input::get('start_date');
        } else {
            $start_date = date('Y-m-d');
        }
        if (Input::get('end_date') != '') {
            $end_date = Input::get('end_date');
        } else {
            $end_date = date('Y-m-d');
        }
        $post_list = Selling::join('sell_details', 'sell_details.jual_id', '=', 'sells.jual_id')
        ->where('jual_keterangan', 'eceran')
        ->whereBetween('jual_tanggal', [$start_date . " 00:00:00", $end_date . " 23:59:59"])
        ->orderBy('jual_tanggal', 'DESC')->get();

        return view('back/report_selling', ['post_list' => $post_list, 'start_date'=> $start_date, 'end_date'=>$end_date, 'copyright' => copyright()]);
    }
    public function selling_distributor()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        if (Input::get('start_date') != '') {
            $start_date = Input::get('start_date');
        } else {
            $start_date = date('Y-m-d');
        }
        if (Input::get('end_date') != '') {
            $end_date = Input::get('end_date');
        } else {
            $end_date = date('Y-m-d');
        }
        $post_list = Selling::join('sell_details', 'sell_details.jual_id', '=', 'sells.jual_id')
            ->where('jual_keterangan', 'grosir')
            ->whereBetween('jual_tanggal', [$start_date . " 00:00:00", $end_date . " 23:59:59"])
            ->orderBy('jual_tanggal', 'DESC')->get();

        return view('back/report_selling_distributor', ['post_list' => $post_list, 'start_date' => $start_date, 'end_date' => $end_date, 'copyright' => copyright()]);
    }
    public function selling_bill()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        if (Input::get('start_date') != '') {
            $start_date = Input::get('start_date');
        } else {
            $start_date = date('Y-m-d');
        }
        if (Input::get('end_date') != '') {
            $end_date = Input::get('end_date');
        } else {
            $end_date = date('Y-m-d');
        }
        $post_list = Selling::join('sell_details', 'sell_details.jual_id', '=', 'sells.jual_id')
            ->where('jual_keterangan', 'bon')
            ->whereBetween('jual_tanggal', [$start_date . " 00:00:00", $end_date . " 23:59:59"])
            ->orderBy('jual_tanggal', 'DESC')->get();

        return view('back/report_selling_bill', ['post_list' => $post_list, 'start_date' => $start_date, 'end_date' => $end_date, 'copyright' => copyright()]);
    }
    public function buying()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        if (Input::get('start_date') != '') {
            $start_date = Input::get('start_date');
        } else {
            $start_date = date('Y-m-d');
        }
        if (Input::get('end_date') != '') {
            $end_date = Input::get('end_date');
        } else {
            $end_date = date('Y-m-d');
        }
        $post_list = Buying::join('buy_details', 'buy_details.beli_id', '=', 'buys.beli_id')
        ->join('items', 'items.barang_id', '=', 'buy_details.d_beli_barang_id')
        ->whereBetween('beli_tanggal', [$start_date . " 00:00:00", $end_date . " 23:59:59"])
        ->orderBy('beli_tanggal', 'DESC')->get();

        return view('back/report_buying', ['post_list' => $post_list, 'start_date' => $start_date, 'end_date' => $end_date, 'copyright' => copyright()]);
    }
    public function retur()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        if (Input::get('start_date') != '') {
            $start_date = Input::get('start_date');
        } else {
            $start_date = date('Y-m-d');
        }
        if (Input::get('end_date') != '') {
            $end_date = Input::get('end_date');
        } else {
            $end_date = date('Y-m-d');
        }
        $post_list = Retur::where('retur_type','=', 'penjualan')->whereBetween('retur_tanggal', [$start_date . " 00:00:00", $end_date . " 23:59:59"])
            ->orderBy('retur_tanggal', 'DESC')->get();

        return view('back/report_retur', ['post_list' => $post_list, 'start_date' => $start_date, 'end_date' => $end_date, 'copyright' => copyright()]);
    }
    public function retur_selling()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        if (Input::get('start_date') != '') {
            $start_date = Input::get('start_date');
        } else {
            $start_date = date('Y-m-d');
        }
        if (Input::get('end_date') != '') {
            $end_date = Input::get('end_date');
        } else {
            $end_date = date('Y-m-d');
        }
        $post_list = Retur::where('retur_type', '=', 'pembelian')->whereBetween('retur_tanggal', [$start_date . " 00:00:00", $end_date . " 23:59:59"])
            ->orderBy('retur_tanggal', 'DESC')->get();

        return view('back/report_retur_selling', ['post_list' => $post_list, 'start_date' => $start_date, 'end_date' => $end_date, 'copyright' => copyright()]);
    }
    public function profit_loss()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        if (Input::get('start_date') != '') {
            $start_date = Input::get('start_date');
        } else {
            $start_date = date('Y-m');
        }
        $jual_keterangan = ['eceran','grosir','bon'];
        $post_list = Selling::join('sell_details', 'sell_details.jual_id', '=', 'sells.jual_id')
            ->whereIn('jual_keterangan', $jual_keterangan)
            ->where('jual_tanggal', 'like',  $start_date . '%')
            ->orderBy('jual_tanggal', 'DESC')->get();

        return view('back/report_profit_loss', ['post_list' => $post_list, 'start_date' => $start_date,  'copyright' => copyright()]);
    }
    public function profit_loss_download()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }

        return Excel::download(new ProfitLossExport, 'Report_Profit_Loss.xlsx');
    }
}
