@extends('layouts.post.post_edit')

@section('content')
@include('layouts.menu')
<div class="row wrapper border-bottom white-bg page-heading">
<div class="col-lg-10">
<h2>Supplier</h2>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="{{ url('home') }}">Home</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ url('supplier') }}">Supplier</a>
    </li>
    <li class="breadcrumb-item active">
        <strong>All Supplier</strong>
    </li>
</ol>
</div>
<div class="col-lg-2">

</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
            <form name="myform" method="post" action="{{ url('supplier/create') }}" enctype="multipart/form-data" class="form" data-async>
                @csrf
				<div class="row">
					<div class="col-lg-12 padding-none">
						<div class="ibox ">
							<div class="ibox-title">
								<h5>Supplier Editor</h5>
								<a href="{{ url('supplier/add') }}" class="btn btn-default pull-left">Add New Supplier</a>
								<button type="submit" name="Pixel_Save" class="btn btn-primary float-right">Save Supplier</button>
							</div>
							<div class="ibox-content">
								<div class="form-group">
									<label for="title">Nama Supplier</label>
									<input type="text" class="form-control" name="suplier_nama" placeholder="Nama Supplier" required>
                                </div>
                                <div class="form-group">
									<label for="title">Sales</label>
									<input type="text" class="form-control" name="suplier_sales" required="required">
                                </div>
                                <div class="form-group">
									<label for="title">Alamat</label>
									<input type="text" class="form-control" name="suplier_alamat" required="required">
                                </div>
                                <div class="form-group">
									<label for="title">Phone</label>
									<input type="text" class="form-control" name="suplier_notelp" required="required">
                                </div>
                                <div class="form-group">
									<label for="title">Rekening</label>
									<input type="text" class="form-control" name="suplier_rekening" required="required">
                                </div>

							</div>
						</div>
					</div>
                </div>

            </form>
		</div>
<div class="footer">
<div class="float-right">
<strong>
    </strong>
</div>
<div>
<strong>Copyright</strong> {{$copyright}}
</div>
</div>
@endsection
