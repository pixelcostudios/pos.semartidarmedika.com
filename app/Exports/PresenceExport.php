<?php

namespace App\Exports;

use DB;
use Image;

use App\Target;
use App\Outlet;
use App\Helpers\Base;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input as input;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

class PresenceExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        if (Input::get('pilot_id') != '') {
            $pilot_id = Input::get('pilot_id');
        } else {
            $pilot_id = '';
        }
        if (Input::get('outlet_id') != '') {
            $outlet_id = Input::get('outlet_id');
        } else {
            $outlet_id = '';
        }
        if (Input::get('pilot_id') != '') {
            $pilot_id = Input::get('pilot_id');
        } else {
            $pilot_id = '';
        }
        if (Input::get('month') != '') {
            $month = Input::get('month');
        } else {
            $month = date('Y-m'); //'';
        }

        $role_id = ['4', '6'];
        $pilot_list_a     = DB::table('my_user')
            ->join('my_user_group', 'my_user.user_id', '=', 'my_user_group.user_id')
            ->leftJoin('my_outlet_group', 'my_user.user_id', '=', 'my_outlet_group.user_id')
            ->join('my_outlet', 'my_outlet_group.outlet_id', '=', 'my_outlet.outlet_id')
            ->whereIn('my_user_group.role_id', $role_id)
            ->where('my_user.user_active', '1')
            ->where('my_user_group.status', '1')
            ->where('role_status', '1')
            ->orderBy('my_user.user_first_name', 'ASC')
            ->get();
        $array_pilot = array();
        foreach ($pilot_list_a as $key) {
            $array_pilot[] = array(
                'pilot_id' => $key->user_id,
                'pilot_name' => $key->user_first_name . ' ' . $key->user_last_name,
                'outlet_id' => $key->outlet_id,
                'outlet_name' => $key->outlet_name,
                'outlet_region' => $key->outlet_region
            );
        }
        $pilot_list     = multi_unique($array_pilot);

        if ($outlet_id != '' and $outlet_id != 'all' and $outlet_id != null) {
            if ($pilot_id != '' and $pilot_id != 'all' and $pilot_id != null) {
                $pilot_list_b     = DB::table('my_user')->join('my_user_group', 'my_user_group.user_id', '=', 'my_user.user_id')->join('my_outlet_group', 'my_outlet_group.user_id', '=', 'my_user.user_id')->join('my_outlet', 'my_outlet.outlet_id', '=', 'my_outlet_group.outlet_id')->where('my_user.user_active', '1')->where('my_outlet.outlet_id', $outlet_id)->where('my_user.user_id', $pilot_id)->whereIn('my_user_group.role_id', $role_id)->get();
                $array_pilot_b = array();
                foreach ($pilot_list_b as $key) {
                    $array_pilot_b[] = array(
                        'pilot_id' => $key->user_id,
                        'pilot_name' => $key->user_first_name . ' ' . $key->user_last_name,
                        'outlet_id' => $key->outlet_id,
                        'outlet_name' => $key->outlet_name,
                        'outlet_region' => $key->outlet_region
                    );
                }
                $pilot_list_b   = multi_unique($array_pilot_b);
                $pilot_array_list     = $pilot_list_b; //DB::table('my_user')->join('my_user_group', 'my_user_group.user_id', '=', 'my_user.user_id')->join('my_outlet_group', 'my_outlet_group.user_id', '=', 'my_user.user_id')->join('my_outlet', 'my_outlet.outlet_id', '=', 'my_outlet_group.outlet_id')->where('my_user.user_active', '1')->where('my_outlet.outlet_id', $outlet_id)->where('my_user.user_id', $pilot_id)->whereIn('my_user_group.role_id', $role_id)->get();
            } else {
                $pilot_array_list     = $pilot_list; //DB::table('my_user')->join('my_user_group', 'my_user_group.user_id', '=', 'my_user.user_id')->join('my_outlet_group', 'my_outlet_group.user_id', '=', 'my_user.user_id')->join('my_outlet', 'my_outlet.outlet_id', '=', 'my_outlet_group.outlet_id')->where('my_user.user_active', '1')->where('my_outlet.outlet_id', $outlet_id)->whereIn('my_user_group.role_id', $role_id)->get();
            }
        } else {
            if ($pilot_id != '' and $pilot_id != 'all' and $pilot_id != null) {
                $pilot_list_c     = DB::table('my_user')->join('my_user_group', 'my_user_group.user_id', '=', 'my_user.user_id')->join('my_outlet_group', 'my_outlet_group.user_id', '=', 'my_user.user_id')->join('my_outlet', 'my_outlet.outlet_id', '=', 'my_outlet_group.outlet_id')->where('my_user.user_active', '1')->whereIn('my_user_group.role_id', $role_id)->where('my_user.user_id', $pilot_id)->orderBy('user_first_name', 'ASC')->get();
                $array_pilot_c = array();
                foreach ($pilot_list_c as $key) {
                    $array_pilot_c[] = array(
                        'pilot_id' => $key->user_id,
                        'pilot_name' => $key->user_first_name . ' ' . $key->user_last_name,
                        'outlet_id' => $key->outlet_id,
                        'outlet_name' => $key->outlet_name,
                        'outlet_region' => $key->outlet_region
                    );
                }
                $pilot_list_c   = multi_unique($array_pilot_c);
                $pilot_array_list     = $pilot_list_c; //DB::table('my_user')->join('my_user_group', 'my_user_group.user_id', '=', 'my_user.user_id')->join('my_outlet_group', 'my_outlet_group.user_id', '=', 'my_user.user_id')->join('my_outlet', 'my_outlet.outlet_id', '=', 'my_outlet_group.outlet_id')->where('my_user.user_active', '1')->whereIn('my_user_group.role_id', $role_id)->where('my_user.user_id', $pilot_id)->orderBy('user_first_name', 'ASC')->get();
            } else {
                $pilot_array_list     = $pilot_list; //DB::table('my_user')->join('my_user_group', 'my_user_group.user_id', '=', 'my_user.user_id')->join('my_outlet_group', 'my_outlet_group.user_id', '=', 'my_user.user_id')->join('my_outlet', 'my_outlet.outlet_id', '=', 'my_outlet_group.outlet_id')->where('my_user.user_active', '1')->whereIn('my_user_group.role_id', $role_id)->orderBy('user_first_name', 'ASC')->get();
            }
        }
        $title_array        = array('r_no' => 'No', 'r_pilot_name' => 'Nama Pilot', 'r_outlet_name' => 'Nama Gerai', 'r_region' => 'Region');

        foreach (range(1, 31) as $days) {
            $days_[] = $days;
        }
        $title = array_merge($title_array, $days_);
        $i = 0;
        $data_array = array(); //
        foreach ($pilot_array_list as $key) {
            $i++;
            foreach ($days_ as $keys) {
                $theday[] = DB::table('my_outlet_presence')->where('outlet_id', '=', $key['outlet_id'])->where('user_id', '=', $key['pilot_id'])->where('date', '=', $month . '-' . sprintf("%02d", $keys))->count();
            }
            $data_array[]   = array_merge(array('r_no' => $i, 'r_pilot_name' => $key['pilot_name'], 'r_outlet_name' => $key['outlet_name'], 'r_region' => $key['outlet_region']), $theday);
        }
        // $post_list   = array_merge($title, $data_array);
        return collect($data_array);//Target::all();
    }
    public function headings(): array
    {
        return [
            'No',
            'Nama Pilot',
            'Nama Gerai',
            'Region',
            '1',
            '2',
            '3',
            '4',
            '5',
            '6',
            '7',
            '8',
            '9',
            '10',
            '11'
        ];
    }
}
