<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sellingdetail extends Model
{
    protected $primaryKey = 'd_jual_id';
    protected $table = "sell_details";
}
