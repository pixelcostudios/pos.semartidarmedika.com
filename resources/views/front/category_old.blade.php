@extends('layouts.front.post')
@section('content')
@include('layouts.front.header')
<section class="content margin-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="content-box">
                    <h3 class="title-small-2"> Berita</h3>
                    <div class="row text-center">
                        @foreach($post_list as $p)
                        <div class="col-sm-6 col-md-4">
                            <div class="blog-box">
                                <div class="blog-box-image">
                                    @foreach (App\Page::where('post_type', 'images')->where([['post_up', '=', $p->post_id]])->skip(0)->take(1)->get() as $p2)
                                    <img src="{{url('/')}}/thumb/crop/340/185/{{$p2->post_slug}}.{{$p2->post_mime_type}}"
                                        alt="{{$p->post_title}}" class="img-responsive" width="100%">
                                    @endforeach
                                </div>
                                <div class="blog-box-content">
                                    <h5><a href="{{url('/')}}/{{$p->post_slug}}">{{$p->post_title}}</a></h5>
                                    <p>{{substr(strip_tags($p->post_content),0,150)}}...</p><br>
                                    <a href="{{url('/')}}/{{$p->post_slug}}" class="btn btn-primary btn-sm site-btn">Read More</a>
                                </div>
                            </div>
                        </div> <!-- End Col -->
                        @endforeach
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<footer>
    <div class="copyright text-center">
        Copyright © 2020. Sistem Informasi
    </div>
</footer>
@endsection
