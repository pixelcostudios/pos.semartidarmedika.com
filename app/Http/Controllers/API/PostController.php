<?php

namespace App\Http\Controllers\API;

use DB;
use App\Categories;
use App\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input as input;

class PostController extends Controller
{
    public $successStatus = 200;

    public function index()
    {
        $post_list    = Post::leftJoin('metas', 'posts.post_id', '=', 'metas.meta_source')->where('posts.post_type', 'post')->where('posts.status', '1')->orderBy('date', 'DESC')->get();
        $data_array = array();
        foreach ($post_list as $key) {
            $images_array = array();
            foreach (Post::where('post_type', 'images')->where([['post_up', '=', $key->post_id]])->skip(0)->take(1)->get() as $p2) {
                $images_array[] = url('/') . '/upload/' .  $p2->post_slug . '.' . $p2->post_mime_type;
            }
            $data_array[] = array('id' => $key->post_id, 'title' => $key->post_title, 'images' => $images_array);
        }
        return response()->json(['status' => 200, 'message' => 'List Post', 'data' => $data_array], $this->successStatus);
    }
    public function detail($id)
    {
        $post_list    = Post::where('post_id', $id)->where('posts.status', '1')->get();
        $data_array = array();
        foreach ($post_list as $key) {
            $images_array = array();
            foreach (Post::where('post_type', 'images')->where([['post_up', '=', $key->post_id]])->skip(0)->take(1)->get() as $p2) {
                $images_array[] = url('/') . '/upload/' .  $p2->post_slug . '.' . $p2->post_mime_type;
            }
            $data_array[] = array('id' => $key->post_id, 'title' => $key->post_title, 'images' => $images_array);
        }
        return response()->json(['status' => 200, 'message' => 'List Post Detail', 'data' => $data_array], $this->successStatus);
    }
}
