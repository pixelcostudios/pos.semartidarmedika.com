<?php

namespace App\Http\Controllers;
use DB;
use App\Page;
use App\Index;
use App\Setting;
use App\Helpers\Base;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index()
    {
        $setting_list   = Setting::where('setting_type', 'meta')->where('setting_status', '1')->get();

        foreach ($setting_list as $key => $value) {
            $setting[] = $value->setting_value;
        }
        $title = '';
        $meta_title         = ($setting) ? $setting[2] : '';
        $meta_key           = ($setting) ? $setting[1] : '';
        $meta_desc          = ($setting) ? $setting[0] : '';

        $gallery_list       = Page::leftJoin('metas', 'posts.post_id', '=', 'metas.meta_source')->where('meta_dest', '3')->where('status', '=', '1')->orderby('date', 'DESC')->limit('6')->get();
        $blog_list          = Page::leftJoin('metas', 'posts.post_id', '=', 'metas.meta_source')->where('meta_dest', '1')->where('status', '=', '1')->orderby('date', 'DESC')->limit('3')->get();
        $reason_list          = Page::leftJoin('metas', 'posts.post_id', '=', 'metas.meta_source')->where('meta_dest', '7')->where('status', '=', '1')->orderby('date', 'DESC')->limit('5')->get();
        $testimonial_list          = Page::leftJoin('metas', 'posts.post_id', '=', 'metas.meta_source')->where('meta_dest', '6')->where('status', '=', '1')->orderby('date', 'DESC')->limit('3')->get();
        $online_list        = Page::where('post_up', '=', '42')->where('status', '=', '1')->get();
        $offline_list       = Page::where('post_up', '=', '43')->where('status', '=', '1')->get();
        $slideshow_list       = Page::where('post_type', '=', 'slideshow')->where('status', '=', '1')->orderby('date', 'DESC')->get();

        $facebook   = Setting::where('setting_name', '=', 'facebook')->limit('1')->value('setting_value');
        $youtube   = Setting::where('setting_name', '=', 'youtube')->limit('1')->value('setting_value');
        $instagram   = Setting::where('setting_name', '=', 'instagram')->limit('1')->value('setting_value');

        $menu = display_menu(5);
        // print_r($setting);
        // $post_list = Member::join('users_groups', 'users_groups.user_id', '=', 'users.id')->join('groups', 'groups.id', '=', 'users_groups.group_id')->orderBy('created_at', 'DESC')->paginate($show);
        $data_array = [
            'title'         => $title,
            'menu'          => $menu,
            'setting'       => array('meta_title' => $meta_title, 'meta_keyword' => $meta_key, 'meta_desc' => $meta_desc),
            'online_list'   => $online_list,
            'offline_list'  => $offline_list,
            'gallery_list'  => $gallery_list,
            'blog_list'     => $blog_list,
            'reason_list'   => $reason_list,
            'testimonial_list'=> $testimonial_list,
            'slideshow_list'=> $slideshow_list,
            'facebook'      => $facebook,
            'youtube'       => $youtube,
            'instagram'     => $instagram
            ];
        // return view('front/index', $data_array)->render();
    }
}
