@extends('layouts.menu.menu')

@section('content')
@include('layouts.menu')
<div class="row wrapper border-bottom white-bg page-heading">
<div class="col-lg-10">
<h2>Menu</h2>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="{{url('/')}}">Home</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{url('menu/')}}">Menu</a>
    </li>
    <li class="breadcrumb-item active">
        <strong>All Menu</strong>
    </li>
</ol>
</div>
<div class="col-lg-2">

</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
<div class="row">
<div class="col-lg-12 padding-none">
    <div class="ibox ">
    <form name="myform" method="post" action="" enctype="multipart/form-data" class="form" data-async>
        @csrf
    <div class="ibox-title">
    <h5>Manage Menu </h5>
    <div class="ibox-tools">
        <a class="collapse-link">
            <i class="fa fa-chevron-up"></i>
        </a>
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            <i class="fa fa-wrench"></i>
        </a>
    </div>
    </div>

    <div class="ibox-content">
        <div class="table-responsive">
            <div class="row">
            <div class="col-sm-4">
                <div id="load"></div>
                <menu id="nestable-menu">
                <div class="form-group">
                    <div class="col-sm-12">
                    <div class="row">
                        <button type="button" class="btn btn-sm btn-white col-sm-6" data-action="expand-all">Expand All</button>
                        <button type="button" class="btn btn-sm btn-white col-sm-6" data-action="collapse-all">Collapse All</button>
                        </div>
                    </div>
                </div>
                </menu>
                <div class="form-group multi_select clearfix clear">
                    <label for="category">Menu Position</label><br>
                    <select id="position" class="form-control position-select">
                    <option value="0">No Selected</option>
                    @foreach ($category_list as $row_cat)
                        <option value="{{$row_cat['cat_id']}}" {{ ($row_cat->cat_id == $cat_id) ? 'selected' : '' }}>{{$row_cat['cat_title']}}</option>
                    @endforeach
                    </select>
                </div>
                <div class="form-group multi_select clearfix clear">
                    <label for="category">Select Menu From Category</label><br>
                    <select id="category" multiple="" class="form-control category-select">
                    </select>
                </div>
                <div class="form-group multi_select clearfix">
                    <label for="pages">Select Menu From Pages</label><br>
                    <select id="pages" multiple="" class="form-control pages-select">
                    </select>
                </div>
                <div class="form-group multi_select clearfix">
                    <label for="post">Select Menu From Post</label><br>
                    <select id="post" multiple="" class="form-control post-select">
                    </select>
                </div>
                <div class="form-group multi_select clearfix">
                    <label for="post">Select Menu From Project</label><br>
                    <select id="project" multiple="" class="form-control project-select">
                    </select>
                </div>
                <div class="form-group multi_select clearfix">
                    <label for="post">Select Menu From Hotel</label><br>
                    <select id="hotel" multiple="" class="form-control hotel-select">
                    </select>
                </div>
                <div class="form-group">
                    <label for="title">Custom Menu Title </label>
                    <input type="text" id="label" class="form-control" placeholder="Title">
                </div>
                <div class="form-group">
                    <label for="title">Custom Menu Link </label>
                    <input type="text" id="link" class="form-control" placeholder="Link">
                </div>
                <div class="col-sm-12">
                    <div class="row">
                        <button id="submit" class="btn btn-sm btn-white col-sm-6">Submit</button>
                        <button id="reset" class="btn btn-sm btn-white col-sm-6">Reset</button>
                    </div>
                </div>

                <input type="hidden" id="id">
                <br/><br />
            </div>
            <div class="col-sm-8">
                <div class="row margin-top-5">
                <div class="col-sm-3"><label class="margin-top-10" for="category">Menu Position</label></div>
                <div class="form-group col-sm-9">
                    <select class="form-control" id="select-cat" onchange="window.location = jQuery('#select-cat option:selected').val();">
                    <option value="{{ url('/') }}/menu">No Selected</option>
                    @foreach ($category_list as $row_cat)
                        <option value="{{ url('/') }}/menu?cat_id={{$row_cat['cat_id']}}" {{ ($row_cat->cat_id == $cat_id) ? 'selected' : '' }}>{{$row_cat['cat_title']}}</option>
                    @endforeach
                    </select>
                </div>
                </div>

                <div class="cf nestable-lists">

                <div class="dd" id="nestable">

                <?php

                $ref   = [];
                $items = [];

                foreach($post_list as $data) {

                    $thisRef = &$ref[$data->id];

                    $thisRef['parent']  = $data->parent;
                    $thisRef['type']    = $data->type;
                    $thisRef['label']   = $data->label;
                    $thisRef['link']    = substr($data->link, 0, 20);
                    $thisRef['position']= $data->position;
                    $thisRef['id']      = $data->id;

                    if($data->parent == 0) {
                            $items[$data->id] = &$thisRef;
                    } else {
                            $ref[$data->parent]['child'][$data->id] = &$thisRef;
                    }
                }

                print get_menu($items);

                ?>
            </div>
                    <div class="clear">
                    </div>
            </div>
            <input type="hidden" id="nestable-output">
            @if (count($post_list) > 0)
            <br>
            <button id="save" class="btn btn-sm btn-primary">Save Menu</button>
            <a href="{{ url('/') }}/menu" class="btn btn-sm btn-white">Reload Menu</a>
            @endif
            </div>

            </div>
            <div class="clear">
        </div>

    </div>
    </form>
</div>
</div>

</div>
</div>
<div class="footer">
<div class="float-right">
<strong>
    </strong>
</div>
<div>
<strong>Copyright</strong> {{$copyright}}
</div>
</div>
@endsection
