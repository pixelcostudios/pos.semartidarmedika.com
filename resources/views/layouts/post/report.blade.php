<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link href="{{ url('/') }}/themes/default/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/themes/default/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="{{ url('/') }}/themes/default/css/animate.css" rel="stylesheet">
    <link href="{{ url('/') }}/themes/default/css/plugins/select2/select2.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/themes/default/js/plugins/datetime/bootstrap-datetimepicker.css" rel="stylesheet" />
    <link href="{{ url('/') }}/themes/default/js/plugins/datetime/bootstrap-datepicker3.min.css" rel="stylesheet" />
    <link href="{{ url('/') }}/themes/default/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
    <!-- Toastr style -->
    <link href="{{ url('/') }}/themes/default/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/themes/default/css/style.css" rel="stylesheet">
    <link href="{{ url('/') }}/themes/default/css/custom.css" rel="stylesheet">
<style>
    @media print {
        /* Hide everything in the body when printing... */
        .printing { display: none; }
        .ibox-content {
            padding: 0;
            border: 0;
        }
        .border-bottom {
            border-bottom: 0;
        }
        .wrapper-content {
            padding: 0;
        }
        /* ...except our special div. */
        body.printing #print-me { display: block; }
    }

    @media screen {
        /* Hide the special layer from the screen. */

        #print-me { display: none; }
    }
    </style>
</head>

<body class="top-navigation pace-done">
    <div id="wrapper">

        <div id="page-wrapper" class="gray-bg">
            @yield('content')
        </div>
    </div>

    <!-- Mainly scripts -->
<script src="{{ url('/') }}/themes/default/js/jquery-3.1.1.min.js"></script>
<script src="{{ url('/') }}/themes/default/js/popper.min.js"></script>
<script src="{{ url('/') }}/themes/default/js/bootstrap.js"></script>
<script src="{{ url('/') }}/themes/default/js/plugins/metismenu/jquery.metismenu.js"></script>
<script src="{{ url('/') }}/themes/default/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="{{ url('/') }}/themes/default/js/inspinia.js"></script>
<script src="{{ url('/') }}/themes/default/js/plugins/pace/pace.min.js"></script>

<!-- Check -->
<script src="{{ url('/') }}/themes/default/js/check.js"></script>

<script src="{{ url('/') }}/themes/default/js/plugins/select2/select2.full.min.js"></script>
<script src="{{ url('/') }}/themes/default/js/plugins/validation/jquery.form-validator.min.js"></script>

<script src="{{ url('/') }}/themes/default/js/plugins/datetime/bootstrap-datetimepicker.min.js"></script>
<script src="{{ url('/') }}/themes/default/js/plugins/datetime/bootstrap-datepicker.min.js"></script>

<!-- Toastr script -->
<script src="{{ url('/') }}/themes/default/js/plugins/toastr/toastr.min.js"></script>
    <script type="text/javascript">
    $.urlParam = function(name) {
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results == null) {
        return null;
    }
    return decodeURI(results[1]) || 0;
}

    $(document).ready(function() {
        function tableToExcel(table, name, filename) {
            let uri = 'data:application/vnd.ms-excel;base64,',
            template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><title></title><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}&lt;/table&gt;&lt;/body&gt;&lt;/html&gt;', base64 = function(s) { return window.btoa(decodeURIComponent(encodeURIComponent(s))) },
            format = function(s, c) {
                return s.replace(/{(\w+)}/g, function(m, p) {
                    return c[p];
                })
            }

            if (!table.nodeType) table = document.getElementById(table)
            var ctx = {
                worksheet: name || 'Worksheet',
                table: table.innerHTML
            }

            var link = document.createElement('a');
            link.download = filename;
            link.href = uri + base64(format(template, ctx));
            link.click();
        }
        $('.download-xls').click(function(){
            var title = $(".title-export").html();
            var start_date = $(".start_date").val();
            var end_date = $(".end_date").val();
            tableToExcel('myTable', 'name', title+'_'+start_date+'_'+end_date+'.xls');
        });
        $(".btn-print").click(function () {
            $("#printarea").show();
            window.print();
        });
    });

    </script>
<script>
    $(document).ready(function() {
    $(".select_list").select2();
    $(".date-picker").datetimepicker({
		minView: 2,format: 'yyyy-mm-dd',autoclose: true
	});
    $('.date-picker-month').datetimepicker({
        format: "yyyy-mm",
        startView: "year",
        minView: "year",
        autoclose: true
    });
    $("#{{request()->segment(1)}}MainNav").addClass('active');
    $("#{{request()->segment(1)}}MainNav .collapse").addClass('in');
    });
	$(document).on("click", ".open-modal_deleted", function () {
		var mypost_id = $(this).data('id');
		$(".modal-body #post_id").val(mypost_id);
		$('#modal_deleted').modal('show');
	});
</script>
</body>
</html>
