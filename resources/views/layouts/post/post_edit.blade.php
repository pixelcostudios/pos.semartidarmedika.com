<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link href="{{ url('/') }}/themes/default/css/bootstrap.min.css" rel="stylesheet">
	<link href="{{ url('/') }}/themes/default/font-awesome/css/font-awesome.css" rel="stylesheet">
	<link href="{{ url('/') }}/themes/default/js/plugins/multiselect/dist/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
	<link href="{{ url('/') }}/themes/default/js/plugins/datetime/bootstrap-datetimepicker.css" rel="stylesheet" />
	<link href="{{ url('/') }}/themes/default/css/plugins/select2/select2.min.css" rel="stylesheet">
	<link href="{{ url('/') }}/themes/default/css/plugins/dualListbox/bootstrap-duallistbox.min.css" rel="stylesheet">
	<link href="{{ url('/') }}/themes/default/css/animate.css" rel="stylesheet">
	<link href="{{ url('/') }}/themes/default/css/style.css" rel="stylesheet">
	<link href="{{ url('/') }}/themes/default/css/custom.css" rel="stylesheet">

</head>

<body class="top-navigation pace-done">
    <div id="wrapper">

        <div id="page-wrapper" class="gray-bg">
            @yield('content')
        </div>
    </div>

 <!-- Mainly scripts -->
<script src="{{ url('/') }}/themes/default/js/jquery-3.1.1.min.js"></script>
<script src="{{ url('/') }}/themes/default/js/popper.min.js"></script>
<script src="{{ url('/') }}/themes/default/js/bootstrap.js"></script>
<script src="{{ url('/') }}/themes/default/js/plugins/metismenu/jquery.metismenu.js"></script>
<script src="{{ url('/') }}/themes/default/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="{{ url('/') }}/themes/default/js/inspinia.js"></script>
<script src="{{ url('/') }}/themes/default/js/plugins/pace/pace.min.js"></script>

<script src="{{ url('/') }}/themes/default/js/plugins/multiselect/dist/js/bootstrap-multiselect.js" type="text/javascript"></script>

<!--CK Editor-->
<script src="{{ url('/') }}/themes/default/js/plugins/ckeditor/ckeditor.js"></script>
<script src="{{ url('/') }}/themes/default/js/plugins/ckeditor/en.js"></script>
<script src="{{ url('/') }}/themes/default/js/plugins/ckeditor/id.js"></script>
<script src="{{ url('/') }}/themes/default/js/plugins/ckeditor/ck.js"></script>

<!--Files Scripts-->
<script src="{{ url('/') }}/themes/default/js/plugins/filestyle/filestyle.js"></script>
<script src="{{ url('/') }}/themes/default/js/plugins/filestyle/aplication.js"></script>

<!-- Check -->
<script src="{{ url('/') }}/themes/default/js/check.js"></script>

<!-- Select2 -->
<script src="{{ url('/') }}/themes/default/js/plugins/select2/select2.full.min.js"></script>

<!--Bootstrap Date Picker-->
<script src="{{ url('/') }}/themes/default/js/plugins/datetime/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript">
        $(document).ready(function() {
            var max_fields = 10; //maximum input boxes allowed
            var wrapper = $(".input_fields_wrap"); //Fields wrapper
            var add_button = $(".add_field_button"); //Add button ID

            var x = 1; //initlal text box count
            $(add_button).click(function(e) { //on add input button click
                e.preventDefault();
                if (x < max_fields) { //max input box allowed
                    x++; //text box increment
                    $(wrapper).append('<div class="form-group row ermoved"><div class="col-sm-5"><label>Value</label><input name="value[]" class="form-control" type="number" placeholder="Minimal..."></div><div class="col-sm-5"><label for="title">Price </label><input type="number" class="form-control" name="price[]" placeholder="Price"></div><div class="col-sm-2 text-center"><label for="title">Delete </label><br><button type="button" class="btn btn-default remove_field"><i class="fa fa-trash"></i></button></div>'); //add input box
                    // $(wrapper).append('<div class="row relative"><div class="form-group col-sm-6"><label for="title">Drug </label><input type="text" class="form-control" name="post_drug[]" placeholder="Drug"></div><div class="form-group col-sm-5"><label for="title">QTY </label><input type="number" class="form-control" name="post_qty[]" placeholder="QTY"></div> <div class="form-group col-sm-1 text-center"><label for="title">Delete </label><br><a href="#" class="remove_field"><i class="fa fa-trash"></i></a></div></div>'); //add input box
                }
            });

            // $(wrapper).on("click", ".remove_field", function(e) { //user click on remove text
            //     e.preventDefault();
            //     $(this).parent().remove();
            //     // alert(this);
            //     x--;
            // });
            $("body").on("click", ".remove_field", function() {
                $(this).parents(".ermoved").remove();
            });

            $("body").on("click", ".remove_button", function() {
                $(this).parents(".deleted").remove();
            });
        });
        $(document).ready(function() {
            // $('#mydata').DataTable();
        });
    </script>
<script>
    $(document).ready(function() {
    $("#{{request()->segment(1)}}MainNav").addClass('active');
    $("#{{request()->segment(1)}}MainNav .collapse").addClass('in');
    });
	$(".date-picker").datetimepicker({
		format: 'yyyy-mm-dd hh:ii:ss',
		autoclose: true,
	});
	$(document).ready(function () {
		$(".select2").select2();
		$('#example-multiple-selected').multiselect();
	});
</script>
</body>
</html>
