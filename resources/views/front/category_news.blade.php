@extends('layouts.front.category')
@section('content')
@include('layouts.front.header')
<section class="content margin-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                <h3 class="title-small-4"> Headline Terbaru</h3>
                <div class="row text-left">
                    @foreach($headline_list as $p)
                        <div class="col-sm-12">
                            <div class="row relative">
                                <div class="blog-box-image col-sm-12">
                                    @foreach (App\Page::where('post_type', 'images')->where([['post_up', '=', $p->post_id]])->skip(0)->take(1)->get() as $p2)
                                    <a href="{{url('/')}}/{{$p->post_slug}}"><img src="{{url('/')}}/thumb/crop/760/400/{{$p2->post_slug}}.{{$p2->post_mime_type}}"
                                        alt="{{$p->post_title}}" class="img-responsive" width="100%"></a>
                                    @endforeach
                                </div>
                                <div class="blog-box-content blog-box-headline col-sm-12 text-left  ">
                                    <h5 class="category"><a href="{{url('/')}}/{{$p->post_slug}}">{{$p->post_title}}</a></h5>
                                    {{time_elapsed_string($p->date, $full = false)}}
                                </div>
                            </div>
                        </div> <!-- End Col -->
                        @endforeach
                </div>
                    <h3 class="title-small-4"> Berita Terbaru</h3>
                    <div class="row text-left">
                        @foreach($post_list as $p)
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="blog-box-image col-sm-4">
                                    @foreach (App\Page::where('post_type', 'images')->where([['post_up', '=', $p->post_id]])->skip(0)->take(1)->get() as $p2)
                                    <img src="{{url('/')}}/thumb/crop/340/185/{{$p2->post_slug}}.{{$p2->post_mime_type}}"
                                        alt="{{$p->post_title}}" class="img-responsive" width="100%">
                                    @endforeach
                                </div>
                                <div class="blog-box-content col-sm-8 text-left  ">
                                    <h5 class="category"><a href="{{url('/')}}/{{$p->post_slug}}">{{$p->post_title}}</a></h5>
                                    {{time_elapsed_string($p->date, $full = false)}}
                                </div>
                            </div>
                        </div> <!-- End Col -->
                        @endforeach
                        <div class="col-sm-12">
                            @if (count($post_list) > 0)
                            @if ($post_list->lastPage() > 1)
                                    <ul class="pagination pull-left">
                                        <li class="{{ ($post_list->currentPage() == 1) ? ' disabled' : '' }} page-item">
                                            <a class=" page-link " href="{{ $post_list->url(1) }}" aria-label="Previous">
                                                <span aria-hidden="true">&laquo;</span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                        </li>
                                        @if ( $post_list->currentPage() > 5 )
                                        <li class="page-item">
                                            <a class="page-link" tabindex="-1">...</a>
                                        </li>
                                        @endif

                                        @for ($i = 1; $i <= $post_list->lastPage(); $i++)
                                            @if ( ($i > ($post_list->currentPage() - 5)) && ($i < ($post_list->currentPage() + 5)) )
                                            <li class="{{ ($post_list->currentPage() == $i) ? ' active' : '' }} page-item">
                                                <a class=" page-link " href="{{ $post_list->url($i) }}">{{ $i }}</a>
                                            </li>
                                            @endif
                                        @endfor
                                        @if ( $post_list->currentPage() < ($post_list->lastPage() - 4) )
                                        <li class="page-item">
                                            <a class="page-link" tabindex="-1">...</a>
                                        </li>
                                        @endif

                                        <li class="{{ ($post_list->currentPage() == $post_list->lastPage()) ? ' disabled' : '' }} page-item">
                                            <a href="{{ $post_list->url($post_list->currentPage()+1) }}" class="page-link" aria-label="Next">
                                                <span aria-hidden="true">&raquo;</span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </li>
                                        <li class="page-item {{($post_list->currentPage()==$post_list->lastPage())?'disabled':''}}">
                                            <a class="page-link" href="{{ $post_list->url($post_list->lastPage()) }}">
                                                Last
                                            </a>
                                        </li>
                                    </ul>
                            @endif
                        @endif
                        </div>
                    </div>

            </div>
            <div class="col-sm-4 sidebar">
                <h3 class="title-small-4"> Terpopuler</h3>
                <div class="row">
                    @foreach($popular_list as $p)
                    <div class="blog-box-image col-sm-4">
                        @foreach (App\Page::where('post_type', 'images')->where([['post_up', '=', $p->post_id]])->skip(0)->take(1)->get() as $p2)
                        <img src="{{url('/')}}/thumb/crop/100/100/{{$p2->post_slug}}.{{$p2->post_mime_type}}"
                            alt="{{$p->post_title}}" class="img-responsive" width="100%">
                        @endforeach
                    </div>
                    <div class="blog-box-content col-sm-8 text-left  ">
                        <h5 class="category_sidebar"><a href="{{url('/')}}/{{$p->post_slug}}">{{$p->post_title}}</a></h5>
                    </div>
                    @endforeach
                </div>
                <br>
                <h3 class="title-small-4"> Kategori</h3>
                <ul class="category_menu">
                    @foreach($category_list as $c)
                    <li><a href="{{url('/')}}/category/{{$c->cat_slug}}">{{$c->cat_title}}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</section>

<footer>
    <div class="copyright text-center">
        Copyright © 2020. Sistem Informasi
    </div>
</footer>
@endsection
