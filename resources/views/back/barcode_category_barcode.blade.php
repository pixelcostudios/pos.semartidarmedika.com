@extends('layouts.post.post')

@section('content')
@include('layouts.menu')
<div class="row wrapper border-bottom white-bg page-heading">
<div class="col-lg-10">
<h2>Barcode Item {{$category}}</h2>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="{{url('/')}}">Home</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{url('barcode/category/')}}">Barcode Item {{$category}}</a>
    </li>
    <li class="breadcrumb-item active">
        <strong>All Barcode Item {{$category}}</strong>
    </li>
</ol>
</div>
<div class="col-lg-2">

</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
<div class="row">
<div class="col-lg-12">
    <div class="ibox ">

            <div class="ibox-title">
                <h5>Manage Barcode Item  {{$category}}</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-wrench"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <div class="table-responsive">
                    <form method="post">
                    @csrf
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Item </th>
                                <th width="7%">Barcode</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($post_list as $p)
                            <tr>
                                <td>{{$p->barang_nama}}</td>

                                <td class="text-center">
                                    <a target="_blank" href="category_barcode/{{  $p->barang_id }}"><i class="fa fa-search"></i></a>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                       </table>

        </form>
    </div>
</div>
</div>
</div>

</div>
</div>
<div class="footer">
<div class="float-right">
<strong>
    </strong>
</div>
<div>
<strong>Copyright</strong> {{$copyright}}
</div>
</div>
@endsection
