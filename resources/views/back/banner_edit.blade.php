@extends('layouts.post.post_edit')

@section('content')
@include('layouts.navtop')
<div class="row wrapper border-bottom white-bg page-heading">
<div class="col-lg-10">
<h2>Banner</h2>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="{{ url('home') }}">Home</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ url('banner') }}">Banner</a>
    </li>
    <li class="breadcrumb-item active">
        <strong>All Banner</strong>
    </li>
</ol>
</div>
<div class="col-lg-2">

</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    @foreach($post_edit as $p)
            <form name="myform" method="post" action="{{ url('banner/update') }}/{{$p->post_id}}" enctype="multipart/form-data" class="form" data-async>
                @csrf
				<div class="row">
					<div class="col-lg-9 padding-none">
						<div class="ibox ">
							<div class="ibox-title">
								<h5>Banner Editor</h5>
								<a href="{{ url('banner/add') }}" class="btn btn-default pull-left">Add New Banner</a>
								<button type="submit" name="Pixel_Save" class="btn btn-primary float-right">Save Banner</button>
							</div>
							<div class="ibox-content">
								<div class="form-group">
									<label for="title">Title  </label>
									<input type="text" class="form-control" name="post_title" placeholder="Title " value="{{$p->post_title}}" required="required">
                                </div>
                                <div class="form-group">
									<label for="title">Link </label>
									<input type="text" class="form-control" name="post_link" placeholder="Link" value="{{$p->post_link}}" required="required">
								</div>
								<div class="row">
									<div class="form-group col-lg-4 col-sm-4 col-xs-12 multi_select clearfix">
										<label for="category">Category</label><br>
										<select id="example-multiple-selected" class="form-control margin-bottom-10" multiple="multiple" name="categories[]">
											@foreach($cat_list as $p_cat)
											<option value="{{$p_cat['cat_id']}}" @if(in_array($p_cat['cat_id'],$array_cat)=='1')selected @endif>{{$p_cat['cat_title']}}</option>
											@endforeach
										</select>
									</div>
									<div class="form-group col-lg-4 col-sm-4 col-xs-12">
										<label for="status">Status</label>
										<select name="status" class="form-control">
											<option value="1" {{ ($p->status == 1) ? 'selected' : '' }}>Publish</option>
											<option value="0" {{ ($p->status == 0) ? 'selected' : '' }}>Draft</option>
										</select>
									</div>
									<div class="form-group col-lg-4 col-sm-4 col-xs-12">
										<label for="date">Date</label>
										<div class="input-group">
											<input name="date" class="form-control date-picker" type="text" value="{{$p->date}}" data-date-format="yyyy-mm-dd hh:mm">
											<span class="input-group-addon tooltip-demo">
												<i class="fa fa-calendar" data-toggle="tooltip" data-placement="top" title="Calendar"></i>
											</span>
										</div>
									</div>
								</div>

								<div class="form-group clear">
									<label for="content">Content ID</label>
									<textarea id="content_id" name="post_content">{{$p->post_content}}</textarea>
								</div>

								<div class="clear"></div>
							</div>
						</div>
					</div>
					<div class="col-lg-3 padding-right-0">
						<div class="bar-title white-bg">
							<div class="tabs-container">
								<div class="tabs-left">
									<ul class="nav nav-tabs tooltip-demo">
										<li><a data-toggle="tab" class="active show" href="#tab_images" aria-expanded="true"> <i class="fa fa-picture-o"
												 data-toggle="tooltip" data-placement="top" title="Images"></i></a></li>
									</ul>
									<div class="tab-content ">
										<div id="tab_images" class="tab-pane active">
											<div class="panel-body">
                                                <div class="input-file"><input type="file" name="upload_photo[]" multiple="multiple" id="input_images"></div>

                                            </div>
												<ul class="gallery margin-bottom-10">
													<li class="padding-bottom-30">
														<img src="../../thumb/crop/210/210/{{$p->post_slug}}.{{$p->post_mime_type}}"
														 width="100%" alt="...">
													</li>
												</ul>
										</div>
									</div>

								</div>

							</div>
							<!--End Tab-->

							<div class="clear"></div>
						</div>
					</div>
                </div>

            </form>
            @endforeach
		</div>
<div class="footer">
<div class="float-right">
<strong>
    </strong>
</div>
<div>
<strong>Copyright</strong> {{$copyright}}
</div>
</div>
@endsection
