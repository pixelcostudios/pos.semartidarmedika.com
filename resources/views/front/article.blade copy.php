@extends('layouts.front.post_detail')
@section('content')
@include('layouts.front.header')
<section class="content margin-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                    @foreach($post_list as $p)
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="blog-post-content">
                                <h4>{{$p->post_title}}</h4>
                                <div class="meta">
                                    <div class="pull-left">
                                        {{time_elapsed_string($p->date, $full = false)}}
                                    </div>
                                    <div class="pull-right">
                                        Bagikan:
                                    </div>
                                </div>
                                @foreach (App\Page::where('post_type', 'images')->where([['post_up', '=', $p->post_id]])->skip(0)->take(1)->get() as $p2)
                                <img src="{{url('/')}}/upload/{{$p2->post_slug}}.{{$p2->post_mime_type}}"
                                    width="100%" alt="{{$p->post_title}}">
                                @endforeach
                                {!!$p->post_content!!}
                            </div>
                        </div> <!-- End Col -->
                    </div>
                    @endforeach
                </div>
                <div class="col-sm-4 sidebar">
                <h3 class="title-small-4"> Terpopuler</h3>
                <div class="row">
                    @foreach($popular_list as $p)
                    <div class="blog-box-image col-sm-4">
                        @foreach (App\Page::where('post_type', 'images')->where([['post_up', '=', $p->post_id]])->skip(0)->take(1)->get() as $p2)
                        <img src="{{url('/')}}/thumb/crop/100/100/{{$p2->post_slug}}.{{$p2->post_mime_type}}"
                            alt="{{$p->post_title}}" class="img-responsive" width="100%">
                        @endforeach
                    </div>
                    <div class="blog-box-content col-sm-8 text-left  ">
                        <h5 class="category_sidebar"><a href="{{url('/')}}/{{$p->post_slug}}">{{$p->post_title}}</a></h5>
                    </div>
                    @endforeach
                </div>
                <br>
                <h3 class="title-small-4"> Kategori</h3>
                <ul class="category_menu">
                    @foreach($category_list as $c)
                    <li><a href="{{url('/')}}/category/{{$c->cat_slug}}">{{$c->cat_title}}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</section>

<footer>
    <div class="copyright text-center">
        Copyright © 2020. Sistem Informasi
    </div>
</footer>
@endsection
