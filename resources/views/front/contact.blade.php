@extends('layouts.front.post_detail')
@section('content')
@include('layouts.front.header_post')
<!-- ++++ Most Bold Title ++++ -->
@foreach ($post_list as $key)
<!-- ++++ banner ++++ -->
    <section class="banner  o-hidden banner-inner contact-banner">
        <div class="container">
            <!--banner text-->
            <div class="banner-txt">
                <h1>{{$key->post_title}}</h1>
                <p class="semi-bold">Get in touch with us to see how our company can help you grow
                    <br /> your business online.</p>
            </div>
            <!--end banner text-->
        </div>
    </section>
    <!-- ++++ end banner ++++ -->
    <!-- ++++ contact form design ++++ -->
    <section class="bg-white o-hidden common-form-section contact-form-wrapper">
        <div class="container">
            <!--section title -->
            <h2 class="b-clor">Send Us a Message</h2>
            <hr class="dark-line" />
            <!--end section title -->
            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
                    <div class="customise-form contact-form">
                        <form class="email_form" method="post">
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="form-group customised-formgroup"> <span class="icon-user"></span>
                                        <input type="text" name="full_name" class="form-control" placeholder="Name">
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="form-group customised-formgroup"> <span class="icon-envelope"></span>
                                        <input type="email" name="email" class="form-control" placeholder="Email">
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="form-group customised-formgroup"> <span class="icon-telephone"></span>
                                        <input type="text" name="phone" class="form-control" placeholder="Phone">
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="form-group customised-formgroup"> <span class="icon-laptop"></span>
                                        <input type="text" name="website" class="form-control" placeholder="Website">
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group customised-formgroup"> <span class="icon-bubble"></span>
                                        <textarea name="message" class="form-control" placeholder="Message"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="btn-wrapper">
                                <button type="submit" class="btn btn-fill">Contact us now!</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="contact-info-box-wrapper">
                        <div class="contact-info-box"> <span class="icon-telephone"></span>
                            <div>
                                <h6>Give us a call</h6>
                                <p>082183300904</p>
                            </div>
                        </div>
                        <div class="contact-info-box"> <span class="icon-envelope"></span>
                            <div>
                                <h6>Send an email</h6>
                                <p><small>lkpsariprabumulih@gmail.com</small></p>
                            </div>
                        </div>
                        <div class="contact-info-box">
                            <ul class="social-links">
                                <li><a href="contact.html#"><span class="icon-facebook"></span></a></li>
                                <li><a href="contact.html#"><span class="icon-twitter"></span></a></li>
                                <li><a href="contact.html#"><span class="icon-instagram"></span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end contact form design -->
    <!-- google map location section start-->
    <section class="bg-white google-map-location">
        <div class="container">
            <!--section title -->
            <h2 class="b-clor text-align-center">Visit Our Work Places</h2>
            <!--end section title --><br>
            <div class="row">
                <div class="col-xs-12">
                    <p class="regular-text text-center">Jln. Padat Karya No. 77 Kel. Muara Dua Kec. Prabumulih Timur Kota Prabumulih, Sumatera Selatan Kode Pos : 31113</p>
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15930.87007767665!2d104.2574372!3d-3.4188431!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x8bebca1b32d7356a!2sLKP%20SARI%20Prabumulih%20Sumatera%20Selatan!5e0!3m2!1sen!2sid!4v1586584600236!5m2!1sen!2sid" width="100%" height="500" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div>
            </div>
        </div>
    </section>
    <!--end google map location section-->
@endforeach
@include('layouts.front.footer')
@endsection
