<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Selling extends Model
{
    protected $primaryKey = 'jual_id';
    protected $table = "sells";
}
