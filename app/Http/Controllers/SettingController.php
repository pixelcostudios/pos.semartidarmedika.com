<?php

namespace App\Http\Controllers;

use DB;
use Image;

// use App\Post;
use App\Setting;
use App\Meta;
use App\Helpers\Base;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input as input;

use Illuminate\Http\Request;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect('/panel/home/');
    }

    public function meta()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        $setting_list   = Setting::where('setting_type', 'meta')->get();

        if (request()->post('Pixel_Save')=='Submit')
        {
            foreach ($setting_list as $key) {
                DB::table('settings')
                    ->where('setting_name', $key->setting_name)
                    ->update(['setting_value' => (request()->post($key->setting_name) != '') ? request()->post($key->setting_name) : '']);
            }
            return redirect('/setting/meta/')->with('success', 'Setting updated!');
        }

        return view('back/setting_meta', [
            'setting_list' => $setting_list,
            'copyright' => copyright()
        ]);
    }

    public function contact()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        $setting_list   = Setting::where('setting_type', 'company_1')->get();

        if (request()->post('Pixel_Save') == 'Submit') {
            foreach ($setting_list as $key) {
                DB::table('settings')
                    ->where('setting_name', $key->setting_name)
                    ->update(['setting_value' => (request()->post($key->setting_name) != '') ? request()->post($key->setting_name) : '']);
            }
            return redirect('/setting/contact/')->with('success', 'Setting updated!');
        }

        return view('back/setting_contact', [
            'setting_list' => $setting_list,
            'copyright' => copyright()
        ]);
    }
    public function social_network()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        $setting_list   = Setting::where('setting_type', 'social')->where('setting_status', '1')->get();

        if (request()->post('Pixel_Save') == 'Submit') {
            foreach ($setting_list as $key) {
                DB::table('settings')
                    ->where('setting_name', $key->setting_name)
                    ->update(['setting_value' => (request()->post($key->setting_name) != '') ? request()->post($key->setting_name) : '']);
            }
            return redirect('/setting/social_network/')->with('success', 'Setting updated!');
        }

        return view('back/setting_social_network', [
            'setting_list' => $setting_list,
            'copyright' => copyright()
        ]);
    }
    public function robots()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        $setting_list   = Setting::where('setting_type', 'robots')->where('setting_status', '1')->get();

        if (request()->post('Pixel_Save') == 'Submit') {
            foreach ($setting_list as $key) {
                DB::table('settings')
                    ->where('setting_name', $key->setting_name)
                    ->update(['setting_value' => (request()->post($key->setting_name) != '') ? request()->post($key->setting_name) : '']);
            }
            return redirect('/setting/robots/')->with('success', 'Setting updated!');
        }

        return view('back/setting_robots', [
            'setting_list' => $setting_list,
            'copyright' => copyright()
        ]);
    }
    public function verified()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        $setting_list   = Setting::where('setting_type', 'verified')->where('setting_status', '1')->get();

        if (request()->post('Pixel_Save') == 'Submit') {
            foreach ($setting_list as $key) {
                DB::table('settings')
                    ->where('setting_name', $key->setting_name)
                    ->update(['setting_value' => (request()->post($key->setting_name) != '') ? request()->post($key->setting_name) : '']);
            }
            return redirect('/setting/verified/')->with('success', 'Setting updated!');
        }

        return view('back/setting_verified', [
            'setting_list' => $setting_list,
            'copyright' => copyright()
        ]);
    }
    public function visitor()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        $setting_list   = Setting::where('setting_type', 'visitor')->where('setting_status', '1')->get();

        if (request()->post('Pixel_Save') == 'Submit') {
            foreach ($setting_list as $key) {
                DB::table('settings')
                    ->where('setting_name', $key->setting_name)
                    ->update(['setting_value' => (request()->post($key->setting_name) != '') ? request()->post($key->setting_name) : '']);
            }
            return redirect('/setting/visitor/')->with('success', 'Setting updated!');
        }

        return view('back/setting_visitor', [
            'setting_list' => $setting_list,
            'copyright' => copyright()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function show(Setting $setting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function edit(Setting $setting)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Setting $setting)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function destroy(Setting $setting)
    {
        //
    }
}
