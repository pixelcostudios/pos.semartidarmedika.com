<?php

namespace App\Http\Controllers;

use DB;
use App\Item;
use App\Selling;
use App\Sellingdetail;
use App\Helpers\Base;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input as input;
use Illuminate\Http\Request;

class SellingController extends Controller
{
    public function retail()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        if (Input::get('show') != '') {
            $show = Input::get('show');
        } else {
            $show = 10;
        }

        $post_list = Selling::where([['jual_keterangan', '=', 'eceran']])->leftjoin('users', 'users.id', '=', 'sells.jual_user_id')->orderBy('jual_tanggal', 'DESC')->paginate($show);

        if (isset($_POST['post-delete']) && $_POST['post-delete'] != "")
        {
            for ($i = 0; $i < count(@$_POST['post-check']); $i++) {
                $post_id = @$_POST['post-check'][$i];

                Selling::where('jual_id', '=', $post_id)->delete();
                Sellingdetail::where('jual_id', '=', $post_id)->delete();
            }
            return redirect('/selling/retail/')->with('success', 'Post updated!');
        }
        elseif (isset($_POST['post-deleted']) && $_POST['post-deleted'] != "")
        {
            $post_id = $_POST['post_id'];

            Selling::where('jual_id', '=', $post_id)->delete();
            Sellingdetail::where('jual_id', '=', $post_id)->delete();

            return redirect('/selling/retail/')->with('success', 'Post updated!');
        }

        return view('back/selling_retail', ['post_list' => $post_list, 'show' => $show, 'copyright' => copyright()]);
    }
    public function retail_search()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        if (Input::get('show') != '') {
            $show = Input::get('show');
        } else {
            $show = 10;
        }
        if (Input::get('query') != '') {
            $query = Input::get('query');
        } else {
            $query = '';
        }

        $post_list = Selling::where([['jual_keterangan', '=', 'eceran']])->leftjoin('users', 'users.id', '=', 'sells.jual_user_id')->where('jual_nofak', 'like', '%' . $query . '%')->orWhere('jual_customer', 'like', '%' . $query . '%')->orderBy('jual_tanggal', 'DESC')->paginate($show);

        if (isset($_POST['post-delete']) && $_POST['post-delete'] != "")
        {
            for ($i = 0; $i < count(@$_POST['post-check']); $i++) {
                $post_id = @$_POST['post-check'][$i];

                Selling::where('jual_id', '=', $post_id)->delete();
                Sellingdetail::where('jual_id', '=', $post_id)->delete();
            }
            return redirect('/selling/retail/')->with('success', 'Post updated!');
        }
        elseif (isset($_POST['post-deleted']) && $_POST['post-deleted'] != "")
        {
            $post_id = $_POST['post_id'];

            Selling::where('jual_id', '=', $post_id)->delete();
            Sellingdetail::where('jual_id', '=', $post_id)->delete();

            return redirect('/selling/retail/')->with('success', 'Post updated!');
        }

        return view('back/selling_retail_search', ['post_list' => $post_list, 'show' => $show, 'query' => $query, 'copyright' => copyright()]);
    }
    public function retail_print($id)
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }

        $post_list = Selling::where([['jual_keterangan', '=', 'eceran'], ['jual_id', '=', $id]])->leftjoin('users', 'users.id', '=', 'sells.jual_user_id')->orderBy('jual_tanggal', 'DESC')->get();
        if (Input::get('type') != '') {
            $type = Input::get('type');
            if($type=='dot')
            {
                return view('back/selling_retail_print_dot', ['post_list' => $post_list, 'copyright' => copyright()]);
            }
            else
            {
                return view('back/selling_retail_print_thermal', ['post_list' => $post_list, 'copyright' => copyright()]);
            }
        } else {
            return view('back/selling_retail_print_dot', ['post_list' => $post_list, 'copyright' => copyright()]);
        }
    }
    public function retail_edit($id)
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        $post_edit      = Selling::where('jual_id', $id)->get();

        return view('back/selling_distributor_edit', [
            'post_edit' => $post_edit,
            'copyright' => copyright()
        ]);
    }
    public function retail_add()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        $post_list      = array();

        // print_r(facture_number());

        return view('back/selling_retail_add', [
            'post_list' => $post_list,
            'copyright' => copyright()
        ]);
    }
    public function retail_create(Request $request)
    {
        $user_id        = Auth::user()->id;
        $jual_nofak     = facture_number();
        Selling::insert([
            'jual_nofak'    => $jual_nofak,
            'jual_tanggal'  => date('Y-m-d H:i:s'),
            'jual_total'    => ($request->get('total') != '') ? str_replace(',','', $request->get('total')) : '',
            'jual_jml_uang' => ($request->get('pay') != '') ? str_replace(',','', $request->get('pay')) : 0,
            'jual_kembalian'=> ($request->get('kembalian') != '') ? str_replace(',','', $request->get('kembalian')) : '',
            'jual_user_id'  => $user_id,
            'jual_keterangan' => 'eceran',
            'jual_tempo'    => ($request->get('tempo') != '') ? $request->get('tempo') : '0000-00-00',
            'jual_customer' => ($request->get('customer') != '') ? $request->get('customer') : '',
            'jual_address' => ($request->get('jual_address') != '') ? $request->get('jual_address') : '',
            'jual_phone' => ($request->get('jual_phone') != '') ? $request->get('jual_phone') : '',
            'jual_seller' => ($request->get('seller') != '') ? $request->get('seller') : '',
            'jual_transport'=> ($request->get('transport') != '') ? str_replace(',','', $request->get('transport')) : 0,
            'jual_status'   => 1,
            'created_at'    => date('Y-m-d H:i:s'),
            'updated_at'    => date('Y-m-d H:i:s'),
            'deleted_at'    => date('Y-m-d H:i:s')
        ]);

        $id = DB::getPdo()->lastInsertId();

        if(count($request->get('post_item'))>0)
        {
            $p_note   = $request->get('post_item');
            $post_discount  = $request->get('post_discount');
            $post_qty  = $request->get('post_qty');
            $post_price  = $request->get('post_price');
            $post_unit  = $request->get('post_unit');
            $total = array();
            $barang_id = custom_number();
            foreach( $p_note as $key => $n )
            {
                if($n!='')
                {
                    $selling_row = Item::where('barang_id','=', $n)->first();
                    Sellingdetail::insert([
                        'jual_id'               => $id,
                        'd_jual_nofak'          => $jual_nofak,
                        'd_jual_barang_id'      => $barang_id,
                        'd_jual_barang_nama'    => (@$selling_row->barang_harpok > 0) ? @$selling_row->barang_nama : $n, //,
                        'd_jual_barang_satuan'  => (@$selling_row->barang_harpok > 0) ? @$selling_row->barang_satuan : @$post_unit[$key],
                        'd_jual_barang_harpok'  => (@$selling_row->barang_harpok > 0) ? @$selling_row->barang_harpok : 0,
                        'd_jual_barang_harjul'  => (@$selling_row->barang_harjul != '') ? @$selling_row->barang_harjul : @$post_price[$key],
                        'd_jual_qty'            => (@$post_qty[$key]) ? @$post_qty[$key] : 0,
                        'd_jual_diskon'         => (@$post_discount[$key] != '') ? @$post_discount[$key] : 0,
                        'd_jual_total'          => (@$post_qty[$key] * @$selling_row->barang_harjul) - @$post_discount[$key],
                        'created_at'            => date('Y-m-d H:i:s'),
                        'updated_at'            => date('Y-m-d H:i:s'),
                        'deleted_at'            => date('Y-m-d H:i:s')
                    ]);
                    if (@$selling_row->barang_harpok > 0)
                    {
                        Item::where('barang_id', '=', @$selling_row->barang_id)->update(['barang_stok' => @$selling_row->barang_stok - @$post_qty[$key]]);
                    }
                    $total[] = ((int)(@$post_qty[$key]) * (int)(@$selling_row->barang_harjul)) - ((int)@$post_discount[$key]);
                }
            }
        }
        Selling::where('jual_id', '=', $id)->update(['jual_total' => array_sum($total), 'jual_kembalian' => str_replace(',', '', $request->get('pay')) - array_sum($total)]);
        return redirect('/selling/retail/')->with('success', 'Post updated!');
    }
    public function distributor()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        if (Input::get('show') != '') {
            $show = Input::get('show');
        } else {
            $show = 10;
        }

        $post_list = Selling::where([['jual_keterangan', '=', 'grosir']])->leftjoin('users', 'users.id', '=', 'sells.jual_user_id')->orderBy('jual_tanggal', 'DESC')->paginate($show);

        if (isset($_POST['post-delete']) && $_POST['post-delete'] != "") {
            for ($i = 0; $i < count(@$_POST['post-check']); $i++) {
                $post_id = @$_POST['post-check'][$i];

                Selling::where('jual_id', '=', $post_id)->delete();
                Sellingdetail::where('jual_id', '=', $post_id)->delete();
            }
            return redirect('/selling/distributor/')->with('success', 'Post updated!');
        } elseif (isset($_POST['post-deleted']) && $_POST['post-deleted'] != "") {
            $post_id = $_POST['post_id'];

            Selling::where('jual_id', '=', $post_id)->delete();
            Sellingdetail::where('jual_id', '=', $post_id)->delete();

            return redirect('/selling/distributor/')->with('success', 'Post updated!');
        }

        return view('back/selling_distributor', ['post_list' => $post_list, 'show' => $show, 'copyright' => copyright()]);
    }
    public function distributor_search()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        if (Input::get('show') != '') {
            $show = Input::get('show');
        } else {
            $show = 10;
        }
        if (Input::get('query') != '') {
            $query = Input::get('query');
        } else {
            $query = '';
        }

        $post_list = Selling::where([['jual_keterangan', '=', 'grosir']])->leftjoin('users', 'users.id', '=', 'sells.jual_user_id')->where('jual_nofak', 'like', '%' . $query . '%')->orWhere('jual_customer', 'like', '%' . $query . '%')->orderBy('jual_tanggal', 'DESC')->paginate($show);

        if (isset($_POST['post-delete']) && $_POST['post-delete'] != "") {
            for ($i = 0; $i < count(@$_POST['post-check']); $i++) {
                $post_id = @$_POST['post-check'][$i];

                Selling::where('jual_id', '=', $post_id)->delete();
                Sellingdetail::where('jual_id', '=', $post_id)->delete();
            }
            return redirect('/selling/distributor/')->with('success', 'Post updated!');
        } elseif (isset($_POST['post-deleted']) && $_POST['post-deleted'] != "") {
            $post_id = $_POST['post_id'];

            Selling::where('jual_id', '=', $post_id)->delete();
            Sellingdetail::where('jual_id', '=', $post_id)->delete();

            return redirect('/selling/distributor/')->with('success', 'Post updated!');
        }

        return view('back/selling_distributor_search', ['post_list' => $post_list, 'show' => $show, 'query' => $query, 'copyright' => copyright()]);
    }
    public function distributor_print($id)
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }

        $post_list = Selling::where([['jual_keterangan', '=', 'grosir'], ['jual_id', '=', $id]])->leftjoin('users', 'users.id', '=', 'sells.jual_user_id')->orderBy('jual_tanggal', 'DESC')->get();
        if (Input::get('type') != '') {
            $type = Input::get('type');
            if ($type == 'dot') {
                return view('back/selling_retail_print_dot', ['post_list' => $post_list, 'copyright' => copyright()]);
            } else {
                return view('back/selling_retail_print_thermal', ['post_list' => $post_list, 'copyright' => copyright()]);
            }
        } else {
            return view('back/selling_retail_print_dot', ['post_list' => $post_list, 'copyright' => copyright()]);
        }
    }
    public function distributor_edit($id)
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        $post_edit      = Selling::where('jual_id', $id)->get();

        return view('back/selling_distributor_edit', [
            'post_edit' => $post_edit,
            'copyright' => copyright()
        ]);
    }
    public function distributor_add()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        $post_list      = array();

        return view('back/selling_distributor_add', [
            'post_list' => $post_list,
            'copyright' => copyright()
        ]);
    }
    public function distributor_create(Request $request)
    {
        $user_id        = Auth::user()->id;
        $jual_nofak     = facture_number();

        Selling::insert([
            'jual_nofak'    => $jual_nofak,
            'jual_tanggal'  => date('Y-m-d H:i:s'),
            'jual_total'    => '',//($request->get('total') != '') ? str_replace(',','', $request->get('total')) : '',
            'jual_jml_uang' => ($request->get('pay') != '') ? str_replace(',','', $request->get('pay')) : '',
            'jual_kembalian'=> '',//($request->get('kembalian') != '') ? str_replace(',','', $request->get('kembalian')) : '',
            'jual_user_id'  => $user_id,
            'jual_keterangan' => 'grosir',
            'jual_tempo'    => ($request->get('tempo') != '') ? $request->get('tempo') : '',
            'jual_customer' => ($request->get('customer') != '') ? $request->get('customer') : '',
            'jual_seller' => ($request->get('seller') != '') ? $request->get('seller') : '',
            'jual_transport'=> ($request->get('transport') != '') ? str_replace(',','', $request->get('transport')) : 0,
            'jual_status'   => 1,
            'created_at'    => date('Y-m-d H:i:s'),
            'updated_at'    => date('Y-m-d H:i:s'),
            'deleted_at'    => date('Y-m-d H:i:s')
        ]);

        $id = DB::getPdo()->lastInsertId();

        if(count($request->get('post_item'))>0)
        {
            $p_note   = $request->get('post_item');
            $post_discount  = $request->get('post_discount');
            $post_qty  = $request->get('post_qty');
            $post_price  = $request->get('post_price');
            $post_unit  = $request->get('post_unit');
            $total = array();
            $barang_id = custom_number();
            foreach( $p_note as $key => $n )
            {
                if($n!='')
                {
                    $selling_row = Item::where('barang_id','=', $n)->first();
                    Sellingdetail::insert([
                        'jual_id'               => $id,
                        'd_jual_nofak'          => $jual_nofak,
                        'd_jual_barang_id'      => $barang_id,
                        'd_jual_barang_nama'    => (@$selling_row->barang_harpok > 0) ? @$selling_row->barang_nama : $n, //,
                        'd_jual_barang_satuan'  => (@$selling_row->barang_harpok > 0) ? @$selling_row->barang_satuan : @$post_unit[$key],
                        'd_jual_barang_harpok'  => (@$selling_row->barang_harpok > 0) ? @$selling_row->barang_harpok : 0,
                        'd_jual_barang_harjul'  => (@$selling_row->barang_harjul_grosir != '') ? @$selling_row->barang_harjul_grosir : @$post_price[$key],
                        'd_jual_qty'            => (@$post_qty[$key]) ? @$post_qty[$key] : 0,
                        'd_jual_diskon'         => (@$post_discount[$key] != '') ? @$post_discount[$key] : 0,
                        'd_jual_total'          => (@$post_qty[$key] * @$selling_row->barang_harjul_grosir) - @$post_discount[$key],
                        'created_at'            => date('Y-m-d H:i:s'),
                        'updated_at'            => date('Y-m-d H:i:s'),
                        'deleted_at'            => date('Y-m-d H:i:s')
                    ]);
                    if (@$selling_row->barang_harpok > 0)
                    {
                        Item::where('barang_id', '=', @$selling_row->barang_id)->update(['barang_stok' => @$selling_row->barang_stok - @$post_qty[$key]]);
                    }
                    $total[] = ((int)(@$post_qty[$key]) * (int)(@$selling_row->barang_harjul)) - ((int)@$post_discount[$key]);
                }
            }
        }
        Selling::where('jual_id', '=', $id)->update(['jual_total' => array_sum($total), 'jual_kembalian' => str_replace(',', '', $request->get('pay')) - array_sum($total)]);
        return redirect('/selling/distributor/')->with('success', 'Post updated!');
    }
    public function bill()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        if (Input::get('show') != '') {
            $show = Input::get('show');
        } else {
            $show = 10;
        }

        $post_list = Selling::where([['jual_keterangan', '=', 'bon']])->leftJoin('users', 'users.id', '=', 'sells.jual_user_id')->orderBy('jual_tanggal', 'DESC')->paginate($show);

        if (isset($_POST['post-delete']) && $_POST['post-delete'] != "") {
            for ($i = 0; $i < count(@$_POST['post-check']); $i++) {
                $post_id = @$_POST['post-check'][$i];

                Selling::where('jual_id', '=', $post_id)->delete();
                Sellingdetail::where('jual_id', '=', $post_id)->delete();
            }
            return redirect('/selling/bill/')->with('success', 'Post updated!');
        } elseif (isset($_POST['post-paid']) && $_POST['post-paid'] != "") {
            $post_id = $_POST['paid_id'];

            $price = Selling::where('jual_id', '=', $post_id)->value('jual_total');

            $post_data = Selling::find($post_id);
            $post_data->jual_jml_uang =  $price;
            $post_data->jual_kembalian = 0;
            $post_data->save();

            return redirect('/selling/bill/')->with('success', 'Post updated!');
        }elseif (isset($_POST['post-deleted']) && $_POST['post-deleted'] != "") {
            $post_id = $_POST['post_id'];

            Selling::where('jual_id', '=', $post_id)->delete();
            Sellingdetail::where('jual_id', '=', $post_id)->delete();

            return redirect('/selling/bill/')->with('success', 'Post updated!');
        }

        return view('back/selling_bill', ['post_list' => $post_list, 'show' => $show, 'copyright' => copyright()]);
    }
    public function bill_search()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        if (Input::get('show') != '') {
            $show = Input::get('show');
        } else {
            $show = 10;
        }
        if (Input::get('query') != '') {
            $query = Input::get('query');
        } else {
            $query = '';
        }

        $post_list = Selling::where([['jual_keterangan', '=', 'bon']])->leftJoin('users', 'users.id', '=', 'sells.jual_user_id')->where('jual_nofak', 'like', '%' . $query . '%')->orWhere('jual_customer', 'like', '%' . $query . '%')->orderBy('jual_tanggal', 'DESC')->paginate($show);

        if (isset($_POST['post-delete']) && $_POST['post-delete'] != "") {
            for ($i = 0; $i < count(@$_POST['post-check']); $i++) {
                $post_id = @$_POST['post-check'][$i];

                Selling::where('jual_id', '=', $post_id)->delete();
                Sellingdetail::where('jual_id', '=', $post_id)->delete();
            }
            return redirect('/selling/bill/')->with('success', 'Post updated!');
        } elseif (isset($_POST['post-paid']) && $_POST['post-paid'] != "") {
            $post_id = $_POST['paid_id'];

            $price = Selling::where('jual_id', '=', $post_id)->value('jual_total');

            $post_data = Selling::find($post_id);
            $post_data->jual_jml_uang =  $price;
            $post_data->jual_kembalian = 0;
            $post_data->save();

            return redirect('/selling/bill/')->with('success', 'Post updated!');
        }elseif (isset($_POST['post-deleted']) && $_POST['post-deleted'] != "") {
            $post_id = $_POST['post_id'];

            Selling::where('jual_id', '=', $post_id)->delete();
            Sellingdetail::where('jual_id', '=', $post_id)->delete();

            return redirect('/selling/bill/')->with('success', 'Post updated!');
        }

        return view('back/selling_bill_search', ['post_list' => $post_list, 'show' => $show, 'query' => $query, 'copyright' => copyright()]);
    }
    public function bill_print($id)
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }

        $post_list = Selling::where([['jual_keterangan', '=', 'bon'], ['jual_id', '=', $id]])->leftjoin('users', 'users.id', '=', 'sells.jual_user_id')->orderBy('jual_tanggal', 'DESC')->get();
        if (Input::get('type') != '') {
            $type = Input::get('type');
            if ($type == 'dot') {
                return view('back/selling_bill_print_dot', ['post_list' => $post_list, 'copyright' => copyright()]);
            } else {
                return view('back/selling_bill_print_thermal', ['post_list' => $post_list, 'copyright' => copyright()]);
            }
        } else {
            return view('back/selling_bill_print_dot', ['post_list' => $post_list, 'copyright' => copyright()]);
        }
    }
    public function bill_edit($id)
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        $post_edit      = Selling::where('jual_id', $id)->get();

        return view('back/selling_bill_edit', [
            'post_edit' => $post_edit,
            'copyright' => copyright()
        ]);
    }
    public function bill_add()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        $post_list      = array();

        return view('back/selling_bill_add', [
            'post_list' => $post_list,
            'copyright' => copyright()
        ]);
    }
    public function bill_create(Request $request)
    {
        $user_id        = Auth::user()->id;
        $jual_nofak     = facture_number();

        Selling::insert([
            'jual_nofak'    => $jual_nofak,
            'jual_tanggal'  => date('Y-m-d H:i:s'),
            'jual_total'    => ($request->get('total') != '') ? str_replace(',','', $request->get('total')) : '',
            'jual_jml_uang' => ($request->get('pay') != '') ? str_replace(',','', $request->get('pay')) : '',
            'jual_kembalian'=> ($request->get('kembalian') != '') ? str_replace(',','', $request->get('kembalian')) : '',
            'jual_user_id'  => $user_id,
            'jual_keterangan' => 'bon',
            'jual_tempo'    => ($request->get('tempo') != '') ? $request->get('tempo') : '',
            'jual_customer' => ($request->get('customer') != '') ? $request->get('customer') : '',
            'jual_seller' => ($request->get('seller') != '') ? $request->get('seller') : '',
            'jual_transport'=> ($request->get('transport') != '') ? str_replace(',','', $request->get('transport')) : 0,
            'jual_status'   => 1,
            'created_at'    => date('Y-m-d H:i:s'),
            'updated_at'    => date('Y-m-d H:i:s'),
            'deleted_at'    => date('Y-m-d H:i:s')
        ]);

        $id = DB::getPdo()->lastInsertId();

        if(count($request->get('post_item'))>0)
        {
            $p_note   = $request->get('post_item');
            $post_discount  = $request->get('post_discount');
            $post_qty  = $request->get('post_qty');
            $post_price  = $request->get('post_price');
            $post_unit  = $request->get('post_unit');
            $total = array();
            $barang_id = custom_number();
            foreach( $p_note as $key => $n )
            {
                if($n!='')
                {
                    $selling_row = Item::where('barang_id','=', $n)->first();
                    Sellingdetail::insert([
                        'jual_id'               => $id,
                        'd_jual_nofak'          => $jual_nofak,
                        'd_jual_barang_id'      => $barang_id,
                        'd_jual_barang_nama'    => (@$selling_row->barang_harpok > 0) ? @$selling_row->barang_nama : $n, //,
                        'd_jual_barang_satuan'  => (@$selling_row->barang_harpok > 0) ? @$selling_row->barang_satuan : @$post_unit[$key],
                        'd_jual_barang_harpok'  => (@$selling_row->barang_harpok > 0) ? @$selling_row->barang_harpok : 0,
                        'd_jual_barang_harjul'  => (@$selling_row->barang_harjul != '') ? @$selling_row->barang_harjul : @$post_price[$key],
                        'd_jual_qty'            => (@$post_qty[$key]) ? @$post_qty[$key] : 0,
                        'd_jual_diskon'         => (@$post_discount[$key] != '') ? @$post_discount[$key] : 0,
                        'd_jual_total'          => (@$post_qty[$key] * @$selling_row->barang_harjul) - @$post_discount[$key],
                        'created_at'            => date('Y-m-d H:i:s'),
                        'updated_at'            => date('Y-m-d H:i:s'),
                        'deleted_at'            => date('Y-m-d H:i:s')
                    ]);
                    if (@$selling_row->barang_harpok > 0)
                    {
                        Item::where('barang_id', '=', @$selling_row->barang_id)->update(['barang_stok' => @$selling_row->barang_stok - @$post_qty[$key]]);
                    }
                    $total[] = ((int)(@$post_qty[$key]) * (int)(@$selling_row->barang_harjul)) - ((int)@$post_discount[$key]);
                }
            }
        }
        Selling::where('jual_id', '=', $id)->update(['jual_total' => array_sum($total), 'jual_kembalian' => str_replace(',', '', $request->get('pay')) - array_sum($total)]);
        return redirect('/selling/bill/')->with('success', 'Post updated!');
    }
}
// UPDATE sells
// INNER JOIN sell_details ON sells.jual_nofak = sell_details.d_jual_nofak
// SET sell_details.jual_id = sells.jual_id
