<div class="row border-bottom white-bg">
    <nav class="navbar navbar-expand-lg navbar-static-top" role="navigation">
        <!--<div class="navbar-header">-->
        <!--<button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">-->
        <!--<i class="fa fa-reorder"></i>-->
        <!--</button>-->

        <a href="#" class="navbar-brand">PT. Semar Tidar Medika</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fa fa-reorder"></i>
        </button>

        <!--</div>-->
        <div class="navbar-collapse collapse" id="navbar">
            <ul class="nav navbar-nav mr-auto">
                <li class="active" >
                    <a aria-expanded="false" role="button" href="{{url('/')}}/home"> Home</a>
                </li>
                @if(in_group(Auth::user()->id)=='admin')
                <li class="dropdown" id="sellingMainNav">
                    <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-shopping-cart"></i> <span class="nav-label">Penjualan</span></a>
                    <ul role="menu" class="dropdown-menu">
                        <li><a href="{{ url('selling/retail') }}">Penjualan (Eceran) </a> </li>
                        <li><a href="{{ url('selling/distributor') }}">Penjualan (Grosir) </a></li>
                        <li><a href="{{ url('selling/bill') }}">Penjualan (Bon) </a></li>
                    </ul>
                </li>
                <li><a href="{{ url('buying') }}"><i class="fa fa-shopping-cart"></i> <span>Pembelian</span></a></li>
                <li class="dropdown" id="returMainNav">
                    <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-circle"></i> <span class="nav-label">Retur</span></a>
                    <ul role="menu" class="dropdown-menu">
                        <li><a href="{{ url('retur/selling') }}">Penjualan</a> </li>
                        <li><a href="{{ url('retur/buying') }}">Pembelian</a></li>
                    </ul>
                </li>
                <li class="dropdown" id="graphMainNav">
                    <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-line-chart"></i> <span class="nav-label">Grafik</span></a>
                    <ul role="menu" class="dropdown-menu">
                        <li><a href="{{ url('graph/stock') }}">Stok Barang</a> </li>
                        <li><a href="{{ url('graph/selling_month') }}">Penjualan PerBulan</a></li>
                        <li><a href="{{ url('graph/selling_year') }}">Penjualan PerTahun</a></li>
                    </ul>
                </li>
                <li class="dropdown" id="reportMainNav">
                    <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bar-chart"></i> <span class="nav-label">Laporan</span></a>
                    <ul role="menu" class="dropdown-menu">
                        <li><a href="{{ url('report/item') }}">Data Barang</a> </li>
                        <li><a href="{{ url('report/item_stock') }}">Stok Barang</a></li>
                        <li><a href="{{ url('report/item_stock_empty') }}">Stok Barang Habis</a></li>
                        <li><a href="{{ url('report/selling') }}">Penjualan Eceran</a></li>
                        <li><a href="{{ url('report/selling_distributor') }}">Penjualan Grosir</a></li>
                        <li><a href="{{ url('report/selling_bill') }}">Penjualan Bon</a></li>
                        <li><a href="{{ url('report/buying') }}">Pembelian</a></li>
                        <li><a href="{{ url('report/retur') }}">Retur Penjualan</a></li>
                        <li><a href="{{ url('report/retur_selling') }}">Retur Pembelian</a></li>
                        <li><a href="{{ url('report/profit_loss') }}">Laba/Rugi</a></li>
                    </ul>
                </li>
                <li class="dropdown" id="barcodeMainNav">
                    <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-barcode"></i> <span class="nav-label">Barcode</span></a>
                    <ul role="menu" class="dropdown-menu">
                        <li><a href="{{ url('barcode/category') }}">By Kategori</a> </li>
                        <li><a href="{{ url('barcode/all') }}">Semua Barang</a></li>
                    </ul>
                </li>
                <li class="dropdown" id="itemMainNav">
                    <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-product-hunt"></i> <span class="nav-label">Barang</span></a>
                    <ul role="menu" class="dropdown-menu">
                        <li><a href="{{ url('item/manage') }}">List Barang</a> </li>
                        <li><a href="{{ url('item/unit') }}">Unit</a></li>
                    </ul>
                </li>
                <li class="dropdown" id="categoriesMainNav">
                    <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-archive"></i> <span class="nav-label">Lainnya</span></a>
                    <ul role="menu" class="dropdown-menu">
                        <li><a href="{{ url('categories') }}?type=product">Merk Barang</a></li>
                        <li><a href="{{ url('supplier') }}">Supplier </a></li>
                    </ul>
                </li>
                <li class="dropdown" id="memberMainNav">
                    <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-users"></i> <span class="nav-label">Member</span></a>
                    <ul role="menu" class="dropdown-menu">
                        <li><a href="{{ url('member') }}">Manage Member </a></li>
                        <li><a href="{{ url('member/role') }}">Manage Roles </a></li>
                    </ul>
                </li>
                <li class="dropdown">
                <a  href="{{ url('report/item_stock_empty') }}"> <span class="label label-danger">{{App\Item::where('barang_stok','=' ,0)->count()}}</span>
                </a>
                </li>
                 @elseif(in_group(Auth::user()->id)=='kasir')
                <li class="dropdown" id="sellingMainNav">
                        <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-shopping-cart"></i> <span class="nav-label">Penjualan</span></a>
                        <ul role="menu" class="dropdown-menu">
                            <li><a href="{{ url('selling/retail_add') }}">Penjualan (Eceran) </a> </li>
                            <li><a href="{{ url('selling/distributor_add') }}">Penjualan (Grosir) </a></li>
                            <li><a href="{{ url('selling/bill_add') }}">Penjualan (Bon) </a></li>
                        </ul>
                    </li>
                    <li class="dropdown" id="returMainNav">
                    <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-circle"></i> <span class="nav-label">Retur</span></a>
                    <ul role="menu" class="dropdown-menu">
                        <li><a href="{{ url('retur/selling') }}">Penjualan</a> </li>
                    </ul>
                </li>
                <li><a href="{{ url('report/selling') }}">Penjualan Eceran</a></li>
                        <li><a href="{{ url('report/selling_distributor') }}">Penjualan Grosir</a></li>
                        <li><a href="{{ url('report/selling_bill') }}">Penjualan Bon</a></li>
                <li><a href="{{ url('item/manage') }}">List Barang</a></li>
                <li><a href="{{ url('report/item_stock_empty') }}"> <span class="label label-danger">{{App\Item::where('barang_stok','=' ,0)->count()}}</span> Stok Barang Habis</a></li>
                @endif
            </ul>

            <ul class="nav navbar-top-links navbar-right">
                <li> | {{Auth::user()->first_name.' '.Auth::user()->last_name}}</li>
                <li>
                    <a href="{{url('/')}}/logout">
                        <i class="fa fa-sign-out"></i> Log out
                    </a>
                </li>
            </ul>
        </div>
    </nav>
</div>
