<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->bigIncrements('post_id');
            $table->integer('user_id');
            $table->string('post_type');
            $table->string('post_up');
            $table->string('post_slug');
            $table->string('post_title');
            $table->longText('post_content');
            $table->longText('post_summary');
            $table->integer('post_price');
            $table->integer('post_unit');
            $table->string('post_link');
            $table->string('post_video');
            $table->string('post_key');
            $table->longText('post_desc');
            $table->integer('post_count');
            $table->integer('post_comment');
            $table->string('post_mime_type');
            $table->dateTime('date');
            $table->integer('status');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
