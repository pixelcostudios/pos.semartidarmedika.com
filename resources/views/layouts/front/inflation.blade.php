<!doctype html>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>{{$title}} | {{ $setting[0] }}</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="{{ url('/') }}/themes/price/css/bootstrap.min.css" rel="stylesheet">
<link href="{{ url('/') }}/themes/price/font-awesome/css/font-awesome.css" rel="stylesheet">
<link href="{{ url('/') }}/themes/price/js/plugins/jQuery-Table-Mobilize/dist/theme-default.min.css" rel="stylesheet">
<link href="{{ url('/') }}/themes/price/css/plugins/select2/select2.min.css" rel="stylesheet">
<link href="{{ url('/') }}/themes/price/js/plugins/datetime/bootstrap-datetimepicker.css" rel="stylesheet" />
<link href="{{ url('/') }}/themes/price/js/plugins/datetime/bootstrap-datepicker3.min.css" rel="stylesheet" />
<link href="{{ url('/') }}/themes/price/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
<link href="{{ url('/') }}/themes/price/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<link href="{{ url('/') }}/themes/price/css/animate.css" rel="stylesheet">
<link href="{{ url('/') }}/themes/price/css/style.css" rel="stylesheet">
</head>
<body style="background:none;">

@yield('content')

<!-- JS here -->
<script src="{{ url('/') }}/themes/price/js/jquery-3.1.1.min.js"></script>
<script src="{{ url('/') }}/themes/price/js/bootstrap.js"></script>
<script src="{{ url('/') }}/themes/price/js/index.js"></script>
<script src="{{ url('/') }}/themes/price/js/plugins/jQuery-Table-Mobilize/dist/jquery.tableMobilize.min.js"></script>
<script src="{{ url('/') }}/themes/price/js/plugins/select2/select2.full.min.js"></script>
<script src="{{ url('/') }}/themes/price/js/plugins/validation/jquery.form-validator.min.js"></script>
<script src="{{ url('/') }}/themes/price/js/plugins/datetime/bootstrap-datetimepicker.min.js"></script>
<script src="{{ url('/') }}/themes/price/js/plugins/datetime/bootstrap-datepicker.min.js"></script>
 <!-- Morris -->
    <script src="{{ url('/') }}/themes/price/js/plugins/morris/raphael-2.1.0.min.js"></script>
    <script src="{{ url('/') }}/themes/price/js/plugins/morris/morris.js"></script>

    <script src="{{ url('/') }}/themes/price/js/plugins/dataTables/datatables.min.js"></script>
    <script src="{{ url('/') }}/themes/price/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
    <script>
        $(function () {
        $.getJSON( "{{url('/')}}/api/inflation", function( data ) {
            var rows = $.map(data, function(value, index) {
                return "{month:'" + value.month + "',local:" + value.local + ", national:" + value.national + "}";
            });
            var data_array = rows.join(',');
            console.log(rows.join(','));

            Morris.Bar({
                element: 'morris-bar-chart',
                data:data,
                xkey: 'month',
                ykeys: ['local', 'national'],
                labels: ['Inflasi Provinsi (%)', 'Inflasi Nasioanal (%)'],
                hideHover: 'auto',
                resize: true,
                barColors: ['#1AAA35', '#cacaca'],
            });
            // Morris.Bar({
            //     element: 'morris-bar-chart',
            //     data: [{
            //         month: '2006',
            //         local: 60,
            //         national: 50
            //     }, {
            //         month: '2007',
            //         local: 75,
            //         national: 65
            //     }, {
            //         month: '2008',
            //         local: 50,
            //         national: 40
            //     }, {
            //         month: '2009',
            //         local: 75,
            //         national: 65
            //     }, {
            //         month: '2010',
            //         local: 50,
            //         national: 40
            //     }, {
            //         month: '2011',
            //         local: 75,
            //         national: 65
            //     }, {
            //         month: '2012',
            //         local: 100,
            //         national: 90
            //     }],
            //     xkey: 'month',
            //     ykeys: ['local', 'national'],
            //     labels: ['Series A', 'Series B'],
            //     hideHover: 'auto',
            //     resize: true,
            //     barColors: ['#1AAA35', '#cacaca'],
            // });

        });
        });
        $(document).ready(function () {
                $('.dataTables-example').DataTable({
                    pageLength: 25,
                    responsive: true,
                    lengthChange: false,
                    bPaginate: false,
                    dom: '<"html5buttons"B>lTfgitp',
                    buttons: [
                        { extend: 'copy' },
                        { extend: 'csv' },
                        { extend: 'excel', title: 'ExampleFile' },
                        { extend: 'pdf', title: 'ExampleFile' },

                        {
                            extend: 'print',
                            customize: function (win) {
                                $(win.document.body).addClass('white-bg');
                                $(win.document.body).css('font-size', '10px');

                                $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                            }
                        }
                    ]

                });

            });
        //$('table').tableMobilize();
    $(document).ready(function () {
        $(".select_list").select2();
        $(".districts").select2({
            placeholder: "Pilih Kabupaten / Kota",
            ajax: {
            url: '{{url('/')}}/api/districts',
            type: "get",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                search: params.term
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
            cache: true
            }
        });

        $.getJSON( "{{url('/')}}/api/inflation", function( data ) {
            var rows = $.map(data, function(value, index) {
                return "<tr><td>" + value.month + "</td><td style=\"text-align: center;\">" + value.local + " %</td><td style=\"text-align: center;\">" + value.national + " %</td></tr>";
            });
            $('.table tbody').html(rows.join(''));
        });

        $(".commoditys").select2({
            placeholder: "Pilih Komoditi",
            ajax: {
            url: '{{url('/')}}/api/commoditys',
            type: "get",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                search: params.term
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
            cache: true
            }
        });
        $(".year").select2({
            placeholder: "Pilih Tahun",
            ajax: {
            url: '{{url('/')}}/api/year',
            type: "get",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                search: params.term
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
            cache: true
            }
        });
        $('.year').on('select2:select', function (e) {
            var year = e.params.data;
            $.getJSON( "{{url('/')}}/api/inflation?year="+year.id, function( data ) {
                var rows = $.map(data, function(value, index) {
                    return "{month:'" + value.month + "',local:" + value.local + ", national:" + value.national + "}";
                });
                var data_array = rows.join(',');
                // console.log(rows.join(','));
                $("#morris-bar-chart").empty();

                morbar = Morris.Bar({
                    element: 'morris-bar-chart',
                    // data:data,
                    xkey: 'month',
                    ykeys: ['local', 'national'],
                    labels: ['Inflasi Provinsi (%)', 'Inflasi Nasioanal (%)'],
                    hideHover: 'auto',
                    resize: true,
                    barColors: ['#1AAA35', '#cacaca'],
                });
                morbar.setData(data);

            });
            // console.log(year.id);
            $.getJSON( "{{url('/')}}/api/inflation?year="+year.id, function( data ) {
                var rows = $.map(data, function(value, index) {
                    return "<tr><td>" + value.month + "</td><td style=\"text-align: center;\">" + value.local + " %</td><td style=\"text-align: center;\">" + value.national + " %</td></tr>";
                });
                $('.table tbody').html(rows.join(''));
            });
        });
        $('.btn-go').click(function(){
            console.log($(".commoditys"). val());
            console.log($(".districts"). val());
            console.log($(".date-picker"). val());
        });
        $(".date-picker").datetimepicker({
            minView: 2,
            format: 'yyyy-mm-dd',
            autoclose: true,
        });
        $(".date-picker-month").datepicker({
            startView: 1,
            minViewMode: 1,
            autoclose: true
        });
    });
    // $('table').tableMobilize();
</script>
</body>
</html>
