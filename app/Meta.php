<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meta extends Model
{
    protected $primaryKey = 'meta_id';
    protected $table = "metas";
}
