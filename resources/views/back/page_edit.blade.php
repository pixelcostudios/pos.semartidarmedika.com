@extends('layouts.post.post_edit')

@section('content')
@include('layouts.navtop')
<div class="row wrapper border-bottom white-bg page-heading">
<div class="col-lg-10">
<h2>Page</h2>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="{{ url('home') }}">Home</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ url('page') }}">Page</a>
    </li>
    <li class="breadcrumb-item active">
        <strong>All Page</strong>
    </li>
</ol>
</div>
<div class="col-lg-2">

</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    @foreach($post_edit as $p)
            <form name="myform" method="post" action="{{ url('page/update') }}/{{$p->post_id}}" enctype="multipart/form-data" class="form" data-async>
                @csrf
				<div class="row">
					<div class="col-lg-9 padding-none">
						<div class="ibox ">
							<div class="ibox-title">
								<h5>Page Editor</h5>
								<a href="{{ url('page/add') }}" class="btn btn-default pull-left">Add New Page</a>
								<button type="submit" name="Pixel_Save" class="btn btn-primary float-right">Save Page</button>
							</div>
							<div class="ibox-content">
								<div class="form-group">
									<label for="title">Title ID </label>
									<div class="input-group">
										<input type="text" class="form-control" id="title" name="post_title" placeholder="Title ID" required="required"
										 value="{{$p->post_title}}">
										<span class="input-group-addon tooltip-demo" title="Preview Link ID"><a target="_blank" href="{{url('/')}}/{{$p->post_slug}}"><i class="fa fa-search"
												 data-toggle="tooltip" data-placement="top" title="Preview Link ID"></i></a></span>
                                    </div>
								</div>
								<div class="row">
									<div class="form-group col-lg-4 col-sm-4 col-xs-12">
										<label for="status">Parent</label>
										<select name="post_up" class="form-control select2">
											<option value="0" {{ ($p->post_up == 0) ? 'selected' : '' }}>Select Parent / No Parent</option>
											@foreach($post_list as $p_row)
											<option value="{{$p_row['post_id']}}" {{ ($p->post_up == $p_row['post_id']) ? 'selected' : '' }}>{{$p_row['post_title']}} [ID#{{$p_row['post_id']}}]
											</option>
											@endforeach
										</select>
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <label for="title">Price</label>
                                        <input type="text" class="form-control" name="post_price" placeholder="Price" value="{{$p->post_price}}">
                                    </div>
                                    <div class="form-group col-lg-4 col-sm-4 col-xs-12">
										<label for="status">Show Gallery</label>
										<select name="post_thumb" class="form-control">
											<option value="1" {{ ($p->post_thumb == 1) ? 'selected' : '' }}>Yes</option>
											<option value="0" {{ ($p->post_thumb == 0) ? 'selected' : '' }}>No</option>
										</select>
									</div>
                                </div>
                                <div class="row">
									<div class="form-group col-lg-6 col-sm-6 col-xs-12">
										<label for="status">Status</label>
										<select name="status" class="form-control">
											<option value="1" {{ ($p->status == 1) ? 'selected' : '' }}>Publish</option>
											<option value="0" {{ ($p->status == 0) ? 'selected' : '' }}>Draft</option>
										</select>
									</div>
									<div class="form-group col-lg-6 col-sm-6 col-xs-12">
										<label for="date">Date</label>
										<div class="input-group">
											<input name="date" class="form-control date-picker" type="text" value="{{$p->date}}" data-date-format="yyyy-mm-dd hh:mm">
											<span class="input-group-addon tooltip-demo">
												<i class="fa fa-calendar" data-toggle="tooltip" data-placement="top" title="Calendar"></i>
											</span>
										</div>
									</div>
								</div>

								<div class="form-group clear">
									<label for="content">Content ID</label>
									<textarea id="content_id" name="post_content">{{$p->post_content}}</textarea>
								</div>

								<div class="form-group clear">
									<label for="summary">Summary ID</label>
									<textarea id="summary_id" name="post_summary">{{$p->post_summary}}</textarea>
								</div>

								<div class="form-group">
									<label for="title">SEO Key ID</label>
									<input type="text" class="form-control" name="post_key" value="{{$p->post_key}}" placeholder="SEO Key ID" value="">
								</div>
								<div class="form-group">
									<label for="title">SEO Desc ID</label>
									<textarea type="text" class="form-control" name="post_desc" placeholder="SEO Desc ID">{{$p->post_desc}}</textarea>
								</div>
								<div class="row">
									<div class="form-group col-lg-6 col-sm-6 col-xs-12 padding-left-0 padding-xs-none">
										<label for="Comment">Comment</label>
										<select name="post_comment" class="form-control">
											<option value="1" {{ ($p->post_comment == 1) ? 'selected' : '' }}>Yes</option>
											<option value="0" {{ ($p->post_comment == 0) ? 'selected' : '' }}>No</option>
										</select>
									</div>
									<div class="form-group col-lg-6 col-sm-6 col-xs-12 padding-right-0 padding-xs-none">
										<label for="Username">Username</label>
										<select name="user_id" class="form-control">
											@foreach($user_list as $p_row)
											<option value="{{$p_row->id}}">{{$p_row->first_name}} {{$p_row->last_name}}
											</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="clear"></div>
							</div>
						</div>
					</div>
					<div class="col-lg-3 padding-right-0">
						<div class="bar-title white-bg">
							<div class="tabs-container">
								<div class="tabs-left">
									<ul class="nav nav-tabs tooltip-demo">
										<li><a data-toggle="tab" class="active show" href="#tab_images" aria-expanded="true"> <i class="fa fa-picture-o"
												 data-toggle="tooltip" data-placement="top" title="Images"></i></a></li>
										<li class=""><a data-toggle="tab" href="#tab_icon" aria-expanded="false"><i class="fa fa-dot-circle-o"
												 data-toggle="tooltip" data-placement="top" title="Icon"></i></a></li>
										<li class=""><a data-toggle="tab" href="#tab_thumb" aria-expanded="false"><i class="fa fa fa-camera"
												 data-toggle="tooltip" data-placement="top" title="Thumb"></i></a></li>
										<li class=""><a data-toggle="tab" href="#tab_background" aria-expanded="false"><i class="fa fa-th-large"
												 data-toggle="tooltip" data-placement="top" title="Background"></i></a></li>
										<li class=""><a data-toggle="tab" href="#tab_pdf" aria-expanded="false"><i class="fa fa-bookmark" data-toggle="tooltip"
												 data-placement="top" title="PDF"></i></a></li>
										<li class=""><a data-toggle="tab" href="#tab_files" aria-expanded="false"><i class="fa fa-file" data-toggle="tooltip"
												 data-placement="top" title="Files"></i></a></li>
									</ul>
									<div class="tab-content ">
										<div id="tab_images" class="tab-pane active">
											<div class="panel-body">
                                                <div class="input-file"><input type="file" name="upload_photo[]" multiple="multiple" id="input_images"></div>
                                                @if (count($images_list) > 0)
												<ul class="gallery margin-top-10">
													<li class="check">
														<label class="float-right">
															<input type="checkbox" class="colored-green check_images">
															<span class="text"></span>
														</label>
														<span>Select All</span>
													</li>
													@foreach($images_list as $p_images)
													<li class="img">
														<img src="../../thumb/crop/100/100/{{$p_images->post_slug}}.{{$p_images->post_mime_type}}"
														 width="100%" height="100" alt="...">
														<label class="float-right">
															<input type="checkbox" class="colored-green images-check" name="post-check[]" value="{{$p_images->post_id}},{{$p_images->post_slug}}.{{$p_images->post_mime_type}},&lt;div class=&quot;caption&quot;&gt;&lt;img src=&quot;{{url('/')}}/upload/{{$p_images->post_slug}}.{{$p_images->post_mime_type}}&quot; width=&quot;100%&quot; alt=&quot;{{$p_images->post_title}} : {{$p->post_title}}&quot; title=&quot;{{$p_images->post_title}} : {{$p->post_title}}&quot; /&gt;{{$p_images->post_title}}&lt;/div&gt;&lt;/br&gt;">
															<span class="text"></span>
														</label>
													</li>
													@endforeach
													<li class="button">
														<div class="btn-group float-left">
															<a class="btn btn-default btn-small dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
																Insert <i class="fa fa-angle-down"></i>
															</a>
															<ul class="dropdown-menu">
																<li><a href="#" onclick="AddImages_id();">Insert Images ID</a></li>
															</ul>
														</div>
														<button type="submit" class="btn btn-default btn-small float-right" value="deleted" name="Pixel_D_Images">Delete</button>
													</li>
												</ul>
												@endif
											</div>
										</div>
										<div id="tab_icon" class="tab-pane">
											<div class="panel-body">
                                                <div class="input-file"><input type="file" name="upload_icon[]" multiple="multiple" id="input_icon"></div>
                                                @if (count($icon_list) > 0)
												<ul class="gallery margin-top-10">
													<li class="check">
														<label class="float-right">
															<input type="checkbox" class="colored-green check_images">
															<span class="text"></span>
														</label>
														<span>Select All</span>
													</li>
													@foreach($icon_list as $p_images)
													<li class="img">
														<img src="../../thumb/crop/100/100/{{$p_images->post_slug}}.{{$p_images->post_mime_type}}"
														 width="100%" height="100" alt="...">
														<label class="float-right">
															<input type="checkbox" class="colored-green images-check" name="post-check[]" value="{{$p_images->post_id}},{{$p_images->post_slug}}.{{$p_images->post_mime_type}},&lt;div class=&quot;caption&quot;&gt;&lt;img src=&quot;{{url('/')}}/upload/{{$p_images->post_slug}}.{{$p_images->post_mime_type}}&quot; width=&quot;100%&quot; alt=&quot;{{$p_images->post_title}} : {{$p->post_title}}&quot; title=&quot;{{$p_images->post_title}} : {{$p->post_title}}&quot; /&gt;{{$p_images->post_title}}&lt;/div&gt;&lt;/br&gt;">
															<span class="text"></span>
														</label>
													</li>
													@endforeach
													<li class="button">
														<div class="btn-group float-left">
															<a class="btn btn-default btn-small dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
																Insert <i class="fa fa-angle-down"></i>
															</a>
															<ul class="dropdown-menu">
																<li><a href="#" onclick="AddImages_id();">Insert Images ID</a></li>
															</ul>
														</div>
														<button type="submit" class="btn btn-default btn-small float-right" value="deleted" name="Pixel_D_Images">Delete</button>
													</li>
												</ul>
												@endif
											</div>
										</div>
										<div id="tab_thumb" class="tab-pane">
											<div class="panel-body">
                                                <div class="input-file"><input type="file" name="upload_thumb[]" multiple="multiple" id="input_thumb"></div>
                                                @if (count($thumb_list) > 0)
												<ul class="gallery margin-top-10">
													<li class="check">
														<label class="float-right">
															<input type="checkbox" class="colored-green check_images">
															<span class="text"></span>
														</label>
														<span>Select All</span>
													</li>
													@foreach($thumb_list as $p_images)
													<li class="img">
														<img src="../../thumb/crop/100/100/{{$p_images->post_slug}}.{{$p_images->post_mime_type}}"
														 width="100%" height="100" alt="...">
														<label class="float-right">
															<input type="checkbox" class="colored-green images-check" name="post-check[]" value="{{$p_images->post_id}},{{$p_images->post_slug}}.{{$p_images->post_mime_type}},&lt;div class=&quot;caption&quot;&gt;&lt;img src=&quot;{{url('/')}}/upload/{{$p_images->post_slug}}.{{$p_images->post_mime_type}}&quot; width=&quot;100%&quot; alt=&quot;{{$p_images->post_title}} : {{$p->post_title}}&quot; title=&quot;{{$p_images->post_title}} : {{$p->post_title}}&quot; /&gt;{{$p_images->post_title}}&lt;/div&gt;&lt;/br&gt;">
															<span class="text"></span>
														</label>
													</li>
													@endforeach
													<li class="button">
														<div class="btn-group float-left">
															<a class="btn btn-default btn-small dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
																Insert <i class="fa fa-angle-down"></i>
															</a>
															<ul class="dropdown-menu">
																<li><a href="#" onclick="AddImages_id();">Insert Images ID</a></li>
															</ul>
														</div>
														<button type="submit" class="btn btn-default btn-small float-right" value="deleted" name="Pixel_D_Images">Delete</button>
													</li>
												</ul>
												@endif
											</div>
										</div>
										<div id="tab_background" class="tab-pane">
											<div class="panel-body">
                                                <div class="input-file"><input type="file" name="upload_background[]" multiple="multiple" id="input_background"></div>
                                                @if (count($background_list) > 0)
												<ul class="gallery margin-top-10">
													<li class="check">
														<label class="float-right">
															<input type="checkbox" class="colored-green check_images">
															<span class="text"></span>
														</label>
														<span>Select All</span>
													</li>
													@foreach($background_list as $p_images)
													<li class="img">
														<img src="../../thumb/crop/100/100/{{$p_images->post_slug}}.{{$p_images->post_mime_type}}"
														 width="100%" height="100" alt="...">
														<label class="float-right">
															<input type="checkbox" class="colored-green images-check" name="post-check[]" value="{{$p_images->post_id}},{{$p_images->post_slug}}.{{$p_images->post_mime_type}},&lt;div class=&quot;caption&quot;&gt;&lt;img src=&quot;{{url('/')}}/upload/{{$p_images->post_slug}}.{{$p_images->post_mime_type}}&quot; width=&quot;100%&quot; alt=&quot;{{$p_images->post_title}} : {{$p->post_title}}&quot; title=&quot;{{$p_images->post_title}} : {{$p->post_title}}&quot; /&gt;{{$p_images->post_title}}&lt;/div&gt;&lt;/br&gt;">
															<span class="text"></span>
														</label>
													</li>
													@endforeach
													<li class="button">
														<div class="btn-group float-left">
															<a class="btn btn-default btn-small dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
																Insert <i class="fa fa-angle-down"></i>
															</a>
															<ul class="dropdown-menu">
																<li><a href="#" onclick="AddImages_id();">Insert Images ID</a></li>
															</ul>
														</div>
														<button type="submit" class="btn btn-default btn-small float-right" value="deleted" name="Pixel_D_Images">Delete</button>
													</li>
												</ul>
												@endif
											</div>
										</div>
										<div id="tab_pdf" class="tab-pane">
											<div class="panel-body">
                                                <div class="input-file"><input type="file" name="upload_pdf[]" multiple="multiple" id="input_pdf"></div>
                                                @if (count($pdf_list) > 0)
												<ul class="file_type margin-top-10">
													<li class="check">
														<label class="float-right">
															<input type="checkbox" class="colored-green check_pdf">
															<span class="text"></span>
														</label>
														<span>Select All</span>
													</li>
													@foreach($pdf_list as $p_images)
													<li class="line">
														<i class="navbar-right">
															<label class="float-right">
																<input type="checkbox" class="colored-green" name="pdf-check[]" value="{{$p_images->post_id}},{{$p_images->post_slug}}.{{$p_images->post_mime_type}},<li><a href=&quot;../../upload/files/{{$p_images->post_slug}}.{{$p_images->post_mime_type}}&quot;>{{$p_images->post_title}}</a></li>">
																<span class="text"></span>
															</label>
														</i>
														<i class="ft ft-pdf"></i>
														{{$p_images->post_title}}
													</li>
													@endforeach
													<li class="button">
														<div class="btn-group float-left">
															<a class="btn btn-default btn-small dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
																Insert <i class="fa fa-angle-down"></i>
															</a>
															<ul class="dropdown-menu">
																<li><a href="#" onclick="AddPDF_id();">Insert Files ID</a></li>
															</ul>
														</div>
														<button type="submit" class="btn btn-default btn-small float-right" value="deleted" name="Pixel_D_PDF">Delete</button>
													</li>
												</ul>
												@endif
											</div>
										</div>
										<div id="tab_files" class="tab-pane">
											<div class="panel-body">
                                                <div class="input-file"><input type="file" name="upload_files[]" multiple="multiple" id="input_files"></div>
                                                @if (count($files_list) > 0)
												<ul class="file_type margin-top-10">
													<li class="check">
														<label class="float-right">
															<input type="checkbox" class="colored-green check_files">
															<span class="text"></span>
														</label>
														<span>Select All</span>
													</li>
													@foreach($files_list as $p_images)
													<li class="line">
	s													<i class="navbar-right">
															<label class="float-right">
																<input type="checkbox" class="colored-green" name="files-check[]" value="{{$p_images->post_id}},{{$p_images->post_slug}}.{{$p_images->post_mime_type}},<li><a href=&quot;../../upload/files/{{$p_images->post_slug}}.{{$p_images->post_mime_type}}&quot;>{{$p_images->post_title}}</a></li>">
																<span class="text"></span>
															</label>
														</i>
														<i class="ft ft-{{$p_images->post_mime_type}}"></i>
														{{$p_images->post_title}}
													</li>
													@endforeach
													<li class="button">
														<div class="btn-group float-left">
															<a class="btn btn-default btn-small dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
																Insert <i class="fa fa-angle-down"></i>
															</a>
															<ul class="dropdown-menu">
																<li><a href="#" onclick="AddFiles_id();">Insert Files ID</a></li>
															</ul>
														</div>
														<button type="submit" class="btn btn-default btn-small float-right" value="deleted" name="Pixel_D_Files">Delete</button>
													</li>
												</ul>
												@endif
											</div>
										</div>
									</div>

								</div>

							</div>
							<!--End Tab-->

							<div class="clear"></div>
						</div>
					</div>
                </div>

            </form>
            @endforeach
		</div>
<div class="footer">
<div class="float-right">
<strong>
    </strong>
</div>
<div>
<strong>Copyright</strong> {{$copyright}}
</div>
</div>
@endsection
