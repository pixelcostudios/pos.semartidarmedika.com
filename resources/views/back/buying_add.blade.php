@extends('layouts.post.buying')

@section('content')
@include('layouts.menu')
<div class="row wrapper border-bottom white-bg page-heading">
<div class="col-lg-10">
<h2>Pembelian</h2>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="{{ url('home') }}">Home</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ url('buying') }}">Pembelian</a>
    </li>
    <li class="breadcrumb-item active">
        <strong>All Pembelian</strong>
    </li>
</ol>
</div>
<div class="col-lg-2">

</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
            <form name="myform" method="post" action="{{ url('buying/create') }}" enctype="multipart/form-data" class="form" data-async>
                @csrf
				<div class="row">
					<div class="col-lg-12 padding-none">
						<div class="ibox ">
							<div class="ibox-title">
								<h5>Pembelian Editor</h5>
								<button type="submit" name="Pixel_Save" class="btn btn-primary float-right">Simpan</button>
							</div>
							<div class="ibox-content">

								<div class="row">
                                     <div class="form-group col-lg-6 col-sm-6">
                                        <label for="title">No Faktur </label>
                                        <input type="text" class="form-control" name="beli_nofak" value="">
                                    </div>
									<div class="form-group col-lg-6 col-sm-6 col-xs-12 clearfix">
										<label for="category">Supplier</label><br>
										<select id="select2" class="form-control margin-bottom-10 select2" name="beli_suplier_id">
											@foreach($supplier_list as $p_cat)
											<option value="{{$p_cat['suplier_id']}}">{{$p_cat['suplier_nama']}}</option>
											@endforeach
										</select>
									</div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        <label for="title">Tanggal Beli </label>
                                        <div class="input-group">
											<input name="beli_tanggal" class="form-control date-picker-day" type="text" data-date-format="yyyy-mm-dd" autocomplete="off" required>
											<span class="input-group-addon tooltip-demo">
												<i class="fa fa-calendar" data-toggle="tooltip" data-placement="top" title="Calendar"></i>
											</span>
										</div>
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label for="title">Tempo</label>
                                        <div class="input-group">
											<input name="beli_tempo" class="form-control date-picker-day" type="text" data-date-format="yyyy-mm-dd" autocomplete="off" required>
											<span class="input-group-addon tooltip-demo">
												<i class="fa fa-calendar" data-toggle="tooltip" data-placement="top" title="Calendar"></i>
											</span>
										</div>
                                    </div>
                                </div>


                                <br>
                                <div class="relative input_fields_wrap">
                                    <div class="text-center margin-buttton-10">
                                            <button class="btn btn-primary add_product_price add_field_button"><i class="fa fa-plus"></i></button>
                                            <br><br>
                                    </div>
                                </div>

								<div class="clear"></div>
							</div>
						</div>
					</div>
                </div>

            </form>
		</div>
<div class="footer">
<div class="float-right">
<strong>
    </strong>
</div>
<div>
<strong>Copyright</strong> {{$copyright}}
</div>
</div>
@endsection
