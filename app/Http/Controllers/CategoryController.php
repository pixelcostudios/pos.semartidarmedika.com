<?php

namespace App\Http\Controllers;

use DB;
use Image;

use App\Post;
use App\Categories;
use App\Setting;
use App\Helpers\Base;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input as input;

use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        $cat_slug = str_replace('.html', '', request()->segment(2));
        $row_cat = Categories::where('cat_type', 'post')->where('cat_slug',$cat_slug)->orderBy('cat_date', 'DESC')->skip(0)->take(1)->get();
        $post_list = Post::leftJoin('metas', 'posts.post_id', '=', 'metas.meta_source')->where('meta_dest', ($row_cat) ? $row_cat[0]->cat_id : 0)->where('posts.post_type', 'post')->orderBy('date', 'DESC')->paginate(9);

        $recent_list    = Post::leftJoin('metas', 'posts.post_id', '=', 'metas.meta_source')->whereIn('meta_dest', array('3,4'))->where('posts.post_type', 'post')->orderBy('date', 'DESC')->limit(5)->get();
        $category_list  = Categories::where('cats.cat_type', 'post')->where('cats.cat_up', 0)->orderBy('cat_date', 'DESC')->get();
        $setting_list   = Setting::where('setting_type', 'meta')->where('setting_status', '1')->get();
        $online_list    = Post::where('post_up', '=', '42')->get();
        $offline_list   = Post::where('post_up', '=', '43')->get();

        $facebook   = Setting::where('setting_name', '=', 'facebook')->limit('1')->value('setting_value');
        $youtube   = Setting::where('setting_name', '=', 'youtube')->limit('1')->value('setting_value');
        $instagram   = Setting::where('setting_name', '=', 'instagram')->limit('1')->value('setting_value');

        $menu = display_menu(5);

        foreach ($setting_list as $key => $value) {
            $setting[] = $value->setting_value;
        }
        $meta_title     = ($setting) ? $setting[2] : '';
        $meta_key       = ($setting) ? $setting[1] : '';
        $meta_desc      = ($setting) ? $setting[0] : '';

        $data_array = [
            'post_list'     => $post_list,
            'recent_list'   => $recent_list,
            'title'         => ($row_cat) ? $row_cat[0]->cat_title : '',
            'menu'          => $menu,
            'description'   => ($row_cat) ? $row_cat[0]->cat_summary : '',
            'keyword'       => ($row_cat) ? $row_cat[0]->cat_key : '',
            'online_list'   => $online_list,
            'offline_list'  => $offline_list,
            'category_list' => $category_list,
            'facebook'      => $facebook,
            'youtube'       => $youtube,
            'instagram'     => $instagram,
            'setting'       => array('meta_title' => $meta_title, 'meta_keyword' => $meta_key, 'meta_desc' => $meta_desc),
            'url'           => request()->segment(1)
        ];

        return view('front/category', $data_array);
    }
    // public function news()
    // {
    //     $cat_slug = str_replace('.html', '', request()->segment(2));
    //     $post_list = Post::leftJoin('metas', 'posts.post_id', '=', 'metas.meta_source')->where('posts.post_type', 'post')->orderBy('date', 'DESC')->paginate(10);
    //     $popular_list = Post::leftJoin('metas', 'posts.post_id', '=', 'metas.meta_source')->where('posts.post_type', 'post')->orderBy('date', 'DESC')->limit(5)->get();
    //     $headline_list = Post::leftJoin('metas', 'posts.post_id', '=', 'metas.meta_source')->where('posts.post_type', 'post')->orderBy('date', 'DESC')->limit(1)->get();
    //     $category_list = Categories::where('cats.cat_type', 'post')->where('cats.cat_up', 0)->orderBy('cat_date', 'DESC')->get();
    //     return view('front/category_news', ['post_list' => $post_list, 'popular_list' => $popular_list, 'headline_list' => $headline_list, 'category_list' => $category_list]);
    // }
}
