<!-- most top information -->
<div class="header-wrapper">
    <header id="top">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12  hidden-xs">
                    <form method="GET" action="{{url('/')}}/search">
                        @csrf
                    <div id="custom-search-input">
                        <div class="input-group"> <span class="input-group-btn">
                                    <button class="btn" type="button"> <span class="icon-magnifier"></span> </button>
                            </span>
                            <input type="text" class="search-query form-control" name="query" placeholder="Search" />
                        </div>
                    </div>
                    </form>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
                    <ul class="pull-right header-right">
                        <li> <a href="tel:082183300904"><span class="icon-telephone"></span><span>082183300904</span></a></li>
                        <li> <img src="{{url('/')}}/themes/modern/images/separator.png" alt="separator" class="img-responsive" /> </li>
                        <li> <a href="mailto:info@company.com"><span class="icon-envelope"></span><span>lkpsariprabumulih@gmail.com</span></a> </li>
                    </ul>
                </div>
            </div>
        </div>
    </header>
    <!-- end most top information -->
    <!--navigation-->
    <nav id="navbar-main" class="navbar main-menu">
        <div class="container">
            <!--Brand and toggle get grouped for better mobile display-->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                <a class="navbar-brand" href="{{url('/')}}"><img src="{{url('/')}}/themes/modern/images/logo3.png" width="190" alt="Brand" class="img-responsive" /></a>
            </div>
            <!--Collect the nav links, and other content for toggling-->
            <div class="collapse navbar-collapse" id="navbar-collapse-1">

                {!!$menu!!}
<!--
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown"> <a href="index.html#" data-toggle="dropdown" class="dropdown-toggle">Tentang Kami</a>
                        <ul class="dropdown-menu">
                            <li><a href="logo-and-branding.html">Profil Lembaga</a></li>
                            <li><a href="website-development.html" class="b-clor">Visi & Misi</a></li>
                        </ul>
                    </li>
                    <li class="dropdown"> <a href="index.html#" data-toggle="dropdown" class="dropdown-toggle">Program</a>
                        <ul class="dropdown-menu">
                            <li><a href="blog.html">Kegiatan</a></li>
                            <li><a href="blog-version-2.html" class="b-clor">Lowongan</a></li>
                            <li><a href="blog-version-2.html" class="b-clor">Alumni</a></li>
                        </ul>
                    </li>
                    <li class="dropdown"> <a href="index.html#" data-toggle="dropdown" class="dropdown-toggle">Informasi</a>
                        <ul class="dropdown-menu">
                            <li><a href="blog.html">Kegiatan</a></li>
                            <li><a href="blog-version-2.html" class="b-clor">Lowongan</a></li>
                            <li><a href="blog-version-2.html" class="b-clor">Alumni</a></li>
                        </ul>
                    </li>
                    <li><a href="contact.html">Blog</a></li>
                    <li><a href="contact.html">Hubungi Kami</a></li>
                    <li class="btn btn-fill" data-toggle="modal" data-target="#getAQuoteModal"><a href="index.html#">GET A QUOTE<span class="icon-chevron-right"></span></a></li>
                </ul>-->
            </div>
        </div>
    </nav>
</div>
<!--end navigation-->
