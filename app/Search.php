<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Search extends Model
{
    protected $primaryKey = 'post_id';
    protected $table = "posts";
}
