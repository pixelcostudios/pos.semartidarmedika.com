<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Post;

class SlugController extends Controller
{
    public function createSlug($title, $id = 0)
    {
        // Normalize the title
        $slug = str_slug($title);

        // Get any that could possibly be related.
        // This cuts the queries down by doing it once.
        $allSlugs = $this->getRelatedSlugs($slug, $id);

        // If we haven't used it before then we are all good.
        if (!$allSlugs->contains('post_slug', $slug)) {
            return $slug;
        }

        // Just append numbers like a savage until we find not used.
        for ($i = 1; $i <= 10; $i++) {
            $newSlug = $slug . '-' . $i;
            if (!$allSlugs->contains('post_slug', $newSlug)) {
                return $newSlug;
            }
        }

        throw new \Exception('Can not create a unique slug');
    }

    protected function getRelatedSlugs($slug, $id = 0)
    {
        return Post::select('post_slug')->where('post_slug', 'like', $slug . '%')
            ->where('post_id', '<>', $id)
            ->get();
    }
    // public function PostSlug($slug,$id)
    // {
    //     // get the slug of the latest created post
    //     $max = Post::where('post_slug', str_slug($slug))->latest('post_id')->skip(0)->first();
    //     $max2 = Post::where('post_slug', str_slug($slug))->latest('post_id')->skip(1)->value('post_slug');

    //     if($max['post_id']==$id)
    //     {
    //         return str_slug($slug);
    //     }
    //     else {
    //         if($max2)
    //         {
    //             if (is_numeric($max['post_slug'][-1])) {
    //                 return preg_replace_callback('/(\d+)$/', function ($mathces) {
    //                     return $mathces[1] + 1;
    //                 }, $max['post_slug']);
    //             }

    //             return "{$slug}-2";
    //         }
    //         else {
    //             return str_slug($slug);
    //         }
    //     }
    //     // if (is_numeric($max[-1])) {
    //     //     return preg_replace_callback('/(\d+)$/', function ($mathces) {
    //     //         return $mathces[1] + 1;
    //     //     }, $max);
    //     // }

    //     // return $max;//"{$slug}-2";
    // }
    public function PostSlug($value,$id=null)
    {
        if($id==null OR $id =='' OR $id == 0)
        {
            $slug = str_slug($value, '-');
            $count = count(Post::whereRaw("(post_slug = '$slug' or post_slug LIKE '$slug-%')")->get());//Post::where('post_slug', str_slug($value, '-'))->count();

            if($count>1)
            {
                return str_slug($value, '-') . '-' . $count;
            }
            else
            {
                return str_slug($value, '-');
            }
        }
        else
        {
            $slug = str_slug($value, '-');
            // $id = $this->attributes['id'];
            $slugCount = count(Post::whereRaw("(post_slug = '$slug' or post_slug LIKE '$slug-%') and post_id != $id")->get());
            if($slugCount==1)
            {
                return str_slug($value, '-');
            }
            else {
                return $slugCount == 0 ? $slug : $slug . '-' . $slugCount;
            }
        }
    }
}
