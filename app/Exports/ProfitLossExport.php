<?php

namespace App\Exports;

use DB;
use Image;

use App\Selling;
use App\Outlet;
use App\Helpers\Base;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input as input;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ProfitLossExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        if (Input::get('start_date') != '') {
            $start_date = Input::get('start_date');
        } else {
            $start_date = date('Y-m');
        }
        $jual_keterangan = ['eceran', 'grosir', 'bon'];
        $post_list = Selling::join('sell_details', 'sell_details.jual_id', '=', 'sells.jual_id')
        ->whereIn('jual_keterangan', $jual_keterangan)
            ->where('jual_tanggal', 'like',  $start_date . '%')
            ->orderBy('jual_tanggal', 'DESC')->get();

        $i = 0;
        $data_array = array();
        foreach ($post_list  as $value  => $p) {
            $total[] = (($p->d_jual_barang_harjul * $p->d_jual_qty) - ($p->d_jual_barang_harpok * $p->d_jual_qty));
            $i++;
            $data_array[] = array(
                'r_no' => $value + 1,
                'jual_tanggal' => $p->jual_tanggal,
                'd_jual_barang_nama'      => $p->d_jual_barang_nama,
                'd_jual_barang_satuan'=>$p->d_jual_barang_satuan,
                'd_jual_barang_harpok'=>$p->d_jual_barang_harpok,
                'd_jual_barang_harjul'=>$p->d_jual_barang_harjul,
                'd_jual_barang_harjuls'=>$p->d_jual_barang_harjul-$p->d_jual_barang_harpok,
                'd_jual_qty'=>$p->d_jual_qty,
                'd_jual_diskon'=>$p->d_jual_diskon,
                'd_jual_total'=>(($p->d_jual_barang_harjul*$p->d_jual_qty)-($p->d_jual_barang_harpok*$p->d_jual_qty))
            );
        }

        return collect($data_array);//Target::all();
    }
    public function headings(): array
    {
        return [
            'No',
            'Tanggal',
            'Nama Barang',
            'Satuan',
            'Harga Pokok',
            'Harga Jual',
            'Keuntungan Per Unit',
            'Item Terjual',
            'Diskon',
            'Untung Bersih'
        ];
    }
}
