@extends('layouts.front.post_detail')
@section('content')
@include('layouts.front.header_post')
<!-- ++++ Most Bold Title ++++ -->
@foreach ($post_list as $key)
<section class="banner  o-hidden banner-inner about-banner">
    <div class="container">
        <!--banner text-->
        <div class="banner-txt">
            <h1>{{$key->post_title}}</h1>
            <p class="semi-bold">{!!$key->post_summary!!}</p>
        <!--end banner text-->
    </div>
</section>

<!-- ++++ Most Bold Title ++++ -->
<!-- ++++ blog standard content ++++ -->
<section class="bg-white o-hidden common-form-section contact-form-wrapper more-about" id="more-about">
    <div class="container relative">
        <div class="row">
            <!-- Content -->
            <div class="col-sm-10 margin-auto">
                <div class="row margin-top-15">
                @foreach (App\Page::where('post_type', 'pages')->where([['post_up', '=', $key->post_id]])->get() as $key)
                <!-- features box -->
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="box-green-border">
                            @foreach (App\Page::where('post_type', 'images')->where([['post_up', '=', $key->post_id]])->skip(0)->take(1)->get() as $p2)
                            <img src="{{url('/')}}/thumb/crop/350/300/{{$p2->post_slug}}.{{$p2->post_mime_type}}"
                                alt="{{$key->post_title}}" class="img-responsive" width="100%">
                            @endforeach
                            <h3>{{$key->post_title}}</h3>
                        {{strip_tags($key->post_summary)}}
                        <!-- End of .service-overlay -->
                    </div>
                </div>
                <!--end features box -->
                @endforeach
            </div>
            </div>
            <!-- End Content -->

        </div>
    </div>
</section>
@endforeach
@include('layouts.front.footer')
@endsection
