<?php

namespace App\Http\Controllers;

use DB;

use App\Categories;
use App\Item;
use App\Helpers\Base;
use App\Exports\BarcodeExport;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input as input;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;

class BarcodeController extends Controller
{
    public function category()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        if (Input::get('show') != '') {
            $show = Input::get('show');
        } else {
            $show = 10;
        }
        $post_list = Categories::where('cats.cat_type', 'product')->where('cats.cat_up', 0)->orderBy('cat_date', 'DESC')->paginate($show);

        return view('back/barcode_category', ['categories_list' => $post_list, 'show' => $show, 'copyright' => copyright()]);
    }
    public function category_barcode($id)
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        $post_list = Item::where('barang_kategori_id','=', $id)->orderBy('barang_id', 'DESC')->get();
        $category = Categories::where('cat_id', $id)->first()->value('cat_title');
        return view('back/barcode_category_barcode', ['post_list' => $post_list, 'category'=> $category, 'copyright' => copyright()]);
    }
    public function all()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        if (Input::get('show') != '') {
            $show = Input::get('show');
        } else {
            $show = 10;
        }

        $post_list = Item::join('cats', 'cats.cat_id', '=', 'items.barang_kategori_id')->orderby('barang_id', 'ASC')->paginate($show);

        return view('back/barcode_all', ['post_list' => $post_list, 'show' => $show, 'copyright' => copyright()]);
    }
    public function all_download()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        // ob_clean();
        // flush();
        // $file = "somefile.xlsx";
        // header('Content-Description: File Transfer');
        // header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        // header('Content-Disposition: attachment; filename=' . basename($file));
        // header('Content-Transfer-Encoding: binary');
        // header('Expires: 0');
        // header('Cache-Control: must-revalidate');
        // header('Pragma: public');
        // header('Content-Length: ' . filesize($file));
        // readfile($file);
        // readfile('barcode_list.xlsx');

        // $post_list = Item::join('cats', 'cats.cat_id', '=', 'items.barang_kategori_id')->orderby('barang_id', 'ASC')->get();

        return Excel::download(new BarcodeExport, 'Barcode.xlsx');

        // return view('back/barcode_all_download', ['post_list' => $post_list, 'copyright' => copyright()]);
    }
}
