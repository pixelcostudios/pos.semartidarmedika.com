<div class="cd-signin-modal js-signin-modal">
<!-- this is the entire modal form, including the background -->
<div class="cd-signin-modal__container">
    <!-- this is the container wrapper -->
    <ul class="cd-signin-modal__switcher js-signin-modal-switcher js-signin-modal-trigger">
        <li><a href="#0" data-signin="login" data-type="login">Sign in</a></li>
        <li><a href="#0" data-signin="signup" data-type="signup">New account</a></li>
    </ul>

    <div class="cd-signin-modal__block js-signin-modal-block" data-type="login">
        <!-- log in form -->
        <form class="cd-signin-modal__form" method="POST" action="{{ url('post-login') }}">
        @csrf
            <p class="cd-signin-modal__fieldset">
                <label
                    class="cd-signin-modal__label cd-signin-modal__label--email cd-signin-modal__label--image-replace"
                    for="signin-email">Username / E-mail</label>
                <input
                    class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border  {{ $errors->has('username') || $errors->has('email') ? ' is-invalid' : '' }}" name="login" value="{{ old('username') ?: old('email') }}" required autofocus
                    id="signin-email" type="text" placeholder="Username / E-mail">
                    @if ($errors->has('username') || $errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @endif
            </p>

            <p class="cd-signin-modal__fieldset">
                <label
                    class="cd-signin-modal__label cd-signin-modal__label--password cd-signin-modal__label--image-replace"
                    for="signin-password">Password</label>
                <input
                    class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border  @error('password') is-invalid @enderror" name="password" required autocomplete="current-password"
                    id="signin-password" type="text" placeholder="Password">
                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>a
                    @enderror
                <a href="#0" class="cd-signin-modal__hide-password js-hide-password">Hide</a>
            </p>

            <p class="cd-signin-modal__fieldset">
                <input type="checkbox" id="remember-me" checked class="cd-signin-modal__input ">
                <label for="remember-me">Remember me</label>
            </p>

            <p class="cd-signin-modal__fieldset">
                <button class="cd-signin-modal__input cd-signin-modal__input--full-width" type="submit">Login</button>
            </p>
        </form>

        <p class="cd-signin-modal__bottom-message js-signin-modal-trigger"><a href="#0" data-signin="reset">Forgot your
                password?</a></p>
    </div> <!-- cd-signin-modal__block -->

    <div class="cd-signin-modal__block js-signin-modal-block" data-type="signup">
        <!-- sign up form -->
        <form class="cd-signin-modal__form" method="POST" action="{{ url('panel/login') }}">
        @csrf
            <p class="cd-signin-modal__fieldset">
                <label
                    class="cd-signin-modal__label cd-signin-modal__label--username cd-signin-modal__label--image-replace"
                    for="signup-username">Username</label>
                <input
                    class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border"
                    id="signup-username" type="text" placeholder="Username" name="username">
                <span class="cd-signin-modal__error">Error message here!</span>
            </p>

            <p class="cd-signin-modal__fieldset">
                <label
                    class="cd-signin-modal__label cd-signin-modal__label--email cd-signin-modal__label--image-replace"
                    for="signup-email">E-mail</label>
                <input
                    class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border {{ $errors->has('username') || $errors->has('email') ? ' is-invalid' : '' }}" name="login" value="{{ old('username') ?: old('email') }}" required autofocus
                    id="signup-email" type="email" placeholder="E-mail">
                    @if ($errors->has('username') || $errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @endif
                <span class="cd-signin-modal__error">Error message here!</span>
            </p>

            <p class="cd-signin-modal__fieldset">
                <label
                    class="cd-signin-modal__label cd-signin-modal__label--password cd-signin-modal__label--image-replace"
                    for="signup-password">Password</label>
                <input
                    class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border @error('password') is-invalid @enderror" name="password" required autocomplete="current-password"
                    id="signup-password" type="text" placeholder="Password">
                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>a
                    @enderror
                <a href="#0" class="cd-signin-modal__hide-password js-hide-password">Hide</a>
                <span class="cd-signin-modal__error">Error message here!</span>
            </p>

            <p class="cd-signin-modal__fieldset">
                <input type="checkbox" id="accept-terms" class="cd-signin-modal__input ">
                <label for="accept-terms">I agree to the <a href="#0">Terms</a></label>
            </p>

            <p class="cd-signin-modal__fieldset">
                <input
                    class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding"
                    type="submit" value="Create account">
            </p>
        </form>
    </div> <!-- cd-signin-modal__block -->

    <div class="cd-signin-modal__block js-signin-modal-block" data-type="reset">
        <!-- reset password form -->
        <p class="cd-signin-modal__message">Lost your password? Please enter your email address. You will receive a link
            to create a new password.</p>

        <form class="cd-signin-modal__form" method="POST" action="{{ url('panel/register') }}">
        @csrf
            <p class="cd-signin-modal__fieldset">
                <label
                    class="cd-signin-modal__label cd-signin-modal__label--email cd-signin-modal__label--image-replace"
                    for="reset-email">E-mail</label>
                <input
                    class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border"
                    id="reset-email" type="email" placeholder="E-mail">
                <span class="cd-signin-modal__error">Error message here!</span>
            </p>

            <p class="cd-signin-modal__fieldset">
                <input
                    class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding"
                    type="submit" value="Reset password">
            </p>
        </form>

        <p class="cd-signin-modal__bottom-message js-signin-modal-trigger"><a href="#0" data-signin="login">Back to
                log-in</a></p>
    </div> <!-- cd-signin-modal__block -->
    <a href="#0" class="cd-signin-modal__close js-close">Close</a>
</div> <!-- cd-signin-modal__container -->
</div> <!-- cd-signin-modal -->
