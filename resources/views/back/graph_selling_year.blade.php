@extends('layouts.post.graph')

@section('content')
@include('layouts.menu')
<div class="row wrapper border-bottom white-bg page-heading">
<div class="col-lg-10">
<h2>Grafik Penjualan Tahunan</h2>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="{{url('/')}}">Home</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{url('graph/selling_year')}}">Grafik Penjualan Tahunan</a>
    </li>
    <li class="breadcrumb-item active">
        <strong>All Grafik Penjualan Tahunan</strong>
    </li>
</ol>
</div>
<div class="col-lg-2">

</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
<div class="row">
<div class="col-lg-12">
    <div class="ibox ">

            <div class="ibox-title">
                <h5>Manage Grafik Penjualan Tahunan</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-wrench"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <form method="get" action="{{ url('graph/selling_year') }}">
                    @csrf
                <div class="row printing">
                    <div class="col-sm-3 report_pilot">
                        <div class="input-group">
                            <input name="start_date" class="form-control date-picker-month" autocomplete="off" type="text" value="{{$start_date}}" data-date-format="yyyy-mm" id="date_start" data-validation="date" data-validation-format="yyyy-mm" placeholder="Bulan" value="">
                            <span class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-1 report_pilot">
                        <button type="submit" name="post-look" value="publish" class="btn col-sm-12 btn-sm btn-primary"> Lihat!</button>
                    </div>
                    <div class="col-sm-1 report_pilot">
                    </div>
                    <div class="col-sm-1">
                    </div>
                </div><br>
                </form>
                <div class="table-responsive">
                    <form method="post">
                    @csrf
<div id="report"></div>


        </form>
    </div>
</div>
</div>
</div>

</div>
</div>
<div class="footer">
<div class="float-right">
<strong>
    </strong>
</div>
<div>
<strong>Copyright</strong> {{$copyright}}
</div>
</div>

@endsection
