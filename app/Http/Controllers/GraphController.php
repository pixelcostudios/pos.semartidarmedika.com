<?php

namespace App\Http\Controllers;

use DB;
use App\Categories;
use App\Item;
use App\Unit;
use App\Selling;
use App\Retur;
use App\Helpers\Base;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input as input;
use Illuminate\Http\Request;

class GraphController extends Controller
{
    public function stock()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        $post_list = Item::selectRaw('cat_title,SUM(barang_stok) AS tot_stok')->join('cats', 'cats.cat_id', '=', 'items.barang_kategori_id')->groupBy('cat_title')->get();
        $category_list = array();
        $value_list = array();
        foreach ($post_list as $result) {
            $category_list[] = $result->cat_title; //ambil bulan
            $value_list[] = (float) $result->tot_stok; //ambil nilai
        }
        return view('back/graph_stock', ['title' => 'Grafik Stok Barang Perkategori', 'title_legend' => 'Stok Barang', 'title_tooltip' => 'Stok', 'post_list' => $post_list, 'category_list' => $category_list, 'value_list' => $value_list, 'copyright' => copyright()]);
    }
    public function selling_month()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        if (Input::get('start_date') != '') {
            $start_date = Input::get('start_date');
        } else {
            $start_date = date('Y-m');
        }
        $post_list = Selling::selectRaw('DATE_FORMAT(jual_tanggal,"%d") AS tanggal,SUM(jual_total) total')->where('jual_tanggal', 'like',  $start_date . '%')->groupBy('tanggal')->get();
        $category_list = array();
        $value_list = array();
        foreach ($post_list as $result) {
            $category_list[] = $result->tanggal; //ambil bulan
            $value_list[] = (float) $result->total; //ambil nilai
        }
        return view('back/graph_selling_month', ['title' => 'Grafik Penjualan Bulan '.date('M',strtotime($start_date)), 'title_legend' => 'Tanggal', 'title_tooltip' => 'Penjualan Tanggal', 'post_list' => $post_list, 'category_list' => $category_list, 'value_list' => $value_list, 'start_date' => $start_date,  'copyright' => copyright()]);
    }
    public function selling_year()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        if (Input::get('start_date') != '') {
            $start_date = Input::get('start_date');
        } else {
            $start_date = date('Y');
        }
        $post_list = Selling::selectRaw('DATE_FORMAT(jual_tanggal,"%m") AS tanggal,SUM(jual_total) total')->where('jual_tanggal', 'like',  $start_date . '%')->groupBy('tanggal')->get();
        $category_list = array();
        $value_list = array();
        foreach ($post_list as $result) {
            $category_list[] = $result->tanggal; //ambil bulan
            $value_list[] = (float) $result->total; //ambil nilai
        }
        return view('back/graph_selling_month', ['title' => 'Grafik Penjualan Tahun '.date('Y',strtotime($start_date)), 'title_legend' => 'Bulan', 'title_tooltip' => 'Penjualan Bulan', 'post_list' => $post_list, 'category_list' => $category_list, 'value_list' => $value_list, 'start_date' => $start_date,  'copyright' => copyright()]);
    }
}
