<?php

namespace App\Http\Controllers;

use DB;
use Image;

use App\Member;
use App\Helpers\Base;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input as input;
use Illuminate\Support\Facades\Hash;

use Illuminate\Http\Request;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        if (Input::get('show') != '') {
            $show = Input::get('show');
        } else {
            $show = 10;
        }

        $post_list = Member::selectRaw('*, users.id as user_id')->leftJoin('users_groups', 'users_groups.user_id', '=', 'users.id')->leftJoin('groups', 'groups.id', '=', 'users_groups.group_id')->orderBy('created_at', 'DESC')->paginate($show);

        if (isset($_POST['post-delete']) && $_POST['post-delete'] != "") {
            for ($i = 0; $i < count(@$_POST['post-check']); $i++) {
                $post_id = @$_POST['post-check'][$i];

                // @unlink('./upload/' . $row_images->post_slug . '.' . $row_images->post_mime_type);

                DB::table('users')->where('id', '=', $post_id)->delete();
            }
            return redirect('/member/')->with('success', 'Member updated!');
        } elseif (isset($_POST['post-deleted']) && $_POST['post-deleted'] != "") {
            $post_id = $_POST['post_id'];

            DB::table('users')->where('id', '=', $post_id)->delete();

            return redirect('/member/')->with('success', 'Member updated!');
        }

        return view('back/member', ['post_list' => $post_list, 'show' => $show, 'copyright' => copyright()]);
    }

    public function role()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        if (Input::get('show') != '') {
            $show = Input::get('show');
        } else {
            $show = 10;
        }

        $post_list = DB::table('groups')->orderBy('name', 'DESC')->paginate($show);

        if (isset($_POST['post-delete']) && $_POST['post-delete'] != "") {
            for ($i = 0; $i < count(@$_POST['post-check']); $i++) {
                $post_id = @$_POST['post-check'][$i];

                DB::table('groups')->where('id', '=', $post_id)->delete();
            }
            return redirect('/member/role/')->with('success', 'Member updated!');
        } elseif (isset($_POST['post-deleted']) && $_POST['post-deleted'] != "") {
            $post_id = $_POST['post_id'];

            DB::table('groups')->where('id', '=', $post_id)->delete();

            return redirect('/member/role/')->with('success', 'Role updated!');
        }

        return view('back/member_role', ['post_list' => $post_list, 'show' => $show, 'copyright' => copyright()]);
    }

    public function role_search()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        if (Input::get('show') != '') {
            $show = Input::get('show');
        } else {
            $show = 10;
        }
        if (Input::get('query') != '') {
            $search = Input::get('query');
        } else {
            $search = '';
        }

        $post_list = DB::table('groups')->where('name', 'like', '%' . $search . '%')->orWhere('description', 'like', '%' . $search . '%')->orderBy('name', 'DESC')->paginate($show);

        if (isset($_POST['post-delete']) && $_POST['post-delete'] != "") {
            for ($i = 0; $i < count(@$_POST['post-check']); $i++) {
                $post_id = @$_POST['post-check'][$i];

                DB::table('groups')->where('id', '=', $post_id)->delete();
            }
            return redirect('/member/role/')->with('success', 'Member updated!');
        } elseif (isset($_POST['post-deleted']) && $_POST['post-deleted'] != "") {
            $post_id = $_POST['post_id'];

            DB::table('groups')->where('id', '=', $post_id)->delete();

            return redirect('/member/role/')->with('success', 'Role updated!');
        }

        return view('back/member_role_search', ['post_list' => $post_list, 'show' => $show, 'copyright' => copyright()]);
    }

    public function role_edit($id)
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        $post_edit      = DB::table('groups')->where('id', $id)->get();

        return view('back/member_role_edit', [
            'post_edit'     => $post_edit,
            'copyright'     => copyright()
        ]);
    }

    public function role_add()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }

        return view('back/member_role_add', [
            'copyright'     => copyright()
        ]);
    }

    public function role_create(Request $request)
    {
        $name          = ($request->get('name') != '') ? $request->get('name') : '';
        $description   = ($request->get('description') != '') ? $request->get('description') : '';

        DB::table('groups')->insert([
            'name'            => $name,
            'description'     => $description,
            // 'created_at'   => date('Y-m-d H:i:s'),
            // 'updated_at'   => date('Y-m-d H:i:s'),
            // 'deleted_at'   => date('Y-m-d H:i:s')
        ]);
        $id = DB::getPdo()->lastInsertId();
        return redirect('/member/role_edit/' . $id)->with('success', 'Role updated!');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $role              = ($request->get('role') != '') ? $request->get('role') : '';
        $username          = ($request->get('username') != '') ? $request->get('username') : '';
        $password          = Hash::make($request->get('password'));
        $email             = ($request->get('email') != '') ? $request->get('email') : '';
        $active            = ($request->get('active') != '') ? $request->get('active') : '';

        $gender            = ($request->get('gender') != '') ? $request->get('gender') : '';
        $first_name        = ($request->get('first_name') != '') ? $request->get('first_name') : '';
        $last_name         = ($request->get('last_name') != '') ? $request->get('last_name') : '';
        $phone             = ($request->get('phone') != '') ? $request->get('phone') : '';
        $mobile            = ($request->get('mobile') != '') ? $request->get('mobile') : '';
        $address           = ($request->get('address') != '') ? $request->get('address') : '';
        $city              = ($request->get('city') != '') ? $request->get('city') : '';
        $province          = ($request->get('province') != '') ? $request->get('province') : '';
        $country           = ($request->get('country') != '') ? $request->get('country') : '';
        $postal            = ($request->get('postal') != '') ? $request->get('postal') : '';
        // $map               = ($request->get('post_title') != '') ? $request->get('map') : '';
        $profession        = ($request->get('profession') != '') ? $request->get('profession') : '';
        $company           = ($request->get('company') != '') ? $request->get('company') : '';
        $facebook          = ($request->get('facebook') != '') ? $request->get('facebook') : '';
        $twitter           = ($request->get('twitter') != '') ? $request->get('twitter') : '';
        $instagram         = ($request->get('instagram') != '') ? $request->get('instagram') : '';
        $google            = ($request->get('google') != '') ? $request->get('google') : '';
        $birthday          = ($request->get('birthday') != '') ? $request->get('birthday') : '';
        $signature         = ($request->get('signature') != '') ? $request->get('signature') : '';

        Member::insert([
            'ip_address'   => request()->ip(),
            'username' 	   => $username,
            'password' 	   => $password,
            'email' 	   => $email,
            'email_verified_at' => date('Y-m-d H:i:s'),
            'gender'       => $gender,
            'first_name'   => $first_name,
            'last_name'    => $last_name,
            'phone'        => $phone,
            'mobile'       => $mobile,
            'address'      => $address,
            'city'         => $city,
            'province'     => $province,
            'country'      => $country,
            'postal'       => $postal,
            'profession'   => $profession,
            'company'      => $company,
            'facebook'     => $facebook,
            'twitter'      => $twitter,
            'instagram'    => $instagram,
            'google'       => $google,
            'birthday'     => $birthday,
            'signature'    => $signature,
            'active' 	   => $active,
            'avatar'       => '',
            'created_at'   => date('Y-m-d H:i:s'),
            'updated_at'   => date('Y-m-d H:i:s')
        ]);

        $id = DB::getPdo()->lastInsertId();

        if(DB::table('users_groups')->where('user_id', $id)->count()==0)
        {
            DB::table('users_groups')->insert(['user_id' => $id, 'group_id' => $role]);
        }
        else
        {
            DB::table('users_groups')->where('user_id', '=', $id)->update(['group_id' => $role]);
        }

        return redirect('/member/edit/' . $id)->with('success', 'Member updated!');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function show(Member $member)
    {
        //
    }
    public function search()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        if (Input::get('show') != '') {
            $show = Input::get('show');
        } else {
            $show = 10;
        }
        if (Input::get('query') != '') {
            $search = Input::get('query');
        } else {
            $search = '';
        }

        $post_list = Member::selectRaw('*, users.id as user_id')->leftJoin('users_groups', 'users_groups.user_id', '=', 'users.id')->leftJoin('groups', 'groups.id', '=', 'users_groups.group_id')->where('username', 'like', '%' . $search . '%')->orWhere('email', 'like', '%' . $search . '%')->orWhere('phone', 'like', '%' . $search . '%')->orderBy('created_at', 'DESC')->paginate($show);

        if (isset($_POST['post-delete']) && $_POST['post-delete'] != "") {
            for ($i = 0; $i < count(@$_POST['post-check']); $i++) {
                $post_id = @$_POST['post-check'][$i];

                // @unlink('./upload/' . $row_images->post_slug . '.' . $row_images->post_mime_type);

                DB::table('users')->where('id', '=', $post_id)->delete();
            }
            return redirect('/member/')->with('success', 'Member updated!');
        } elseif (isset($_POST['post-deleted']) && $_POST['post-deleted'] != "") {
            $post_id = $_POST['post_id'];

            DB::table('users')->where('id', '=', $post_id)->delete();

            return redirect('/member/')->with('success', 'Member updated!');
        }

        return view('back/member_search', ['post_list' => $post_list, 'show' => $show, 'search' => $search, 'copyright' => copyright()]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        $post_edit      = Member::selectRaw('*, users.id as user_id')->leftJoin('users_groups', 'users_groups.user_id', '=', 'users.id')->where('users.id', $id)->get();
        // $group_list     = DB::table('users_groups')->join('groups', 'groups.id', '=', 'users_groups.group_id')->get();
        $group_list     = DB::table('groups')->get();

        return view('back/member_edit', [
            'post_edit'     => $post_edit,
            'group_list'    => $group_list,
            'copyright'     => copyright()
        ]);
    }

    public function add()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        $group_list     = DB::table('groups')->get();

        return view('back/member_add', [
            'group_list'    => $group_list,
            'copyright'     => copyright()
        ]);
    }
    public function profile()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        $id =  Auth::user()->id;
        $post_edit      = Member::selectRaw('*, users.id as user_id')->leftJoin('users_groups', 'users_groups.user_id', '=', 'users.id')->where('users.id', $id)->get();

        return view('back/profile', [
            'post_edit'     => $post_edit,
            'copyright'     => copyright()
        ]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        $post_data = Member::find($id);
        $role                         = ($request->get('role') != '') ? $request->get('role') : '';
        $post_data->username          = ($request->get('username') != '') ? $request->get('username') : '';
        if(($request->get('password') != ''))
        {
            $post_data->password      = Hash::make($request->get('password'));
        }
        $post_data->email             = ($request->get('email') != '') ? $request->get('email') : '';
        $post_data->active            = ($request->get('active') != '') ? $request->get('active') : '';

        $post_data->gender            = ($request->get('gender') != '') ? $request->get('gender') : '';
        $post_data->first_name        = ($request->get('first_name') != '') ? $request->get('first_name') : '';
        $post_data->last_name         = ($request->get('last_name') != '') ? $request->get('last_name') : '';
        $post_data->phone             = ($request->get('phone') != '') ? $request->get('phone') : '';
        $post_data->mobile            = ($request->get('mobile') != '') ? $request->get('mobile') : '';
        $post_data->address           = ($request->get('address') != '') ? $request->get('address') : '';
        $post_data->city              = ($request->get('city') != '') ? $request->get('city') : '';
        $post_data->province          = ($request->get('province') != '') ? $request->get('province') : '';
        $post_data->country           = ($request->get('country') != '') ? $request->get('country') : '';
        $post_data->postal            = ($request->get('postal') != '') ? $request->get('postal') : '';
        // $post_data->map               = ($request->get('post_title') != '') ? $request->get('map') : '';
        $post_data->profession        = ($request->get('profession') != '') ? $request->get('profession') : '';
        $post_data->company           = ($request->get('company') != '') ? $request->get('company') : '';
        $post_data->facebook          = ($request->get('facebook') != '') ? $request->get('facebook') : '';
        $post_data->twitter           = ($request->get('twitter') != '') ? $request->get('twitter') : '';
        $post_data->instagram         = ($request->get('instagram') != '') ? $request->get('instagram') : '';
        $post_data->google            = ($request->get('google') != '') ? $request->get('google') : '';
        $post_data->birthday          = ($request->get('birthday') != '') ? $request->get('birthday') : '';
        $post_data->signature         = ($request->get('signature') != '') ? $request->get('signature') : '';
        $post_data->save();

        if(DB::table('users_groups')->where('user_id', $id)->count()==0)
        {
            DB::table('users_groups')->insert(['user_id' => $id, 'group_id' => $role]);
        }
        else
        {
            DB::table('users_groups')->where('user_id', '=', $id)->update(['group_id' => $role]);
        }

        if ($request->hasfile('upload_photo')) {
            foreach ($request->file('upload_photo') as $image) {
                $original_name = $image->getClientOriginalName();
                $file_name = ext_name($original_name);
                $file_ext = ext_filename($original_name);

                $image->move(public_path() . '/upload/', strip_i_slug($file_name) . '.' . $file_ext);
                // Member::insert([
                //     'user_id' => $request->get('user_id'),
                //     'post_type' => 'images',
                //     'post_up' => $id,
                // ]);
            }
        }

        return redirect('/member/edit/' . $id)->with('success', 'Member updated!');
    }
    public function profile_update(Request $request)
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        $post_data = Member::find(Auth::user()->id);
        $post_data->username          = ($request->get('username') != '') ? $request->get('username') : '';
        if (($request->get('password') != '')) {
            $post_data->password      = Hash::make($request->get('password'));
        }
        $post_data->gender            = ($request->get('gender') != '') ? $request->get('gender') : '';
        $post_data->first_name        = ($request->get('first_name') != '') ? $request->get('first_name') : '';
        $post_data->last_name         = ($request->get('last_name') != '') ? $request->get('last_name') : '';
        $post_data->phone             = ($request->get('phone') != '') ? $request->get('phone') : '';
        $post_data->mobile            = ($request->get('mobile') != '') ? $request->get('mobile') : '';
        $post_data->address           = ($request->get('address') != '') ? $request->get('address') : '';
        $post_data->city              = ($request->get('city') != '') ? $request->get('city') : '';
        $post_data->province          = ($request->get('province') != '') ? $request->get('province') : '';
        $post_data->country           = ($request->get('country') != '') ? $request->get('country') : '';
        $post_data->postal            = ($request->get('postal') != '') ? $request->get('postal') : '';
        // $post_data->map               = ($request->get('post_title') != '') ? $request->get('map') : '';
        $post_data->profession        = ($request->get('profession') != '') ? $request->get('profession') : '';
        $post_data->company           = ($request->get('company') != '') ? $request->get('company') : '';
        $post_data->facebook          = ($request->get('facebook') != '') ? $request->get('facebook') : '';
        $post_data->twitter           = ($request->get('twitter') != '') ? $request->get('twitter') : '';
        $post_data->instagram         = ($request->get('instagram') != '') ? $request->get('instagram') : '';
        $post_data->google            = ($request->get('google') != '') ? $request->get('google') : '';
        $post_data->birthday          = ($request->get('birthday') != '') ? $request->get('birthday') : '';
        $post_data->signature         = ($request->get('signature') != '') ? $request->get('signature') : '';
        $post_data->save();

        if ($request->hasfile('upload_photo')) {
            foreach ($request->file('upload_photo') as $image) {
                $original_name = $image->getClientOriginalName();
                $file_name = ext_name($original_name);
                $file_ext = ext_filename($original_name);

                $image->move(public_path() . '/upload/', strip_i_slug($file_name) . '.' . $file_ext);
                $files_data = Member::find(Auth::user()->id);
                $files_data->avatar     = strip_i_slug($file_name) . '.' . $file_ext;
                $files_data->save();
            }
        }

        return redirect('/profile')->with('success', 'Profile updated!');
    }
    public function role_update(Request $request, $id)
    {
        $name                         = ($request->get('name') != '') ? $request->get('name') : '';
        $description                  = ($request->get('description') != '') ? $request->get('description') : '';

        DB::table('groups')
            ->where('id', $id)
            ->update(['name' => $name, 'description'=> $description]);

        return redirect('/member/role_edit/' . $id)->with('success', 'Member Role updated!');
    }
}
