<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link href="../themes/default/css/bootstrap.min.css" rel="stylesheet">
	<link href="../themes/default/font-awesome/css/font-awesome.css" rel="stylesheet">
	<link href="../themes/default/js/plugins/multiselect/dist/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
	<link href="../themes/default/js/plugins/datetime/bootstrap-datetimepicker.css" rel="stylesheet" />
	<link href="../themes/default/css/plugins/select2/select2.min.css" rel="stylesheet">
	<link href="../themes/default/css/plugins/dualListbox/bootstrap-duallistbox.min.css" rel="stylesheet">
	<link href="../themes/default/css/animate.css" rel="stylesheet">
	<link href="../themes/default/css/style.css" rel="stylesheet">
	<link href="../themes/default/css/custom.css" rel="stylesheet">

</head>

<body class="top-navigation pace-done">
    <div id="wrapper">

        <div id="page-wrapper" class="gray-bg">
            @yield('content')
        </div>
    </div>

 <!-- Mainly scripts -->
<script src="../themes/default/js/jquery-3.1.1.min.js"></script>
<script src="../themes/default/js/popper.min.js"></script>
<script src="../themes/default/js/bootstrap.js"></script>
<script src="../themes/default/js/plugins/metismenu/jquery.metismenu.js"></script>
<script src="../themes/default/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Peity -->
<script src="../themes/default/js/plugins/peity/jquery.peity.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="../themes/default/js/inspinia.js"></script>
<script src="../themes/default/js/plugins/pace/pace.min.js"></script>

<script src="../themes/default/js/plugins/multiselect/dist/js/bootstrap-multiselect.js" type="text/javascript"></script>

<!--CK Editor-->
<script src="../themes/default/js/plugins/ckeditor/ckeditor.js"></script>
<script src="../themes/default/js/plugins/ckeditor/en.js"></script>
<script src="../themes/default/js/plugins/ckeditor/id.js"></script>
<script src="../themes/default/js/plugins/ckeditor/ck.js"></script>

<!--Files Scripts-->
<script src="../themes/default/js/plugins/filestyle/filestyle.js"></script>
<script src="../themes/default/js/plugins/filestyle/aplication.js"></script>

<!-- Check -->
<script src="../themes/default/js/check.js"></script>

<!-- Select2 -->
<script src="../themes/default/js/plugins/select2/select2.full.min.js"></script>

<!--Bootstrap Date Picker-->
<script src="../themes/default/js/plugins/datetime/bootstrap-datetimepicker.min.js"></script>

<script>
    $(document).ready(function() {
    $("#{{$type}}MainNav").addClass('active');
    $("#{{$type}}MainNav .collapse").addClass('in');
    });
	$(".date-picker").datetimepicker({
		format: 'yyyy-mm-dd hh:ii:ss',
		autoclose: true,
	});
	$(document).ready(function () {
		$(".select2").select2();
		$('#example-multiple-selected').multiselect();
	});
</script>
</body>
</html>
