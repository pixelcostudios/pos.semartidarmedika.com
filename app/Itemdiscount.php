<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Itemdiscount extends Model
{
    protected $primaryKey = 'discount_id';
    protected $table = "item_discounts";
}
