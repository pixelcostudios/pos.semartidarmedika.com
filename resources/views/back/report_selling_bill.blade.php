@extends('layouts.post.report')

@section('content')
@include('layouts.menu')
<div class="row wrapper border-bottom white-bg page-heading printing">
<div class="col-lg-10">
<h2 class="title-export">Report Penjualan Barang Bon</h2>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="{{url('/')}}">Home</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{url('report/selling_bill/')}}">Report Penjualan Barang Bon</a>
    </li>
    <li class="breadcrumb-item active">
        <strong>All Report Penjualan Barang Bon</strong>
    </li>
</ol>
</div>
<div class="col-lg-2">

</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
<div class="row">
<div class="col-lg-12 padding-none">
    <div class="ibox ">

            <div class="ibox-title printing">
                <h5>Manage Report Penjualan Barang Bon</h5>
            </div>
            <div class="ibox-content">
                <form method="get" action="{{ url('report/selling_bill') }}">
                    @csrf
                <div class="row printing">
                    <div class="col-sm-3 report_pilot">
                        <div class="input-group">
                            <input name="start_date" class="form-control date-picker start_date" autocomplete="off" type="text" value="{{$start_date}}" data-date-format="yyyy-mm-dd" id="date_start" data-validation="date" data-validation-format="yyyy-mm-dd" placeholder="Mulai Tanggal" value="">
                            <span class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-3 report_pilot">
                        <div class="input-group">
                            <input name="end_date" class="form-control date-picker end_date" autocomplete="off" type="text" value="{{$end_date}}" data-date-format="yyyy-mm-dd" id="date_start" data-validation="date" data-validation-format="yyyy-mm-dd" placeholder="Sampai Tanggal" value="">
                            <span class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-1 report_pilot">
                            <button type="submit" name="post-look" value="publish" class="btn col-sm-12 btn-sm btn-primary"> Lihat!</button>
                        </div>
                        <div class="col-sm-1 report_pilot">
                            <div data-toggle="buttons" class="btn-group">
                                <button type="button" class="btn btn-sm btn-white download-xls"> <i class="fa fa-download"></i> Download </button>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <button type="button" class="btn btn-default btn-sm btn-print"><i class="fa fa-print"></i> Print</button>
                        </div>
                </div><br>
                </form>
                <div class="table-responsive" id="printarea">
                    <form method="post">
                    @csrf
                    <table class="table table-striped" id="myTable">
                        <tbody>
                             <tr>
                                 <th>No </th>
                                <th>No Faktur </th>
                                <th>Tanggal </th>
                                <th>Kode Barang	 </th>
                                <th>Nama Barang	</th>
                                <th>Satuan</th>
                                <th>Harga Jual</th>
                                <th>Qty</th>
                                <th>Diskon</th>
                                <th>Total</th>
                                <th>Status</th>
                            </tr>
                            @php
                            $total = array();
                            @endphp
                            @foreach($post_list as $value  => $p)
                            <tr>
                                <td>{{$value+1}} </td>
                                <td>{{$p->jual_nofak}} </td>
                                <td>{{$p->jual_tanggal}} </td>
                                <td>{{$p->d_jual_barang_id}} </td>
                                <td>{{$p->d_jual_barang_nama}} </td>
                                <td>{{$p->d_jual_barang_satuan}} </td>
                                <td>{{($p->d_jual_barang_harjul)}} </td>
                                <td>{{$p->d_jual_qty}} </td>
                                <td>{{$p->d_jual_diskon}} </td>
                                <td>{{($p->d_jual_qty*$p->d_jual_barang_harjul)-$p->d_jual_diskon}} </td>
                                <td>@if($p->jual_kembalian==0)
                                <span class="label label-primary">Lunas</span>
                                        @else
                                        <span class="label label-warning">Bon</span>
                                    @endif </td>
                            </tr>
                            @php
                            $total[] = ($p->d_jual_qty*$p->d_jual_barang_harjul)-$p->d_jual_diskon;
                            @endphp
                            @endforeach
                            <tr>
                                <td colspan="10" class="text-right">Grand Total</td>
                                <td class="text-right">{{currency(array_sum($total))}}</td>
                            </tr>
                        </tbody>
                       </table>

        </form>
    </div>
</div>
</div>
</div>

</div>
</div>
<div class="footer printing">
<div class="float-right">
<strong>
    </strong>
</div>
<div>
<strong>Copyright</strong> {{$copyright}}
</div>
</div>
@endsection
