<?php

namespace App\Exports;

use DB;

use App\Item;
use App\Categories;
use App\Buying;
use App\Helpers\Base;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input as input;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ReportBuyingExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        if (Input::get('start_date') != '') {
            $start_date = Input::get('start_date');
        } else {
            $start_date = date('Y-m-d');
        }
        if (Input::get('end_date') != '') {
            $end_date = Input::get('end_date');
        } else {
            $end_date = date('Y-m-d');
        }
        $post_list = Buying::join('buy_details', 'buy_details.beli_id', '=', 'buys.beli_id')
        ->join('items', 'items.barang_id', '=', 'buy_details.d_beli_barang_id')
        ->whereBetween('beli_tanggal', [$start_date . " 00:00:00", $end_date . " 23:59:59"])
        ->orderBy('beli_tanggal', 'DESC')->get();

        $post_array = array();
        $i=0;
        foreach ($post_list as $key)
        {
            $i++;
            $data_array[] = array(
                'r_no'  => $i + 1,
                'beli_nofak'  => $key->beli_nofak,
                'beli_tanggal' => $key->beli_tanggal,
                'd_beli_barang_id' => $key->d_beli_barang_id,
                'barang_nama' => $key->barang_nama,
                'barang_satuan' => $key->barang_satuan,
                'd_beli_harga' => $key->d_beli_harga,
                'd_beli_jumlah' => $key->d_beli_jumlah,
                'd_beli_jumlah_' => $key->d_beli_jumlah*$key->d_beli_harga,
                'beli_payment' => $key->beli_payment
            );
        }
        
        // $i = 0;
        // $post_list_array = array();
        // foreach ($post_list as $row) {
        //     $i++;
        //     $post_list_array[] = array(
        //         'r_no'  => $i,
        //         'r_id'  => $row->barang_id,
        //         'r_jul' => $row->barang_harjul,
        //         'r_name'=> $row->barang_nama
        //     );
        // }

        return collect($data_array);//Target::all();
    }
    public function headings(): array
    {
        return [
            'No',
            'No Faktur',
            'Tanggal',
            'Kode Barang',
            'Nama Barang',
            'Satuan',
            'Harga Pokok',
            'Qty',
            'Total',
            'Pembayaran',
        ];
    }
}
