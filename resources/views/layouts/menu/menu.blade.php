<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>

    <link href="{{ url('/') }}/themes/default/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/themes/default/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="{{ url('/') }}/themes/default/js/plugins/multiselect/dist/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <link href="{{ url('/') }}/themes/default/css/plugins/dualListbox/bootstrap-duallistbox.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/themes/default/css/animate.css" rel="stylesheet">
    <link href="{{ url('/') }}/themes/default/css/style.css" rel="stylesheet">
    <link href="{{ url('/') }}/themes/default/css/custom.css" rel="stylesheet">
</head>

<body>
    <div id="wrapper">
        @include('layouts.nav')

        <div id="page-wrapper" class="gray-bg">
            @yield('content')
        </div>
    </div>

    <!-- Mainly scripts -->
<script src="{{ url('/') }}/themes/default/js/jquery-3.1.1.min.js"></script>
<script src="{{ url('/') }}/themes/default/js/popper.min.js"></script>
<script src="{{ url('/') }}/themes/default/js/bootstrap.js"></script>
<script src="{{ url('/') }}/themes/default/js/plugins/metismenu/jquery.metismenu.js"></script>
<script src="{{ url('/') }}/themes/default/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="{{ url('/') }}/themes/default/js/inspinia.js"></script>
<script src="{{ url('/') }}/themes/default/js/plugins/pace/pace.min.js"></script>

<script src="{{ url('/') }}/themes/default/js/plugins/simple-menu/jquery.nestable.js"></script>
<script src="{{ url('/') }}/themes/default/js/plugins/multiselect/dist/js/bootstrap-multiselect.js" type="text/javascript" ></script>

<!-- Check -->
<script src="{{ url('/') }}/themes/default/js/check.js"></script>
<script>
$(document).ready(function()
{
    var updateOutput = function(e)
    {
        var list   = e.length ? e : $(e.target),
            output = list.data('output');
        if (window.JSON) {
            output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
        } else {
            output.val('JSON browser support required for this demo.');
        }
    };

    // activate Nestable for list 1
    $('#nestable').nestable({
        group: 1
    })
    .on('change', updateOutput);

    // output initial serialised data
    updateOutput($('#nestable').data('output', $('#nestable-output')));

    $('#nestable-menu').on('click', function(e)
    {
        var target = $(e.target),
            action = target.data('action');
        if (action === 'expand-all') {
            $('.dd').nestable('expandAll');
        }
        if (action === 'collapse-all') {
            $('.dd').nestable('collapseAll');
        }
    });
});
</script>

<script>
  $(document).ready(function(){
    $("#load").hide();
    $("#submit").click(function(){
       $("#load").show();
    //    $('#category :selected').each(function(i, sel){
    //       selecteded = $(sel).val();
    //    });

        var categoryID = "";
        $("#category :selected").each(function(i,obj)
        {
            categoryID=categoryID+$(obj).val()+",";
        });
        categoryID = categoryID.substring(0,categoryID.length - 1);

        var pagesID = "";
        $("#pages :selected").each(function(i,obj)
        {
            pagesID=pagesID+$(obj).val()+",";
        });
        pagesID = pagesID.substring(0,pagesID.length - 1);

        var postID = "";
        $("#post :selected").each(function(i,obj)
        {
            postID=postID+$(obj).val()+",";
        });
        postID = postID.substring(0,postID.length - 1);

        var positionID = "";
        $("#position :selected").each(function(i,obj)
        {
            positionID=positionID+$(obj).val()+",";
        });
        positionID = positionID.substring(0,positionID.length - 1);

        var projectID = "";
        $("#project :selected").each(function(i,obj)
        {
            poprojectIDsitionID=projectID+$(obj).val()+",";
        });
        projectID = projectID.substring(0,projectID.length - 1);

        var hotelID = "";
        $("#hotel :selected").each(function(i,obj)
        {
            hotelID=hotelID+$(obj).val()+",";
        });
        hotelID = hotelID.substring(0,hotelID.length - 1);
        var dataString = {
              label     : $("#label").val(),
              link      : $("#link").val(),
              category  : categoryID,
              pages     : pagesID,
              post      : postID,
              position  : positionID,
              project   : projectID,
              hotel     : hotelID,
              id        : $("#id").val()
            };

        $.ajax({
            type: "POST",
            url: "{{ url('/') }}/menu/add",
            data: dataString,
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            dataType: "json",
            cache : false,
            success: function(data)
            {
                console.log(dataString);
                // console.log($('meta[name="csrf-token"]').attr('content'));
                // console.log(data.type);
              if(data.type == 'add')
              {
                 $("#menu-id").append(data.menu);
              }
              else if(data.type == 'edit')
              {
                 $('#label_show'+data.id).html(data.label);
                 $('#link_show'+data.id).html(data.link);
                 $('#post_show'+data.id).html(data.post);
                 $('#position_show'+data.id).html(data.position);
              }
            //   $("#menu-id").append(data.menu);
              $('#label').val('');
              $('#link').val('');
              $('#category').serialize();
              $('#pages').serialize();
              $('#post').serialize();
              $('#position').serialize();
              $('#project').serialize();
              $('#hotel').serialize();
              $('#id').val('');
              $("#load").hide();
            } ,error: function(xhr, status, error) {
              alert(error);
            },
        });
    });

    $('.dd').on('change', function() {
        $("#load").show();

          var dataString = {
              data : $("#nestable-output").val(),
            };

        $.ajax({
            type: "POST",
            url: "{{ url('/') }}/menu/update",
            data: dataString,
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            cache : false,
            success: function(data){
                console.log(dataString)
              $("#load").hide();
            } ,error: function(xhr, status, error) {
              alert(error);
              console.log(status)
            },
        });
    });

    $("#save").click(function(){
         $("#load").show();

          var dataString = {
              data : $("#nestable-output").val(),
            };

        $.ajax({
            type: "POST",
            url: "{{ url('/') }}/menu/update",
            data: dataString,
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            cache : false,
            success: function(data){
                console.log(data);
              $("#load").hide();
              alert('Data has been saved');

            } ,error: function(xhr, status, error) {
              alert(error);
              console.log(status);
            },
        });
    });


    $(document).on("click",".del-button",function() {
        var x = confirm('Delete this menu?');
        var id = $(this).attr('id');
        if(x){
            $("#load").show();
             $.ajax({
                type: "POST",
                url: "{{ url('/') }}/menu/delete_parent",
                data: { id : id },
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                cache : false,
                success: function(data){
                  $("#load").hide();
                  $("li[data-id='" + id +"']").remove();
                } ,error: function(xhr, status, error) {
                  alert(error);
                },
            });
        }
    });

    $(document).on("click",".edit-button",function() {
        var id          = $(this).attr('id');
        var label       = $(this).attr('label');
        var link        = $(this).attr('link');
        var position    = $(this).attr('position');
        $("#id").val(id);
        $("#label").val(label);
        $("#link").val(link);
        $("#position").val(position).change();
    });

    $(document).on("click","#reset",function() {
        $('#label').val('');
        $('#link').val('');
        $('#position').val('');
        $('#id').val('');
    });

  });

</script>
<script>
$(document).ready(function()
{
    // $('#example-multiple-selected').multiselect();
    //Category Multiple Select
    $('.category-select').multiselect({
        includeSelectAllOption: true,
        maxHeight: 400,
        dropUp: true,
        enableFiltering: true,
        filterBehavior: 'value'
    });
    $.ajax({
      type: 'GET',
      url: '{{ url('/') }}/menu/list?type=category',
      dataType: 'json',
      success: function(data) {
          //console.log(data);
         $.each(data, function (i, item) {
             post_id     = item.cat_id;
             post_title  = item.cat_title;
             if($(".category-select option[value='"+post_id+"']").length == 0){
                $('.category-select').append('<option value="'+post_id+'">' + post_title + '</option>');
             }
        });
        $('.category-select').multiselect('rebuild');
      },
      error: function() {
            alert('error loading items');
      }
     });
    $('.category-select').trigger( 'change' );
    //Pages Multiple Select
    $('.pages-select').multiselect({
        includeSelectAllOption: true,
        maxHeight: 400,
        dropUp: true,
        enableFiltering: true,
        filterBehavior: 'value'
    });
    $.ajax({
      type: 'GET',
      url: '{{ url('/') }}/menu/list?type=pages',
      dataType: 'json',
      success: function(data) {
          //console.log(data);
         $.each(data, function (i, item) {
             post_id     = item.post_id;
             post_title  = item.post_title;
             if($(".pages-select option[value='"+post_id+"']").length == 0){
                $('.pages-select').append('<option value="'+post_id+'">' + post_title + '</option>');
             }
        });
        $('.pages-select').multiselect('rebuild');
      },
      error: function() {
            alert('error loading items');
      }
     });
    $('.pages-select').trigger( 'change' );
    //Post Multiple Select
    $('.post-select').multiselect({
        includeSelectAllOption: true,
        maxHeight: 400,
        dropUp: true,
        enableFiltering: true,
        filterBehavior: 'value'
    });
    $.ajax({
      type: 'GET',
      url: '{{ url('/') }}/menu/list?type=post',
      dataType: 'json',
      success: function(data) {
          //console.log(data);
         $.each(data, function (i, item) {
             post_id     = item.post_id;
             post_title  = item.post_title;
             if($(".post-select option[value='"+post_id+"']").length == 0){
                $('.post-select').append('<option value="' + post_id + '">' + post_title + '</option>');
             }
        });
        $('.post-select').multiselect('rebuild');
      },
      error: function() {
            alert('error loading items');
      }
     });
    $('.post-select').trigger( 'change' );
    //Project Multiple Select
    $('.project-select').multiselect({
        includeSelectAllOption: true,
        maxHeight: 400,
        dropUp: true,
        enableFiltering: true,
        filterBehavior: 'value'
    });
    $.ajax({
      type: 'GET',
      url: '{{ url('/') }}/menu/list?type=project',
      dataType: 'json',
      success: function(data) {
          //console.log(data);
         $.each(data, function (i, item) {
             post_id     = item.post_id;
             post_title  = item.post_title;
             if($(".project-select option[value='"+post_id+"']").length == 0){
                $('.project-select').append('<option value="'+post_id+'">' + post_title + '</option>');
             }
        });
        $('.project-select').multiselect('rebuild');
      },
      error: function() {
            alert('error loading items');
      }
     });
    $('.project-select').trigger( 'change' );
    //Hotel Multiple Select
    $('.hotel-select').multiselect({
        includeSelectAllOption: true,
        maxHeight: 400,
        dropUp: true,
        enableFiltering: true,
        filterBehavior: 'value'
    });
    $.ajax({
      type: 'GET',
      url: '{{ url('/') }}/menu/list?type=hotel',
      dataType: 'json',
      success: function(data) {
          //console.log(data);
         $.each(data, function (i, item) {
             post_id     = item.post_id;
             post_title  = item.post_title;
             if($(".hotel-select option[value='"+post_id+"']").length == 0){
                $('.hotel-select').append('<option value="'+post_id+'">' + post_title + '</option>');
             }
        });
        $('.hotel-select').multiselect('rebuild');
      },
      error: function() {
            alert('error loading items');
      }
     });
    $('.hotel-select').trigger( 'change' );
});
</script>
<script type="text/javascript">
    $(document).ready(function() {
    $("#{{request()->segment(1)}}MainNav").addClass('active');
    $("#{{request()->segment(1)}}MainNav .collapse").addClass('in');
    });

	$(document).on("click", ".open-modal_deleted", function () {
		var mypost_id = $(this).data('id');
		$(".modal-body #post_id").val(mypost_id);
		$('#modal_deleted').modal('show');
	});
</script>
</body>
</html>
