<?php

namespace App\Http\Controllers\API;

use DB;
use App\Comment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input as input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Validator;

class DiscussionController extends Controller
{
    public $successStatus = 200;

    public function index()
    {
        if (Input::get('user_id') != '') {
            $user_id = Input::get('user_id');
        } else {
            $user_id = null;
        }
        if (Input::get('parent') != '') {
            $parent = Input::get('parent');
        } else {
            $parent = null;
        }
        $post_list = Comment::where('cmt_type', '=', 'discussion')->where('parent', '=', $parent)->where('user_id', '=', $user_id)->orderBy('date', 'DESC')->get();

        $data_array = array();
        foreach ($post_list as $key) {
            $data_array[] = array('id' => $key->cmt_id, 'title' => $key->cmt_title, 'content' => $key->cmt_content);
        }
        return response()->json(['status' => 200, 'message' => 'List Discussion', 'data' => $data_array], $this->successStatus);
    }
    public function add(Request $request)
    {
        Comment::insert([
            'user_id' => $request->get('user_id'),
            'parent' => $request->get('parent'),
            'cmt_type' => 'discussion',
            'cmt_division' =>  $request->get('division'),
            'cmt_title' => $request->get('title'),
            'cmt_name' => '',
            'cmt_email' => '',
            'cmt_content' => $request->get('content'),
            'cmt_reply' => '',
            'date' => date('Y-m-d H:i:s'),
            'status' => 1
        ]);
        $id = DB::getPdo()->lastInsertId();

        if($id)
        {
            $data_array = array(
                'cmt_id' => $id,
                'user_id' => $request->get('user_id'),
                'parent' => $request->get('parent'),
                'cmt_title' => $request->get('title'),
                'cmt_content' => $request->get('content'),
                'date' => date('Y-m-d H:i:s'),
                'status' => 1);
            return response()->json(['status' => 200, 'message' => 'List Discussion', 'data' => $data_array], $this->successStatus);
        }
        else
        {
            return response()->json(['status' => 401, 'message' => 'Errror Add Discussion'], 401);
        }
    }
    public function addsub(Request $request)
    {
        Comment::insert([
            'user_id' => $request->get('user_id'),
            'parent' => $request->get('parent'),
            'cmt_type' => 'discussion',
            'cmt_division' =>  $request->get('division').'_sub',
            'cmt_title' => '',
            'cmt_name' => '',
            'cmt_email' => '',
            'cmt_content' => $request->get('content'),
            'cmt_reply' => '',
            'date' => date('Y-m-d H:i:s'),
            'status' => 1
        ]);
        $id = DB::getPdo()->lastInsertId();

        if ($id) {
            $data_array = array(
                'cmt_id' => $id,
                'user_id' => $request->get('user_id'),
                'parent' => $request->get('parent'),
                'cmt_title' => $request->get('title'),
                'cmt_content' => $request->get('content'),
                'date' => date('Y-m-d H:i:s'),
                'status' => 1
            );
            return response()->json(['status' => 200, 'message' => 'List Discussion', 'data' => $data_array], $this->successStatus);
        } else {
            return response()->json(['status' => 401, 'message' => 'Errror Add Discussion'], 401);
        }
    }
    public function delete(Request $request)
    {
        $id = Comment::where('cmt_id', '=', $request->get('id'))->where('user_id', '=', $request->get('user_id'))->delete();

        if ($id) {
            return response()->json(['status' => 200, 'message' => 'Success Delete Discussion'], $this->successStatus);
        } else {
            return response()->json(['status' => 401, 'message' => 'Errror Delete Discussion'], 401);
        }
    }
    public function doctor()
    {
        if (Input::get('user_id') != '') {
            $user_id = Input::get('user_id');
        } else {
            $user_id = null;
        }
        if (Input::get('parent') != '') {
            $parent = Input::get('parent');
        } else {
            $parent = null;
        }
        $post_list = Comment::where('cmt_type', '=', 'discussion')->where('parent', '=', $parent)->where('user_id', '=', $user_id)->where('cmt_division', '=', 'doctor')->orderBy('date', 'DESC')->get();
        $data_array = array();
        foreach ($post_list as $key) {
            $data_array[] = array('id' => $key->cmt_id, 'title' => $key->cmt_title, 'content' => $key->cmt_content);
        }
        return response()->json(['status' => 200, 'message' => 'List Discussion Doctor', 'data' => $data_array], $this->successStatus);
    }
    public function ustad()
    {
        if (Input::get('user_id') != '') {
            $user_id = Input::get('user_id');
        } else {
            $user_id = null;
        }
        if (Input::get('parent') != '') {
            $parent = Input::get('parent');
        } else {
            $parent = null;
        }
        $post_list = Comment::where('cmt_type', '=', 'discussion')->where('parent', '=', $parent)->where('user_id', '=', $user_id)->where('cmt_division', '=', 'doctor')->orderBy('date', 'DESC')->get();
        $data_array = array();
        foreach ($post_list as $key) {
            $data_array[] = array('id' => $key->cmt_id, 'title' => $key->cmt_title, 'content' => $key->cmt_content);
        }
        return response()->json(['status' => 200, 'message' => 'List Discussion Ustad', 'data' => $data_array], $this->successStatus);
    }
}
