<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ThumbController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        echo 'Thumbnail';
    }
    protected function iCrop_Browser($url, $thumb_width, $thumb_height)
    {
        /*$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
		curl_setopt($ch, CURLOPT_AUTOREFERER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSLVERSION,3);
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)") ;
		$data = curl_exec($ch);

		//Gleen Ferdinand

		curl_close($ch);*/
        // $url     = str_replace(base_url(), '', $url);
        $data     = file_get_contents($url);

        $imgInfo = getimagesize($url);
        $image = imagecreateFROMstring($data);

        $width = imagesx($image);
        $height = imagesy($image);

        $original_aspect = $width / $height;
        $thumb_aspect = $thumb_width / $thumb_height;

        if ($original_aspect >= $thumb_aspect) {
            // If image is wider than thumbnail (in aspect ratio sense)
            $new_height = $thumb_height;
            $new_width = $width / ($height / $thumb_height);
        } else {
            // If the thumbnail is wider than the image
            $new_width = $thumb_width;
            $new_height = $height / ($width / $thumb_width);
        }

        $thumb = imagecreatetruecolor($thumb_width, $thumb_height);

        imagealphablending($thumb, false);
        imagesavealpha($thumb, true);
        $transparent = imagecolorallocatealpha($thumb, 255, 255, 255, 127);
        imagefilledrectangle($thumb, 0, 0, $thumb_width, $thumb_height, $transparent);

        // Resize and crop
        imagecopyresampled(
            $thumb,
            $image,
            0 - ($new_width - $thumb_width) / 2, // Center the image horizontally
            0 - ($new_height - $thumb_height) / 2, // Center the image vertically
            0,
            0,
            $new_width,
            $new_height,
            $width,
            $height
        );

        switch ($imgInfo[2]) {
            case 1:
                header('Content-Type: image/gif');
                imagegif($thumb, NULL);
                break;
            case 2:
                header('Content-Type: image/jpeg');
                imagejpeg($thumb, NULL);
                break;
            case 3:
                header('Content-Type: image/png');
                imagepng($thumb, NULL);
                break;
            default:
                trigger_error('Failed resize image!', E_USER_WARNING);
                break;
        }

        return $thumb;
    }
    protected function iResize_Browser($url, $thumb_width)
    {
        /*$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
		$data = curl_exec($ch);

		curl_close($ch);*/
        // $url     = str_replace(base_url(), '', $url);
        $data = file_get_contents($url);

        $imgInfo = getimagesize($url);
        $image = imagecreateFROMstring($data);

        $width = imagesx($image);
        $height = imagesy($image);

        $thumb_height = ($thumb_width / $width) * $height;

        $thumb = imagecreatetruecolor($thumb_width, $thumb_height);

        imagealphablending($thumb, false);
        imagesavealpha($thumb, true);
        $transparent = imagecolorallocatealpha($thumb, 255, 255, 255, 127);
        imagefilledrectangle($thumb, 0, 0, $thumb_width, $thumb_height, $transparent);

        imagecopyresampled($thumb, $image, 0, 0, 0, 0, $thumb_width, $thumb_height, $width, $height);

        switch ($imgInfo[2]) {
            case 1:
                header('Content-Type: image/gif');
                imagegif($thumb, NULL);
                break;
            case 2:
                header('Content-Type: image/jpeg');
                imagejpeg($thumb, NULL);
                break;
            case 3:
                header('Content-Type: image/png');
                imagepng($thumb, NULL);
                break;
            default:
                trigger_error('Failed resize image!', E_USER_WARNING);
                break;
        }

        return $thumb;
    }
    public function crop()
    {
        $width      = request()->segment(3) ? request()->segment(3) : '300';
        $height     = request()->segment(4) ? request()->segment(4) : '300';

        if (file_exists('upload/' . request()->segment(5))) {
            $result     = url('/') . '/upload/' . request()->segment(5);
        } else {
            $result    = url('/') . '/themes/default/img/default.png';
        }

        $handle = curl_init($result);
        curl_setopt($handle,  CURLOPT_RETURNTRANSFER, TRUE);
        $response = curl_exec($handle);
        $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);

        $this->iCrop_Browser($result, $width, $height);
        curl_close($handle);
    }
    public function resize()
    {
        $width      = request()->segment(3) ? request()->segment(3) : '300';

        if (file_exists('upload/' . request()->segment(4))) {
            $result     = url('/') . '/upload/' . request()->segment(4);
        } else {
            $result    = url('/') . '/themes/default/img/default.png';
        }

        $handle = curl_init($result);
        curl_setopt($handle,  CURLOPT_RETURNTRANSFER, TRUE);
        $response = curl_exec($handle);
        $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);

        $this->iResize_Browser($result, $width);
        curl_close($handle);
    }
}
