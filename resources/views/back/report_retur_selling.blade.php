@extends('layouts.post.report')

@section('content')
@include('layouts.menu')
<div class="row wrapper border-bottom white-bg page-heading printing">
<div class="col-lg-10">
<h2>Report Retur Pembelian</h2>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="{{url('/')}}">Home</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{url('report/retur_selling/')}}">Report Retur Pembelian</a>
    </li>
    <li class="breadcrumb-item active">
        <strong>All Report Retur Pembelian</strong>
    </li>
</ol>
</div>
<div class="col-lg-2">

</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
<div class="row">
<div class="col-lg-12 padding-none">
    <div class="ibox ">

            <div class="ibox-title printing">
                <h5>Manage Report Retur Pembelian</h5>
            </div>
            <div class="ibox-content">
                <form method="get" action="{{ url('report/retur_selling') }}">
                    @csrf
                <div class="row printing">
                    <div class="col-sm-3 report_pilot">
                        <div class="input-group">
                            <input name="start_date" class="form-control date-picker" autocomplete="off" type="text" value="{{$start_date}}" data-date-format="yyyy-mm-dd" id="date_start" data-validation="date" data-validation-format="yyyy-mm-dd" placeholder="Mulai Tanggal" value="">
                            <span class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-3 report_pilot">
                        <div class="input-group">
                            <input name="end_date" class="form-control date-picker" autocomplete="off" type="text" value="{{$end_date}}" data-date-format="yyyy-mm-dd" id="date_start" data-validation="date" data-validation-format="yyyy-mm-dd" placeholder="Sampai Tanggal" value="">
                            <span class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-1 report_pilot">
                            <button type="submit" name="post-look" value="publish" class="btn col-sm-12 btn-sm btn-primary"> Lihat!</button>
                        </div>
                        <div class="col-sm-1 report_pilot">
                            <div data-toggle="buttons" class="btn-group">
                                <label data-toggle="modal" data-target="#modal_export" class="btn btn-sm btn-white"> <i class="fa fa-download"></i> Download </label>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <button type="button" class="btn btn-default btn-sm btn-print"><i class="fa fa-print"></i> Print</button>
                        </div>
                </div><br>
                </form>
                <div class="table-responsive" id="printarea">
                    <form method="post">
                    @csrf
                    <table class="table table-striped">
                        <tbody>
                             <tr>
                                <th>No </th>
                                <th>Tanggal</th>
                                <th>Kode Barang</th>
                                <th>Nama Barang</th>
                                <th class="text-center">Satuan</th>
                                <th class="text-center">Harga(Rp)</th>
                                <th class="text-center">Jumlah</th>
                                <th class="text-center">Subtotal(Rp)</th>
                                <th class="text-center">Keterangan</th>
                            </tr>
                            @foreach($post_list as $value  => $p)
                            <tr>
                                <td>{{$value+1}} </td>
                                <td>{{$p->retur_tanggal}} </td>
                                <td>{{$p->retur_barang_id}} </td>
                                <td>{{$p->retur_barang_nama}} </td>
                                <td>{{$p->retur_barang_satuan}} </td>
                                <td class="text-right">{{currency($p->retur_harjul)}} </td>
                                <td class="text-center">{{$p->retur_qty}} </td>
                                <td class="text-right">{{currency($p->retur_harjul*$p->retur_qty)}} </td>
                                <td>{{$p->retur_keterangan}} </td>
                            </tr>
                            @endforeach

                        </tbody>
                       </table>

        </form>
    </div>
</div>
</div>
</div>

</div>
</div>
<div class="footer printing">
<div class="float-right">
<strong>
    </strong>
</div>
<div>
<strong>Copyright</strong> {{$copyright}}
</div>
</div>
@endsection
