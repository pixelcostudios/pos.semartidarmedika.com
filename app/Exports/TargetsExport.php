<?php

namespace App\Exports;

use DB;
use Image;

use App\Target;
use App\Outlet;
use App\Helpers\Base;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input as input;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

class TargetsExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        if (Input::get('show') != '') {
            $show = Input::get('show');
        } else {
            $show = 10;
        }
        if (Input::get('outlet_id') != '') {
            $outlet_id      = Input::get('outlet_id');
        } else {
            $outlet_id      = null;
        }
        if (Input::get('year') != '') {
            $year = Input::get('year');
        } else {
            $year = date('Y');
        }

        $outlet_list    = Outlet::orderBy('outlet_name', 'ASC')->get();

        if ($outlet_id == 'all' or $outlet_id == null) {
            $outlet_array   = $outlet_list;
        } else {
            $outlet_array   = Outlet::where('outlet_id', '=', $outlet_id)->orderBy('outlet_name', 'ASC')->get();
        }

        $month_list = array('JANUARI', 'FEBRUARI', 'MARET', 'APRIL', 'MEI', 'JUNI', 'JULI', 'AGUSTUS', 'SEPTEMBER', 'OKTOBER', 'NOVEMBER', 'DESEMBER');

        $i = 0;
        $post_list = array();
        foreach ($outlet_array as $row) {
            $i++;
            $user_array = array(
                'r_no' => $i,
                'r_outlet_name' => $row->outlet_name,
                'r_region'      => $row->outlet_region
            );
            // $outlet_id = $row->outlet_id;

            $array_target = array();
            foreach (range(1, 12) as $row_range) {
                $month = $year . "-" . sprintf('%02d', $row_range);
                $result = Target::where([['target_month', '=', $month], ['outlet_id', '=', $row->outlet_id]])->first();
                $array_target[] = ($result['target']) ? $result['target'] : "0";
            }
            $post_list[]   = array_merge($user_array, $array_target);
        }

        return collect($post_list);//Target::all();
    }
    public function headings(): array
    {
        return [
            'No',
            'Outlet Name',
            'Region',
            'JANUARI',
            'FEBRUARI',
            'MARET',
            'APRIL',
            'MEI',
            'JUNI',
            'JULI',
            'AGUSTUS',
            'SEPTEMBER',
            'OKTOBER',
            'NOVEMBER',
            'DESEMBER'
        ];
    }
}
