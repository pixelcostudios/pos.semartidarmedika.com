<?php

namespace App\Http\Controllers;

use DB;
use Image;

use App\Media;
use App\Post;
use App\Helpers\Base;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input as input;

use Illuminate\Http\Request;

class MediaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        if (Input::get('show') != '') {
            $show = Input::get('show');
        } else {
            $show = 10;
        }
        $post_type = ['images', 'icon', 'thumb', 'background', 'pdf', 'files'];
        $post_list = Media::whereIn('post_type', $post_type)->orderBy('date', 'DESC')->paginate($show)->onEachSide(5);

        if (isset($_POST['post-delete']) && $_POST['post-delete'] != "") {
            for ($i = 0; $i < count(@$_POST['post-check']); $i++) {
                $post_id = @$_POST['post-check'][$i];


                foreach (Media::whereIn('post_type', $post_type)->where([['post_id', '=', $post_id]])->get() as $row_images) {
                    if ($row_images->post_type == 'pdf' or $row_images->post_type == 'files') {
                        @unlink('./upload/files/' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                    } else {
                        @unlink('./upload/' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                        @unlink('./upload/c_' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                        @unlink('./upload/c1_' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                    }
                    DB::table('posts')->where('post_id', '=', $row_images->post_id)->delete();
                }

                DB::table('posts')->where('post_id', '=', $post_id)->delete();
            }
            return redirect('/page/')->with('success', 'Post updated!');
        } elseif (isset($_POST['post-deleted']) && $_POST['post-deleted'] != "") {
            $post_id = $_POST['post_id'];
            $post_type = array('images', 'thumb', 'background', 'pdf', 'files', 'icon');

            $post_type = ['images', 'icon', 'thumb', 'background', 'pdf', 'files'];
            foreach (Media::whereIn('post_type', $post_type)->where([['post_id', '=', $post_id]])->get() as $row_images) {
                if ($row_images->post_type == 'pdf' or $row_images->post_type == 'files') {
                    @unlink('./upload/files/' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                } else {
                    @unlink('./upload/' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                    @unlink('./upload/c_' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                    @unlink('./upload/c1_' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                }
                DB::table('posts')->where('post_id', '=', $row_images->post_id)->delete();
            }

            DB::table('posts')->where('post_id', '=', $post_id)->delete();

            return redirect('/page/')->with('success', 'Post updated!');
        }

        return view('back/media', ['post_list' => $post_list, 'show' => $show, 'copyright' => copyright()]);
    }

    public function search()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        if (Input::get('show') != '') {
            $show = Input::get('show');
        } else {
            $show = 10;
        }
        if (Input::get('query') != '') {
            $search = Input::get('query');
        } else {
            $search = '';
        }

        $post_type = ['images', 'icon', 'thumb', 'background', 'pdf', 'files'];
        $post_list = Media::whereIn('post_type', $post_type)->where('post_title', 'like', '%' . $search . '%')->orWhere('post_content', 'like', '%' . $search . '%')->orWhere('post_summary', 'like', '%' . $search . '%')->orderBy('date', 'DESC')->paginate($show)->onEachSide(5);

        if (isset($_POST['post-delete']) && $_POST['post-delete'] != "") {
            for ($i = 0; $i < count(@$_POST['post-check']); $i++) {
                $post_id = @$_POST['post-check'][$i];


                foreach (Media::whereIn('post_type', $post_type)->where([['post_id', '=', $post_id]])->get() as $row_images) {
                    if ($row_images->post_type == 'pdf' or $row_images->post_type == 'files') {
                        @unlink('./upload/files/' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                    } else {
                        @unlink('./upload/' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                        @unlink('./upload/c_' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                        @unlink('./upload/c1_' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                    }
                    DB::table('posts')->where('post_id', '=', $row_images->post_id)->delete();
                }

                DB::table('posts')->where('post_id', '=', $post_id)->delete();
            }
            return redirect('/page/')->with('success', 'Post updated!');
        } elseif (isset($_POST['post-deleted']) && $_POST['post-deleted'] != "") {
            $post_id = $_POST['post_id'];
            $post_type = array('images', 'thumb', 'background', 'pdf', 'files', 'icon');

            $post_type = ['images', 'icon', 'thumb', 'background', 'pdf', 'files'];
            foreach (Media::whereIn('post_type', $post_type)->where([['post_id', '=', $post_id]])->get() as $row_images) {
                if ($row_images->post_type == 'pdf' or $row_images->post_type == 'files') {
                    @unlink('./upload/files/' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                } else {
                    @unlink('./upload/' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                    @unlink('./upload/c_' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                    @unlink('./upload/c1_' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                }
                DB::table('posts')->where('post_id', '=', $row_images->post_id)->delete();
            }

            DB::table('posts')->where('post_id', '=', $post_id)->delete();

            return redirect('/page/')->with('success', 'Post updated!');
        }

        return view('back/media_search', ['post_list' => $post_list, 'show' => $show, 'search' => $search, 'copyright' => copyright()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Media  $media
     * @return \Illuminate\Http\Response
     */
    public function show(Media $media)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Media  $media
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        $post_edit      = Post::where('post_id', $id)->get();

        return view('back/media_edit', [
            'post_edit' => $post_edit,
            'copyright' => copyright()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Media  $media
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post_data = Media::find($id);
        $post_data->post_slug = ($request->get('post_title') != '') ? strip_i_slug($request->get('post_title')) : '';
        $post_data->post_title = ($request->get('post_title') != '') ? $request->get('post_title') : '';
        $post_data->post_link     = ($request->get('post_link') != '') ? $request->get('post_link') : '';
        $post_data->post_content = ($request->get('post_content') != '') ? $request->get('post_content') : '';
        $post_data->date = ($request->get('date') != '') ? $request->get('date') : date('Y-m-d H:i:s');
        $post_data->status = $request->get('status');
        $post_data->save();

        // $post_edit      = Post::where('post_id', $id)->get();
        $media = Media::where([['post_id', '=', $id]])->get();

        if ($request->hasfile('upload_photo')) {
            foreach ($request->file('upload_photo') as $image) {
                $original_name = $image->getClientOriginalName();
                $file_name = ext_name($original_name);
                $file_ext = ext_filename($original_name);

                $resize_image = Image::make($image->getRealPath());
                $resize_image->resize(150, 150, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(public_path() . '/upload/r_' . strip_i_slug($file_name) . '.' . $file_ext);

                $img_crop = Image::make($image->getRealPath());
                $img_crop->fit(300);

                foreach ($media as $row_images) {
                    if ($row_images->post_type == 'pdf' or $row_images->post_type == 'files') {
                        @unlink('./upload/files/' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                    } else {
                        @unlink('./upload/' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                        @unlink('./upload/c_' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                        @unlink('./upload/c1_' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                    }
                }

                // $img->crop($request->input('w'), $request->input('h'), $request->input('x1'), $request->input('y1'));
                $img_crop->save(public_path() . '/upload/c_' . strip_i_slug($request->get('post_title')) . '.' . $file_ext);

                $image->move(public_path() . '/upload/', strip_i_slug($request->get('post_title')) . '.' . $file_ext);
                // $data[] = $original_name; //Auth::id()

                // $post_data = Post::find($id);
                // $post_data->post_slug = strip_i_slug($file_name);
                // $post_data->post_mime_type = $file_ext;
                // $post_data->save();
            }
        }
        return redirect('/media/edit/' . $id)->with('success', 'Post updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Media  $media
     * @return \Illuminate\Http\Response
     */
    public function destroy(Media $media)
    {
        //
    }
}
