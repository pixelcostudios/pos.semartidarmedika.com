<?php

namespace App\Http\Controllers\API;
use DB;
use App\Province;
use App\District;
use App\Subdistrict;
use App\Village;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input as input;

class LocationController extends Controller
{
    public $successStatus = 200;

    public function province()
    {
        if (Input::get('query') != '') {
            $query = Input::get('query');
            $post_list = Province::where('province', 'like', '%' . $query . '%')->orderBy('province', 'ASC')->get();
        } else {
            $post_list = Province::orderBy('province', 'ASC')->get();
        }

        return response()->json(['status' => 200, 'message' => 'List Province', 'data' => $post_list], $this->successStatus);
    }
    public function district(Request $request)
    {
        if (Input::get('id') != '') {
            $id = Input::get('id');
        } else {
            $id = 0;
        }
        if (Input::get('query') != '') {
            $query = Input::get('query');
            $post_list = District::where('province_id', '=', $id)->where('districts', 'like', '%' . $query . '%')->orderBy('districts', 'ASC')->get();
        } else {
            $post_list = District::where('province_id', '=', $id)->orderBy('districts', 'ASC')->get();
        }

        return response()->json(['status' => 200, 'message' => 'List District', 'data' => $post_list], $this->successStatus);
    }
    public function subdistrict(Request $request)
    {
        if (Input::get('id') != '') {
            $id = Input::get('id');
        } else {
            $id = 0;
        }
        if (Input::get('query') != '') {
            $query = Input::get('query');
            $post_list = Subdistrict::where('districts_id', '=', $id)->where('subdistrict', 'like', '%' . $query . '%')->orderBy('subdistrict', 'ASC')->get();
        } else {
            $post_list = Subdistrict::where('districts_id', '=', $id)->orderBy('subdistrict', 'ASC')->get();
        }

        return response()->json(['status' => 200, 'message' => 'List Subdistrict', 'data' => $post_list], $this->successStatus);
    }
    public function village(Request $request)
    {
        if (Input::get('id') != '') {
            $id = Input::get('id');
        } else {
            $id = 0;
        }
        if (Input::get('query') != '') {
            $query = Input::get('query');
            $post_list = Village::where('subdistrict_id', '=', $id)->where('village', 'like', '%' . $query . '%')->orderBy('village', 'ASC')->get();
        } else {
            $post_list = Village::where('subdistrict_id', '=', $id)->orderBy('village', 'ASC')->get();
        }

        return response()->json(['status' => 200, 'message' => 'List Village', 'data' => $post_list], $this->successStatus);
    }
}
