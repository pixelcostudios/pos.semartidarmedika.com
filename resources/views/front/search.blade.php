@extends('layouts.front.post')
@section('content')
@include('layouts.front.header_post')
<!-- ++++ Most Bold Title ++++ -->

<section class="banner  o-hidden banner-inner about-banner">
    <div class="container">
        <!--banner text-->
        <div class="banner-txt">
            <h1>Penncarian "{{$title}}"</h1>
            <p class="semi-bold"></p>
        <!--end banner text-->
    </div>
</section>
<section class="bg-white o-hidden blog-p2 blog-content">
        <!--blog content row one-->
        <div class="container">
            <div class="row">
                @foreach ($post_list as $key)
                <!--blog content box-->
                <div class="col-xs-12 col-sm-4 margin-bottom-30">
                    <div class="box-content-with-img equalheight is-featured">
                        @foreach (App\Page::where('post_type', 'images')->where([['post_up', '=', $key->post_id]])->skip(0)->take(1)->get() as $p2)
                            <a href="{{url('/')}}/{{$key->post_slug}}"><img src="{{url('/')}}/thumb/crop/350/300/{{$p2->post_slug}}.{{$p2->post_mime_type}}"
                                alt="{{$key->post_title}}" class="img-responsive" width="100%"></a>
                            @endforeach

                        <div class="box-content-text">
                            <p class="gray-text"><span class="icon-calendar-full"></span>{{$key->date}}</p>
                            <h3 class="semi-bold"><a href="{{url('/')}}/{{$key->post_slug}}">{{$key->post_title}}</a></h3>
                            <p class="regular-text">{{substr(strip_tags($key->post_content),0,120)}}</p>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>

        </div>
        <!--end blog content row one-->
    </section>

@include('layouts.front.footer')
@endsection
