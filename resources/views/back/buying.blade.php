@extends('layouts.post.buying')

@section('content')
@include('layouts.menu')
<div class="row wrapper border-bottom white-bg page-heading">
<div class="col-lg-10">
<h2>Pembelian Barang</h2>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="{{url('/')}}">Home</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{url('buying/')}}">Pembelian Barang</a>
    </li>
    <li class="breadcrumb-item active">
        <strong>All Pembelian Barang</strong>
    </li>
</ol>
</div>
<div class="col-lg-2">

</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
<div class="row">
<div class="col-lg-12 padding-none">
    <div class="ibox ">

            <div class="ibox-title">
                <h5>Manage Pembelian Barang </h5>
                <a href="{{ url('buying/add') }}" class="btn btn-default pull-left">Pembelian</a>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-wrench"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <form method="get" action="{{ url('buying') }}">
                    @csrf
                <div class="row printing">
                    <div class="col-sm-3 report_pilot">
                        <div class="input-group">
                            <input name="start_date" class="form-control date-picker-day" autocomplete="off" type="text" value="{{$start_date}}" data-date-format="yyyy-mm-dd" id="date_start" data-validation="date" data-validation-format="yyyy-mm-dd" placeholder="Mulai Tanggal" value="">
                            <span class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-3 report_pilot">
                        <div class="input-group">
                            <input name="end_date" class="form-control date-picker-day" autocomplete="off" type="text" value="{{$end_date}}" data-date-format="yyyy-mm-dd" id="date_start" data-validation="date" data-validation-format="yyyy-mm-dd" placeholder="Sampai Tanggal" value="">
                            <span class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-1 report_pilot">
                            <button type="submit" name="post-look" value="publish" class="btn col-sm-12 btn-sm btn-primary"> Lihat!</button>
                        </div>
                        <div class="col-sm-1 report_pilot">
                            <div data-toggle="buttons" class="btn-group">
                                <label data-toggle="modal" data-target="#modal_export" class="btn btn-sm btn-white"> <i class="fa fa-download"></i> Download </label>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <button type="button" class="btn btn-default btn-sm btn-print"><i class="fa fa-print"></i> Print</button>
                        </div>
                </div><br>
                </form>
                <div class="table-responsive">
                    <form method="post">
                    @csrf
                    <table class="table table-bordered">
                        <thead>
                            <tr>

                                <th width="1%">
                                    <label>
                                        <input type="checkbox" class="colored-green i-checks checkall">
                                        <span class="text"></span>
                                    </label>
                                </th>
                                <th>Detail</th>

                                <th width="9%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($post_list as $p)
                            <tr>
                                <td>
                                    <label style="margin-bottom:0;">
                                        <input type="checkbox" name="post-check[]" value="{{  $p->beli_id }}" class="colored-green">
                                        <span class="text"></span>
                                    </label>
                                </td>
                                <td >
                                    <table class="table sub_table no-border" style="margin-bottom:5px;">
                                        <tr>
                                            <td width="15%">No Faktur</td>
                                            <td width="1%">:</td>
                                            <td>{{$p->faktur}}</td>
                                            <td width="15%">Tanggal</td>
                                            <td width="1%">:</td>
                                            <td>{{$p->beli_tanggal}}</td>
                                            <td>Kasir</td>
                                            <td>:</td>
                                            <td>{{$p->first_name}} {{$p->last_name}}</td>
                                        </tr>
                                        <tr>
                                            <td>Supplier</td>
                                            <td>:</td>
                                            <td>{{$p->suplier_nama}}</td>
                                            <td>Tempo</td>
                                            <td>:</td>
                                            <td>{{$p->beli_tempo}}</td>
                                            <td width="15%"></td>
                                            <td width="1%"></td>
                                            <td></td>
                                        </tr>
                                    </table>
                                    <table class="table sub_table">
                                        <tr>
                                            <td>Item</td>
                                            <td>Kode Barang</td>
                                            <td class="text-center">Qty</td>
                                            <td class="text-center">Price</td>
                                            <td class="text-center">Sub Total</td>
                                        </tr>
                                        @php
                                            $total_quantity = 0;
                                            $total_cost = 0;
                                        @endphp
                                        @foreach (DB::table('buy_details')->leftJoin('items', 'items.barang_id', '=', 'buy_details.d_beli_barang_id')->where('beli_id','=',$p->beli_id)->get() as $key)
                                        <tr>
                                            <td>{{$key->barang_nama}}</td>
                                            <td>{{$key->d_beli_barang_id}}</td>
                                            <td class="text-center">{{$key->d_beli_jumlah}}</td>
                                            <td class="text-center">{{currency($key->d_beli_harga)}}</td>
                                            <td class="text-center">{{currency($key->d_beli_jumlah*$key->d_beli_harga)}}</td>
                                        </tr>
                                        @php
                                            $total_quantity += $key->d_beli_jumlah;
                                            $total_cost += $key->d_beli_harga*$key->d_beli_jumlah;
                                        @endphp
                                        @endforeach
                                        <tr>
                                            <td colspan="4" class="text-right">Total</td>
                                            <td class="text-center">{{currency($total_cost)}}</td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <a class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                            Action
                                        </a>
                                        <ul class="dropdown-menu drop-right">
                                            <!--<li><a href="{{url('/')}}/buying_edit/{{  $p->beli_id }}">Edit</a></li>-->
                                            <li><a class="open-modal_deleted" data-toggle="modal" href="#" data-id="{{  $p->beli_id }}"
                                                    data-target="#modal_deleted">Delete</a></li>
                                        </ul>
                                    </div>
                                    <br><br>
                                    <div class="text-left">
                                    <a target="_blank" href="{{url('/')}}/buying/print/{{$p->beli_id}}?type=dot" style="margin-bottom:10px; display:block;"><i class="fa fa-print"></i> Dot Matrix</a>
                                    <!--<a target="_blank" href="{{url('/')}}/buying/print/{{$p->beli_id}}?type=thermal"><i class="fa fa-print"></i> Thermal</a>-->
                                    </div>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                        @if (count($post_list) > 0)
                                                                <tfoot>
                            <tr>
                                <td colspan="8" class="footable-visible">
                                    <div class="pull-left margin-top-20">
                                        <div class="btn-group">
                                            <div class="btn-group dropup">
                                                <button type="button" class="btn btn-default btn-custom button-sm dropdown-toggle" data-toggle="dropdown"
                                                    aria-expanded="false">
                                                    <i class="fa fa-ellipsis-horizontal"></i> Action
                                                </button>
                                                <ul class="dropdown-menu dropdown-archive">
                                                    <li>
                                                        <button type="button" data-toggle="modal" data-target="#modal_delete" class="btn btn-default btn-sm"><i
                                                                class="fa fa-trash-o"></i>
                                                            Delete</button>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                    @if ($post_list->lastPage() > 1)
                                            <ul class="pagination pull-right">
                                                <li class="page-item"><a href="#" class="page-link">{{ $post_list->total() }} Page</a></li>
                                                <li class="{{ ($post_list->currentPage() == 1) ? ' disabled' : '' }} page-item">
                                                    <a class=" page-link " href="{{ $post_list->url(1) }}&show={{$show}}&start_date={{$start_date}}&end_date={{$end_date}}" aria-label="Previous">
                                                        <span aria-hidden="true">&laquo;</span>
                                                        <span class="sr-only">Previous</span>
                                                    </a>
                                                </li>
                                                @if ( $post_list->currentPage() > 5 )
                                                <li class="page-item">
                                                    <a class="page-link" tabindex="-1">...</a>
                                                </li>
                                                @endif

                                                @for ($i = 1; $i <= $post_list->lastPage(); $i++)
                                                    @if ( ($i > ($post_list->currentPage() - 5)) && ($i < ($post_list->currentPage() + 5)) )
                                                    <li class="{{ ($post_list->currentPage() == $i) ? ' active' : '' }} page-item">
                                                        <a class=" page-link " href="{{ $post_list->url($i) }}&show={{$show}}&start_date={{$start_date}}&end_date={{$end_date}}">{{ $i }}</a>
                                                    </li>
                                                    @endif
                                                @endfor
                                                @if ( $post_list->currentPage() < ($post_list->lastPage() - 4) )
                                                <li class="page-item">
                                                    <a class="page-link" tabindex="-1">...</a>
                                                </li>
                                                @endif

                                                <li class="{{ ($post_list->currentPage() == $post_list->lastPage()) ? ' disabled' : '' }} page-item">
                                                    <a href="{{ $post_list->url($post_list->currentPage()+1) }}&show={{$show}}&start_date={{$start_date}}&end_date={{$end_date}}" class="page-link" aria-label="Next">
                                                        <span aria-hidden="true">&raquo;</span>
                                                        <span class="sr-only">Next</span>
                                                    </a>
                                                </li>
                                                <li class="page-item {{($post_list->currentPage()==$post_list->lastPage())?'disabled':''}}">
                                                    <a class="page-link" href="{{ $post_list->url($post_list->lastPage()) }}">
                                                        Last
                                                    </a>
                                                </li>
                                            </ul>
                                    @endif

                                </td>
                            </tr>
                        </tfoot>
                        @endif
                       </table>

                <div class="modal fade" id="modal_publish">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title">Are you sure ?</h4>

                        </div>
                        <div class="modal-body">
                            <p>Do you want to change this Pembelian Barang to Publish.</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" name="post-publish" value="publish" class="btn btn-primary">Publish</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="modal_draft">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title">Are you sure ?</h4>

                        </div>
                        <div class="modal-body">
                            <p>Do you want to change this Pembelian Barang to Draft.</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" name="post-draft" value="draft" class="btn btn-warning">UnPublish</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="modal_delete">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title">Are you sure ?</h4>

                        </div>
                        <div class="modal-body">
                            <p>Do you want to delete this Pembelian Barang.</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" name="post-delete" value="delete" class="btn btn-danger">Delete</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="modal_deleted">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title">Are you sure ?</h4>

                        </div>
                        <div class="modal-body">
                            <p>Do you want to delete this Pembelian Barang.</p>
                            <input type="hidden" name="post_id" id="post_id" value="" />
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" name="post-deleted" value="delete" class="btn btn-danger">Delete</button>
                        </div>
                    </div>
                </div>
            </div>


        </form>
    </div>
</div>
</div>
</div>

</div>
</div>
<div class="footer">
<div class="float-right">
<strong>
    </strong>
</div>
<div>
<strong>Copyright</strong> {{$copyright}}
</div>
</div>
@endsection
