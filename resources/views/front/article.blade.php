@extends('layouts.front.post_detail')
@section('content')
@include('layouts.front.header')
<!-- ++++ Most Bold Title ++++ -->
<section class="blog-title">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1>Blog</h1>
            </div>
        </div>
    </div>
</section>
<!-- ++++ Most Bold Title ++++ -->
<!-- ++++ blog standard content ++++ -->
<section class="page-section bg-white o-hidden blog-content">
    <div class="container relative">
        <div class="row">
            @foreach ($post_list as $key)
            <!-- Content -->
            <div class="col-sm-8">
                <!-- Post -->
                <!-- Post Title -->
                <h2 class="blog-item-title font-alt">{{$key->post_title}}</h2>
                <!-- Date, Categories, Author, Comments -->
                <div class="blog-item-data"> <a href="blog-details.html#"><i class="icon-calendar-full"></i> {{$key->date}}</a> <span class="separator">&nbsp;</span>
                    @foreach (App\Categories::leftJoin('metas', 'cats.cat_id', '=', 'metas.meta_dest')->where('metas.meta_source', $key->post_id)->where('cat_type', 'post')->get() as $p2)
                    <a href="{{url('/')}}/category/{{$p2->cat_slug}}"><i class="icon-list4"></i> {{$p2->cat_title}}</a> <span class="separator">&nbsp;</span>
                    @endforeach
                    <br class="visible-xs">
                    <a href=""><i class="icon-user"></i> Admin</a> <span class="separator">&nbsp;</span>  </div>
                <!-- Image -->
                @if($key->post_thumb==1)
                    @if(count($gallery)>0)
                        <div class="blog-media">
                            <ul class="clearlist content-slider">
                                @foreach ($gallery as $p2)
                                <li><img src="{{url('/')}}/upload/{{$p2->post_slug}}.{{$p2->post_mime_type}}"
                                    alt="{{$key->post_title}}" ></li>
                                @endforeach
                            </ul>
                        </div>
                        @else
                        <div class="blog-media">
                        @foreach ($gallery as $p2)
                        <img src="{{url('/')}}/upload/{{$p2->post_slug}}.{{$p2->post_mime_type}}"
                                    alt="{{$key->post_title}}" width="100%">
                        @endforeach
                        </div>
                    @endif
                @endif
                <div class="blog-item-body">
                    {!!$key->post_content!!}
                </div>
                <!--blog share-->
                <div class="blog-post-share">
                    <ul class="social-links">
                        <li>Share this post:</li>
                        <li><a href="blog-details.html#"><span class="icon-facebook"></span></a></li>
                        <li><a href="blog-details.html#"><span class="icon-twitter"></span></a></li>
                        <li><a href="blog-details.html#"><span class="icon-instagram"></span></a></li>
                    </ul>
                </div>
                <!--blog share end-->

                <section class="bg-white o-hidden margin-top-15">
                    <h2 class="b-clor">Leave a comment</h2>
                    <hr class="dark-line">
                    <!-- Form -->
                    <div class="customise-form contact-form">
                        <form>
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="form-group customised-formgroup"> <span class="icon-user"></span>
                                        <input class="form-control" placeholder="Name">
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="form-group customised-formgroup"> <span class="icon-envelope"></span>
                                        <input class="form-control" placeholder="Email">
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group customised-formgroup"> <span class="icon-laptop"></span>
                                        <input class="form-control" placeholder="Website">
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group customised-formgroup"> <span class="icon-bubble"></span>
                                        <textarea class="form-control" placeholder="Message"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="btn-wrapper">
                                <button type="submit" class="btn btn-fill">Send comment</button>
                            </div>
                        </form>
                    </div>
                    <!-- End Form -->
                </section>

            </div>
            <!-- End Content -->
            @endforeach
            <!-- Sidebar -->
            <div class="col-sm-4 col-md-3 col-md-offset-1 sidebar">
                <!-- Search Widget -->
                <div class="widget">
                    <form class="form-inline form" method="GET" action="{{url('/')}}/search">
                        @csrf
                        <div class="search-wrap">
                            <button class="search-button animate" type="submit" title="Start Search"> <i class="icon-magnifier"></i> </button>
                            <input type="text" class="form-control search-field" name="query" placeholder="Search...">
                        </div>
                    </form>
                </div>
                <!-- End Search Widget -->
                <!-- Widget -->
                <div class="widget">
                    <h5 class="widget-title font-alt">Informasi Terbaru</h5>
                    <div class="widget-body">
                        <ul class="clearlist widget-posts">
                            @foreach ($recent_list as $row_l)
                            <li class="clearfix">
                                <div class="widget-posts-descr"> <a href="{{url('/')}}/{{$row_l->post_slug}}" title="">{{$row_l->post_title}}</a> {{$row_l->date}} </div>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <!-- End Widget -->
                <!-- Widget
                <div class="widget">
                    <h5 class="widget-title font-alt">Recent Comments</h5>
                    <div class="widget-body">
                        <ul class="clearlist widget-comments">
                            <li> <span>Tony Stark</span> <a href="blog-details.html#" title="">Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat...</a> </li>
                            <li> <span>Tony Stark</span> <a href="blog-details.html#" title="">Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat...</a> </li>
                            <li> <span>Tony Stark</span> <a href="blog-details.html#" title="">Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat...</a> </li>
                        </ul>
                    </div>
                </div>
                End Widget -->
                <!-- Widget -->
                <div class="widget">
                    <h5 class="widget-title font-alt">Categories</h5>
                    <div class="widget-body">
                        <ul class="clearlist widget-menu">
                            @foreach ($category_list as $cat)
                            <li> <a href="{{url('/')}}/category/{{$cat->cat_slug}}" title="">{{$cat->cat_title}}</a> </li>@endforeach
                        </ul>
                    </div>
                </div>
                <!-- End Widget -->
            </div>
            <!-- End Sidebar -->
        </div>
    </div>
</section>
@include('layouts.front.footer')
@endsection
