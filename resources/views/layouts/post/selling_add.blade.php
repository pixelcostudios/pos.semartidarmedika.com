<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link href="../themes/default/css/bootstrap.min.css" rel="stylesheet">
	<link href="../themes/default/font-awesome/css/font-awesome.css" rel="stylesheet">
	<link href="../themes/default/js/plugins/datetime/bootstrap-datetimepicker.css" rel="stylesheet" />
	<link href="../themes/default/css/plugins/select2/select2.min.css" rel="stylesheet">
	<link href="../themes/default/css/animate.css" rel="stylesheet">
	<link href="../themes/default/css/style.css" rel="stylesheet">
	<link href="../themes/default/css/custom.css" rel="stylesheet">

</head>

<body class="top-navigation pace-done">
    <div id="wrapper">

        <div id="page-wrapper" class="gray-bg">
            @yield('content')
        </div>
    </div>

 <!-- Mainly scripts -->
<script src="../themes/default/js/jquery-3.1.1.min.js"></script>
<script src="../themes/default/js/popper.min.js"></script>
<script src="../themes/default/js/bootstrap.js"></script>
<script src="../themes/default/js/plugins/metismenu/jquery.metismenu.js"></script>
<script src="../themes/default/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="../themes/default/js/inspinia.js"></script>
<script src="../themes/default/js/plugins/pace/pace.min.js"></script>

<!-- Select2 -->
<script src="../themes/default/js/plugins/select2/select2.full.min.js"></script>
<script src="../themes/default/js/jquery.price_format.min.js"></script>
<script src="../themes/default/js/onscan.js"></script>
<!--Bootstrap Date Picker-->
<script src="../themes/default/js/plugins/datetime/bootstrap-datetimepicker.min.js"></script>
<script>
$(document).ready(function () {
    $(document).on("input",'.qty' ,function(e) {
        // var qty = e.target.value; var price = $(this).parents('.item-select').find('.price').val();
        var sum = 0;
            $('.price').each(function(){
                var qty = $(this).parents('.item-select').find('.qty').val();
                var discount = $(this).parents('.item-select').find('.discount').val();
                sum += (parseFloat(this.value)*qty)-discount;
            });
        $(".total").val(parseInt(sum));
    });
    $(document).on("input",'.price' ,function(e) {
        var sum = 0;
            $('.price').each(function(){
                var qty = $(this).parents('.item-select').find('.qty').val();
                var discount = $(this).parents('.item-select').find('.discount').val();
                sum += (parseFloat(this.value)*qty)-discount;
            });
        $(".total").val(parseInt(sum));
    });
    $(document).on("input",'.discount' ,function(e) {
        var sum = 0;
        $('.price').each(function(){
            var qty = $(this).parents('.item-select').find('.qty').val();
            var discount = $(this).parents('.item-select').find('.discount').val();
            sum += (parseFloat(this.value)*qty)-discount;
        });
        $(".total").val(parseInt(sum));

        var pay_total   = $(".pay").val();
        if (pay_total=='')
        {
            var pay = 0;
        }
        else
        {
            var pay = pay_total.replace(/[^\d]/g, "");
        }

        var transport_total   = $(".transport").val();
        var transport   = transport_total.replace(/[^\d]/g, "");

        var total_tax = (parseInt(transport)*parseInt(sum))/100;
        $(".return").val(parseInt(pay)-(total_tax+parseInt(sum)));
    });
    $(document).on("input",'.pay' ,function(e) {
        var pay_total   = e.target.value;
        var transport_total   = $(".transport").val();
        var pay         = pay_total.replace(/[^\d]/g, "");
        var transport   = transport_total.replace(/[^\d]/g, "");
        var total       = $(".total").val();
        // console.log(transport);
        // console.log((parseInt(transport)*parseInt(total)/100));
        var total_tax = (parseInt(transport)*parseInt(total))/100;
        $(".return").val(parseInt(pay)-(total_tax+parseInt(total)));
    });
    $(document).on("input",'.transport' ,function(e) {
        var transport_total   = e.target.value;
        var pay_total   = $(".pay").val();
        if (pay_total=='')
        {
            var pay = 0;
        }
        else
        {
            var pay = pay_total.replace(/[^\d]/g, "");
        }
        var transport   = transport_total.replace(/[^\d]/g, "");
        var sum = 0;
        $('.price').each(function(){
            var qty = $(this).parents('.item-select').find('.qty').val();
            var discount = $(this).parents('.item-select').find('.discount').val();
            sum += (parseFloat(this.value)*qty)-discount;
        });

        // var total       = $(".total").val();
        var total_tax = (parseInt(transport)*parseInt(sum))/100;
        console.log(total_tax);
        $(".return").val(parseInt(pay)-(total_tax+parseInt(sum)));
    });
    $('.item_select').on('select2:select', function (e) {
        // var item = $(".item_select"). val();
        var select_val = $(e.currentTarget).val();
        console.log(select_val);
        // $(this).find('.item-select .price').val(select_type);
        var parent = this;
        $.getJSON( "{{url('/')}}/api/web/item_detail?id="+select_val, function( data ) {
            $(parent).parents('.item-select').find('.price').val(data.price);
            $(parent).parents('.item-select').find('.unit').val(data.unit);
            $(parent).parents('.item-select').find('.stock').val(data.stock);
            console.log(data);

            var sum = 0;
            $('.price').each(function(){
                var price = $(this).parents('.item-select').find('.price').val();
                var qty = $(this).parents('.item-select').find('.qty').val();
                var discount = $(this).parents('.item-select').find('.discount').val();
                sum += (parseFloat(price)*qty)-discount;
            });
            $(".total").val(parseInt(sum));
        });
    });
});
$(document).ready(function() {
    function fireTestInput(){
		onScan.simulate(document, document.getElementById("iTestInput").value);
	}

    onScan.attachTo(document, {
        suffixKeyCodes: [13], // enter-key expected at the end of a scan
        reactToPaste: true, // Compatibility to built-in scanners in paste-mode (as opposed to keyboard-mode)
        onScan: function(sCode, iQty) { // Alternative to document.addEventListener('scan')
            // console.log('Scanned: ' + iQty + 'x ' + sCode);
            console.log(sCode);
            // $(".input_fields_wrap").append('<div class="row item-select"> <div class="form-group col-sm-3"> <label>Item</label><br> <select class="form-control item_select" name="post_item[]" data-placeholder="Pilih Item"> </select> </div> <div class="form-group col-sm-2"> <label>Harga</label><br> <input type="text" class="form-control price" placeholder="Price " readonly> </div> <div class="form-group col-sm-1"> <label>Satuan</label><br> <input type="text" class="form-control unit" placeholder="Satuan " readonly> </div> <div class="form-group col-sm-1"> <label>Stock</label><br> <input type="text" class="form-control stock" placeholder="Stock " readonly> </div> <div class="form-group col-sm-2"> <label>Discount</label><br> <input type="text" class="form-control discount" name="post_discount[]" placeholder="Discount" value=""> </div> <div class="form-group col-sm-1"> <label>QTY</label><br> <input type="text" class="form-control qty" name="post_qty[]" placeholder="QTY" required="required" value="1"> </div> <div class="form-group col-sm-1 text-center"> <label>Hapus</label><br> <button class="btn btn-danger remove_field"><i class="fa fa-trash"></i></button> </div> </div>');
            // var parent = '<select class="form-control item_select select2-hidden-accessible" name="post_item[]" data-placeholder="Pilih Item" tabindex="-1" aria-hidden="true"> <option value="BR000001">Holcim-TEST | BR000001</option></select>';
            $.getJSON( "{{url('/')}}/api/web/item_detail?id="+sCode, function( data ) {
                // console.log(data);
                $(".input_fields_wrap").append('<div class="row item-select"> <div class="form-group col-sm-3"> <label>Item</label><br> <select class="form-control item_select" name="post_item[]" data-placeholder="Pilih Item"><option value="'+data.id+'">'+data.text+' | '+data.id+'</option></select> </div> <div class="form-group col-sm-2"> <label>Harga</label><br> <input type="text" class="form-control price" placeholder="Price " readonly value="'+data.price+'"> </div> <div class="form-group col-sm-1"> <label>Satuan</label><br> <input type="text" class="form-control unit" placeholder="Satuan " readonly value="'+data.unit+'"> </div> <div class="form-group col-sm-1"> <label>Stock</label><br> <input type="text" class="form-control stock" placeholder="Stock " readonly value="'+data.stock+'"> </div> <div class="form-group col-sm-2"> <label>Discount</label><br> <input type="text" class="form-control discount" name="post_discount[]" placeholder="Discount" value=""> </div> <div class="form-group col-sm-1"> <label>QTY</label><br> <input type="text" class="form-control qty" name="post_qty[]" placeholder="QTY" required="required" value="1"> </div> <div class="form-group col-sm-1 text-center"> <label>Hapus</label><br> <button class="btn btn-danger remove_field"><i class="fa fa-trash"></i></button> </div> </div>');
                $(parent).parents('.item-select').find('.price').val(data.price);
                $(parent).parents('.item-select').find('.unit').val(data.unit);
                $(parent).parents('.item-select').find('.stock').val(data.stock);

                var sum = 0;
                $('.price').each(function(){
                    var price = $(this).parents('.item-select').find('.price').val();
                    var qty = $(this).parents('.item-select').find('.qty').val();
                    var discount = $(this).parents('.item-select').find('.discount').val();
                    sum += (parseFloat(price)*qty)-discount;
                });
                $(".total").val(parseInt(sum));
            });
        }
    });
});
$(document).ready(function() {
    var max_fields          = 50; //maximum input boxes allowed
    var wrapper             = $(".input_fields_wrap"); //Fields wrapper
    var add_button          = $(".add_field_button"); //Add button ID
    var add_button_custom   = $(".add_field_button_custom"); //Add button ID

    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div class="row item-select"> <div class="form-group col-sm-3"> <label>Item</label><br> <select class="form-control item_select item_select_custom" name="post_item[]" data-placeholder="Pilih Item"> </select> </div> <div class="form-group col-sm-2"> <label>Harga</label><br> <input type="text" class="form-control price" placeholder="Price " readonly name="post_price[]"> </div> <div class="form-group col-sm-1"> <label>Satuan</label><br> <input type="text" class="form-control unit" placeholder="Satuan " readonly name="post_unit[]"> </div> <div class="form-group col-sm-1"> <label>Stock</label><br> <input type="text" class="form-control stock" placeholder="Stock " readonly> </div> <div class="form-group col-sm-2"> <label>Discount</label><br> <input type="text" class="form-control discount" name="post_discount[]" placeholder="Discount" value=""> </div> <div class="form-group col-sm-1"> <label>QTY</label><br> <input type="text" class="form-control qty" name="post_qty[]" placeholder="QTY" required="required" value="1"> </div> <div class="form-group col-sm-1 text-center"> <label>Hapus</label><br> <button class="btn btn-danger remove_field"><i class="fa fa-trash"></i></button> </div> </div>');
            $('.item_select_custom').select2({
                placeholder: "Pilih Item",
                ajax: {
                url: '{{url('/')}}/api/web/item',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                    search: params.term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                cache: false
                }
            });
            $('.item_select_custom').on('select2:select', function (e) {
                var select_val = $(e.currentTarget).val();
                var parent = this;
                $.getJSON( "{{url('/')}}/api/web/item_detail?id="+select_val, function( data ) {
                    console.log(parent);
                    $(parent).parents('.item-select').find('.price').val(data.price);
                    $(parent).parents('.item-select').find('.unit').val(data.unit);
                    $(parent).parents('.item-select').find('.stock').val(data.stock);

                    var sum = 0;
                    $('.price').each(function(){
                        var price = $(this).parents('.item-select').find('.price').val();
                        var qty = $(this).parents('.item-select').find('.qty').val();
                        var discount = $(this).parents('.item-select').find('.discount').val();
                        sum += (parseFloat(price)*qty)-discount;
                    });
                    $(".total").val(parseInt(sum));
                });
            });
			// $(wrapper).append('<div class="row relative"><div class="form-group col-sm-6"><label for="title">Drug </label><input type="text" class="form-control" name="post_drug[]" placeholder="Drug"></div><div class="form-group col-sm-5"><label for="title">QTY </label><input type="number" class="form-control" name="post_qty[]" placeholder="QTY"></div> <div class="form-group col-sm-1 text-center"><label for="title">Delete </label><br><a href="#" class="remove_field"><i class="fa fa-trash"></i></a></div></div>'); //add input box
        }
    });

    var y = 1; //initlal text box count
    $(add_button_custom).click(function(e){ //on add input button click
        e.preventDefault();
        if(y < max_fields){ //max input box allowed
            y++; //text box increment
            $(wrapper).append('<div class="row item-select"> <div class="form-group col-sm-3"> <label>Custom Item</label><br><input type="text" class="form-control item_select" name="post_item[]" placeholder="Custom Item"> </div> <div class="form-group col-sm-2"> <label>Harga</label><br> <input type="text" class="form-control price" placeholder="Price " name="post_price[]"> </div> <div class="form-group col-sm-1"> <label>Satuan</label><br> <input type="text" class="form-control unit" placeholder="Satuan " name="post_unit[]"> </div> <div class="form-group col-sm-1"> <label>Stock</label><br> <input type="text" class="form-control stock" placeholder="Stock " readonly> </div> <div class="form-group col-sm-2"> <label>Discount</label><br> <input type="text" class="form-control discount" name="post_discount[]" placeholder="Discount" value=""> </div> <div class="form-group col-sm-1"> <label>QTY</label><br> <input type="text" class="form-control qty" name="post_qty[]" placeholder="QTY" required="required" value="1"> </div> <div class="form-group col-sm-1 text-center"> <label>Hapus</label><br> <button class="btn btn-danger remove_field"><i class="fa fa-trash"></i></button> </div> </div>');
        }
    });

    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault();
        // $(this).parent('div').remove();
        // $(parent).parents('.item-select').find('.price').val(data.price);
        $(this).parents('.item-select').remove();
        x--;
        console.log(this);
    })
});
</script>
<script type="text/javascript">
$(document).ready(function(){
        $('.total').priceFormat({
            prefix: '',
            //centsSeparator: '',
            centsLimit: 0,
            thousandsSeparator: ','
        });
        $('#jml_uang2').priceFormat({
            prefix: '',
            //centsSeparator: '',
            centsLimit: 0,
            thousandsSeparator: ''
        });
        // $('.transport').priceFormat({
        //     prefix: '',
        //     //centsSeparator: '',
        //     centsLimit: 0,
        //     thousandsSeparator: ','
        // });
        $('.return').priceFormat({
            prefix: '',
            //centsSeparator: '',
            centsLimit: 0,
            thousandsSeparator: ','
        });
        $('.pay').priceFormat({
            prefix: '',
            //centsSeparator: '',
            centsLimit: 0,
            thousandsSeparator: ','
        });
    });
</script>
<script>
    $(document).ready(function () {
        $(".select2").select2();
        $(".item_select").select2({
            placeholder: "Pilih Item",
            ajax: {
            url: '{{url('/')}}/api/web/item',
            type: "get",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                search: params.term
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
            cache: true
            }
        });

	});
    $(document).ready(function() {
    $("#{{request()->segment(1)}}MainNav").addClass('active');
    $("#{{request()->segment(1)}}MainNav .collapse").addClass('in');
    });
	$(".date-picker").datetimepicker({
		format: 'yyyy-mm-dd hh:ii:ss',
		autoclose: true,
	});
</script>

</body>
</html>
