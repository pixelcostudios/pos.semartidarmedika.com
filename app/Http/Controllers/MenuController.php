<?php

namespace App\Http\Controllers;

use DB;
use Image;

use App\Menu;
use App\Post;
use App\Categories;
use App\Helpers\Base;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input as input;

use Illuminate\Http\Request;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        if (Input::get('type') != '') {
            $show = Input::get('type');
        } else {
            $show = 'post';
        }
        if (Input::get('cat_id') != '') {
            $cat_id = Input::get('cat_id');
        } else {
            $cat_id = null;
        }
        if($cat_id!='' AND $cat_id!=null)
        {
            $post_list = Menu::where('position', '=', $cat_id)->orderBy('sort','ASC')->get();
        }
        else {
            $post_list = Menu::orderBy('sort', 'ASC')->get();
        }
        $category_list = Categories::where('cat_type','=','menu')->get();

        return view('back/menu', ['post_list' => $post_list, 'category_list' => $category_list, 'cat_id' => $cat_id,'copyright' => copyright()]);
    }

    public function list()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        if (Input::get('type') != '') {
            $type = Input::get('type');
        } else {
            $type = 'post';
        }

        $data_array = array();
        if($type=='post')
        {
            $post_list       = post_list_recrusive_type($post_type = array('post'), $post_up = 0, $depth = 5, $now = 0);

            foreach ($post_list as $row) {
                $data_array[] = array(
                    'post_id'    => $row['post_id'],
                    'post_title' => $row['post_title'] . ' - ' . $row['post_type'],
                    'post_slug'  => $row['post_slug']
                );
            }
            echo json_encode($data_array);
        }
        elseif($type=='pages')
        {
            $post_list       = post_list_recrusive_type($post_type = array('pages'), $post_up = 0, $depth = 5, $now = 0);

            foreach ($post_list as $row) {
                $data_array[] = array(
                    'post_id'    => $row['post_id'],
                    'post_title' => $row['post_title'] . ' - ' . $row['post_type'],
                    'post_slug'  => $row['post_slug']
                );
            }
            echo json_encode($data_array);
        }
        elseif($type=='project')
        {
            $post_list       = post_list_recrusive_type($post_type = array('project'), $post_up = 0, $depth = 5, $now = 0);

            foreach ($post_list as $row) {
                $data_array[] = array(
                    'post_id'    => $row['post_id'],
                    'post_title' => $row['post_title'] . ' - ' . $row['post_type'],
                    'post_slug'  => $row['post_slug']
                );
            }
            echo json_encode($data_array);
        }
        elseif($type=='hotel')
        {
            $post_list       = post_list_recrusive_type($post_type = array('hotel'), $post_up = 0, $depth = 5, $now = 0);

            foreach ($post_list as $row) {
                $data_array[] = array(
                    'post_id'    => $row['post_id'],
                    'post_title' => $row['post_title'] . ' - ' . $row['post_type'],
                    'post_slug'  => $row['post_slug']
                );
            }
            echo json_encode($data_array);
        }
        elseif($type=='category')
        {
            $cat_list       = cat_list_recrusive_type($cat_type = array('post', 'banner'), $cat_up = 0, $depth = 5, $now = 0);

            foreach ($cat_list as $row) {
                $data_array[] = array(
                    'cat_id'    => $row['cat_id'],
                    'cat_title' => $row['cat_title'] . ' - ' . $row['cat_type'],
                    'cat_slug'  => $row['cat_slug']
                );
            }
            echo json_encode($data_array);
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function show(Menu $menu)
    {
        //
    }

    public function add(Request $request)
    {
        // $id             = '';
        // $label          = 'Penemuan';
        // $link           = '';
        // $position       = '4';

        // $label          = $request->get('label');
        // $link           = $request->get('link');
        // $category       = $request->get('category');
        // $pages          = $request->get('pages');
        // $post           = $request->get('post');
        // $project        = $request->get('project');
        // $hotel          = $request->get('hotel');
        $arr = array();
        if ($request->get('id') != '') {
            $id             = $request->get('id');
            $label          = $request->get('label');
            $link           = $request->get('link');
            $position       = $request->get('position');
            DB::table('menus')
                ->where('id', $id)
                ->update(['label' => $label, 'link' => $link, 'position'=> $position]);

            $arr['type']        = 'edit';
            $arr['label']       = $label;
            $arr['link']        = $link;
            $arr['id']          = $id;
            $arr['position']    = $position;
        } else {
            $label          = $request->get('label');
            $link           = $request->get('link');
            $position       = $request->get('position');
            $category       = $request->get('category');
            $pages          = $request->get('pages');
            $post           = $request->get('post');
            $project        = $request->get('project');
            $hotel          = $request->get('hotel');

            if ($category!='') {
                $categoryDB = Categories::whereIn('cat_id', explode(',', $category))->get();// $this->model_menu->me_ca_list_in(explode(',', $category));
                foreach ($categoryDB as $row) {
                    DB::table('menus')->insert([
                        ['type'=>'category', 'label' => $row->cat_title, 'link' => $row->cat_slug, 'position'=> $position]
                    ]);
                    $id = DB::getPdo()->lastInsertId();
                    $arr['menu'] = '<li class="dd-item dd3-item" data-id="' . $id . '" >
                                <div class="dd-handle dd3-handle">Drag</div>
                                <div class="dd3-content"><span id="label_show' . $id . '">' . $label . '</span>
                                    <span class="span-right">/<span id="link_show' . $id . '">' . $link . '</span> &nbsp;&nbsp;
                                        <a class="edit-button" id="' . $id . '" label="' . $label . '" link="' . $link . '" ><i class="fa fa-pencil"></i></a>
                                        <a class="del-button" id="' . $id . '"><i class="fa fa-trash"></i></a>
                                    </span>
                                </div>';

                    $arr['type'] = 'add';
                }
            }
            if ($pages!='') {
                $pagesDB = Post::where('post_type','=','pages')->whereIn('post_id', explode(',', $pages))->get();//$this->model_menu->me_pg_list_in('pages', explode(',', $pages));
                foreach ($pagesDB as $row) {
                    DB::table('menus')->insert([
                        ['type' => $row->post_type, 'label' => $row->post_title, 'link' => $row->post_slug, 'position' => $position]
                    ]);
                    $id = DB::getPdo()->lastInsertId();
                    $arr['menu'] = '<li class="dd-item dd3-item" data-id="' . $id . '" >
                                <div class="dd-handle dd3-handle">Drag</div>
                                <div class="dd3-content"><span id="label_show' . $id . '">' . $label . '</span>
                                    <span class="span-right">/<span id="link_show' . $id . '">' . $link . '</span> &nbsp;&nbsp;
                                        <a class="edit-button" id="' . $id . '" label="' . $label . '" link="' . $link . '" ><i class="fa fa-pencil"></i></a>
                                        <a class="del-button" id="' . $id . '"><i class="fa fa-trash"></i></a>
                                    </span>
                                </div>';

                    $arr['type'] = 'add';
                }
            }
            if ($post!='') {
                $postDB = Post::where('post_type', '=', 'post')->whereIn('post_id', explode(',', $post))->get();
                foreach ($postDB as $row) {
                    DB::table('menus')->insert([
                        ['type' => $row->post_type, 'label' => $row->post_title, 'link' => $row->post_slug, 'position' => $position]
                    ]);
                    $id = DB::getPdo()->lastInsertId();
                    $arr['menu'] = '<li class="dd-item dd3-item" data-id="' . $id . '" >
                                <div class="dd-handle dd3-handle">Drag</div>
                                <div class="dd3-content"><span id="label_show' . $id . '">' . $label . '</span>
                                    <span class="span-right">/<span id="link_show' . $id . '">' . $link . '</span> &nbsp;&nbsp;
                                        <a class="edit-button" id="' . $id . '" label="' . $label . '" link="' . $link . '" ><i class="fa fa-pencil"></i></a>
                                        <a class="del-button" id="' . $id . '"><i class="fa fa-trash"></i></a>
                                    </span>
                                </div>';

                    $arr['type'] = 'add';
                }
            }
            if ($project!='') {
                $postDB = Post::where('post_type', '=', 'project')->whereIn('post_id', explode(',', $project))->get();
                foreach ($postDB as $row) {
                    DB::table('menus')->insert([
                        ['type' => $row->post_type, 'label' => $row->post_title, 'link' => $row->post_slug, 'position' => $position]
                    ]);
                    $id = DB::getPdo()->lastInsertId();
                    $arr['menu'] = '<li class="dd-item dd3-item" data-id="' . $id . '" >
                                <div class="dd-handle dd3-handle">Drag</div>
                                <div class="dd3-content"><span id="label_show' . $id . '">' . $label . '</span>
                                    <span class="span-right">/<span id="link_show' . $id . '">' . $link . '</span> &nbsp;&nbsp;
                                        <a class="edit-button" id="' . $id . '" label="' . $label . '" link="' . $link . '" ><i class="fa fa-pencil"></i></a>
                                        <a class="del-button" id="' . $id . '"><i class="fa fa-trash"></i></a>
                                    </span>
                                </div>';

                    $arr['type'] = 'add';
                }
            }
            if ($hotel!='') {
                $postDB = Post::where('post_type', '=', 'hotel')->whereIn('post_id', explode(',', $hotel))->get();
                foreach ($postDB as $row) {
                    DB::table('menus')->insert([
                        ['type' => $row->post_type, 'label' => $row->post_title, 'link' => $row->post_slug, 'position' => $position]
                    ]);
                    $id = DB::getPdo()->lastInsertId();
                    $arr['menu'] = '<li class="dd-item dd3-item" data-id="' . $id . '" >
                                <div class="dd-handle dd3-handle">Drag</div>
                                <div class="dd3-content"><span id="label_show' . $id . '">' . $label . '</span>
                                    <span class="span-right">/<span id="link_show' . $id . '">' . $link . '</span> &nbsp;&nbsp;
                                        <a class="edit-button" id="' . $id . '" label="' . $label . '" link="' . $link . '" ><i class="fa fa-pencil"></i></a>
                                        <a class="del-button" id="' . $id . '"><i class="fa fa-trash"></i></a>
                                    </span>
                                </div>';

                    $arr['type'] = 'add';
                }
            }
            if ($label != '') {
                DB::table('menus')->insert([
                    ['label' => $label, 'link' => $link, 'position' => $position]
                ]);
                $id = DB::getPdo()->lastInsertId();

                $arr['menu'] = '<li class="dd-item dd3-item" data-id="' . $id . '" >
                                <div class="dd-handle dd3-handle">Drag</div>
                                <div class="dd3-content"><span id="label_show' . $id . '">' . $label . '</span>
                                    <span class="span-right">/<span id="link_show' . $id . '">' . $link . '</span> &nbsp;&nbsp;
                                        <a class="edit-button" id="' . $id . '" label="' . $label . '" link="' . $link . '" ><i class="fa fa-pencil"></i></a>
                                        <a class="del-button" id="' . $id . '"><i class="fa fa-trash"></i></a>
                                    </span>
                                </div>';

                $arr['type'] = 'add';
            }
        }
        print json_encode($arr);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function edit(Menu $menu)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        // sleep(1);

        // if (isset($_POST)) {

        //     DB::table('menus')
        //         ->where('id', $request->get('pk'))
        //         ->update(['menu_position' => $request->get('value')]);

        // } else {
        //     header('HTTP/1.0 400 Bad Request', true, 400);
        //     echo "This field is required!";
        // }
        $data               = json_decode(request()->get('data'));
        $readbleArray       = parseJsonArray($data, $parent = 0);

        $i = 0;
        foreach ($readbleArray as $row) {
            $i++;
            $parent         = $row['parent'];
            $sort           = $i;

            DB::table('menus')
                ->where('id', $row['id'])
                ->update(['parent' => $parent, 'sort'=>$sort]);
        }
    }

    public function delete_parent(Request $request)
    {
        $parent = $request->get('id');

        if(DB::table('menus')->where('parent','=', $parent)->count()>0)
        {
            DB::table('menus')->where('parent', '=', $parent)->delete();
        }
        else
        {
            DB::table('menus')->where('id', '=', $parent)->delete();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function destroy(Menu $menu)
    {
        //
    }
}
