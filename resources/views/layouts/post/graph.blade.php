<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link href="{{ url('/') }}/themes/default/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/themes/default/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="{{ url('/') }}/themes/default/css/animate.css" rel="stylesheet">
    <link href="{{ url('/') }}/themes/default/css/plugins/select2/select2.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/themes/default/js/plugins/datetime/bootstrap-datetimepicker.css" rel="stylesheet" />
    <link href="{{ url('/') }}/themes/default/js/plugins/datetime/bootstrap-datepicker3.min.css" rel="stylesheet" />
    <!-- Toastr style -->
    <link href="{{ url('/') }}/themes/default/css/style.css" rel="stylesheet">
    <link href="{{ url('/') }}/themes/default/css/custom.css" rel="stylesheet">
<style>
    @media print {
        /* Hide everything in the body when printing... */
        .printing { display: none; }
        .ibox-content {
            padding: 0;
            border: 0;
        }
        .border-bottom {
            border-bottom: 0;
        }
        .wrapper-content {
            padding: 0;
        }
        /* ...except our special div. */
        body.printing #print-me { display: block; }
    }

    @media screen {
        /* Hide the special layer from the screen. */

        #print-me { display: none; }
    }
    </style>
</head>

<body class="top-navigation pace-done">
    <div id="wrapper">

        <div id="page-wrapper" class="gray-bg">
            @yield('content')
        </div>
    </div>

    <!-- Mainly scripts -->
<script src="{{ url('/') }}/themes/default/js/jquery-3.1.1.min.js"></script>
<script src="{{ url('/') }}/themes/default/js/bootstrap.js"></script>
<script src="{{ url('/') }}/themes/default/js/plugins/metismenu/jquery.metismenu.js"></script>
<script src="{{ url('/') }}/themes/default/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Check -->
<script src="{{ url('/') }}/themes/default/js/check.js"></script>

<script src="{{ url('/') }}/themes/default/js/plugins/select2/select2.full.min.js"></script>
<script src="{{ url('/') }}/themes/default/js/plugins/validation/jquery.form-validator.min.js"></script>

<script src="{{ url('/') }}/themes/default/js/plugins/highcharts/highcharts.js"></script>

<script src="{{ url('/') }}/themes/default/js/plugins/datetime/bootstrap-datetimepicker.min.js"></script>
<script src="{{ url('/') }}/themes/default/js/plugins/datetime/bootstrap-datepicker.min.js"></script>
<script src="{{ url('/') }}/themes/default/js/plugins/dataTables/datatables.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#report').highcharts({
        chart: {
            type: 'line',
            margin: 75,
            options3d: {
                enabled: false,
                alpha: 10,
                beta: 25,
                depth: 70
            }
        },
        title: {
            text: '{{$title}}',
            style: {
                    fontSize: '18px',
                    fontFamily: 'Verdana, sans-serif'
            }
        },
        subtitle: {
           text: '',
           style: {
                    fontSize: '15px',
                    fontFamily: 'Verdana, sans-serif'
            }
        },
        plotOptions: {
            column: {
                depth: 25
            }
        },
        credits: {
            enabled: false
        },
        xAxis: {
            categories:  {!!json_encode($category_list)!!} },
        exporting: {
            enabled: false
        },
        yAxis: {
            title: {
                text: 'Total {{$title_tooltip}}'
            },
        },
        tooltip: {
             formatter: function() {
                 return 'Total {{$title_tooltip}} <b>' + this.x + '</b> Adalah <b>' + Highcharts.numberFormat(this.y,0) + '</b>  ';
             }
          },
        series: [{
            name: '{{$title_legend}}',
            data: {{json_encode($value_list)}},
            shadow : true,
            dataLabels: {
                enabled: true,
                color: '#045396',
                align: 'center',
                formatter: function() {
                     return Highcharts.numberFormat(this.y, 0);
                }, // one decimal
                y: 0, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
});
</script>
    <script type="text/javascript">
    $(document).ready(function() {
        $(".btn-print").click(function () {
            $("#printarea").show();
            window.print();
        });
    });
    </script>
    <script>
        $(document).ready(function() {
            $(".date-picker").datetimepicker({
                minView: 2,
                format: 'yyyy-mm-dd',
                autoclose: true,
            });
            $(".date-picker-month").datepicker({
                startView: 1,
                minViewMode: 1,
                autoclose: true
            });
            $.validate({
                modules: 'location, date, security, file'
            });
        });
    </script>
<script>
    $(document).ready(function() {
    $(".select_list").select2();
    $("#{{request()->segment(1)}}MainNav").addClass('active');
    $("#{{request()->segment(1)}}MainNav .collapse").addClass('in');
    });
	$(document).on("click", ".open-modal_deleted", function () {
		var mypost_id = $(this).data('id');
		$(".modal-body #post_id").val(mypost_id);
		$('#modal_deleted').modal('show');
	});
</script>
</body>
</html>
