<?php
namespace App\Http\Controllers;

use DB;
use Image;

use App\Post;
use App\Search;
use App\Categories;
use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input as input;

class SearchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Input::get('query') != '') {
            $query = Input::get('query');
        } else {
            $query = '';
        }
        $post_list = Post::where('post_title','like', '%'.$query.'%')->orWhere('post_content', 'like', '%' . $query . '%')->orWhere('post_summary', 'like', '%' . $query . '%')->orderBy('date', 'DESC')->paginate(9);

        $recent_list    = Post::leftJoin('metas', 'posts.post_id', '=', 'metas.meta_source')->whereIn('meta_dest', array('3,4'))->where('posts.post_type', 'post')->orderBy('date', 'DESC')->limit(5)->get();
        $category_list  = Categories::where('cats.cat_type', 'post')->where('cats.cat_up', 0)->orderBy('cat_date', 'DESC')->get();
        $setting_list   = Setting::where('setting_type', 'meta')->where('setting_status', '1')->get();
        $online_list    = Post::where('post_up', '=', '42')->get();
        $offline_list   = Post::where('post_up', '=', '43')->get();

        $facebook   = Setting::where('setting_name', '=', 'facebook')->limit('1')->value('setting_value');
        $youtube   = Setting::where('setting_name', '=', 'youtube')->limit('1')->value('setting_value');
        $instagram   = Setting::where('setting_name', '=', 'instagram')->limit('1')->value('setting_value');

        $menu = display_menu(5);

        foreach ($setting_list as $key => $value) {
            $setting[] = $value->setting_value;
        }
        $meta_title     = ($setting) ? $setting[2] : '';
        $meta_key       = ($setting) ? $setting[1] : '';
        $meta_desc      = ($setting) ? $setting[0] : '';

        $data_array = [
            'post_list'     => $post_list,
            'recent_list'   => $recent_list,
            'title'         => $query,
            'menu'          => $menu,
            'description'   => '',
            'keyword'       => '',
            'online_list'   => $online_list,
            'offline_list'  => $offline_list,
            'category_list' => $category_list,
            'facebook'      => $facebook,
            'youtube'       => $youtube,
            'instagram'     => $instagram,
            'setting'       => array('meta_title' => $meta_title, 'meta_keyword' => $meta_key, 'meta_desc' => $meta_desc),
            'url'           => request()->segment(1)
        ];

        return view('front/search', $data_array);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Search  $search
     * @return \Illuminate\Http\Response
     */
    public function show(Search $search)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Search  $search
     * @return \Illuminate\Http\Response
     */
    public function edit(Search $search)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Search  $search
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Search $search)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Search  $search
     * @return \Illuminate\Http\Response
     */
    public function destroy(Search $search)
    {
        //
    }
}
