@extends('layouts.categories.categories')

@section('content')
@include('layouts.menu')
<div class="row wrapper border-bottom white-bg page-heading">
<div class="col-lg-10">
<h2>Merek Barang</h2>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="{{url('/')}}">Home</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{url('categories/')}}?type={{ $type }}">Merek Barang</a>
    </li>
    <li class="breadcrumb-item active">
        <strong>All Merek Barang</strong>
    </li>
</ol>
</div>
<div class="col-lg-2">

</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
<div class="row">
<div class="col-lg-12">
    <div class="ibox ">

            <div class="ibox-title">
                <h5>Manage Merek Barang </h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-wrench"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="col-sm-3">
                        <form method="get" action="{{ url('categories/search') }}">
                    @csrf
                    <input type="hidden" name="type" value="{{$type}}">
                        <div class="input-group">
                            <input type="text" name="query" placeholder="Search" class="input-sm form-control">
                            <span class="input-group-btn">
                                <button type="submit" name="Pixel_Search" class="btn btn-sm btn-primary"> Go!</button> </span></div>
                        </form>
                    </div>
                    <div class="col-sm-6 m-b-xs">
                        <div data-toggle="buttons" class="btn-group">
                            <label onclick="location.href='categories/add?type={{ $type }}'" class="btn btn-sm btn-primary">
                                <i class="fa fa-plus"></i> Add New Merk Barang </label>
                            <!--<label class="btn btn-sm btn-white active"> <input type="radio" id="option2" name="options"> Week </label>
                                                    <label class="btn btn-sm btn-white"> <input type="radio" id="option3" name="options"> Month </label>-->
                        </div>
                    </div>
                    <div class="col-sm-3 m-b-xs">

                        <select id="select-show" onchange="window.location = jQuery('#select-show option:selected').val();" class="input-sm form-control input-s-sm inline float-right category-show">
                            <option  value="{{url('/')}}/categories/?type={{$type}}&show=10" @if($show=='10')selected @endif>10</option>
                            <option  value="{{url('/')}}/categories/?type={{$type}}&show=25" @if($show=='25')selected @endif>25</option>
                            <option  value="{{url('/')}}/categories/?type={{$type}}&show=50" @if($show=='50')selected @endif>50</option>
                            <option  value="{{url('/')}}/categories/?type={{$type}}&show=100" @if($show=='100')selected @endif>100</option>
                        </select>
                    </div>
                </div>
                <div class="table-responsive">
                    <form method="post">
                    @csrf
                    <table class="table table-striped">
                        <thead>
                            <tr>

                                <th width="1%">
                                    <label>
                                        <input type="checkbox" class="colored-green i-checks checkall">
                                        <span class="text"></span>
                                    </label>
                                </th>
                                <th>Title </th>
                                <th>Type </th>
                                <th>Date</th>
                                <th width="7%">Status</th>
                                <th width="7%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($categories_list as $p)
                            <tr>
                                <td>
                                    <label>
                                        <input type="checkbox" name="post-check[]" value="{{  $p->cat_id }}" class="colored-green">
                                        <span class="text"></span>
                                    </label>
                                </td>
                                <td><a href="{{url('/')}}/categories/edit/{{$p->cat_id}}?type={{$p->cat_type}}">{{$p->cat_title}}</a> &nbsp; &nbsp; <a target="_blank" href="category/{{  $p->cat_slug }}"><i class="fa fa-search"></i></a></td>
                                <td>
                                   {{$p->cat_type}}
                                </td>
                                <td>
                                   {{$p->cat_date}}
                                </td>
                                <td>
                                    @if($p->cat_status=='1')
                                        <span class="label label-primary">Publish</span>
                                        @else
                                        <span class="label label-warning">Draft</span>
                                    @endif
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <a class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                            Action
                                        </a>
                                        <ul class="dropdown-menu drop-right">
                                            <li><a href="{{url('/')}}/categories/edit/{{  $p->cat_id }}">Edit</a></li>
                                            <li><a class="open-modal_deleted" data-toggle="modal" href="#" data-id="{{  $p->cat_id }}"
                                                    data-target="#modal_deleted">Delete</a></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                            @foreach(cat_list_recrusive($p->cat_type, $p->cat_id, $depth = 5, $now = 1) as $p_sub)
                            <tr>
                                <td>
                                    <label>
                                        <input type="checkbox" name="post-check[]" value="{{ $p_sub['cat_id'] }}" class="colored-green">
                                        <span class="text"></span>
                                    </label>
                                </td>
                                <td>
                                    @foreach (App\Post::where('post_type', 'cat_images')->where([['post_up', '=', $p_sub['cat_id']]])->skip(0)->take(1)->get() as $p2)
                                    <img src="{{url('/')}}/thumb/crop/40/30/{{$p2->post_slug}}.{{$p2->post_mime_type}}"
                                        width="40" alt="{{$p2->post_title}}">
                                    @endforeach
                                </td>
                                <td><a href="{{url('/')}}/categories/edit/{{$p_sub['cat_id']}}">{{$p_sub['cat_title']}}</a> &nbsp; &nbsp; <a target="_blank" href="category/{{  $p_sub['cat_slug'] }}"><i class="fa fa-search"></i></a></td>
                                <td>
                                   {{$p_sub['cat_type']}}
                                </td>
                                <td>
                                   {{$p_sub['cat_date']}}
                                </td>
                                <td>
                                    @if($p_sub['cat_status']=='1')
                                        <span class="label label-primary">Publish</span>
                                        @else
                                        <span class="label label-warning">Draft</span>
                                    @endif
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <a class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                            Action
                                        </a>
                                        <ul class="dropdown-menu drop-right">
                                            <li><a href="{{url('/')}}/categories/edit/{{ $p_sub['cat_id'] }}">Edit</a></li>
                                            <li><a class="open-modal_deleted" data-toggle="modal" href="#" data-id="{{ $p_sub['cat_id'] }}"
                                                    data-target="#modal_deleted">Delete</a></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                            @endforeach

                        </tbody>
                        @if (count($categories_list) > 0)
                        <tfoot>
                            <tr>
                                <td colspan="7" class="footable-visible">
                                    <div class="pull-left margin-top-20">
                                        <div class="btn-group">
                                            <div class="btn-group dropup">
                                                <button type="button" class="btn btn-default btn-custom button-sm dropdown-toggle" data-toggle="dropdown"
                                                    aria-expanded="false">
                                                    <i class="fa fa-ellipsis-horizontal"></i> Action
                                                </button>
                                                <ul class="dropdown-menu dropdown-archive">
                                                    <li class="margin-bottom-3">
                                                        <button type="button" data-toggle="modal" data-target="#modal_publish" class="btn btn-default btn-sm"><i
                                                                class="fa fa-check"></i>
                                                            Publish</button>
                                                    </li>
                                                    <li class="margin-bottom-3">
                                                        <button type="button" data-toggle="modal" data-target="#modal_draft" class="btn btn-default btn-sm"><i
                                                                class="fa fa-pencil-square-o"></i>
                                                            UnPublish</button>
                                                    </li>
                                                    <li>
                                                        <button type="button" data-toggle="modal" data-target="#modal_delete" class="btn btn-default btn-sm"><i
                                                                class="fa fa-trash-o"></i>
                                                            Delete</button>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                    @if ($categories_list->lastPage() > 1)
                                            <ul class="pagination pull-right">
                                                <li class="page-item"><a href="#" class="page-link">{{ $categories_list->total() }} Post</a></li>
                                                <li class="{{ ($categories_list->currentPage() == 1) ? ' disabled' : '' }} page-item">
                                                    <a class=" page-link " href="{{ $categories_list->url(1) }}&type={{$type}}&show={{$show}}" aria-label="Previous">
                                                        <span aria-hidden="true">&laquo;</span>
                                                        <span class="sr-only">Previous</span>
                                                    </a>
                                                </li>
                                                @if ( $categories_list->currentPage() > 5 )
                                                <li class="page-item">
                                                    <a class="page-link" tabindex="-1">...</a>
                                                </li>
                                                @endif

                                                @for ($i = 1; $i <= $categories_list->lastPage(); $i++)
                                                    @if ( ($i > ($categories_list->currentPage() - 5)) && ($i < ($categories_list->currentPage() + 5)) )
                                                    <li class="{{ ($categories_list->currentPage() == $i) ? ' active' : '' }} page-item">
                                                        <a class=" page-link " href="{{ $categories_list->url($i) }}&type={{$type}}&show={{$show}}">{{ $i }}</a>
                                                    </li>
                                                    @endif
                                                @endfor
                                                @if ( $categories_list->currentPage() < ($categories_list->lastPage() - 4) )
                                                <li class="page-item">
                                                    <a class="page-link" tabindex="-1">...</a>
                                                </li>
                                                @endif

                                                <li class="{{ ($categories_list->currentPage() == $categories_list->lastPage()) ? ' disabled' : '' }} page-item">
                                                    <a href="{{ $categories_list->url($categories_list->currentPage()+1) }}&type={{$type}}&show={{$show}}" class="page-link" aria-label="Next">
                                                        <span aria-hidden="true">&raquo;</span>
                                                        <span class="sr-only">Next</span>
                                                    </a>
                                                </li>
                                                <li class="page-item {{($categories_list->currentPage()==$categories_list->lastPage())?'disabled':''}}">
                                                    <a class="page-link" href="{{ $categories_list->url($categories_list->lastPage()) }}">
                                                        Last
                                                    </a>
                                                </li>
                                            </ul>
                                    @endif

                                </td>
                            </tr>
                        </tfoot>
                        @endif
                       </table>

                <div class="modal fade" id="modal_publish">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title">Are you sure ?</h4>

                        </div>
                        <div class="modal-body">
                            <p>Do you want to change this Categories to Publish.</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" name="post-publish" value="publish" class="btn btn-primary">Publish</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="modal_draft">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title">Are you sure ?</h4>

                        </div>
                        <div class="modal-body">
                            <p>Do you want to change this Categories to Draft.</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" name="post-draft" value="draft" class="btn btn-warning">UnPublish</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="modal_delete">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title">Are you sure ?</h4>

                        </div>
                        <div class="modal-body">
                            <p>Do you want to delete this Categories.</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" name="post-delete" value="delete" class="btn btn-danger">Delete</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="modal_deleted">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title">Are you sure ?</h4>

                        </div>
                        <div class="modal-body">
                            <p>Do you want to delete this Categories.</p>
                            <input type="hidden" name="post_id" id="post_id" value="" />
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" name="post-deleted" value="delete" class="btn btn-danger">Delete</button>
                        </div>
                    </div>
                </div>
            </div>


        </form>
    </div>
</div>
</div>
</div>

</div>
</div>
<div class="footer">
<div class="float-right">
<strong>
    </strong>
</div>
<div>
<strong>Copyright</strong> {{$copyright}}
</div>
</div>
@endsection
