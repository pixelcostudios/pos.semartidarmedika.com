@extends('layouts.post.post')

@section('content')
@include('layouts.menu')
<div class="row wrapper border-bottom white-bg page-heading printing">
<div class="col-lg-10">
<h2>Report Barang</h2>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="{{url('/')}}">Home</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{url('report/item/')}}">Report Barang</a>
    </li>
    <li class="breadcrumb-item active">
        <strong>All Report Barang</strong>
    </li>
</ol>
</div>
<div class="col-lg-2">

</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
<div class="row">
<div class="col-lg-12 padding-none">
    <div class="ibox ">

            <div class="ibox-title printing">
                <h5 class="title-export">Manage Report Barang</h5>
                <a href="{{url('/')}}/report/item_download" class="btn btn-default float-right download-xls"> <i class="fa fa-download"></i> Download </a>
                <button type="button" class="btn btn-default float-right btn-print"><i class="fa fa-print"></i> Print</button>
            </div>
            <div class="ibox-content">
                <div class="table-responsive" id="printarea">
                    <form method="post">
                    @csrf
                    <table class="table table-striped">
                        <tbody>

                            @foreach($post_list as $p)
                            <tr>
                                <th colspan="7">{{$p->cat_title}} </th>
                            </tr>
                            <tr>
                                <th>No</th>
                                <th>Kode Barang </th>
                                <th>Nama Barang </th>
                                <th>Satuan </th>
                                <th>Harga Pokok </th>
                                <th>Harga Jual </th>
                                <th>Stok</th>
                            </tr>
                            @foreach (App\Item::where('barang_kategori_id','=',$p->cat_id)->get() as $value  => $key)
                            <tr>
                                <td>{{$value+1}} </td>
                                <td>{{$key->barang_id}} </td>
                                <td>{{$key->barang_nama}} </td>
                                <td>{{$key->barang_satuan}} </td>
                                <td>{{$key->barang_harpok}} </td>
                                <td>{{$key->barang_harjul}} </td>
                                <td>{{ceil($key->barang_stok)}} </td>
                            </tr>
                            @endforeach
                            @endforeach

                        </tbody>
                       </table>

        </form>
    </div>
</div>
</div>
</div>

</div>
</div>
<div class="footer printing">
<div class="float-right">
<strong>
    </strong>
</div>
<div>
<strong>Copyright</strong> {{$copyright}}
</div>
</div>
@endsection
