<!doctype html>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>{{ config('app.name', 'Laravel') }}</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="themes/price/css/bootstrap.min.css" rel="stylesheet">
<link href="themes/price/font-awesome/css/font-awesome.css" rel="stylesheet">
<link href="themes/price/css/plugins/select2/select2.min.css" rel="stylesheet">
<link href="themes/price/js/plugins/datetime/bootstrap-datetimepicker.css" rel="stylesheet" />
<link href="themes/price/js/plugins/datetime/bootstrap-datepicker3.min.css" rel="stylesheet" />
<link href="themes/price/css/animate.css" rel="stylesheet">
<link href="themes/price/css/style.css" rel="stylesheet">
</head>
<body>

@yield('content')

<!-- JS here -->
<script src="themes/price/js/jquery-3.1.1.min.js"></script>
<script src="themes/price/js/bootstrap.js"></script>
<script src="themes/price/js/index.js"></script>
<script src="themes/price/js/plugins/select2/select2.full.min.js"></script>
<script src="themes/price/js/plugins/validation/jquery.form-validator.min.js"></script>
<script src="themes/price/js/plugins/datetime/bootstrap-datetimepicker.min.js"></script>
<script src="themes/price/js/plugins/datetime/bootstrap-datepicker.min.js"></script>
<!-- ChartJS-->
<script src="themes/price/js/plugins/chartJs/Chart.min.js"></script>
<!--<script src="themes/price/js/demo/chartjs-demo.js"></script>-->
<script>
    $(function () {
        $.getJSON( "{{url('/')}}/api/maps_districts?districts=2&commoditys=15&date=2020-01-10", function( data ) {
            // var rows = $.map(data, function(value, index) {
            //     return "{month:'" + value.month + "',local:" + value.local + ", national:" + value.national + "}";
            // });
            // var data_array = rows.join(',');
            // console.log(rows.join(','));

            $("#q_districts").html(data.districts);
            $("#q_commodity").html(data.commoditys);
            $("#q_price").html("Rp. "+data.price);
            $("#q_price_status").html(data.price_status);

            var lineData = {
                labels: data.graph.date,
                datasets: [

                    {
                        label: "Harga",
                        backgroundColor: 'rgba(26,179,148,0.5)',
                        borderColor: "rgba(26,179,148,0.7)",
                        pointBackgroundColor: "rgba(26,179,148,1)",
                        pointBorderColor: "#fff",
                        data: data.graph.price
                    }
                ]
            };

            var lineOptions = {
                responsive: true,
            };

            var ctx = document.getElementById("lineChart").getContext("2d");
            new Chart(ctx, {type: 'line', data: lineData, options: {
                scales:
                {
                    yAxes: [{
                        display: false,
                        ticks: {
                            display: false //this will remove only the label
                        }
                    }]
                }
            }});
        });
});

    $(document).ready(function () {
        $(".select_list").select2();
        $(".districts").select2({
        placeholder: "Pilih Kabupaten / Kota",
        ajax: {
        url: '{{url('/')}}/api/districts',
        type: "get",
        dataType: 'json',
        delay: 250,
        data: function (params) {
            return {
            search: params.term
            };
        },
        processResults: function (response) {
            return {
                results: response
            };
        },
        cache: true
        }
        });
        $(".commoditys").select2({
            placeholder: "Pilih Komoditi",
            ajax: {
            url: '{{url('/')}}/api/commoditys',
            type: "get",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                search: params.term
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
            cache: true
            }
        });
        $('.btn-go').click(function(){
            console.log($(".commoditys"). val());
            console.log($(".districts"). val());
            console.log($(".date-picker"). val());
        });
        $(".date-picker").datetimepicker({
            minView: 2,
            format: 'yyyy-mm-dd',
            autoclose: true,
        });
        $(".date-picker-month").datepicker({
            startView: 1,
            minViewMode: 1,
            autoclose: true
        });
    });
</script>
</body>
</html>
