@extends('layouts.post.post_edit')

@section('content')
@include('layouts.menu')
<div class="row wrapper border-bottom white-bg page-heading">
<div class="col-lg-10">
<h2>Barang</h2>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="{{ url('home') }}">Home</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ url('item/manage') }}">Barang</a>
    </li>
    <li class="breadcrumb-item active">
        <strong>All Barang</strong>
    </li>
</ol>
</div>
<div class="col-lg-2">

</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    @foreach($post_edit as $p)
            <form name="myform" method="post" action="{{ url('item/update') }}/{{$p->barang_id}}" enctype="multipart/form-data" class="form" data-async>
                @csrf
				<div class="row">
					<div class="col-lg-12 padding-none">
						<div class="ibox ">
							<div class="ibox-title">
								<h5>Barang Editor</h5>
								<a href="{{ url('item/add') }}" class="btn btn-default pull-left">Add New Barang</a>
								<button type="submit" name="Pixel_Save" class="btn btn-primary float-right">Save Barang</button>
							</div>
							<div class="ibox-content">
								<div class="form-group">
									<label for="title">Kode Barang  </label>
									<input type="text" class="form-control" name="barang_id" placeholder="Barang ID " value="{{$p->barang_id}}" >
                                </div>
                                <div class="form-group">
									<label for="title">Nama Barang </label>
									<input type="text" class="form-control" name="barang_nama" value="{{$p->barang_nama}}" required="required">
								</div>
								<div class="row">
									<div class="form-group col-lg-6 col-sm-6 col-xs-12 multi_select clearfix">
										<label for="category">Merek Barang</label><br>
										<select id="select2" class="form-control margin-bottom-10 select2"name="barang_kategori_id">
											@foreach($category_list as $p_cat)
											<option value="{{$p_cat['cat_id']}}" @if($p_cat['cat_id']==$p->barang_kategori_id) selected @endif>{{$p_cat['cat_title']}}</option>
											@endforeach
										</select>
									</div>
									<div class="form-group col-lg-6 col-sm-6 col-xs-12 multi_select clearfix">
										<label for="category">Satuan</label><br>
										<select id="category" class="form-control margin-bottom-10 select2" name="barang_satuan">
											@foreach($unit_list as $p_cat)
											<option value="{{$p_cat['unit_nama']}}" @if($p_cat['unit_nama']==$p->barang_satuan) selected @endif>{{$p_cat['unit_nama']}}</option>
											@endforeach
										</select>
									</div>
								</div>
                                <div class="form-group" style="display: none;">
									<label for="title">No Shading </label>
									<input type="text" class="form-control" name="barang_no_shading" value="{{$p->barang_no_shading}}">
                                </div>
                                <div class="form-group">
									<label for="title">Harga Pokok</label>
									<input type="text" class="form-control" name="barang_harpok" value="{{$p->barang_harpok}}" required="required">
                                </div>
                                <div class="form-group">
									<label for="title">Harga (Eceran)</label>
									<input type="text" class="form-control" name="barang_harjul" value="{{$p->barang_harjul}}">
                                </div>
                                <div class="form-group">
									<label for="title">Harga (Grosir)</label>
									<input type="text" class="form-control" name="barang_harjul_grosir" value="{{$p->barang_harjul_grosir}}">
                                </div>
                                <div class="form-group">
									<label for="title">Stock </label>
									<input type="text" class="form-control" name="barang_stok" value="{{$p->barang_stok}}" required="required">
                                </div>
                                <div class="form-group">
									<label for="title">Minimal Stock </label>
									<input type="text" class="form-control" name="barang_min_stok" value="{{$p->barang_min_stok}}" required="required">
								</div>

                                <div class="clear"></div>
                                <div class="relative input_fields_wrap">
                                <div class="text-center margin-buttton-10">
                                    <button type="button" class="btn btn-primary add_product_price add_field_button"><i class="fa fa-plus"></i></button>
                                </div>
                                @foreach ($discount_list as $key)
                                    <div class="form-group row deleted">
                                        <div class="col-sm-5">
                                            <label for="title">Value </label>
                                            <input name="value_{{$key['discount_id']}}" class="form-control" type="number" placeholder="Minimal..." value="{{$key['discount_value']}}">
                                        </div>
                                        <div class="col-sm-5">
                                            <label for="title">Price </label>
                                            <input type="number" class="form-control" name="price_{{$key['discount_id']}}" placeholder="Price..." value="{{$key['discount_price']}}">
                                        </div>
                                        <div class="col-sm-2 text-center"><label for="title">Delete </label><br><button type="button" data-id="{{$key['discount_id']}}" class="btn btn-default remove_button"><i class="fa fa-trash"></i></button></div>
                                    </div>
                                @endforeach
                            </div>
							</div>
						</div>
					</div>
                </div>

            </form>
            @endforeach
		</div>
<div class="footer">
<div class="float-right">
<strong>
    </strong>
</div>
<div>
<strong>Copyright</strong> {{$copyright}}
</div>
</div>
@endsection
