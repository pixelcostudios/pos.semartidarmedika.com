<?php

namespace App\Http\Controllers;

use DB;
use Image;
use App\Post;
use App\Member;

use App\Helpers\Base;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input as input;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (!Auth::check()) {
            return redirect('/panel');
        }
        $post_count         = Post::where('post_type','=','post')->count();
        $pages_count        = Post::where('post_type', '=', 'pages')->count();
        $slideshow_count    = Post::where('post_type', '=', 'slideshow')->count();
        $member_count       = Member::count();
        return view('back/home', ['copyright' => copyright(), 'post_count'=>$post_count, 'pages_count' => $pages_count, 'slideshow_count' => $slideshow_count, 'member_count' => $member_count]);
    }
}
