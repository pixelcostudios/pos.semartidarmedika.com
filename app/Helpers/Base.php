<?php
function ext_name($filepath)
{
    preg_match('/[^?]*/', $filepath, $matches);
    $string = $matches[0];
    $pattern = preg_split('/\./', $string, -1, PREG_SPLIT_OFFSET_CAPTURE);
    $lastdot = $pattern[count($pattern) - 1][1];
    $filename = basename(substr($string, 0, $lastdot - 1));
    return $filename;
}
function ext_filename($filepath)
{
    preg_match('/[^?]*/', $filepath, $matches);
    $string = $matches[0];

    $pattern = preg_split('/\./', $string, -1, PREG_SPLIT_OFFSET_CAPTURE);

    # check if there is any extension
    if (count($pattern) == 1) {
        $ext = 'none';
        //exit;
    } elseif (count($pattern) > 1) {
        $filenamepart = $pattern[count($pattern) - 1][0];
        preg_match('/[^?]*/', $filenamepart, $matches);
        $ext = str_replace('com/', '', $matches[0]);
    }
    return $ext;
}
function strip_i_name($result)
{
    $string = strtolower($result);
    $a = array('+', '%2F%', ' ', '.jpg', '.png', '.gif', '.bmp');
    $b = array(' ', ' ', '-', '', '', '', '');
    $string = str_replace($a, $b, $string);
    $slug = preg_replace('/[^a-z0-9-]/', '', $string);
    $c = array('----', '---', '--');
    $d = array('-', '-', '');
    $slug = str_replace($c, $d, $slug);
    return $slug;
}
function strip_i_slug($result)
{
    $string = strtolower($result);
    $a = array('+', '%2F%', ' ', ')', '(','|');
    $b = array(' ', ' ', '-','',' ',' ');
    $string = str_replace($a, $b, $string);
    $slug = preg_replace('/[^a-z0-9-]/', '', $string);
    $c = array('----', '---', '--');
    $d = array('-', '-', '-');
    $slug = str_replace($c, $d, $slug);
    return $slug;
}
function strip_i_title($result)
{
    $a = array('+', '%2F%', '_', '-', '.jpg', '.png', '.gif', '.bmp', '    ', '   ', '  ', '.');
    $b = array(' ', ' ', ' ', ' ', '', '', '', '', ' ', ' ', ' ', ' ');
    $result = str_replace($a, $b, $result);
    $result = ucwords(preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', $result));
    return $result;
}
function post_list_recrusive($post_type, $post_up, $depth, $now)
{
    $data = array();
    foreach (DB::table('posts')->where([['post_type', '=', $post_type], ['post_up', '=', $post_up]])->orderBy('date', 'DESC')->get() as $row) {
        for ($i = 0; $i < $now; $i++)
            $row->post_title = '| - ' . $row->post_title;
        $data[] = [
            'post_id'       => $row->post_id,
            'post_title'    => $row->post_title,
            'post_slug'     => $row->post_slug,
            'status'        => $row->status,
            'date'          => $row->date,
        ];
        if ($depth > 0) {
            $data2 = array();
            foreach (DB::table('posts')->where([['post_type', '=', $post_type], ['post_up', '=', $row->post_id]])->orderBy('date', 'DESC')->get() as $row2) {
                for ($i = 0; $i < $now; $i++)
                    $row2->post_title = '| - ' . $row2->post_title;
                $data2[] = [
                    'post_id'       => $row2->post_id,
                    'post_title'    => '| - ' . $row2->post_title,
                    'post_slug'     => $row2->post_slug,
                    'status'        => $row2->status,
                    'date'          => $row2->date,
                ];
            }
            $data = array_merge($data, $data2);
        }
    }
    return $data;
}
function post_list_recrusive_type($post_type, $post_up, $depth, $now)
{
    $data = array();
    foreach (DB::table('posts')->whereIn('post_type', $post_type)->where('post_up', '=', $post_up)->orderBy('date', 'DESC')->get() as $row) {
        for ($i = 0; $i < $now; $i++)
            $row->post_title = '| - ' . $row->post_title;
        $data[] = [
            'post_id'       => $row->post_id,
            'post_title'    => $row->post_title,
            'post_slug'     => $row->post_slug,
            'post_type'     => $row->post_type,
            'status'        => $row->status,
            'date'          => $row->date,
        ];
        if ($depth > 0) {
            $data2 = array();
            foreach (DB::table('posts')->whereIn('post_type', $post_type)->where('post_up', '=', $row->post_id)->orderBy('date', 'DESC')->get() as $row2) {
                for ($i = 0; $i < $now; $i++)
                    $row2->post_title = '| - ' . $row2->post_title;
                $data2[] = [
                    'post_id'       => $row2->post_id,
                    'post_title'    => '| - ' . $row2->post_title,
                    'post_slug'     => $row2->post_slug,
                    'post_type'     => $row2->post_type,
                    'status'        => $row2->status,
                    'date'          => $row2->date,
                ];
            }
            $data = array_merge($data, $data2);
        }
    }
    return $data;
}
function cat_list_recrusive($cat_type, $cat_up, $depth, $now)
{
    $data = array();
    foreach (DB::table('cats')->where([['cat_type', '=', $cat_type], ['cat_up', '=', $cat_up]])->get() as $row) {
        for ($i = 0; $i < $now; $i++)
            $row->cat_title = '| - ' . $row->cat_title;
        $data[] = [
            'cat_id'          => $row->cat_id,
            'cat_type'        => $row->cat_type,
            'cat_title'       => $row->cat_title,
            'cat_slug'        => $row->cat_slug,
            'cat_status'      => $row->cat_status,
            'cat_date'        => $row->cat_date,
        ];
        if ($depth > 0) {
            $data2 = array();
            foreach (DB::table('cats')->where([['cat_type', '=', $cat_type], ['cat_up', '=', $row->cat_id]])->get() as $row2) {
                for ($i = 0; $i < $now; $i++)
                    $row2->cat_title = '| - ' . $row2->cat_title;
                $data2[] = [
                    'cat_id'          => $row2->cat_id,
                    'cat_type'        => $row2->cat_type,
                    'cat_title'       => '| - ' . $row2->cat_title,
                    'cat_slug'        => $row2->cat_slug,
                    'cat_status'      => $row2->cat_status,
                    'cat_date'        => $row2->cat_date,
                ];
            }
            $data = array_merge($data, $data2);
        }
    }
    return $data;
}
function cat_list_recrusive_type($cat_type, $cat_up, $depth, $now)
{
    $data = array();
    foreach (DB::table('cats')->whereIn('cat_type',  $cat_type)->where('cat_up', '=', $cat_up)->get() as $row) {
        for ($i = 0; $i < $now; $i++)
            $row->cat_title = '| - ' . $row->cat_title;
        $data[] = [
            'cat_id'          => $row->cat_id,
            'cat_type'        => $row->cat_type,
            'cat_title'       => $row->cat_title,
            'cat_slug'        => $row->cat_slug,
            'cat_status'      => $row->cat_status,
            'cat_date'        => $row->cat_date,
        ];
        if ($depth > 0) {
            $data2 = array();
            foreach (DB::table('cats')->whereIn('cat_type', $cat_type)->where('cat_up', '=', $row->cat_id)->get() as $row2) {
                for ($i = 0; $i < $now; $i++)
                    $row2->cat_title = '| - ' . $row2->cat_title;
                $data2[] = [
                    'cat_id'          => $row2->cat_id,
                    'cat_type'        => $row2->cat_type,
                    'cat_title'       => '| - ' . $row2->cat_title,
                    'cat_slug'        => $row2->cat_slug,
                    'cat_status'      => $row2->cat_status,
                    'cat_date'        => $row2->cat_date,
                ];
            }
            $data = array_merge($data, $data2);
        }
    }
    return $data;
}
function in_group($user_id)
{
    return DB::table('users_groups')->join('groups', 'groups.id', '=', 'users_groups.group_id')->WHERE('users_groups.user_id',  $user_id)->value('name');
}
function facture_number()
{
    $sells = DB::table('sells')->where('jual_tanggal', 'like', '%'.date('Y-m-d') . '%')->orderby('jual_id', 'DESC')->skip(0)->take(1)->value('jual_nofak');
    // $kd = "";
    if ($sells != '') {
        // foreach ($sells as $k) {
        //     $tmp = ((int) $k->kd_max);
        //     $kd = substr("%06", $tmp) + 1;
        // }
        $kd = date('dmY') . substr($sells, 8) + 1;
    } else {
        $kd = date('dmY') . "001";
    }
    return $kd;
}
function custom_number()
{
    $sells = DB::table('sells')->where('jual_tanggal', 'like', '%' . date('Y-m-d') . '%')->orderby('jual_id', 'DESC')->skip(0)->take(1)->value('jual_nofak');
    // $kd = "";
    if ($sells != '') {
        $kd = date('Ymd') . substr($sells, 8) + 1;
    } else {
        $kd = date('Ymd') . "001";
    }
    return $kd;
}
function buying_number()
{
    $sells = DB::table('buys')->where('beli_tanggal', 'like', date('Y-m-d') . '%')->orderby('beli_nofak', 'DESC')->skip(0)->take(1)->value('beli_nofak');
    // $kd = "";
    if ($sells!='') {
        // foreach ($sells as $k) {
        //     $tmp = ((int) $k->kd_max);
        //     $kd = substr("%06", $tmp) + 1;
        // }
        $kd = date('dmy') . substr($sells, 6)+1;
    } else {
        $kd = date('dmy')."001";
    }
    return $kd;
}
function get_menu($items, $class = 'dd-list')
{
    $html = "<ol class=\"" . $class . "\" id=\"menu-id\">";

    foreach ($items as $key => $value) {
        $html .= '<li class="dd-item dd3-item" data-id="' . $value['id'] . '" >
	                    <div class="dd-handle dd3-handle">Drag</div>
	                    <div class="dd3-content"><span id="label_show' . $value['id'] . '">' . $value['label'] . ' | ' . $value['type'] . '</span>
	                        <span class="span-right"><span id="link_show' . $value['id'] . '">' . $value['link'] . '</span> &nbsp;&nbsp;
	                            <a class="edit-button" id="' . $value['id'] . '" position="' . $value['position'] . '" label="' . $value['label'] . '" link="' . $value['link'] . '" ><i class="fa fa-pencil"></i></a>
	                            <a class="del-button" id="' . $value['id'] . '"><i class="fa fa-trash"></i></a></span>
	                    </div>';
        if (array_key_exists('child', $value)) {
            $html .= get_menu($value['child'], 'child');
        }
        $html .= "</li>";
    }
    $html .= "</ol>";

    return $html;
}
function parseJsonArray($jsonArray, $parent = 0)
{
    $return = array();
    if ($jsonArray != '')
    {
        foreach ($jsonArray as $subArray) {
            $returnSubSubArray = array();
            if (isset($subArray->children)) {
                $returnSubSubArray = parseJsonArray($subArray->children, $subArray->id);
            }

            $return[]     = array('id' => $subArray->id, 'parent' => $parent);
            $return     = array_merge($return, $returnSubSubArray);
        }
    }
    return $return;
}
function copyright()
{
    $setting = DB::table('settings')->where('setting_name','copyright')->value('setting_value');
    return $setting;
}
function imploadValue($types)
{
    $strTypes = implode(",", $types);
    return $strTypes;
}
function explodeValue($types)
{
    $strTypes = explode(",", $types);
    return $strTypes;
}
function random_code()
{
    return rand(1111, 9999);
}
function currency($n)
{
    // $n = (0 + str_replace(",", "", $n));
    // return number_format($n, 0, ',', '.');//str_replace(",", ".", number_format($n));
    return number_format($n, 0, ',', '.');
}
function remove_special_char($text)
{
    $t = $text;
    $specChars = array(
        ' ' => '-',    '!' => '',    '"' => '',
        '#' => '',    '$' => '',    '%' => '',
        '&amp;' => '',    '\'' => '',   '(' => '',
        ')' => '',    '*' => '',    '+' => '',
        ',' => '',    '₹' => '',    '.' => '',
        '/-' => '',    ':' => '',    ';' => '',
        '<' => '',    '=' => '',    '>' => '',
        '?' => '',    '@' => '',    '[' => '',
        '\\' => '',   ']' => '',    '^' => '',
        '_' => '',    '`' => '',    '{' => '',
        '|' => '',    '}' => '',    '~' => '',
        '-----' => '-',    '----' => '-',    '---' => '-',
        '/' => '',    '--' => '-',   '/_' => '-',

    );
    foreach ($specChars as $k => $v) {
        $t = str_replace($k, $v, $t);
    }
    return $t;
}
// function multi_unique($src)
// {
//     $output = array_map(
//         "unserialize",
//         array_unique(array_map("serialize", $src))
//     );
//     return $output;
// }
function multi_unique($array)
{
    foreach ($array as $k => $na)
        $new[$k] = serialize($na);
    $uniq = array_unique($new);
    foreach ($uniq as $k => $ser)
        $new1[$k] = unserialize($ser);
    return ($new1);
}

function unique_multidim_array($array, $key)
{
    $temp_array = array();
    $i = 0;
    $key_array = array();

    foreach ($array as $val) {
        if (!in_array($val[$key], $key_array)) {
            $key_array[$i] = $val[$key];
            $temp_array[$i] = $val;
        }
        $i++;
    }
    return $temp_array;
}
function super_unique($array)
{
    $result = array_map("unserialize", array_unique(array_map("serialize", $array)));

    foreach ($result as $key => $value) {
        if (is_array($value)) {
            $result[$key] = super_unique($value);
        }
    }

    return $result;
}
function array_iunique($array)
{
    $lowered = array_map('strtolower', $array);
    return array_intersect_key($array, array_unique($lowered));
}
function array_has_duplicates(array $array)
{
    $uniq = array_unique($array);
    return count($uniq) != count($array);
}
function cvf_convert_object_to_array($data) {

    if (is_object($data)) {
        $data = get_object_vars($data);
    }

    if (is_array($data)) {
        return array_map(__FUNCTION__, $data);
    }
    else {
        return $data;
    }
}
function time_elapsed_string($datetime, $full = false)
{
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'tahun',
        'm' => 'bulan',
        'w' => 'minggu',
        'd' => 'hari',
        'h' => 'jam',
        'i' => 'menit',
        's' => 'detik',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? '' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' yang lalu' : 'baru saja';
}
function Stand_Deviation($arr)
{
    $num_of_elements = count($arr);

    $variance = 0.0;

    // calculating mean using array_sum() method
    $average = array_sum($arr) / $num_of_elements;

    foreach ($arr as $i) {
        // sum of squares of differences between
        // all numbers and means.
        $variance += pow(($i - $average), 2);
    }

    return (float) sqrt($variance / $num_of_elements);
}
function ArrayReplace($Array, $Find, $Replace)
{
    if (is_array($Array)) {
        foreach ($Array as $Key => $Val) {
            if (is_array($Array[$Key])) {
                $Array[$Key] = ArrayReplace($Array[$Key], $Find, $Replace);
            } else {
                if ($Key == $Find) {
                    $Array[$Key] = $Replace;
                }
            }
        }
    }
    return $Array;
}
function array_search_id($search_value, $array, $id_path) {

    if(is_array($array) && count($array) > 0) {

        foreach($array as $key => $value) {

            $temp_path = array();//$id_path;

            // Adding current key to search path
            array_push($temp_path, $key);

            // Check if this value is an array
            // with atleast one element
            if(is_array($value) && count($value) > 0) {
                $res_path = array_search_id(
                        $search_value, $value, $temp_path);

                if ($res_path != null) {
                    return $res_path;
                }
            }
            else if($value == $search_value) {
                return join("", $temp_path);
            }
        }
    }

    return null;
}
function array_search_ids($search_value, $array, $id_path)
{

    if (is_array($array) && count($array) > 0) {

        foreach ($array as $key => $value) {

            $temp_path = $id_path;

            // Adding current key to search path
            array_push($temp_path, $key);

            // Check if this value is an array
            // with atleast one element
            if (is_array($value) && count($value) > 0) {
                $res_path = array_search_id(
                    $search_value,
                    $value,
                    $temp_path
                );

                if ($res_path != null) {
                    return $res_path;
                }
            } else if ($value == $search_value) {
                return join(" --> ", $temp_path);
            }
        }
    }
    return null;
}
function menu_list_position($position, $parent)
{
    return DB::table('menus')->where('position', $position)->where('parent', $parent)->orderby('sort', 'ASC')->get();
}
function menu_list_position_count($position, $parent)
{
    return DB::table('menus')->where('position', $position)->where('parent', $parent)->count();
}
function display_menu($position)
{
    $menu_list = menu_list_position($position, 0);
    $html = "<ul class=\"nav navbar-nav navbar-right\">";
    foreach ($menu_list as $row_key) {
        $category_sub = menu_list_position($position, $row_key->id);
        switch ($row_key->type) {
            case 'pages':
            case 'post':
            case 'hotel':
                $link    = url('/') . '/' . $row_key->link ;
                break;

            case 'category':
                $link    = url('/') . '/' . 'category/' . $row_key->link ;
                break;

            default:
                if (strpos($row_key->link, "http://") !== false) {
                    $link    = $row_key->link;
                } else {
                    $link    = ($row_key->link == '') ? url('/') : url('/') .'/'. $row_key->link ;
                }
                break;
        }
        if (menu_list_position_count($position, $row_key->id) > 0) {
            $html .= "<li class=\"dropdown\"><a href=\"#\" class=\"dropdown-toggle js-activated\" data-toggle=\"dropdown\">" . $row_key->label . " <i class=\"fa fa-angle-down\" aria-hidden=\"true\"></i></a>
				<ul class=\"dropdown-menu\">";
            foreach ($category_sub as $row_subkey) {
                switch ($row_subkey->type) {
                    case 'pages':
                    case 'post':
                    case 'hotel':
                        $link    = url('/') . '/' . $row_subkey->link ;
                        break;

                    case 'category':
                        $link    = url('/') . '/' . 'category/' . $row_subkey->link ;
                        break;

                    default:
                        if (strpos($row_subkey->link, "http://") !== false) {
                            $link    = $row_subkey->link;
                        } else {
                            $link    = ($row_subkey->link == '') ? url('/') : url('/') . '/' . $row_subkey->link ;
                        }
                        break;
                }
                $html .= "<li><a href=\"" . $link . "\">" . $row_subkey->label . "</a> </li>";
            }
            $html .= "</ul>
				</li>";
        } else {
            if ($row_key->label == 'Home') {
                $html .= "<li><a class=\"active\" href=\"" . url('/') . "\">" . $row_key->label . "</a></li>";
            } else {
                $html .= "<li><a  href=\"" . $link . "\">" . $row_key->label . "</a></li>";
            }
        }
    }
    $html .= "</ul>";
    return $html;
}
