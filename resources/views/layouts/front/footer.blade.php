<!-- ++++ footer ++++ -->
<footer id="footer">
<!--scroll to top-->
<a class="top-btn page-scroll" href="#top"><span class="icon-chevron-up b-clor regular-text text-center"></span></a>
<!--end scroll to top-->

<!--footer content-->
<div class="light-ash-bg">
    <div class="container">
        <ul>
            <li>
                <!-- footer left content-->
                <ul>
                    <li>
                        <a href="{{url('/')}}"><img src="{{url('/')}}/themes/modern/images/logo2.png" alt="logo" class="img-responsive logo" width="150"></a>
                    </li>
                    <li>
                        <p class="extra-small-text">&copy; 2020</p>
                    </li>
                    <li>
                        <p class="extra-small-text">LKP Sari Prabumulih.</p>
                    </li>
                </ul>
                <!--end footer left content-->
            </li>
            <li>
                <!--footer service list-->
                <ul>
                    <li style="margin-bottom:20px"><span class="regular-text text-color-light">Program</span></li>
                    @foreach ($online_list as $key)
                    <li><a href="{{url('/')}}/{{$key->post_slug}}" class="regular-text text-color-light">{{$key->post_title}}</a></li>
                    @endforeach
                    @foreach ($offline_list as $key)
                    <li><a href="{{url('/')}}/{{$key->post_slug}}" class="regular-text text-color-light">{{$key->post_title}}</a></li>
                    @endforeach
                </ul>
                <!--end footer service list-->
            </li>
            <li>
                <!--footer Resources list-->
                <ul>
                    <li><a class="regular-text text-color-light">Jam Buka</a></li>
                    <li><small>Senin : 08:00 AM – 18.00 PM</small></li>
                    <li><small>Selasa : 08:00 AM – 18.00 PM</small></li>
                    <li><small>Rabu : 08:00 AM – 18.00 PM</small></li>
                    <li><small>Kamis : 08:00 AM – 18.00 PM</small></li>
                    <li><small>Jumat : 08:00 AM – 18.00 PM</small></li>
                    <li><small>Sabtu : 08:00 AM – 18.00 PM</small></li>
                </ul>
                <!--end footer Resources list-->
            </li>
            <li class="big-width">
                <!--footer social media list-->
                <ul class="list-inline">
                    <li>
                        <p class="regular-text text-color-light">Get in Touch</p>
                        <ul class="social-links">
                            <li><a href="{{$facebook}}"><span class="icon-facebook"></span></a></li>
                            <li><a href="{{$youtube}}"><span class="icon-youtube"></span></a></li>
                            <li><a href="{{$instagram}}"><span class="icon-instagram"></span></a></li>
                        </ul>
                    </li>
                    <li><small><b>LPK Sari</b> <br>Jln. Padat Karya No. 77 Kel. Muara Dua Kec. Prabumulih Timur
                            Kota Prabumulih, Sumatera Selatan Kode Pos : 31113
                        <br> Email : lkpsariprabumulih@gmail.com<br> Telpn : 0713 - 321016</small>

                    </li>
                </ul>
                <!--end footer social media list-->
            </li>
        </ul>
    </div>
</div>
<!--end footer content-->
</footer>
<!--end footer-->
<a href="https://api.whatsapp.com/send?phone=+6282183300904&text=Selamat%20Datang%20di%20LKP%20Sari." class="float" target="_blank">
<img src="{{url('/')}}/themes/modern/images/wa.png" width="70%">
</a>
