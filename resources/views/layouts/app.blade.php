<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link href="themes/default/css/bootstrap.min.css" rel="stylesheet">
    <link href="themes/default/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="themes/default/css/animate.css" rel="stylesheet">
    <link href="themes/default/css/style.css" rel="stylesheet">
    <link href="themes/default/css/custom.css" rel="stylesheet">

</head>

<body class="top-navigation pace-done">
    <div id="wrapper">


        <div id="page-wrapper" class="gray-bg">
            @yield('content')
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="themes/default/js/jquery-3.1.1.min.js"></script>
    <script src="themes/default/js/popper.min.js"></script>
    <script src="themes/default/js/bootstrap.js"></script>
    <script src="themes/default/js/plugins/metismenu/jquery.metismenu.js"></script>
    <script src="themes/default/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="themes/default/js/inspinia.js"></script>
    <!--<script src="themes/default/js/plugins/pace/pace.min.js"></script>-->
    <script type="text/javascript">
    $(document).ready(function() {
    $("#{{request()->segment(1)}}MainNav").addClass('active');
    $("#manageStockSubMenu").addClass('active');
    });
    </script>
</body>

</html>
