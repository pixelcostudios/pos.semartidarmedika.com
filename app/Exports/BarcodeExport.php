<?php

namespace App\Exports;

use DB;

use App\Item;
use App\Helpers\Base;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input as input;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

class BarcodeExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        $post_list = Item::get();

        $i = 0;
        $post_list_array = array();
        foreach ($post_list as $row) {
            $i++;
            $post_list_array[] = array(
                'r_no'  => $i,
                'r_id'  => $row->barang_id,
                'r_jul' => $row->barang_harjul,
                'r_name'=> $row->barang_nama
            );
        }

        return collect($post_list_array);//Target::all();
    }
    public function headings(): array
    {
        return [
            'No',
            'Code',
            'Harga Jual Barang',
            'Nama Barang',
        ];
    }
}
