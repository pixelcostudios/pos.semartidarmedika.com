<?php

namespace App\Http\Controllers;

use DB;
use App\Supplier;
use App\Item;
use App\Helpers\Base;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input as input;
use Illuminate\Http\Request;


class SupplierController extends Controller
{
    public function index()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        if (Input::get('show') != '') {
            $show = Input::get('show');
        } else {
            $show = 10;
        }
        if (Input::get('start_date') != '') {
            $start_date = Input::get('start_date');
        } else {
            $start_date = date('Y-m-d');
        }
        if (Input::get('end_date') != '') {
            $end_date = Input::get('end_date');
        } else {
            $end_date = date('Y-m-d');
        }
        $post_list = Supplier::orderBy('created_at', 'DESC')->paginate($show);

        if (isset($_POST['post-delete']) && $_POST['post-delete'] != "") {
            for ($i = 0; $i < count(@$_POST['post-check']); $i++) {
                $post_id = @$_POST['post-check'][$i];

                Supplier::where('suplier_id', '=', $post_id)->delete();
            }
            return redirect('/supplier')->with('success', 'Post updated!');
        } elseif (isset($_POST['post-deleted']) && $_POST['post-deleted'] != "") {
            $post_id = $_POST['post_id'];

            Supplier::where('suplier_id', '=', $post_id)->delete();

            return redirect('/supplier/')->with('success', 'Post updated!');
        }

        return view('back/supplier', ['post_list' => $post_list, 'start_date' => $start_date, 'end_date' => $end_date, 'show' => $show, 'copyright' => copyright()]);
    }
    public function search()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        if (Input::get('show') != '') {
            $show = Input::get('show');
        } else {
            $show = 10;
        }
        if (Input::get('start_date') != '') {
            $start_date = Input::get('start_date');
        } else {
            $start_date = date('Y-m-d');
        }
        if (Input::get('end_date') != '') {
            $end_date = Input::get('end_date');
        } else {
            $end_date = date('Y-m-d');
        }
        if (Input::get('query') != '') {
            $query = Input::get('query');
        } else {
            $query = '';
        }
        $post_list = Supplier::where('suplier_nama', 'like', '%' . $query . '%')->orWhere('suplier_sales', 'like', '%' . $query . '%')->orWhere('suplier_notelp', 'like', '%' . $query . '%')->orWhere('suplier_rekening', 'like', '%' . $query . '%')->orderBy('created_at', 'DESC')->paginate($show);

        if (isset($_POST['post-delete']) && $_POST['post-delete'] != "") {
            for ($i = 0; $i < count(@$_POST['post-check']); $i++) {
                $post_id = @$_POST['post-check'][$i];

                Supplier::where('suplier_id', '=', $post_id)->delete();
            }
            return redirect('/supplier')->with('success', 'Post updated!');
        } elseif (isset($_POST['post-deleted']) && $_POST['post-deleted'] != "") {
            $post_id = $_POST['post_id'];

            Supplier::where('suplier_id', '=', $post_id)->delete();

            return redirect('/supplier/')->with('success', 'Post updated!');
        }

        return view('back/supplier_search', ['post_list' => $post_list, 'start_date' => $start_date, 'end_date' => $end_date, 'show' => $show, 'query' => $query, 'copyright' => copyright()]);
    }
    public function add()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }

        return view('back/supplier_add', [
            'copyright' => copyright()
        ]);
    }
    public function create(Request $request)
    {
        $suplier_nama             = ($request->get('suplier_nama') != '') ? $request->get('suplier_nama') : '';
        $suplier_sales           = ($request->get('suplier_sales') != '') ? $request->get('suplier_sales') : '';
        $suplier_alamat        = ($request->get('suplier_alamat') != '') ? $request->get('suplier_alamat') : '';
        $suplier_notelp           = ($request->get('suplier_notelp') != '') ? $request->get('suplier_notelp') : '';
        $suplier_rekening             = ($request->get('suplier_rekening') != '') ? $request->get('suplier_rekening') : 0;

        Supplier::insert([
            'suplier_nama' => $suplier_nama,
            'suplier_sales' => $suplier_sales,
            'suplier_alamat' => $suplier_alamat,
            'suplier_notelp' => $suplier_notelp,
            'suplier_rekening' => $suplier_rekening,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'deleted_at' => date('Y-m-d H:i:s')
        ]);

        $id = DB::getPdo()->lastInsertId();

        return redirect('/supplier')->with('success', 'Post updated!');
    }
    public function edit($id)
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        $post_edit      = Supplier::where('suplier_id', $id)->get();

        return view('back/supplier_edit', [
            'post_edit' => $post_edit,
            'copyright' => copyright()
        ]);
    }
    public function update(Request $request, $id)
    {
        $post_data = Supplier::find($id);
        $post_data->suplier_nama             = ($request->get('suplier_nama') != '') ? $request->get('suplier_nama') : '';
        $post_data->suplier_sales           = ($request->get('suplier_sales') != '') ? $request->get('suplier_sales') : '';
        $post_data->suplier_alamat        = ($request->get('suplier_alamat') != '') ? $request->get('suplier_alamat') : '';
        $post_data->suplier_notelp           = ($request->get('suplier_notelp') != '') ? $request->get('suplier_notelp') : '';
        $post_data->suplier_rekening             = ($request->get('suplier_rekening') != '') ? $request->get('suplier_rekening') : 0;
        $post_data->save();

        return redirect('/supplier')->with('success', 'Post updated!');
    }
}
