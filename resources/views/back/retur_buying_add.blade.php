@extends('layouts.post.retur')

@section('content')
@include('layouts.menu')
<div class="row wrapper border-bottom white-bg page-heading">
<div class="col-lg-10">
<h2>Retur Penjualan</h2>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="{{ url('home') }}">Home</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ url('retur/buying') }}">Retur Penjualan</a>
    </li>
    <li class="breadcrumb-item active">
        <strong>All Retur Penjualan</strong>
    </li>
</ol>
</div>
<div class="col-lg-2">

</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
            <form name="myform" method="post" action="{{ url('retur/buying_create') }}" enctype="multipart/form-data" class="form" data-async>
                @csrf
				<div class="row">
					<div class="col-lg-12 padding-none">
						<div class="ibox ">
							<div class="ibox-title">
								<h5>Retur Penjualan Editor</h5>
								<button type="submit" name="Pixel_Save" class="btn btn-primary float-right">Simpan</button>
							</div>
							<div class="ibox-content">


                                <div class="relative input_fields_wrap">
                                    <div class="text-center margin-buttton-10">
                                            <button class="btn btn-primary add_product_price add_field_button"><i class="fa fa-plus"></i></button>
                                            <br><br>
                                    </div>

                                    <div class="row item-select">
                                        <div class="form-group col-sm-3">
                                            <label>Item</label><br>
                                            <select class="form-control item_select" name="item[]" data-placeholder="Pilih Item" required="required">
                                            </select>
                                            <input type="hidden" class="form-control name" name="name[]">
                                        </div>
                                        <div class="form-group col-sm-2">
                                            <label>Harga</label><br>
                                            <input type="text" class="form-control price" placeholder="Price " name="price[]" required="required">
                                        </div>
                                        <div class="form-group col-sm-1">
                                            <label>Satuan</label><br>
                                            <input type="text" class="form-control unit" placeholder="Satuan " name="unit[]" required="required">
                                        </div>
                                        <div class="form-group col-sm-1">
                                            <label>QTY</label><br>
                                            <input type="text" class="form-control qty" name="qty[]" placeholder="QTY" value="1" required="required">
                                        </div>
                                        <div class="form-group col-sm-4">
                                            <label>Keterangan</label><br>
                                            <input type="text" class="form-control desc" name="desc[]">
                                        </div>
                                    </div>
                                </div>


								<div class="clear"></div>
							</div>
						</div>
					</div>
                </div>

            </form>
		</div>
<div class="footer">
<div class="float-right">
<strong>
    </strong>
</div>
<div>
<strong>Copyright</strong> {{$copyright}}
</div>
</div>
@endsection
