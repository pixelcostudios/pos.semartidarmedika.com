@extends('layouts.setting.setting')

@section('content')
@include('layouts.menu')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Member Role</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ url('home') }}">Home</a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{ url('member/role/') }}">Member Role</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>All Member Role</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
<form name="myform" method="post" action="{{ url('member/role_create') }}" enctype="multipart/form-data" class="form" data-async>
    @csrf
        <div class="row">
            <div class="col-lg-12 padding-none">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Member Role Editor</h5>
                        <button type="submit" name="Pixel_Save" value="Submit" class="btn btn-primary float-right">Save</button>
                    </div>
					<div class="ibox-content">
                        <div class="form-group ">
                            <label for="title">Name </label>
                                <input type="text" name="name" class="form-control" placeholder="Name" required="required" autocomplete="off">
                        </div>
                        <div class="form-group">
                            <label for="title">Description </label>
                                <input type="text" name="description" class="form-control" placeholder="Description" required="required" autocomplete="off">
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>

    </form>

</div>
<div class="footer">
    <div class="float-right">
        <strong>
            </strong>
    </div>
    <div>
        <strong>Copyright</strong> {{$copyright}}
    </div>
</div>
@endsection
