<?php

namespace App\Http\Controllers\API;

use DB;
use App\Categories;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input as input;

class ProductController extends Controller
{
    public $successStatus = 200;

    public function index()
    {
        if (Input::get('cat') != '') {
            $cat = Input::get('cat');
            $post_list    = Product::leftJoin('metas', 'posts.post_id', '=', 'metas.meta_source')->where('meta_dest', $cat)->where('posts.post_type', 'product')->where('posts.status', '1')->orderBy('date', 'DESC')->get();
            $data_array = array();
            foreach ($post_list as $key) {
                $images_array = array();
                foreach (Product::where('post_type', 'images')->where([['post_up', '=', $key->post_id]])->skip(0)->take(1)->get() as $p2) {
                    $images_array[] = url('/') . '/upload/' .  $p2->post_slug . '.' . $p2->post_mime_type;
                }
                $data_array[] = array('id' => $key->post_id, 'title' => $key->post_title, 'price' => $key->post_price, 'weight' => $key->post_weight, 'unit' => $key->post_unit, 'images' => $images_array);
            }
        } else {
            $post_list    = Product::leftJoin('metas', 'posts.post_id', '=', 'metas.meta_source')->where('posts.post_type', 'product')->where('posts.status', '1')->orderBy('date', 'DESC')->get();
            $data_array = array();
            foreach ($post_list as $key) {
                $images_array = array();
                foreach (Product::where('post_type', 'images')->where([['post_up', '=', $key->post_id]])->skip(0)->take(1)->get() as $p2) {
                    $images_array[] = url('/') . '/upload/'. $p2->post_slug .'.'. $p2->post_mime_type;
                }
                $data_array[] = array('id' => $key->post_id, 'title' => $key->post_title, 'price' => $key->post_price, 'weight' => $key->post_weight, 'unit' => $key->post_unit, 'images' => $images_array);
            }
        }

        return response()->json(['status' => 200, 'message' => 'List Product', 'data' => $data_array], $this->successStatus);
    }
    public function category()
    {
        $post_list = Categories::where('cat_type', '=', 'product')->orderBy('cat_title', 'ASC')->get();
        $data_array = array();
        foreach ($post_list as $key)
        {
            $data_array[] = array('cat_id'=>$key->cat_id, 'cat_title' => $key->cat_title);
        }

        return response()->json(['status' => 200, 'message' => 'List Product Category', 'data' => $data_array], $this->successStatus);
    }
}
