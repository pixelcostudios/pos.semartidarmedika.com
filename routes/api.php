<?php

// use DB;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('web/item', 'API\ItemController@item');
Route::get('web/item_detail', 'API\ItemController@item_detail');
Route::get('web/item_detail_distributor', 'API\ItemController@item_detail_distributor');
Route::get('web/item_buying_detail', 'API\ItemController@item_buying_detail');
Route::get('web/customer', 'API\CustomerController@index');

// Route::get('districts', function () {
//     // If the Content-Type and Accept headers are set to 'application/json',
//     // this will return a JSON structure. This will be cleaned up later.
//     return DB::table('districts')->All();
// });
