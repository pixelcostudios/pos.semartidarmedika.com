@extends('layouts.app')

@section('content')
@include('layouts.menu')
<div class="wrapper wrapper-content">
    @if(in_group(Auth::user()->id)=='admin')
    <div class="row">
        <div class="col-lg-3">
                <div class="widget-head-color-box blue text-center widget-box">
                    <a href="{{ url('selling/retail') }}">
                        <div class="widget-icon">
                            <i class="fa fa-shopping-cart"></i>
                        </div>
                        <div>
                            <span>Penjualan Eceran</span>
                        </div>
                    </a>
                </div>
        </div>
        <div class="col-lg-3">
                <div class="widget-head-color-box green text-center widget-box">
                    <a href="{{ url('selling/distributor') }}">
                        <div class="widget-icon">
                            <i class="fa fa-shopping-cart"></i>
                        </div>
                        <div>
                            <span>Penjualan Grosir</span>
                        </div>
                    </a>
                </div>
        </div>
        <div class="col-lg-3">
                <div class="widget-head-color-box light-orange text-center widget-box">
                    <a href="{{ url('selling/bill') }}">
                        <div class="widget-icon">
                            <i class="fa fa-shopping-cart"></i>
                        </div>
                        <div>
                            <span>Penjualan Bon</span>
                        </div>
                    </a>
                </div>
        </div>
        <div class="col-lg-3">
                <div class="widget-head-color-box color text-center widget-box">
                    <a href="{{ url('supplier') }}">
                        <div class="widget-icon">
                            <i class="fa fa fa-truck"></i>
                        </div>
                        <div>
                            <span>Supplier</span>
                        </div>
                    </a>
                </div>
        </div>
        <div class="col-lg-3">
                <div class="widget-head-color-box purple text-center widget-box">
                    <a href="{{ url('categories?type=product') }}"> 
                        <div class="widget-icon">
                            <i class="fa fa fa-sitemap"></i>
                        </div>
                        <div>
                            <span>Kategori Barang</span>
                        </div>
                    </a>
                </div>
        </div>
        <div class="col-lg-3">
                <div class="widget-head-color-box red text-center widget-box">
                    <a href="{{ url('item/manage') }}">
                        <div class="widget-icon">
                            <i class="fa fa-shopping-bag"></i>
                        </div>
                        <div>
                            <span>Barang</span>
                        </div>
                    </a>
                </div>
        </div>
        <div class="col-lg-3">
                <div class="widget-head-color-box blue text-center widget-box">
                    <a href="{{ url('member') }}">
                        <div class="widget-icon">
                            <i class="fa fa-user"></i>
                        </div>
                        <div>
                            <span>Pengguna</span>
                        </div>
                    </a>
                </div>
        </div>
        <div class="col-lg-3">
                <div class="widget-head-color-box light-red text-center widget-box">
                    <a href="{{ url('buying') }}">
                        <div class="widget-icon">
                            <i class="fa fa-user"></i>
                        </div>
                        <div>
                            <span>Pembelian Barang</span>
                        </div>
                    </a>
                </div>
        </div>

    </div>
    @elseif(in_group(Auth::user()->id)=='kasir')
<div class="row">
        <div class="col-lg-3">
                <div class="widget-head-color-box blue text-center widget-box">
                    <a href="{{ url('selling/retail') }}">
                        <div class="widget-icon">
                            <i class="fa fa-shopping-cart"></i>
                        </div>
                        <div>
                            <span>Penjualan Eceran</span>
                        </div>
                    </a>
                </div>
        </div>
        <div class="col-lg-3">
                <div class="widget-head-color-box green text-center widget-box">
                    <a href="{{ url('selling/distributor') }}">
                        <div class="widget-icon">
                            <i class="fa fa-shopping-cart"></i>
                        </div>
                        <div>
                            <span>Penjualan Grosir</span>
                        </div>
                    </a>
                </div>
        </div>
        <div class="col-lg-3">
                <div class="widget-head-color-box light-orange text-center widget-box">
                    <a href="{{ url('selling/bill') }}">
                        <div class="widget-icon">
                            <i class="fa fa-shopping-cart"></i>
                        </div>
                        <div>
                            <span>Penjualan Bon</span>
                        </div>
                    </a>
                </div>
        </div>


    </div>
    @endif
</div>


<div class="footer">
    <div class="pull-right">
        <?php //echo $this->base->disk_size();
        ?></strong> .
    </div>
    <div>
        <strong>Copyright</strong> {{$copyright}}
    </div>
</div>
@endsection
