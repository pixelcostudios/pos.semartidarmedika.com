<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buyingdetail extends Model
{
    protected $primaryKey = 'd_beli_id';
    protected $table = "buy_details";
}
