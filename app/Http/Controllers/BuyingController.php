<?php

namespace App\Http\Controllers;

use DB;
use App\Buying;
use App\Item;
use App\Buyingdetail;
use App\Helpers\Base;
use App\Supplier;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input as input;
use Illuminate\Http\Request;

class BuyingController extends Controller
{
    public function index()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        if (Input::get('show') != '') {
            $show = Input::get('show');
        } else {
            $show = 10;
        }
        if (Input::get('start_date') != '') {
            $start_date = Input::get('start_date');
        } else {
            $start_date = date('Y-m-d');
        }
        if (Input::get('end_date') != '') {
            $end_date = Input::get('end_date');
        } else {
            $end_date = date('Y-m-d');
        }
        $post_list = Buying::selectRaw('*, beli_nofak as faktur')->whereBetween('beli_tanggal', [$start_date . " 00:00:00", $end_date . " 23:59:59"])
            ->leftJoin('suppliers', 'suppliers.suplier_id', '=', 'buys.beli_suplier_id')
            ->leftJoin('users', 'users.id', '=', 'buys.beli_user_id')
            ->orderBy('beli_tanggal', 'DESC')->paginate($show);

        if (isset($_POST['post-delete']) && $_POST['post-delete'] != "") {
            for ($i = 0; $i < count(@$_POST['post-check']); $i++) {
                $post_id = @$_POST['post-check'][$i];

                Buying::where('beli_id', '=', $post_id)->delete();
                Buyingdetail::where('beli_id', '=', $post_id)->delete();
            }
            return redirect('/buying')->with('success', 'Post updated!');
        } elseif (isset($_POST['post-deleted']) && $_POST['post-deleted'] != "") {
            $post_id = $_POST['post_id'];

            Buying::where('beli_id', '=', $post_id)->delete();
            Buyingdetail::where('beli_id', '=', $post_id)->delete();

            return redirect('/buying/')->with('success', 'Post updated!');
        }

        return view('back/buying', ['post_list' => $post_list, 'start_date' => $start_date, 'end_date' => $end_date, 'show' => $show, 'copyright' => copyright()]);
    }
    public function add()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        $supplier_list  = Supplier::get();

        return view('back/buying_add', [
            'supplier_list' => $supplier_list,
            'copyright' => copyright()
        ]);
    }
    public function create(Request $request)
    {
        $beli_nofak             = ($request->get('beli_nofak') != '') ? $request->get('beli_nofak') : buying_number();
        $beli_tanggal           = ($request->get('beli_tanggal') != '') ? $request->get('beli_tanggal') : '';
        $beli_suplier_id        = ($request->get('beli_suplier_id') != '') ? $request->get('beli_suplier_id') : '';
        $beli_user_id           = Auth::user()->id;
        $beli_tempo             = ($request->get('beli_tempo') != '') ? $request->get('beli_tempo') : 0;

        Buying::insert([
            'beli_nofak' => $beli_nofak,
            'beli_tanggal' => $beli_tanggal,
            'beli_suplier_id' => $beli_suplier_id,
            'beli_user_id' => $beli_user_id,
            'beli_tempo' => $beli_tempo,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'deleted_at' => date('Y-m-d H:i:s')
        ]);

        $id = DB::getPdo()->lastInsertId();

        if ($request->get('post_item') != '') {
            $p_note     = $request->get('post_item');
            $post_qty   = $request->get('post_qty');
            $post_price   = $request->get('post_price');
            foreach ($p_note as $key => $n) {
                if ($n != '') {
                    $stock = Item::where('barang_id','=', $n)->value('barang_stok');
                    Buyingdetail::insert([
                        'beli_id'           => $id,
                        'd_beli_nofak'      => $beli_nofak,
                        'd_beli_barang_id'  => $n,
                        'd_beli_harga'      => $post_price[$key],
                        'd_beli_jumlah'     => $post_qty[$key],
                        'd_beli_total'      => $post_price[$key]*$post_qty[$key],
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                        'deleted_at'        => date('Y-m-d H:i:s')
                    ]);
                    Item::where('barang_id', '=', $n)->update(['barang_stok' => $stock + $post_qty[$key]]);
                }
            }

        }

        return redirect('/buying')->with('success', 'Post updated!');
    }
    public function print($id)
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }

        $post_list = Buying::where([['beli_nofak', '=', $id]])->leftjoin('users', 'users.id', '=', 'buys.beli_user_id')->orderBy('beli_tanggal', 'DESC')->get();
        if (Input::get('type') != '') {
            $type = Input::get('type');
            if ($type == 'dot') {
                return view('back/buying_print_dot', ['id' => $id, 'post_list' => $post_list, 'copyright' => copyright()]);
            } else {
                return view('back/buying_print_thermal', ['id' => $id, 'post_list' => $post_list, 'copyright' => copyright()]);
            }
        } else {
            return view('back/buying_print_dot', ['id' => $id, 'post_list' => $post_list, 'copyright' => copyright()]);
        }
    }
}
