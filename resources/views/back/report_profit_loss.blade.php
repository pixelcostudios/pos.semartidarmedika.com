@extends('layouts.post.report')

@section('content')
@include('layouts.menu')
<div class="row wrapper border-bottom white-bg page-heading printing">
<div class="col-lg-10">
<h2>Report Laba / Rugi</h2>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="{{url('/')}}">Home</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{url('report/profit_loss/')}}">Report Laba / Rugi</a>
    </li>
    <li class="breadcrumb-item active">
        <strong>All Report Laba / Rugi</strong>
    </li>
</ol>
</div>
<div class="col-lg-2">

</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
<div class="row">
<div class="col-lg-12 padding-none">
    <div class="ibox ">

            <div class="ibox-title printing">
                <h5>Manage Report Laba / Rugi</h5>
            </div>
            <div class="ibox-content">
                <form method="get" action="{{ url('report/profit_loss') }}">
                    @csrf
                <div class="row printing">
                    <div class="col-sm-3 report_pilot">
                        <div class="input-group">
                            <input name="start_date" class="form-control date-picker-month" autocomplete="off" type="text" value="{{$start_date}}" data-date-format="yyyy-mm" id="date_start" data-validation="date" data-validation-format="yyyy-mm" placeholder="Bulan">
                            <span class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-1 report_pilot">
                            <button type="submit" name="post-look" value="publish" class="btn col-sm-12 btn-sm btn-primary"> Lihat!</button>
                        </div>
                        <div class="col-sm-1 report_pilot">
                            <div data-toggle="buttons" class="btn-group">
                                <label onclick="location.href='profit_loss_download?start_date={{$start_date}}'" class="btn btn-sm btn-white"> <i class="fa fa-download"></i> Download </label>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <button type="button" class="btn btn-default btn-sm btn-print"><i class="fa fa-print"></i> Print</button>
                        </div>
                </div><br>
                </form>
                <div class="table-responsive" id="printarea">
                    <form method="post">
                    @csrf
                    <table class="table table-striped">
                        <tbody>
                             <tr>
                                <th>No </th>
                                <th>Tanggal</th>
                                <th>Nama Barang</th>
                                <th>Satuan</th>
                                <th class="text-right">Harga Pokok</th>
                                <th class="text-right">Harga Jual</th>
                                <th class="text-right">Keuntungan Per Unit</th>
                                <th>Item Terjual</th>
                                <th class="text-right">Diskon</th>
                                <th class="text-right">Untung Bersih</th>
                            </tr>
                            @php
                            $total = array();
                            @endphp
                            @foreach($post_list as $value  => $p)
                            @php
                            $total[] = (($p->d_jual_barang_harjul*$p->d_jual_qty)-($p->d_jual_barang_harpok*$p->d_jual_qty));
                            @endphp
                            <tr>
                                <td>{{$value+1}} </td>
                                <td>{{$p->jual_tanggal}} </td>
                                <td>{{$p->d_jual_barang_nama}} </td>
                                <td>{{$p->d_jual_barang_satuan}} </td>
                                <td class="text-right">{{$p->d_jual_barang_harpok}} </td>
                                <td class="text-right">{{$p->d_jual_barang_harjul}} </td>
                                <td class="text-right">{{$p->d_jual_barang_harjul-$p->d_jual_barang_harpok}} </td>
                                <td class="text-center">{{$p->d_jual_qty}} </td>
                                <td class="text-right">{{$p->d_jual_diskon}} </td>
                                <td class="text-right">{{(($p->d_jual_barang_harjul*$p->d_jual_qty)-($p->d_jual_barang_harpok*$p->d_jual_qty))}} </td>
                            </tr>
                            @endforeach
                            <tr>
                                <td colspan="9">Total</td>
                                <td class="text-right">{{array_sum($total)}} </td>
                            </tr>
                        </tbody>
                       </table>

        </form>
    </div>
</div>
</div>
</div>

</div>
</div>
<div class="footer printing">
<div class="float-right">
<strong>
    </strong>
</div>
<div>
<strong>Copyright</strong> {{$copyright}}
</div>
</div>
@endsection
