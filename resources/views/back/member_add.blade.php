@extends('layouts.setting.setting')

@section('content')
@include('layouts.menu')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Member</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ url('home') }}">Home</a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{ url('member') }}">Member</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>All Member</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
<form name="myform" method="post" action="{{ url('member/create') }}" enctype="multipart/form-data" class="form" data-async>
    @csrf
        <div class="row">
            <div class="col-lg-12 padding-none">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Member Editor</h5>
                        <button type="submit" name="Pixel_Save" value="Submit" class="btn btn-primary float-right">Save Setting</button>
                    </div>
					<div class="ibox-content">
								<div class="ibox-title-detail ibox-border-bottom-1 clear">
                            <h5 class="pull-left">Login Information</h5>
								</div>
						<div class="row">
                            <div class="form-group col-sm-4 padding-left-0">
                                <label for="title">Email Address </label>
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                    <input type="email" name="email" class="form-control" placeholder="Email Address" required="required" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group col-sm-4 padding-left-0">
                                <label for="title">Username </label>
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    <input type="text" name="username" class="form-control" placeholder="Username" required="required" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group col-sm-4 padding-right-0">
                                <label for="title">Password </label>
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                    <input type="password" name="password" class="form-control" placeholder="Password" autocomplete="off">
                                </div>
                            </div>
						</div>
                        <div class="ibox-title-detail ibox-border-top-1 ibox-border-bottom-1 clear">
                            <h5 class="pull-left">Account Type</h5>
						</div>
						<div class="row">
                            <div class="form-group col-sm-6 padding-left-0">
                                <label for="title">Member Role </label>
                                <select name="role" class="form-control select2">
                                    <option value="0">No Selected</option>
                                    @foreach($group_list as $p_role)
                                    <option value="{{$p_role->id}}">{{$p_role->description}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-sm-6 padding-right-0">
                                <label for="title">Status </label>
                                <select name="active" class="form-control select2">
                                    <option value="1">Confirm</option>
                                    <option value="0">Unconfirm</option>
                                </select>
							</div>
						</div>
                        <div class="ibox-title-detail ibox-border-top-1 ibox-border-bottom-1 clear">
                            <h5 class="pull-left">Personal Information</h5>
                        </div>
							<div class="row">
                            <div class="form-group col-sm-6 padding-left-0">
                                <label for="title">Gender </label>
                                <select name="gender" class="form-control select2">
                                    <option value="Mr">Mr</option>
                                    <option value="Mrs">Mrs</option>
                                </select>
                            </div>
                            <div class="form-group col-sm-6 padding-right-0">
                                <label for="title">Join Date </label>
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input disabled type="text" class="form-control" placeholder="Automatically Added">
                                </div>
                            </div>
                            <div class="form-group col-sm-6 padding-left-0">
                                <label for="title">First Name </label>
                                <input type="text" class="form-control" name="first_name" placeholder="First Name" required="required">
                            </div>
                            <div class="form-group col-sm-6 padding-right-0">
                                <label for="title">Last Name </label>
                                <input type="text" class="form-control" name="last_name" placeholder="Last Name">
                            </div>
                            <div class="form-group col-sm-6 padding-left-0">
                                <label for="title">Address </label>
                                <input type="text" class="form-control" name="address" placeholder="Address">
                            </div>
                            <div class="form-group col-sm-6 padding-right-0">
                                <label for="title">City</label>
                                <input type="text" class="form-control" name="city" placeholder="City">
                            </div>
                            <div class="form-group col-sm-6 padding-left-0">
                                <label for="title">Province</label>
                                <input type="text" class="form-control" name="province" placeholder="Province">
                            </div>
                            <div class="form-group col-sm-6 padding-right-0">
                                <label for="title">Country</label>
                                <input type="text" class="form-control" name="country" placeholder="Country">
                            </div>
                            <div class="form-group col-sm-6 padding-left-0">
                                <label for="title">Postal Code</label>
                                <input type="text" class="form-control" name="postal" placeholder="Postal Code">
                            </div>
                            <div class="form-group col-sm-6 padding-right-0 relative" id="data_3">
                                <label for="title">Birth Date </label>
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" name="birthday" class="form-control">
                                </div>
                            </div>
							<div class="form-group col-sm-6 padding-left-0">
                                <label for="title">Profession</label>
                                <input type="text" class="form-control" name="profession" placeholder="Profession">
                            </div>
                            <div class="form-group col-sm-6 padding-right-0">
                                <label for="title">Company</label>
                                <input type="text" class="form-control" name="company" placeholder="Company">
                            </div>
                            <div class="form-group col-sm-12 clear">
                                <label for="title">Signature</label>
                                <textarea class="form-control" rows="3" name="signature" placeholder="Signature"></textarea>
                            </div>
							</div>

                            <div class="ibox-title-detail ibox-border-top-1 ibox-border-bottom-1 clear">
                                <h5 class="pull-left">Contact Information</h5>
                            </div>
							<div class="row">
                            <div class="form-group col-sm-6 padding-left-0">
                                <label for="title">Phone</label>
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                    <input type="text" name="phone" class="form-control" placeholder="Phone">
                                </div>
                            </div>
                            <div class="form-group col-sm-6 padding-right-0">
                                <label for="title">Mobile Phone</label>
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-mobile-phone"></i></span>
                                    <input type="text" name="mobile" class="form-control" placeholder="Mobile Phone">
                                </div>
                            </div>
							</div>
                            <div class="ibox-title-detail ibox-border-top-1 ibox-border-bottom-1 clear">
                                <h5 class="pull-left">Social Network</h5>
							</div>
							<div class="row">
                            <div class="form-group col-sm-6 padding-left-0">
                                <label for="title">Facebook</label>
                                <input type="text" class="form-control" name="facebook" placeholder="Facebook">
                            </div>
                            <div class="form-group col-sm-6 padding-right-0">
                                <label for="title">Twitter</label>
                                <input type="text" class="form-control" name="twitter" placeholder="Twitter">
                            </div>
                            <div class="form-group col-sm-6 padding-left-0">
                                <label for="title">Instagram</label>
                                <input type="text" class="form-control" name="instagram" placeholder="Instagram">
                            </div>
                            <div class="form-group col-sm-6 padding-left-0">
                                <label for="title">Google Plus</label>
                                <input type="text" class="form-control" name="google" placeholder="Google Plus">
							</div>
							</div>
								<div class="clear"></div>
							</div>
                </div>
            </div>
        </div>

    </form>

</div>
<div class="footer">
    <div class="float-right">
        <strong>
            </strong>
    </div>
    <div>
        <strong>Copyright</strong> {{$copyright}}
    </div>
</div>
@endsection
