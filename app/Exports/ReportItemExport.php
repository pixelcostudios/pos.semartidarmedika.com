<?php

namespace App\Exports;

use DB;

use App\Item;
use App\Categories;
use App\Helpers\Base;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input as input;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ReportItemExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        $post_list = Categories::where('cats.cat_type', 'product')->where('cats.cat_up', 0)->orderBy('cat_date', 'DESC')->get();

        $post_array = array();
        foreach ($post_list as $p)
        {
            $post_array = array(array(
                'r_no'  => $p->cat_title,
                'r_id'  => '',
                'r_nama' => '',
                'r_satuan' => '',
                'r_harpok' => '',
                'r_harjul' => '',
                'r_stok' => ''
            ));
            $post_list_array = array();
            foreach (Item::where('barang_kategori_id', '=', $p->cat_id)->get() as $value  => $key) {
                $post_list_array[] = array(
                    'r_no'  => $value + 1,
                    'r_id'  => $key->barang_id,
                    'r_nama' => $key->barang_nama,
                    'r_satuan' => $key->barang_satuan,
                    'r_harpok' => $key->barang_harpok,
                    'r_harjul' => $key->barang_harjul,
                    'r_stok' => $key->barang_stok
                );
            }
            $data_array[]= array_merge($post_array, $post_list_array);
        }


        // $i = 0;
        // $post_list_array = array();
        // foreach ($post_list as $row) {
        //     $i++;
        //     $post_list_array[] = array(
        //         'r_no'  => $i,
        //         'r_id'  => $row->barang_id,
        //         'r_jul' => $row->barang_harjul,
        //         'r_name'=> $row->barang_nama
        //     );
        // }

        return collect($data_array);//Target::all();
    }
    public function headings(): array
    {
        return [
            'No',
            'Kode Barang',
            'Nama Barang',
            'Satuan',
            'Harga Pokok',
            'Harga Jual',
            'Stok'
        ];
    }
}
