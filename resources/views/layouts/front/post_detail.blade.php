<!DOCTYPE html>
<html lang="en">

<head>
<title>{{$title}} | {{ $setting['meta_title'] }}</title>
<!-- description -->
<meta name="description" content="{{ $description }}">
<!-- keywords -->
<meta name="keywords" content="{{ $keyword }}">
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta charset="utf-8">
<meta name="author" content="http://pixelcostudios.com">
<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1" />
<!-- ++++ favicon ++++ -->
<link rel="icon" type="image/png" sizes="32x32" href="{{url('/')}}/themes/modern/css/icon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="{{url('/')}}/themes/modern/icon/favicon-96x96.png">
<link rel="apple-touch-icon" sizes="57x57" href="{{url('/')}}/themes/modern/icon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="114x114" href="{{url('/')}}/themes/modern/icon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="114x114" href="{{url('/')}}/themes/modern/icon/apple-icon-114x114.png">
<!-- ++++ bootstrap ++++ -->
<link rel="stylesheet" href="{{url('/')}}/themes/modern/css/bootstrap.css" />
<!-- ++++ owl carousel ++++ -->
<link rel="stylesheet" href="{{url('/')}}/themes/modern/css/owl.carousel.min.css" />
<!-- ++++ magnific-popup  ++++ -->
<link rel="stylesheet" href="{{url('/')}}/themes/modern/css/magnific-popup.css" />
<!-- ++++ font IcoMoon ++++ -->
<link rel="stylesheet" href="{{url('/')}}/themes/modern/css/fonts.css" />
<!-- ++++ style ++++ -->
<link rel="stylesheet" href="{{url('/')}}/themes/modern/css/style.css" />
<!-- responsive css -->
<link rel="stylesheet" href="{{url('/')}}/themes/modern/css/responsive.css" />
<!-- Slider Revolution CSS Files -->
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/themes/modern/revolution/css/settings.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/themes/modern/revolution/css/layers.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/themes/modern/revolution/css/navigation.css">
<!-- ++++ [if IE]>
    <script src="{{url('/')}}/themes/modern/js/html5shiv.js"></script>
<![endif] ++++ -->

</head>
<body>

@yield('content')
<!--js library of jQuery-->
<script type="text/javascript" src="{{url('/')}}/themes/modern/js/jquery.min.js"></script>
<!--custom js-->
<script type="text/javascript" src="{{url('/')}}/themes/modern/js/script.js"></script>
<!--js library of bootstrap-->
<script type="text/javascript" src="{{url('/')}}/themes/modern/js/bootstrap.min.js"></script>
<!--js library for number counter-->
<script type="text/javascript" src="{{url('/')}}/themes/modern/js/waypoints.min.js"></script>
<script type="text/javascript" src="{{url('/')}}/themes/modern/js/jquery.counterup.min.js"></script>
<!-- js library for owl carousel -->
    <script type="text/javascript" src="{{url('/')}}/themes/modern/js/owl.carousel.min.js"></script>
<!--modernizer library-->
<script type="text/javascript" src="{{url('/')}}/themes/modern/js/modernizr.js"></script>
<!--js library for category filter -->
<script type="text/javascript" src="{{url('/')}}/themes/modern/js/jquery.mixitup.min.js"></script>
<!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
<script type="text/javascript" src="{{url('/')}}/themes/modern/revolution/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="{{url('/')}}/themes/modern/revolution/js/jquery.themepunch.revolution.min.js"></script>
</body>
</html>
