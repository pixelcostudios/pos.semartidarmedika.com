<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', 'IndexController@index');

// Route::group(['prefix' => 'panel'], function () {
//     Route::auth();
// });

// Authentication Routes...
// Route::get('login', 'Auth\AuthController@showLoginForm');
// Route::post('login', 'Auth\AuthController@login');
// Route::post('login', 'Auth\AuthController@loginUser');
// Route::get('logout', 'Auth\AuthController@logout');

Route::get('/panel', 'PanelController@index');
Route::get('/panel/login/', 'PanelController@index');
Route::post('post-login', 'PanelController@postLogin');

Route::get('/panel/login', 'AuthController@index');
// Route::post('post-login', 'AuthController@postLogin');
Route::get('registration', 'AuthController@registration');
Route::post('post-registration', 'AuthController@postRegistration');
Route::get('dashboard', 'AuthController@dashboard');
Route::get('logout', 'AuthController@logout');

// Registration Routes...
Route::get('register', 'Auth\AuthController@showRegistrationForm');
Route::post('register', 'Auth\AuthController@register');

// Password Reset Routes...
Route::get('password/reset/{token?}', 'Auth\PasswordController@showResetForm');
Route::post('password/email', 'Auth\PasswordController@sendResetLinkEmail');
Route::post('password/reset', 'Auth\PasswordController@reset');
// Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/post', 'PostController@index');
Route::post('/post', 'PostController@index');
Route::get('/post/edit/{id}', 'PostController@edit');
Route::post('/post/update/{id}', 'PostController@update');
Route::get('/post/add', 'PostController@add');
Route::post('/post/create', 'PostController@create');
Route::get('/post/search/', 'PostController@search');

Route::get('/page', 'PageController@index');
Route::post('/page', 'PageController@index');
Route::get('/page/edit/{id}', 'PageController@edit');
Route::post('/page/update/{id}', 'PageController@update');
Route::get('/page/add', 'PageController@add');
Route::post('/page/create', 'PageController@create');
Route::get('/page/search/', 'PageController@search');

Route::get('/selling/', 'SellingController@index');
Route::post('/selling/', 'SellingController@index');
Route::get('/selling/edit/{id}', 'SellingController@edit');
Route::post('/selling/update/{id}', 'SellingController@update');
Route::get('/selling/add', 'SellingController@add');
Route::post('/selling/create', 'SellingController@create');
Route::get('/selling/search/', 'SellingController@search');
Route::post('/selling/search/', 'SellingController@search');
Route::get('/selling/print/{id}', 'SellingController@print');

Route::get('/selling/retail', 'SellingController@retail');
Route::post('/selling/retail', 'SellingController@retail');
Route::get('/selling/retail_edit/{id}', 'SellingController@retail_edit');
Route::post('/selling/retail_update/{id}', 'SellingController@retail_update');
Route::get('/selling/retail_add', 'SellingController@retail_add');
Route::post('/selling/retail_create', 'SellingController@retail_create');
Route::get('/selling/retail_search/', 'SellingController@retail_search');
Route::post('/selling/retail_search/', 'SellingController@retail_search');
Route::get('/selling/retail_print/{id}', 'SellingController@retail_print');

Route::get('/selling/distributor', 'SellingController@distributor');
Route::post('/selling/distributor', 'SellingController@distributor');
Route::get('/selling/distributor_edit/{id}', 'SellingController@distributor_edit');
Route::post('/selling/distributor_update/{id}', 'SellingController@distributor_update');
Route::get('/selling/distributor_add', 'SellingController@distributor_add');
Route::post('/selling/distributor_create', 'SellingController@distributor_create');
Route::get('/selling/distributor_search/', 'SellingController@distributor_search');
Route::post('/selling/distributor_search/', 'SellingController@distributor_search');
Route::get('/selling/distributor_print/{id}', 'SellingController@distributor_print');

Route::get('/selling/bill', 'SellingController@bill');
Route::post('/selling/bill', 'SellingController@bill');
Route::get('/selling/bill_edit/{id}', 'SellingController@bill_edit');
Route::post('/selling/bill_update/{id}', 'SellingController@bill_update');
Route::get('/selling/bill_add', 'SellingController@bill_add');
Route::post('/selling/bill_create', 'SellingController@bill_create');
Route::get('/selling/bill_search/', 'SellingController@bill_search');
Route::post('/selling/bill_search/', 'SellingController@bill_search');
Route::get('/selling/bill_print/{id}', 'SellingController@bill_print');

Route::get('/buying', 'BuyingController@index');
Route::post('/buying', 'BuyingController@index');
Route::get('/buying/edit/{id}', 'BuyingController@edit');
Route::get('/buying/print/{id}', 'BuyingController@print');
Route::post('/buying/update/{id}', 'BuyingController@update');
Route::get('/buying/add', 'BuyingController@add');
Route::post('/buying/create', 'BuyingController@create');
Route::get('/buying/search/', 'BuyingController@search');
Route::post('/buying/search/', 'BuyingController@search');

Route::get('/retur/selling', 'ReturController@selling');
Route::post('/retur/selling', 'ReturController@selling');
Route::get('/retur/selling_edit/{id}', 'ReturController@selling_edit');
Route::post('/retur/selling_update/{id}', 'ReturController@selling_update');
Route::get('/retur/selling_add', 'ReturController@selling_add');
Route::post('/retur/selling_create', 'ReturController@selling_create');
Route::get('/retur/selling_search/', 'ReturController@selling_search');
Route::post('/retur/selling_search/', 'ReturController@selling_search');

Route::get('/retur/buying', 'ReturController@buying');
Route::post('/retur/buying', 'ReturController@buying');
Route::get('/retur/buying_edit/{id}', 'ReturController@buying_edit');
Route::post('/retur/buying_update/{id}', 'ReturController@buying_update');
Route::get('/retur/buying_add', 'ReturController@buying_add');
Route::post('/retur/buying_create', 'ReturController@buying_create');
Route::get('/retur/buying_search/', 'ReturController@buying_search');
Route::post('/retur/buying_search/', 'ReturController@buying_search');

Route::get('/supplier', 'SupplierController@index');
Route::post('/supplier', 'SupplierController@index');
Route::get('/supplier/edit/{id}', 'SupplierController@edit');
Route::post('/supplier/update/{id}', 'SupplierController@update');
Route::get('/supplier/add', 'SupplierController@add');
Route::post('/supplier/create', 'SupplierController@create');
Route::get('/supplier/search/', 'SupplierController@search');
Route::post('/supplier/search/', 'SupplierController@search');

Route::get('/customer', 'CustomerController@index');
Route::post('/customer', 'CustomerController@index');
Route::get('/customer/edit/{id}', 'CustomerController@edit');
Route::post('/customer/update/{id}', 'CustomerController@update');
Route::get('/customer/add', 'CustomerController@add');
Route::post('/customer/create', 'CustomerController@create');
Route::get('/customer/search/', 'CustomerController@search');
Route::post('/customer/search/', 'CustomerController@search');
Route::get('/customer/download/', 'CustomerController@download');

Route::get('/barcode/category', 'BarcodeController@category');
Route::get('/barcode/category_barcode/{id}', 'BarcodeController@category_barcode');
Route::get('/barcode/category_detail/{id}', 'BarcodeController@category_detail');
Route::get('/barcode/all', 'BarcodeController@all');
Route::get('/barcode/all_download', 'BarcodeController@all_download');

Route::get('/banner', 'BannerController@index');
Route::post('/banner', 'BannerController@index');
Route::get('/banner/edit/{id}', 'BannerController@edit');
Route::post('/banner/update/{id}', 'BannerController@update');
Route::get('/banner/add', 'BannerController@add');
Route::post('/banner/create', 'BannerController@create');
Route::get('/banner/search/', 'BannerController@search');
Route::post('/banner/search/', 'BannerController@search');

Route::get('/categories', 'CategoriesController@index');
Route::post('/categories', 'CategoriesController@index');
Route::get('/categories/edit/{id}', 'CategoriesController@edit');
Route::post('/categories/update/{id}', 'CategoriesController@update');
Route::get('/categories/add', 'CategoriesController@add');
Route::post('/categories/create', 'CategoriesController@create');
Route::get('/categories/search/', 'CategoriesController@search');
Route::post('/categories/search/', 'CategoriesController@search');

Route::get('/slideshow', 'SlideshowController@index');
Route::post('/slideshow', 'SlideshowController@index');
Route::get('/slideshow/edit/{id}', 'SlideshowController@edit');
Route::post('/slideshow/update/{id}', 'SlideshowController@update');
Route::get('/slideshow/add', 'SlideshowController@add');
Route::post('/slideshow/create', 'SlideshowController@create');
Route::get('/slideshow/search/', 'SlideshowController@search');
Route::post('/slideshow/search/', 'SlideshowController@search');

Route::get('/item/manage', 'ItemController@index');
Route::post('/item/manage', 'ItemController@index');
Route::get('/item/edit/{id}', 'ItemController@edit');
Route::post('/item/update/{id}', 'ItemController@update');
Route::get('/item/add', 'ItemController@add');
Route::post('/item/create', 'ItemController@create');
Route::get('/item/search/', 'ItemController@search');
Route::post('/item/search/', 'ItemController@search');
Route::get('/item/unit', 'ItemController@unit');
Route::post('/item/unit', 'ItemController@unit');
Route::get('/item/unit_edit/{id}', 'ItemController@unit_edit');
Route::post('/item/unit_update/{id}', 'ItemController@unit_update');
Route::get('/item/unit_add', 'ItemController@unit_add');
Route::post('/item/unit_create', 'ItemController@unit_create');
Route::get('/item/unit_search/', 'ItemController@unit_search');
Route::post('/item/unit_search/', 'ItemController@unit_search');

Route::get('/report/item', 'ReportController@item');
Route::get('/report/item_download', 'ReportController@item_download');
Route::get('/report/item_stock', 'ReportController@item_stock');
Route::get('/report/item_stock_download', 'ReportController@item_stock_download');
Route::get('/report/item_stock_empty', 'ReportController@item_stock_empty');
Route::get('/report/item_stock_empty_download', 'ReportController@item_stock_empty_download');
Route::get('/report/selling', 'ReportController@selling');
Route::get('/report/selling_distributor', 'ReportController@selling_distributor');
Route::get('/report/selling_bill', 'ReportController@selling_bill');
Route::get('/report/buying', 'ReportController@buying');
Route::get('/report/download_buying', 'ReportController@download_buying');
Route::get('/report/retur', 'ReportController@retur');
Route::get('/report/retur_selling', 'ReportController@retur_selling');
Route::get('/report/selling_by_date', 'ReportController@selling_by_date');
Route::get('/report/selling_by_month', 'ReportController@selling_by_month');
Route::get('/report/selling_by_year', 'ReportController@selling_by_year');
Route::get('/report/profit_loss', 'ReportController@profit_loss');
Route::get('/report/profit_loss_download', 'ReportController@profit_loss_download');
Route::get('/report/customer', 'ReportController@customer');
Route::get('/report/customer_print/{id}', 'ReportController@customer_print');

Route::get('/graph/stock', 'GraphController@stock');
Route::get('/graph/selling_month', 'GraphController@selling_month');
Route::get('/graph/selling_year', 'GraphController@selling_year');

Route::get('/media', 'MediaController@index');
Route::post('/media', 'MediaController@index');
Route::get('/media/edit/{id}', 'MediaController@edit');
Route::post('/media/update/{id}', 'MediaController@update');
Route::get('/media/search/', 'MediaController@search');

Route::get('/menu', 'MenuController@index');
Route::post('/menu', 'MenuController@index');
Route::get('/menu/add', 'MenuController@add');
Route::post('/menu/add', 'MenuController@add');
Route::post('/menu/update', 'MenuController@update');
Route::post('/menu/delete_parent', 'MenuController@delete_parent');
Route::get('/menu/list', 'MenuController@list');

Route::get('/menu/search/', 'MenuController@search');

Route::get('/member', 'MemberController@index');
Route::post('/member', 'MemberController@index');
Route::get('/member/edit/{id}', 'MemberController@edit');
Route::post('/member/update/{id}', 'MemberController@update');
Route::get('/member/add', 'MemberController@add');
Route::post('/member/create', 'MemberController@create');
Route::get('/member/search/', 'MemberController@search');
Route::post('/member/search/', 'MemberController@search');
Route::get('/member/role', 'MemberController@role');
Route::post('/member/role', 'MemberController@role');
Route::get('/member/role_edit/{id}', 'MemberController@role_edit');
Route::post('/member/role_update/{id}', 'MemberController@role_update');
Route::get('/member/role_add', 'MemberController@role_add');
Route::post('/member/role_create', 'MemberController@role_create');
Route::get('/member/role_search/', 'MemberController@role_search');
Route::post('/member/role_search/', 'MemberController@role_search');
Route::get('/profile', 'MemberController@profile');
Route::post('/profile_update', 'MemberController@profile_update');

Route::get('/setting', 'SettingController@index');
Route::post('/setting', 'SettingController@index');
Route::get('/setting/meta/', 'SettingController@meta');
Route::post('/setting/meta/', 'SettingController@meta');
Route::get('/setting/contact/', 'SettingController@contact');
Route::post('/setting/contact/', 'SettingController@contact');
Route::get('/setting/social_network/', 'SettingController@social_network');
Route::post('/setting/social_network/', 'SettingController@social_network');
Route::get('/setting/robots/', 'SettingController@robots');
Route::post('/setting/robots/', 'SettingController@robots');
Route::get('/setting/verified/', 'SettingController@verified');
Route::post('/setting/verified/', 'SettingController@verified');
Route::get('/setting/visitor/', 'SettingController@visitor');
Route::post('/setting/visitor/', 'SettingController@visitor');
Route::post('/setting/update/{id}', 'SettingController@update');

Route::get('/thumb/crop/{width}/{heigh}/{files}', 'ThumbController@crop');
Route::get('/thumb/resize/{width}/{files}', 'ThumbController@resize');

Route::get('/category/{slug}', 'CategoryController@index');
Route::get('/search', 'SearchController@index');
Route::post('/search', 'SearchController@index');
Route::get('/{slug}', 'ArticleController@index');
Route::get('/{slug}/detail', 'ArticleController@detail');
