<?php

namespace App\Exports;

use DB;

use App\Customer;
use App\Helpers\Base;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input as input;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CustomerExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        $post_list = Customer::get();

        $i = 0;
        $post_list_array = array();
        foreach ($post_list as $row) {
            $i++;
            $post_list_array[] = array(
                'r_no'     => $i,
                'r_nama'   => $row->customer_nama,
                'r_alamat' => $row->customer_alamat,
                'r_notelp' => $row->customer_notelp
            );
        }

        return collect($post_list_array);//Target::all();
    }
    public function headings(): array
    {
        return [
            'No',
            'Nama',
            'Alamat',
            'Telepon',
        ];
    }
}
