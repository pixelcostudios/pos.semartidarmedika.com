<html lang="en" moznomarginboxes mozdisallowselectionprint>

<head>
    <title>Faktur Penjualan Barang</title>
    <meta charset="utf-8">
    <style>
        /* @font-face {
            font-family: 'Dot Matrix';
            src: url('{{ url('/') }}/themes/default/fonts/DotMatrix/DotMatrixBold.woff2') format('woff2'),
                url('{{ url('/') }}/themes/default/fonts/DotMatrix/DotMatrixBold.woff') format('woff');
            font-weight: bold;
            font-style: normal;
        }

        @font-face {
            font-family: 'Dot Matrix';
            src: url('{{ url('/') }}/themes/default/fonts/DotMatrix/DotMatrix.woff2') format('woff2'),
                url('{{ url('/') }}/themes/default/fonts/DotMatrix/DotMatrix.woff') format('woff');
            font-weight: normal;
            font-style: normal;
        } */
        body{
            font-family: 'Tahoma';
        }

        @media print {
            @page {
                margin: 0;
            }
            .hide{
                display: none;
            }
            body {
                margin: 5px;
                font-family: 'Tahoma';
                font-weight: normal;
                font-style: normal;
            }
        }
    </style>
</head>
<!--<body onload="window.print()">-->

<body onload="window.print()">
    @foreach ($post_list as $p)
    <div id="laporan">

        <table border="0" align="center" style="width:700px; border:none;margin-top:5px;margin-bottom:0px;">
            <tr>
<td><a class="hide" href="#" onclick="window.print()" style="position: absolute; top:0; right:0"><img src="{{ url('/') }}/themes/default/img/print.png"></a></td>

            </tr>

        </table>
        <div style="text-align: left;">
            <p style="margin:0 0 10px; font-size:27px; text-align:center"><b>Toko PT. Semar Tidar Medika</b></p>
            <p style="margin:0 0 5px; text-align:center">Jl. Blabak Sawangan Km 1.1, Tapen, Pagersari, Blabak, Magelang</p>
            <p style=" margin:0 0 5px; text-align:center">Telp. (0293) 7184563</p>
            <p style="margin:0 0 5px; text-align:center">{{$p->jual_tanggal}}</p>
            <p style=" margin:0 0 5px; border-bottom: 1px dotted #000; width:100%;"></p>

            <table width="100%">
                <tr>
                    <td>No Nota : {{$p->jual_tanggal}}</td>
                    <td style="text-align:right;">Kasir : {{$p->first_name}} {{$p->last_name}}</td>
                </tr>
            </table>

            <p style=" margin:5px 0 5px; border-bottom: 1px dotted #000; width:100%;"></p>
            @php
                $i=0;
                $diskon = array();
                $total = array();
                @endphp
            @foreach (App\Sellingdetail::where('jual_id','=',$p->jual_id)->get() as $value => $key)
                {{$key->d_jual_barang_nama}}<br>
                <span style=" display:block; float:left;">{{$key->d_jual_qty}} x {{currency($key->d_jual_barang_harjul)}} </span>
                <span style="float:right;"> {{currency($key->d_jual_qty*$key->d_jual_barang_harjul)}}</span>
                <p style="margin:20px 0 5px; border-bottom: 1px dotted #000; width:100%; clear:both;"></p>
                @php
                $diskon[] = $key->d_jual_diskon;
                $total[] = $key->d_jual_qty*$key->d_jual_barang_harjul-$key->d_jual_diskon;
                @endphp
            @endforeach
            @php
                $ppn = ($key->jual_transport!='') ? $key->jual_transport : 0; 
                $total_ = ($ppn*array_sum($total))/100;
            @endphp

            <span style="width: 80px; display:block; float:right; text-align:right">{{currency(array_sum($total))}}</span> <span style="float:right;">Total </span> <br>
            <span style="width: 80px; display:block; float:right; text-align:right">{{currency(array_sum($diskon))}}</span><span style="float:right;">Diskon </span> <br>
            <span style="width: 80px; display:block; float:right; text-align:right">{{currency(array_sum($total))}}</span><span style="float:right;">Bayar </span> <br>
            <span style="width: 80px; display:block; float:right; text-align:right">{{currency((array_sum($total)+$total_)-$p->jual_jml_uang)}}</span><span style="float:right;">Kembalian </span>
            <p style="margin:20px 0 5px; border-bottom: 1px dotted #000; width:100%; clear:both;"></p>
            <br>

            <div style="width:100%; text-align:center;">NPWP : Semar Tidar Medika - 63.602.604.9-524.000<br>Rekening : (BNI) Semar Tidar Medika - 2070220225</div>

            ( {{$p->first_name}} {{$p->last_name}} )

        </div>

    </div>
    @endforeach
</body>

</html>
