<?php
namespace App\Http\Controllers;

use DB;
use Image;

use App\Post;
use App\Categories;
use App\Meta;
use App\Inflation;
use App\Setting;
use App\Helpers\Base;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input as input;

use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function index()
    {
        $post_list      = DB::table('posts')->where('post_slug', request()->segment(1))->get();
        $recent_list    = Post::leftJoin('metas', 'posts.post_id', '=', 'metas.meta_source')->whereIn('meta_dest', array('3,4'))->where('posts.post_type', 'post')->orderBy('date', 'DESC')->limit(5)->get();
        $category_list  = Categories::where('cats.cat_type', 'post')->where('cats.cat_up', 0)->orderBy('cat_date', 'DESC')->get();
        $setting_list   = Setting::where('setting_type', 'meta')->where('setting_status', '1')->get();
        $online_list    = Post::where('post_up', '=', '42')->get();
        $offline_list   = Post::where('post_up', '=', '43')->get();

        $facebook   = Setting::where('setting_name', '=', 'facebook')->limit('1')->value('setting_value');
        $youtube   = Setting::where('setting_name', '=', 'youtube')->limit('1')->value('setting_value');
        $instagram   = Setting::where('setting_name', '=', 'instagram')->limit('1')->value('setting_value');

        $menu = display_menu(5);

        foreach ($setting_list as $key => $value) {
            $setting[] = $value->setting_value;
        }
        $meta_title     = ($setting) ? $setting[2] : '';
        $meta_key       = ($setting) ? $setting[1] : '';
        $meta_desc      = ($setting) ? $setting[0] : '';

        foreach ($post_list as $key)
        {
            $data_array = [
                'post_list'     => $post_list,
                'recent_list'   => $recent_list,
                'title'         => $key->post_title,
                'menu'          => $menu,
                'description'   => $key->post_summary,
                'keyword'       => $key->post_key,
                'online_list'   => $online_list,
                'offline_list'  => $offline_list,
                'category_list' => $category_list,
                'facebook'      => $facebook,
                'youtube'       => $youtube,
                'instagram'     => $instagram,
                'gallery'       => Post::where('post_type', 'images')->where([['post_up', '=', $key->post_id]])->get(),
                'setting'       => array('meta_title' => $meta_title, 'meta_keyword' => $meta_key, 'meta_desc' => $meta_desc),
                'url'           => request()->segment(1)
                ];
            if ($key->post_id == '2')
            {
                return view('front/contact', $data_array);
            }
            elseif ($key->post_id == '41')
            {
                return view('front/team', $data_array);
            }
            elseif ($key->post_id == '1' OR $key->post_id == '31')
            {
                return view('front/about', $data_array);
            }
            elseif($key->post_type=='pages')
            {
                return view('front/pages', $data_array);
            }
            else
            {
                return view('front/article', $data_array);
            }
        }
    }
}
