<?php

namespace App\Http\Controllers;
use DB;
use App\Item;
use App\Itemdiscount;
use App\Unit;
use App\Categories;
use App\Helpers\Base;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input as input;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    public function index()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        if (Input::get('show') != '') {
            $show = Input::get('show');
        } else {
            $show = 10;
        }

        $post_list = Item::selectRAW('barang_id, barang_nama, barang_satuan, barang_harjul, barang_harjul_grosir, barang_stok, barang_min_stok, cat_title')->leftJoin('cats', 'cats.cat_id', '=', 'items.barang_kategori_id')->orderby(DB::raw("barang_id+0"), 'DESC')->paginate($show);

        if (isset($_POST['post-delete']) && $_POST['post-delete'] != "") {
            for ($i = 0; $i < count(@$_POST['post-check']); $i++) {
                $post_id = @$_POST['post-check'][$i];

                Item::where('barang_id', '=', $post_id)->limit(1)->delete();
            }
            return redirect('/item/manage/')->with('success', 'Post updated!');
        } elseif (isset($_POST['post-deleted']) && $_POST['post-deleted'] != "") {
            $post_id = $_POST['post_id'];

            Item::where('barang_id', '=', $post_id)->limit(1)->delete();

            return redirect('/item/manage/')->with('success', 'Post updated!');
        }

        return view('back/item', ['post_list' => $post_list, 'show' => $show, 'copyright' => copyright()]);
    }
    public function search()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        if (Input::get('show') != '') {
            $show = Input::get('show');
        } else {
            $show = 10;
        }
        if (Input::get('query') != '') {
            $query = Input::get('query');
        } else {
            $query = '';
        }

        $post_list = Item::selectRAW('barang_id, barang_nama, barang_satuan, barang_harjul, barang_harjul_grosir, barang_stok, barang_min_stok, cat_title')->leftJoin('cats', 'cats.cat_id', '=', 'items.barang_kategori_id')->where('items.barang_id', 'like', '%' . $query . '%')->orWhere('items.barang_nama', 'like', '%' . $query . '%')->orderby(DB::raw("barang_id+0"), 'DESC')->paginate($show);

        if (isset($_POST['post-delete']) && $_POST['post-delete'] != "") {
            for ($i = 0; $i < count(@$_POST['post-check']); $i++) {
                $post_id = @$_POST['post-check'][$i];

                Item::where('barang_id', '=', $post_id)->limit(1)->delete();
            }
            return redirect('/item/manage/')->with('success', 'Post updated!');
        } elseif (isset($_POST['post-deleted']) && $_POST['post-deleted'] != "") {
            $post_id = $_POST['post_id'];

            Item::where('barang_id', '=', $post_id)->limit(1)->delete();

            return redirect('/item/manage/')->with('success', 'Post updated!');
        }

        return view('back/item_search', ['post_list' => $post_list, 'query' => $query, 'show' => $show, 'copyright' => copyright()]);
    }
    public function edit($id)
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        $post_edit      = Item::where('barang_id', $id)->get();
        $discount_list  = Itemdiscount::where('barang_id', $id)->get();
        $unit_list      = Unit::get();
        $category_list  = Categories::where('cat_type','=','product')->get();

        return view('back/item_edit', [
            'post_edit' => $post_edit,
            'discount_list' => $discount_list,
            'unit_list' => $unit_list,
            'category_list' => $category_list,
            'copyright' => copyright()
        ]);
    }
    public function add()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        $unit_list      = Unit::get();
        $category_list  = Categories::where('cat_type', '=', 'product')->get();

        $q = Item::selectRaw('MAX(RIGHT(barang_id,6)) AS kd_max')->get();// $this->db->query("SELECT MAX(RIGHT(barang_id,6)) AS kd_max FROM tbl_barang");
        $kd = ""; //print_r(Item::count());
        // print_r($q[0]['kd_max']);
        if (Item::count() > 0) {
            // foreach ($q as $k) {
                $tmp = ((int) $q[0]['kd_max']) + 1;
                $kd = sprintf("%06s", $tmp);
            // }
        } else {
            $kd = "000001";
        }
        $code = "BR" . $kd;

        return view('back/item_add', [
            'code'      => $code,
            'unit_list' => $unit_list,
            'category_list' => $category_list,
            'copyright' => copyright()
        ]);
    }
    public function create(Request $request)
    {
        $barang_id              = ($request->get('barang_id') != '') ? $request->get('barang_id') : '';
        $barang_nama            = ($request->get('barang_nama') != '') ? $request->get('barang_nama') : '';
        $barang_satuan          = ($request->get('barang_satuan') != '') ? $request->get('barang_satuan') : '';
        $barang_no_shading      = ($request->get('barang_no_shading') != '') ? $request->get('barang_no_shading') : '';
        $barang_harpok          = ($request->get('barang_harpok') != '') ? $request->get('barang_harpok') : 0;
        $barang_harjul          = ($request->get('barang_harjul') != '') ? $request->get('barang_harjul') : 0;
        $barang_stok            = ($request->get('barang_stok') != '') ? $request->get('barang_stok') : 0;
        $barang_min_stok        = ($request->get('barang_min_stok') != '') ? $request->get('barang_min_stok') : 0;
        $barang_tgl_input       = date('Y-m-d H:i:s');
        $barang_tgl_last_update = date('Y-m-d H:i:s');
        $barang_kategori_id     = ($request->get('barang_kategori_id') != '') ? $request->get('barang_kategori_id') : 0;

        if(Item::where('barang_id',$barang_id)->count()==0)
        {
            Item::insert([
                'barang_id' => $barang_id,
                'barang_nama' => $barang_nama,
                'barang_satuan' => $barang_satuan,
                'barang_no_shading' => $barang_no_shading,
                'barang_harpok' => $barang_harpok,
                'barang_harjul' => $barang_harjul,
                'barang_harjul_grosir' => '0',
                'barang_harjul_grosir_1' => '0',
                'barang_harjul_grosir_2' => '0',
                'barang_harjul_grosir_3' => '0',
                'barang_stok' => $barang_stok,
                'barang_min_stok' => $barang_min_stok,
                'barang_tgl_input' => $barang_tgl_input,
                'barang_tgl_last_update' => $barang_tgl_last_update,
                'barang_kategori_id' => $barang_kategori_id,
                'barang_user_id' => Auth::user()->id,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'deleted_at' => date('Y-m-d H:i:s')
            ]);

            $id = DB::getPdo()->lastInsertId();

            if ($request->get('value') != '') {
                $p_note     = $request->get('value');
                $post_qty   = $request->get('price');
                foreach ($p_note as $key => $n) {
                    if ($n != '') {
                        // $data_array = array(
                        //     'barang_id'    => $id,
                        //     'discount_value'    => $n,
                        //     'discount_price'    => $post_qty[$key]
                        // );
                        Itemdiscount::insert([
                            'barang_id' => $id,
                            'discount_value' => $n,
                            'discount_price' => $post_qty[$key]
                        ]);
                    }
                }
            }
        }
        
        return redirect('/item/manage')->with('success', 'Post updated!');
    }
    public function update(Request $request, $id)
    {

        $data_array = array(
            'barang_nama'           => ($request->get('barang_nama') != '') ? $request->get('barang_nama') : '',
            'barang_satuan'         => ($request->get('barang_satuan') != '') ? $request->get('barang_satuan') : '',
            'barang_no_shading'     => ($request->get('barang_no_shading') != '') ? $request->get('barang_no_shading') : '',
            'barang_harpok'         => ($request->get('barang_harpok') != '') ? $request->get('barang_harpok') : 0,
            'barang_harjul'         => ($request->get('barang_harjul') != '') ? $request->get('barang_harjul') : 0,
            'barang_harjul_grosir'  => ($request->get('barang_harjul_grosir') != '') ? $request->get('barang_harjul_grosir') : 0,
            'barang_stok'           => ($request->get('barang_stok') != '') ? $request->get('barang_stok') : 0,
            'barang_min_stok'       => ($request->get('barang_min_stok') != '') ? $request->get('barang_min_stok') : 0,
            'barang_kategori_id'    => ($request->get('barang_kategori_id') != '') ? $request->get('barang_kategori_id') : 0,
        );
        DB::table('items')
            ->where('barang_id', $id)
            ->update($data_array);
        // $post_data = Item::find($id);
        // $post_data->barang_nama            = ($request->get('barang_nama') != '') ? $request->get('barang_nama') : '';
        // $post_data->barang_satuan          = ($request->get('barang_satuan') != '') ? $request->get('barang_satuan') : '';
        // $post_data->barang_no_shading      = ($request->get('barang_no_shading') != '') ? $request->get('barang_no_shading') : '';
        // $post_data->barang_harpok          = ($request->get('barang_harpok') != '') ? $request->get('barang_harpok') : 0;
        // $post_data->barang_harjul          = (cbarang_harjul') != '') ? $request->get('barang_harjul') : 0;
        // $post_data->barang_harjul_grosir   = ($request->get('barang_harjul_grosir') != '') ? $request->get('barang_harjul_grosir') : 0;
        // $post_data->barang_stok            = ($request->get('barang_stok') != '') ? $request->get('barang_stok') : 0;
        // $post_data->barang_min_stok        = ($request->get('barang_min_stok') != '') ? $request->get('barang_min_stok') : 0;
        // $post_data->barang_kategori_id     = ($request->get('barang_kategori_id') != '') ? $request->get('barang_kategori_id') : 0;
        // $post_data->save();

        foreach (Itemdiscount::where('barang_id', $id)->get() as $key_product) {
            $discount_value         = $request->get('value_' . $key_product['discount_id']);
            $discount_price         = $request->get('price_' . $key_product['discount_id']);

            $data_discount_array = array(
                'discount_value'      => $discount_value,
                'discount_price'         => $discount_price,
            );
            DB::table('item_discounts')
                ->where('discount_id', $key_product['discount_id'])
                ->update($data_discount_array);
            // $this->m_barang->barang_discount_update($key_product['discount_id'], $data_array);
        }

        if ($request->get('value')!='') {
            $p_note     = $request->get('value');
            $post_qty   = $request->get('price');
            foreach ($p_note as $key => $n) {
                if ($n != '') {
                    // $data_array = array(
                    //     'barang_id'    => $id,
                    //     'discount_value'    => $n,
                    //     'discount_price'    => $post_qty[$key]
                    // );
                    Itemdiscount::insert([
                        'barang_id' => $id,
                        'discount_value' => $n,
                        'discount_price' => $post_qty[$key]
                    ]);
                }
            }
        }

        return redirect('/item/edit/' . $id)->with('success', 'Post updated!');
    }
    //Unit
    public function unit()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        if (Input::get('show') != '') {
            $show = Input::get('show');
        } else {
            $show = 10;
        }

        $post_list = Unit::paginate($show);

        if (isset($_POST['post-delete']) && $_POST['post-delete'] != "") {
            for ($i = 0; $i < count(@$_POST['post-check']); $i++) {
                $post_id = @$_POST['post-check'][$i];

                Unit::where('unit_id', '=', $post_id)->delete();
            }
            return redirect('/item/unit/')->with('success', 'Post updated!');
        } elseif (isset($_POST['post-deleted']) && $_POST['post-deleted'] != "") {
            $post_id = $_POST['post_id'];

            Unit::where('unit_id', '=', $post_id)->delete();

            return redirect('/item/unit/')->with('success', 'Post updated!');
        }

        return view('back/item_unit', ['post_list' => $post_list, 'show' => $show, 'copyright' => copyright()]);
    }
    public function unit_search()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        if (Input::get('show') != '') {
            $show = Input::get('show');
        } else {
            $show = 10;
        }
        if (Input::get('query') != '') {
            $query = Input::get('query');
        } else {
            $query = '';
        }
        $post_list = Unit::where('unit_nama', 'like', '%' . $query . '%')->paginate($show);

        if (isset($_POST['post-delete']) && $_POST['post-delete'] != "") {
            for ($i = 0; $i < count(@$_POST['post-check']); $i++) {
                $post_id = @$_POST['post-check'][$i];

                Unit::where('unit_id', '=', $post_id)->delete();
            }
            return redirect('/item/unit/')->with('success', 'Post updated!');
        } elseif (isset($_POST['post-deleted']) && $_POST['post-deleted'] != "") {
            $post_id = $_POST['post_id'];

            Unit::where('unit_id', '=', $post_id)->delete();

            return redirect('/item/unit/')->with('success', 'Post updated!');
        }

        return view('back/item_unit_search', ['post_list' => $post_list, 'show' => $show, 'copyright' => copyright()]);
    }
    public function unit_edit($id)
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        $post_edit      = Unit::where('unit_id', $id)->get();

        return view('back/item_unit_edit', [
            'post_edit' => $post_edit,
            'copyright' => copyright()
        ]);
    }
    public function unit_add()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        return view('back/item_unit_add', [
            'copyright' => copyright()
        ]);
    }
    public function unit_create(Request $request)
    {
        $unit_nama     = ($request->get('unit_nama') != '') ? $request->get('unit_nama') : '';

        Unit::insert([
            'unit_nama' => $unit_nama,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'deleted_at' => date('Y-m-d H:i:s')
        ]);

        $id = DB::getPdo()->lastInsertId();
        return redirect('/item/unit_edit/' . $id)->with('success', 'Post updated!');
    }
    public function unit_update(Request $request, $id)
    {
        $post_data = Unit::find($id);
        $post_data->unit_nama = ($request->get('unit_nama') != '') ? $request->get('unit_nama') : '';
        $post_data->save();

        return redirect('/item/unit_edit/' . $id)->with('success', 'Post updated!');
    }
}
