@extends('layouts.post.post_add')

@section('content')
@include('layouts.navtop')
<div class="row wrapper border-bottom white-bg page-heading">
<div class="col-lg-10">
<h2>Post</h2>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="{{ url('home') }}">Home</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ url('post') }}">Post</a>
    </li>
    <li class="breadcrumb-item active">
        <strong>All Post</strong>
    </li>
</ol>
</div>
<div class="col-lg-2">

</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
            <form name="myform" method="post" action="{{ url('post/create/') }}" enctype="multipart/form-data" class="form" data-async>
                @csrf
				<div class="row">
					<div class="col-lg-9 padding-none">
						<div class="ibox ">
							<div class="ibox-title">
								<h5>Post Editor</h5>
								<a href="{{ url('post/add') }}" class="btn btn-default pull-left">Add New Article</a>
								<button type="submit" name="Pixel_Save" class="btn btn-primary float-right">Save Article</button>
							</div>
							<div class="ibox-content">
								<div class="form-group">
									<label for="title">Title ID </label>
									<input type="text" class="form-control" id="title" name="post_title" placeholder="Title ID" required="required">
								</div>
								<div class="row">
									<div class="form-group col-lg-4 col-sm-4 col-xs-12">
										<label for="status">Parent</label>
										<select name="post_up" class="form-control select2">
											<option value="0">Select Parent / No Parent</option>
											@foreach($post_list as $p_row)
											<option value="{{$p_row['post_id']}}">{{$p_row['post_title']}} [ID#{{$p_row['post_id']}}]
											</option>
											@endforeach
										</select>
									</div>
									<div class="form-group col-lg-4 col-sm-4 col-xs-12 multi_select clearfix">
										<label for="category">Category</label><br>
										<select id="example-multiple-selected" class="form-control margin-bottom-10" multiple="multiple" name="categories[]">
											@foreach($cat_list as $p_cat)
											<option value="{{$p_cat['cat_id']}}">{{$p_cat['cat_title']}}</option>
											@endforeach
										</select>
                                    </div>
                                    <div class="form-group col-lg-4 col-sm-4 col-xs-12">
										<label for="status">Show Gallery</label>
										<select name="post_thumb" class="form-control">
											<option value="1">Yes</option>
											<option value="0">No</option>
										</select>
									</div>
                                </div>
                                <div class="row">
									<div class="form-group col-lg-6 col-sm-6 col-xs-12">
										<label for="status">Status</label>
										<select name="status" class="form-control">
											<option value="1">Publish</option>
											<option value="0">Draft</option>
										</select>
									</div>
									<div class="form-group col-lg-6 col-sm-6 col-xs-12">
										<label for="date">Date</label>
										<div class="input-group">
											<input name="date" class="form-control date-picker" type="text" data-date-format="yyyy-mm-dd hh:mm">
											<span class="input-group-addon tooltip-demo">
												<i class="fa fa-calendar" data-toggle="tooltip" data-placement="top" title="Calendar"></i>
											</span>
										</div>
									</div>
								</div>

								<div class="form-group clear">
									<label for="content">Content ID</label>
									<textarea id="content_id" name="post_content"></textarea>
								</div>

								<div class="form-group clear">
									<label for="summary">Summary ID</label>
									<textarea id="summary_id" name="post_summary"></textarea>
								</div>

								<div class="form-group">
									<label for="title">SEO Key ID</label>
									<input type="text" class="form-control" name="post_key" placeholder="SEO Key ID" value="">
								</div>
								<div class="form-group">
									<label for="title">SEO Desc ID</label>
									<textarea type="text" class="form-control" name="post_desc" placeholder="SEO Desc ID"></textarea>
								</div>
								<div class="row">
									<div class="form-group col-lg-6 col-sm-6 col-xs-12 padding-left-0 padding-xs-none">
										<label for="Comment">Comment</label>
										<select name="post_comment" class="form-control">
											<option value="1">Yes</option>
											<option value="0">No</option>
										</select>
									</div>
									<div class="form-group col-lg-6 col-sm-6 col-xs-12 padding-right-0 padding-xs-none">
										<label for="Username">Username</label>
										<select name="user_id" class="form-control">
											@foreach($user_list as $p_row)
											<option value="{{$p_row->id}}">{{$p_row->first_name}} {{$p_row->last_name}}
											</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="clear"></div>
							</div>
						</div>
					</div>
					<div class="col-lg-3 padding-right-0">
						<div class="bar-title white-bg">
							<div class="tabs-container">
								<div class="tabs-left">
									<ul class="nav nav-tabs tooltip-demo">
										<li><a data-toggle="tab" class="active show" href="#tab_images" aria-expanded="true"> <i class="fa fa-picture-o"
												 data-toggle="tooltip" data-placement="top" title="Images"></i></a></li>
										<li class=""><a data-toggle="tab" href="#tab_icon" aria-expanded="false"><i class="fa fa-dot-circle-o"
												 data-toggle="tooltip" data-placement="top" title="Icon"></i></a></li>
										<li class=""><a data-toggle="tab" href="#tab_thumb" aria-expanded="false"><i class="fa fa fa-camera"
												 data-toggle="tooltip" data-placement="top" title="Thumb"></i></a></li>
										<li class=""><a data-toggle="tab" href="#tab_background" aria-expanded="false"><i class="fa fa-th-large"
												 data-toggle="tooltip" data-placement="top" title="Background"></i></a></li>
										<li class=""><a data-toggle="tab" href="#tab_pdf" aria-expanded="false"><i class="fa fa-bookmark" data-toggle="tooltip"
												 data-placement="top" title="PDF"></i></a></li>
										<li class=""><a data-toggle="tab" href="#tab_files" aria-expanded="false"><i class="fa fa-file" data-toggle="tooltip"
												 data-placement="top" title="Files"></i></a></li>
									</ul>
									<div class="tab-content ">
										<div id="tab_images" class="tab-pane active">
											<div class="panel-body">
												<div class="input-file"><input type="file" name="upload_photo[]" multiple="multiple" id="input_images"></div>
											</div>
										</div>
										<div id="tab_icon" class="tab-pane">
											<div class="panel-body">
												<div class="input-file"><input type="file" name="upload_icon[]" multiple="multiple" id="input_icon"></div>
											</div>
										</div>
										<div id="tab_thumb" class="tab-pane">
											<div class="panel-body">
												<div class="input-file"><input type="file" name="upload_thumb[]" multiple="multiple" id="input_thumb"></div>
											</div>
										</div>
										<div id="tab_background" class="tab-pane">
											<div class="panel-body">
												<div class="input-file"><input type="file" name="upload_background[]" multiple="multiple" id="input_background"></div>
											</div>
										</div>
										<div id="tab_pdf" class="tab-pane">
											<div class="panel-body">
												<div class="input-file"><input type="file" name="upload_pdf[]" multiple="multiple" id="input_pdf"></div>
											</div>
										</div>
										<div id="tab_files" class="tab-pane">
											<div class="panel-body">
												<div class="input-file"><input type="file" name="upload_files[]" multiple="multiple" id="input_files"></div>
											</div>
										</div>
									</div>

								</div>

							</div>
							<!--End Tab-->

							<div class="clear"></div>
						</div>
					</div>
                </div>

            </form>
		</div>
<div class="footer">
<div class="float-right">
<strong>
    </strong>
</div>
<div>
<strong>Copyright</strong> {{$copyright}}
</div>
</div>
@endsection
