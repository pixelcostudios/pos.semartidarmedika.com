<script>
function loadPage(page){					
	newwindow=window.open('index.php/agents/'+page,'name','height=750,width=1200,toolbar=0,menubar=0,location=0,scrollbars=yes');
		if (window.focus) {newwindow.focus()}
			return false;
}
</script>
<div class="row">
			<div class="col-md-12">
             <button type="button" class="btn btn-sm btn-success" onClick="loadPage('spareparts')"> Sparepart</button>
             <button type="button" class="btn btn-sm btn-success" onClick="loadPage('dealers')"> Dealers</button>
             <button type="button" class="btn btn-sm btn-success" onClick="loadPage('partshops')"> Partshops</button>
             <button type="button" class="btn btn-sm btn-success" onClick="loadPage('pricelist')"> Pricelist</button>
             <button type="button" class="btn btn-sm btn-success" onClick="loadPage('troubleshooting')"> Troubleshooting</button>
             <button type="button" class="btn btn-sm btn-success" onClick="loadPage('spesification')"> Spesification</button>
		<br><br>
<?php 
$user = $this->session->userdata('users')->user_name;
$pass = md5($this->session->userdata('users')->password);
?>
<script>
var refreshId = setInterval(function()
{
    //$('#status').load('index.php/agents/status');
	$('#agent_status').load('index.php/agents/status');
	$('#incoming').load('index.php/agents/incoming_total'); 
	$('#total_call').load('index.php/agents/total_call');
	$('#outgoing').load('index.php/agents/outgoing_total');
	$('#missed').load('index.php/agents/missed_total');
	$('#incoming_list').load('index.php/agents/today_incoming');
	$('#abandon').load('index.php/agents/abandon');
	$('#queue_in').load('index.php/agents/inqueue');
}, 5000);
</script>
<!-- 5. $UPLOADS_CHART =============================================================================

				Uploads chart
-->
					<!-- Javascript -->
				<script>
					init.push(function () {
						// Easy Pie Charts
						var easyPieChartDefaults = {
							animate: 2000,
							scaleColor: false,
							lineWidth: 6,
							lineCap: 'square',
							size: 90,
							trackColor: '#e5e5e5'
						}
						$('#easy-pie-chart-1').easyPieChart($.extend({}, easyPieChartDefaults, {
							barColor: LanderApp.settings.consts.COLORS[1]
						}));
						$('#easy-pie-chart-2').easyPieChart($.extend({}, easyPieChartDefaults, {
							barColor: LanderApp.settings.consts.COLORS[1]
						}));
						$('#easy-pie-chart-3').easyPieChart($.extend({}, easyPieChartDefaults, {
							barColor: LanderApp.settings.consts.COLORS[1]
						}));
					});
				</script>
				<!-- / Javascript -->
				<script>
					function openWO(){					
					newwindow=window.open('index.php/agents/callnote/0' , 'name','height=550,width=500,toolbar=0,menubar=0,location=0');
						if (window.focus) {newwindow.focus()}
						return false;
					}
                </script>
                <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-sm modal-info">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Pause agent</h4>
        </div>
        <?php echo form_open('agents/pause');?>
        <script>
        function notes_active(val){
			if(val==='Lainnya'){
				$("#notes").attr('required',true);
			}
			else{
				$("#notes").attr('required',false);
			}	
		}
        </script>
        <div class="modal-body">
          
          	<table class="table">
            	<tr>
                	<td>Reason</td>
                    <td>
                    	<input type="hidden" name="interface" value="<?php echo $this->session->userdata('interface');?>">
                    	<select name="pause_reason" class="form-control" onChange="notes_active(this.value)" >
                        	<option value="0">- Choose - </option>
                            <option value="Briefing"> Briefing </option>
                            <option value="Istirahat Makan"> Istirahat Makan</option>
                            <option value="Shalat"> Shalat</option>
                            <option value="Toilet"> Toilet</option>
                            <option value="Lainnya"> Lainnya</option>
                        </select>
                    </td>
               </tr>
               <tr>
               		<td>Notes</td>
                    <td>
                    	<input name="pause_note" class="form-control" id="notes" />
                    </td>
                </tr>
            </table>
          
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-default" >Simpan</button>
          
        </div>
        <?php echo form_close();?>
      </div>
    </div>
  </div>
  <!-- end Modal -->
				<div class="stat-panel">
					<div class="stat-row">
						<!-- Small horizontal padding, bordered, without right border, top aligned text -->
						<div class="stat-cell col-sm-4 padding-sm-hr bordered no-border-r valign-top">
                        <?php if($this->session->userdata('paused')!=1){?>
                        	<button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#myModal">
                            <i class="fa fa-pause"></i> Pause</button>
                        <?php } else{?>
                        	<a href="<?php echo base_url().'index.php/agents/unpause';?>" class="btn btn-danger btn-sm"> <i class="fa fa-play"></i> Unpaused</a>
                        <?php }?>
							<!-- Small padding, without top padding, extra small horizontal padding -->
							<h4 class="padding-sm no-padding-t padding-xs-hr"><i class="fa fa-phone text-primary"></i>&nbsp;&nbsp;Today Calls : 
                            <div id="total_call" style="font-size:45px; text-align:center;"></div></h4>
							<!-- Without margin -->
                            <ul class="list-group no-margin">
                            <div id="agent_status"></div>
                         
                            </ul>
					
						</div> <!-- /.stat-cell -->
						<!-- Primary background, small padding, vertically centered text -->
						<div class="stat-cell col-sm-8  padding-sm valign-middle">
							<div class="col-xs-4">
						<!-- Centered text -->
						<div class="stat-panel text-center">
							<div class="stat-row">
								<!-- Dark gray background, small padding, extra small text, semibold text -->
								<div class="stat-cell bg-dark-gray padding-sm text-xs text-semibold">
									<i class="fa fa-sign-in"></i>&nbsp;&nbsp;Incoming Call
								</div>
							</div> <!-- /.stat-row -->
							<div class="stat-row">
								<!-- Bordered, without top border, without horizontal padding -->
								<div class="stat-cell bordered no-border-t no-padding-hr">
									<div class="pie-chart" data-percent="<?php echo $this->session->userdata('incoming_percent');?>" id="easy-pie-chart-1">
										<div class="pie-chart-label"><div id="incoming" style="font-size:36px;"></div></div>
									</div>
								</div>
							</div> <!-- /.stat-row -->
						</div> <!-- /.stat-panel -->
					</div>
					<div class="col-xs-4">
						<div class="stat-panel text-center">
							<div class="stat-row">
								<!-- Dark gray background, small padding, extra small text, semibold text -->
								<div class="stat-cell bg-dark-gray padding-sm text-xs text-semibold">
									<i class="fa fa-sign-out"></i>&nbsp;&nbsp;Outgoing Call
								</div>
							</div> <!-- /.stat-row -->
							<div class="stat-row">
								<!-- Bordered, without top border, without horizontal padding -->
								<div class="stat-cell bordered no-border-t no-padding-hr">
									<div class="pie-chart" data-percent="<?php echo $this->session->userdata('outgoing_percent');?>" id="easy-pie-chart-2">
										<div class="pie-chart-label"><div id="outgoing" style="font-size:36px;" ></div></div>
									</div>
								</div>
							</div> <!-- /.stat-row -->
						</div> <!-- /.stat-panel -->
					</div>
					<div class="col-xs-4">
						<div class="stat-panel text-center">
							<div class="stat-row">
								<!-- Dark gray background, small padding, extra small text, semibold text -->
								<div class="stat-cell bg-dark-gray padding-sm text-xs text-semibold">
									<i class="fa fa-phone"></i>&nbsp;&nbsp;Missed Call
								</div>
							</div> <!-- /.stat-row -->
							<div class="stat-row">
								<!-- Bordered, without top border, without horizontal padding -->
								<div class="stat-cell bordered no-border-t no-padding-hr">
									<div class="pie-chart" data-percent="<?php echo $this->session->userdata('missed_percent');?>" id="easy-pie-chart-3">
										<div class="pie-chart-label"><div id="missed" style="font-size:36px;"></div></div>
									</div>
								</div>
							</div> <!-- /.stat-row -->
						</div> <!-- /.stat-panel -->
					</div>
						</div>
					</div>
				</div> <!-- /.stat-panel -->
<!-- /5. $UPLOADS_CHART -->


			</div>
<!-- /6. $EASY_PIE_CHARTS -->

			
		</div>
<!-- /9. $UNIQUE_VISITORS_STAT_PANEL -->

		<!-- Page wide horizontal line -->
		<hr class="no-grid-gutter-h grid-gutter-margin-b no-margin-t">

		<div class="row">

<!-- 10. $SUPPORT_TICKETS ==========================================================================

			Support tickets
-->
			<!-- Javascript -->
			<script>
				init.push(function () {
					$('#dashboard-support-tickets .panel-body > div').slimScroll({ height: 300, alwaysVisible: true, color: '#888',allowPageScroll: true });
				})
			</script>
			<!-- / Javascript -->
			<div class="col-md-4">
				<div class="panel panel-warning widget-support-tickets" id="dashboard-support-tickets">
					<div class="panel-heading">
						<span class="panel-title"><i class="panel-title-icon fa fa-bullhorn"></i>In Queue</span>
						<div class="panel-heading-controls">
							<div class="panel-heading-text"><a href="#"></a></div>
						</div>
					</div> <!-- / .panel-heading -->
					<div class="panel-body tab-content-padding">
						<!-- Panel padding, without vertical padding -->
						<div class="panel-padding no-padding-vr">
							<div id="queue_in"></div>
							

						</div>
					</div> <!-- / .panel-body -->
				</div> <!-- / .panel -->
			</div>
			<div class="col-md-4">
				<div class="panel panel-success widget-support-tickets" id="dashboard-support-tickets">
					<div class="panel-heading">
						<span class="panel-title"><i class="panel-title-icon fa fa-bullhorn"></i>Incoming Call Activity</span>
						<div class="panel-heading-controls">
							<div class="panel-heading-text"><a href="#">Last 5 Calls</a></div>
						</div>
					</div> <!-- / .panel-heading -->
					<div class="panel-body tab-content-padding">
						<!-- Panel padding, without vertical padding -->
						<div class="panel-padding no-padding-vr">
							<div id="incoming_list"></div>
							

						</div>
					</div> <!-- / .panel-body -->
				</div> <!-- / .panel -->
			</div>
            
            <div class="col-md-4">
				<div class="panel panel-danger widget-support-tickets" id="dashboard-support-tickets">
					<div class="panel-heading">
						<span class="panel-title"><i class="panel-title-icon fa fa-bullhorn"></i>Abandoned Call</span>
						<div class="panel-heading-controls">
							<div class="panel-heading-text"><a href="#">Last 5 Calls</a></div>
						</div>
					</div> <!-- / .panel-heading -->
					<div class="panel-body tab-content-padding">
						<!-- Panel padding, without vertical padding -->
						<div class="panel-padding no-padding-vr">
						
							<div id="abandon"></div>
						</div>
					</div> <!-- / .panel-body -->
				</div> <!-- / .panel -->
			</div>
<!-- /10. $SUPPORT_TICKETS -->


<!-- /11. $RECENT_ACTIVITY -->
		</div>

		<!-- Page wide horizontal line -->
		<hr class="no-grid-gutter-h grid-gutter-margin-b no-margin-t">

	              <!-- Modal Call Note -->
  <div class="modal fade" id="callNote" role="dialog">
    <div class="modal-dialog modal-sm modal-info">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Pause agent</h4>
        </div>
        <?php echo form_open('agents/pause');?>
        <script>
        function notes_active(val){
			if(val==='Lainnya'){
				$("#notes").attr('required',true);
			}
			else{
				$("#notes").attr('required',false);
			}	
		}
        </script>
        <div class="modal-body">
          
          	<table class="table">
            	<tr>
                	<td>Reason</td>
                    <td>
                    	<input type="hidden" name="interface" value="<?php echo $this->session->userdata('interface');?>">
                    	<select name="pause_reason" class="form-control" onChange="notes_active(this.value)" >
                        	<option value="0">- Choose - </option>
                            <option value="Briefing"> Briefing </option>
                            <option value="Istirahat Makan"> Istirahat Makan</option>
                            <option value="Shalat"> Shalat</option>
                            <option value="Toilet"> Toilet</option>
                            <option value="Lainnya"> Lainnya</option>
                        </select>
                    </td>
               </tr>
               <tr>
               		<td>Notes</td>
                    <td>
                    	<input name="pause_note" class="form-control" id="notes" />
                    </td>
                </tr>
            </table>
          
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-default" >Simpan</button>
          
        </div>
        <?php echo form_close();?>
      </div>
    </div>
  </div>
  <!-- end Modal -->	