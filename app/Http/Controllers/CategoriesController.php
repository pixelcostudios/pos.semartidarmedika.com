<?php

namespace App\Http\Controllers;

use DB;
use Image;

use App\Post;
use App\Categories;
use App\Helpers\Base;
use App\Http\Controllers\Slug;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input as input;

use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function index()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        if (Input::get('show') != '') {
            $show = Input::get('show');
        } else {
            $show = 10;
        }
        if (Input::get('type') != '') {
            $type = Input::get('type');
            $post_list = Categories::where('cats.cat_type', $type)->where('cats.cat_up', 0)->orderBy('cat_date', 'DESC')->paginate($show);
        } else {
            $type = '';
            $post_list = Categories::where('cats.cat_up', 0)->orderBy('cat_date', 'DESC')->paginate($show);
        }

        if (isset($_POST['post-publish']) && $_POST['post-publish'] != "") {
            for ($i = 0; $i < count(@$_POST['post-check']); $i++) {
                $post_id = @$_POST['post-check'][$i];
                DB::table('cats')
                    ->where('cat_id', $post_id)
                    ->update(['cat_status' => '1']);
            }
            return redirect('/categories/?type='. $type)->with('success', 'Post updated!');
        }
        elseif (isset($_POST['post-draft']) && $_POST['post-draft'] != "") {
            for ($i = 0; $i < count(@$_POST['post-check']); $i++) {
                $post_id = @$_POST['post-check'][$i];
                DB::table('cats')
                    ->where('cat_id', $post_id)
                    ->update(['cat_status' => '0']);
            }
            return redirect('/categories/?type=' . $type)->with('success', 'Post updated!');
        }
        elseif (isset($_POST['post-delete']) && $_POST['post-delete'] != "") {
            for ($i = 0; $i < count(@$_POST['post-check']); $i++) {
                $post_id = @$_POST['post-check'][$i];

                $post_type = ['images', 'icon', 'thumb', 'background', 'pdf', 'files'];
                foreach (Post::whereIn('post_type', $post_type)->where([['post_up', '=', $post_id]])->get() as $row_images) {
                    if ($row_images->post_type == 'pdf' or $row_images->post_type == 'files') {
                        @unlink('./upload/files/' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                    } else {
                        @unlink('./upload/' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                        @unlink('./upload/c_' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                        @unlink('./upload/c1_' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                    }
                    DB::table('posts')->where('post_id', '=', $row_images->post_id)->delete();
                }

                DB::table('cats')->where('cat_id', '=', $post_id)->delete();
            }
            return redirect('/categories/?type=' . $type)->with('success', 'Post updated!');
        } elseif (isset($_POST['post-deleted']) && $_POST['post-deleted'] != "") {
            $post_id = $_POST['post_id'];
            $post_type = array('images', 'thumb', 'background', 'pdf', 'files', 'icon');

            $post_type = ['images', 'icon', 'thumb', 'background', 'pdf', 'files'];
            foreach (Post::whereIn('post_type', $post_type)->where([['post_up', '=', $post_id]])->get() as $row_images) {
                if ($row_images->post_type == 'pdf' or $row_images->post_type == 'files') {
                    @unlink('./upload/files/' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                } else {
                    @unlink('./upload/' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                    @unlink('./upload/c_' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                    @unlink('./upload/c1_' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                }
                DB::table('posts')->where('post_id', '=', $row_images->post_id)->delete();
            }

            DB::table('cats')->where('cat_id', '=', $post_id)->delete();

            return redirect('/categories/?type=' . $type)->with('success', 'Categories updated!');
        }

        return view('back/categories', ['categories_list' => $post_list, 'show' => $show, 'type' => $type, 'copyright' => copyright()]);
    }
    public function search()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        if (Input::get('show') != '') {
            $show = Input::get('show');
        } else {
            $show = 10;
        }
        if (Input::get('query') != '') {
            $query = Input::get('query');
        } else {
            $query = '';
        }
        if (Input::get('type') != '') {
            $type = Input::get('type');
            $post_list = Categories::where('cats.cat_type', $type)->where('cat_up', 0)->where('cat_title', 'like', '%' . $query . '%')->orWhere('cat_summary', 'like', '%' . $query . '%')->orderBy('cat_date', 'DESC')->paginate($show);
        } else {
            $type='';
            $post_list = Categories::where('cat_up', 0)->where('cat_title', 'like', '%' . $query . '%')->orWhere('cat_summary', 'like', '%' . $query . '%')->orderBy('cat_date', 'DESC')->paginate($show);
        }

        if (isset($_POST['post-delete']) && $_POST['post-delete'] != "") {
            for ($i = 0; $i < count(@$_POST['post-check']); $i++) {
                $post_id = @$_POST['post-check'][$i];

                $post_type = ['images', 'icon', 'thumb', 'background', 'pdf', 'files'];
                foreach (Post::whereIn('post_type', $post_type)->where([['post_up', '=', $post_id]])->get() as $row_images) {
                    if ($row_images->post_type == 'pdf' or $row_images->post_type == 'files') {
                        @unlink('./upload/files/' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                    } else {
                        @unlink('./upload/' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                        @unlink('./upload/c_' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                        @unlink('./upload/c1_' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                    }
                    DB::table('posts')->where('post_id', '=', $row_images->post_id)->delete();
                }

                DB::table('cats')->where('cat_id', '=', $post_id)->delete();
            }
            return redirect('/post/')->with('success', 'Post updated!');
        } elseif (isset($_POST['post-deleted']) && $_POST['post-deleted'] != "") {
            $post_id = $_POST['post_id'];
            $post_type = array('images', 'thumb', 'background', 'pdf', 'files', 'icon');

            $post_type = ['images', 'icon', 'thumb', 'background', 'pdf', 'files'];
            foreach (Post::whereIn('post_type', $post_type)->where([['post_up', '=', $post_id]])->get() as $row_images) {
                if ($row_images->post_type == 'pdf' or $row_images->post_type == 'files') {
                    @unlink('./upload/files/' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                } else {
                    @unlink('./upload/' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                    @unlink('./upload/c_' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                    @unlink('./upload/c1_' . $row_images->post_slug . '.' . $row_images->post_mime_type);
                }
                DB::table('posts')->where('post_id', '=', $row_images->post_id)->delete();
            }

            DB::table('cats')->where('cat_id', '=', $post_id)->delete();

            return redirect('/categories/')->with('success', 'Categories updated!');
        }

        return view('back/categories_search', ['post_list' => $post_list, 'show' => $show, 'type' => $type, 'query' => $query, 'copyright' => copyright()]);
    }

    public function edit($id)
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        if (Input::get('type') != '') {
            $type = Input::get('type');
        } else {
            $type = 'post';
        }

        $post_edit      = Categories::where('cat_id', $id)->get();
        $post_list      = Categories::where('cat_type', 'post')->get();
        $images_list    = Post::where([['post_type', '=', 'cat_images'], ['post_up', '=', $id]])->get();
        $icon_list      = Post::where([['post_type', '=', 'cat_icon'], ['post_up', '=', $id]])->get();
        $thumb_list     = Post::where([['post_type', '=', 'cat_thumb'], ['post_up', '=', $id]])->get();
        $background_list = Post::where([['post_type', '=', 'cat_background'], ['post_up', '=', $id]])->get();
        $pdf_list       = Post::where([['post_type', '=', 'cat_pdf'], ['post_up', '=', $id]])->get();
        $files_list     = Post::where([['post_type', '=', 'cat_files'], ['post_up', '=', $id]])->get();

        function ca_list_recrusive($cat_type, $cat_up, $depth, $now)
        {
            $data = array();
            foreach (DB::table('cats')->where([['cat_type', '=', 'post'], ['cat_up', '=', '0']])->get() as $row) {
                for ($i = 0; $i < $now; $i++)
                    $row->cat_title = '| - ' . $row->cat_title;
                $data[] = [
                    'cat_id'          => $row->cat_id,
                    'cat_title'       => $row->cat_title,
                ];
                if ($depth > 0) {
                    $data2 = array();
                    foreach (DB::table('cats')->where([['cat_type', '=', 'post'], ['cat_up', '=', $row->cat_id]])->get() as $row2) {
                        for ($i = 0; $i < $now; $i++)
                            $row2->cat_title = '| - ' . $row2->cat_title;
                        $data2[] = [
                            'cat_id'          => $row2->cat_id,
                            'cat_title'       => '| - ' . $row2->cat_title,
                        ];
                    }
                    $data = array_merge($data, $data2);
                }
            }
            return $data;
        }

        $cat_list = ca_list_recrusive($cat_type = 'post', $cat_up = 0, $depth = 5, $now = 0);

        return view('back/categories_edit', [
            'post_edit'     => $post_edit,
            'cat_list'      => $cat_list,
            'images_list'   => $images_list,
            'icon_list'     => $icon_list,
            'thumb_list'    => $thumb_list,
            'background_list' => $background_list,
            'pdf_list'      => $pdf_list,
            'files_list'    => $files_list,
            'type'          => $type,
            'copyright'     => copyright()
        ]);
    }
    public function add()
    {
        if (!Auth::check()) {
            return redirect('/panel/login/');
        }
        if (Input::get('type') != '') {
            $type = Input::get('type');
        } else {
            $type = 'post';
        }
        function ca_list_recrusive($cat_type, $cat_up, $depth, $now)
        {
            $data = array();
            foreach (DB::table('cats')->where([['cat_type', '=', 'post'], ['cat_up', '=', '0']])->get() as $row) {
                for ($i = 0; $i < $now; $i++)
                    $row->cat_title = '| - ' . $row->cat_title;
                $data[] = [
                    'cat_id'          => $row->cat_id,
                    'cat_title'       => $row->cat_title,
                ];
                if ($depth > 0) {
                    $data2 = array();
                    foreach (DB::table('cats')->where([['cat_type', '=', 'post'], ['cat_up', '=', $row->cat_id]])->get() as $row2) {
                        for ($i = 0; $i < $now; $i++)
                            $row2->cat_title = '| - ' . $row2->cat_title;
                        $data2[] = [
                            'cat_id'          => $row2->cat_id,
                            'cat_title'       => '| - ' . $row2->cat_title,
                        ];
                    }
                    $data = array_merge($data, $data2);
                }
            }
            return $data;
        }

        $cat_list = ca_list_recrusive($cat_type = 'post', $cat_up = 0, $depth = 5, $now = 0);

        return view('back/categories_add', [
            'cat_list' => $cat_list,
            'type' => $type,
            'copyright' => copyright()
        ]);
    }
    public function create(Request $request)
    {
        $controller = new SlugController;
        $cat_type      = ($request->get('cat_type') != '') ? $request->get('cat_type') : 'post';
        $cat_up        = $request->get('cat_up');
        $cat_slug      = ($request->get('cat_title') != '') ? $controller->createSlug($request->get('cat_title'),'-') : '';
        $cat_title     = ($request->get('cat_title') != '') ? $request->get('cat_title') : '';
        $cat_summary   = ($request->get('cat_summary') != '') ? $request->get('cat_summary') : '';
        $cat_key       = ($request->get('cat_key') != '') ? $request->get('cat_key') : '';
        $cat_desc      = ($request->get('cat_desc') != '') ? $request->get('cat_desc') : '';
        $cat_date      = ($request->get('cat_date') != '') ? $request->get('cat_date') : date('Y-m-d H:i:s');
        $cat_status    = $request->get('cat_status');

        Categories::insert([
            'cat_type' => $cat_type,
            'cat_up' => $cat_up,
            'cat_slug' =>  $cat_slug,
            'cat_title' => $cat_title,
            'cat_summary' => $cat_summary,
            'cat_key' => $cat_key,
            'cat_desc' => $cat_desc,
            'cat_date' => $cat_date,
            'cat_status' => $cat_status,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'deleted_at' => date('Y-m-d H:i:s')
        ]);

        $id = DB::getPdo()->lastInsertId();

        if ($request->hasfile('upload_photo')) {
            foreach ($request->file('upload_photo') as $image) {
                $original_name = $image->getClientOriginalName();
                $file_name = ext_name($original_name);
                $file_ext = ext_filename($original_name);

                $resize_image = Image::make($image->getRealPath());
                $resize_image->resize(150, 150, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(public_path() . '/upload/r_' . strip_i_slug($file_name) . '.' . $file_ext);

                $img_crop = Image::make($image->getRealPath());
                $img_crop->fit(300);
                // $img->crop($request->input('w'), $request->input('h'), $request->input('x1'), $request->input('y1'));
                $img_crop->save(public_path() . '/upload/c_' . strip_i_slug($file_name) . '.' . $file_ext);

                $image->move(public_path() . '/upload/', strip_i_slug($file_name) . '.' . $file_ext);
                // $data[] = $original_name;

                Post::insert([
                    'user_id' => Auth::user()->id,
                    'post_type' => 'cat_images',
                    'post_up' => $id,
                    'post_slug' =>  strip_i_slug($file_name),
                    'post_title' => $file_name,
                    'post_content' => '',
                    'post_summary' => '',
                    'post_price' => '0',
                    'post_unit' => '0',
                    'post_link' => '',
                    'post_video' => '',
                    'post_key' => '',
                    'post_desc' => '',
                    'post_count' => '0',
                    'post_mime_type' => $file_ext,
                    'post_comment' => '1',
                    'date' => date('Y-m-d H:i:s'),
                    'status' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'deleted_at' => date('Y-m-d H:i:s')
                ]);
            }
        }
        if ($request->hasfile('upload_icon')) {
            foreach ($request->file('upload_icon') as $image) {
                $original_name = $image->getClientOriginalName();
                $file_name = ext_name($original_name);
                $file_ext = ext_filename($original_name);

                $image->move(public_path() . '/upload/', strip_i_slug($file_name) . '.' . $file_ext);

                Post::insert([
                    'user_id' => Auth::user()->id,
                    'post_type' => 'cat_icon',
                    'post_up' => $id,
                    'post_slug' =>  strip_i_slug($file_name),
                    'post_title' => $file_name,
                    'post_content' => '',
                    'post_summary' => '',
                    'post_price' => '0',
                    'post_unit' => '0',
                    'post_link' => '',
                    'post_video' => '',
                    'post_key' => '',
                    'post_desc' => '',
                    'post_count' => '0',
                    'post_mime_type' => $file_ext,
                    'post_comment' => '1',
                    'date' => date('Y-m-d H:i:s'),
                    'status' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'deleted_at' => date('Y-m-d H:i:s')
                ]);
            }
        }
        if ($request->hasfile('upload_thumb')) {
            foreach ($request->file('upload_thumb') as $image) {
                $original_name = $image->getClientOriginalName();
                $file_name = ext_name($original_name);
                $file_ext = ext_filename($original_name);

                $image->move(public_path() . '/upload/', strip_i_slug($file_name) . '.' . $file_ext);

                Post::insert([
                    'user_id' => Auth::user()->id,
                    'post_type' => 'cat_thumb',
                    'post_up' => $id,
                    'post_slug' =>  strip_i_slug($file_name),
                    'post_title' => $file_name,
                    'post_content' => '',
                    'post_summary' => '',
                    'post_price' => '0',
                    'post_unit' => '0',
                    'post_link' => '',
                    'post_video' => '',
                    'post_key' => '',
                    'post_desc' => '',
                    'post_count' => '0',
                    'post_mime_type' => $file_ext,
                    'post_comment' => '1',
                    'date' => date('Y-m-d H:i:s'),
                    'status' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'deleted_at' => date('Y-m-d H:i:s')
                ]);
            }
        }
        if ($request->hasfile('upload_background')) {
            foreach ($request->file('upload_background') as $image) {
                $original_name = $image->getClientOriginalName();
                $file_name = ext_name($original_name);
                $file_ext = ext_filename($original_name);

                $image->move(public_path() . '/upload/', strip_i_slug($file_name) . '.' . $file_ext);

                Post::insert([
                    'user_id' => Auth::user()->id,
                    'post_type' => 'cat_background',
                    'post_up' => $id,
                    'post_slug' =>  strip_i_slug($file_name),
                    'post_title' => $file_name,
                    'post_content' => '',
                    'post_summary' => '',
                    'post_price' => '0',
                    'post_unit' => '0',
                    'post_link' => '',
                    'post_video' => '',
                    'post_key' => '',
                    'post_desc' => '',
                    'post_count' => '0',
                    'post_mime_type' => $file_ext,
                    'post_comment' => '1',
                    'date' => date('Y-m-d H:i:s'),
                    'status' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'deleted_at' => date('Y-m-d H:i:s')
                ]);
            }
        }
        if ($request->hasfile('upload_pdf')) {
            foreach ($request->file('upload_pdf') as $image) {
                $original_name = $image->getClientOriginalName();
                $file_name = ext_name($original_name);
                $file_ext = ext_filename($original_name);

                $image->move(public_path() . '/upload/', strip_i_slug($file_name) . '.' . $file_ext);

                Post::insert([
                    'user_id' => Auth::user()->id,
                    'post_type' => 'cat_pdf',
                    'post_up' => $id,
                    'post_slug' =>  strip_i_slug($file_name),
                    'post_title' => $file_name,
                    'post_content' => '',
                    'post_summary' => '',
                    'post_price' => '0',
                    'post_unit' => '0',
                    'post_link' => '',
                    'post_video' => '',
                    'post_key' => '',
                    'post_desc' => '',
                    'post_count' => '0',
                    'post_mime_type' => $file_ext,
                    'post_comment' => '1',
                    'date' => date('Y-m-d H:i:s'),
                    'status' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'deleted_at' => date('Y-m-d H:i:s')
                ]);
            }
        }
        if ($request->hasfile('upload_files')) {
            foreach ($request->file('upload_files') as $image) {
                $original_name = $image->getClientOriginalName();
                $file_name = ext_name($original_name);
                $file_ext = ext_filename($original_name);

                $image->move(public_path() . '/upload/', strip_i_slug($file_name) . '.' . $file_ext);

                Post::insert([
                    'user_id' => Auth::user()->id,
                    'post_type' => 'cat_files',
                    'post_up' => $id,
                    'post_slug' =>  strip_i_slug($file_name),
                    'post_title' => $file_name,
                    'post_content' => '',
                    'post_summary' => '',
                    'post_price' => '0',
                    'post_unit' => '0',
                    'post_link' => '',
                    'post_video' => '',
                    'post_key' => '',
                    'post_desc' => '',
                    'post_count' => '0',
                    'post_mime_type' => $file_ext,
                    'post_comment' => '1',
                    'date' => date('Y-m-d H:i:s'),
                    'status' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'deleted_at' => date('Y-m-d H:i:s')
                ]);
            }
        }
        return redirect('/categories/edit/' . $id. '?type='. $cat_type)->with('success', 'Category updated!');
    }
    public function update(Request $request, $id)
    {
        $controller = new SlugController;
        $post_data = Categories::find($id);
        $post_data->cat_up        = $request->get('cat_up');
        $post_data->cat_slug      = ($request->get('cat_title') != '') ? $controller->createSlug($request->get('cat_title'),'-') : '';
        $post_data->cat_title     = ($request->get('cat_title') != '') ? $request->get('cat_title') : '';
        $post_data->cat_summary   = ($request->get('cat_summary') != '') ? $request->get('cat_summary') : '';
        $post_data->cat_key       = ($request->get('cat_key') != '') ? $request->get('cat_key') : '';
        $post_data->cat_desc      = ($request->get('cat_desc') != '') ? $request->get('cat_desc') : '';
        $post_data->cat_date      = ($request->get('cat_date') != '') ? $request->get('cat_date') : date('Y-m-d H:i:s');
        $post_data->cat_status    = $request->get('cat_status');
        $post_data->save();

        if (is_array(@$_POST['post-check'])) {
            for ($i = 0; $i < count(@$_POST['post-check']); $i++) {
                $i_name = explode(",", $_POST['post-check'][$i]);

                if (file_exists(public_path() . '/upload/' . $i_name[1])) {
                    unlink(public_path() . "/upload/" . $i_name[1]);
                }
                if (file_exists(public_path() . '/upload/c_' . $i_name[1])) {
                    unlink(public_path() . "/upload/c_" . $i_name[1]);
                }
                if (file_exists(public_path() . '/upload/r_' . $i_name[1])) {
                    unlink(public_path() . "/upload/r_" . $i_name[1]);
                }
                DB::table('posts')->where('post_id', '=', $i_name[0])->delete();
            }
        }
        if (is_array(@$_POST['pdf-check'])) {
            for ($i = 0; $i < count(@$_POST['pdf-check']); $i++) {
                $i_name = explode(",", $_POST['pdf-check'][$i]);

                if (file_exists(public_path() . '/upload/files/' . $i_name[1])) {
                    unlink(public_path() . "/upload/files/" . $i_name[1]);
                }
                DB::table('posts')->where('post_id', '=', $i_name[0])->delete();
            }
        }
        if (is_array(@$_POST['files-check'])) {
            for ($i = 0; $i < count(@$_POST['files-check']); $i++) {
                $i_name = explode(",", $_POST['files-check'][$i]);

                if (file_exists(public_path() . '/upload/files/' . $i_name[1])) {
                    unlink(public_path() . "/upload/files/" . $i_name[1]);
                }
                DB::table('posts')->where('post_id', '=', $i_name[0])->delete();
            }
        }

        if ($request->hasfile('upload_photo')) {
            foreach ($request->file('upload_photo') as $image) {
                $original_name = $image->getClientOriginalName();
                $file_name = ext_name($original_name);
                $file_ext = ext_filename($original_name);

                $resize_image = Image::make($image->getRealPath());
                $resize_image->resize(150, 150, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(public_path() . '/upload/r_' . strip_i_slug($file_name) . '.' . $file_ext);

                $img_crop = Image::make($image->getRealPath());
                $img_crop->fit(300);
                // $img->crop($request->input('w'), $request->input('h'), $request->input('x1'), $request->input('y1'));
                $img_crop->save(public_path() . '/upload/c_' . strip_i_slug($file_name) . '.' . $file_ext);

                $image->move(public_path() . '/upload/', strip_i_slug($file_name) . '.' . $file_ext);
                // $data[] = $original_name; //Auth::id()

                Post::insert([
                    'user_id' => Auth::user()->id,
                    'post_type' => 'cat_images',
                    'post_up' => $id,
                    'post_slug' =>  strip_i_slug($file_name),
                    'post_title' => $file_name,
                    'post_content' => '',
                    'post_summary' => '',
                    'post_price' => '0',
                    'post_unit' => '0',
                    'post_link' => '',
                    'post_video' => '',
                    'post_key' => '',
                    'post_desc' => '',
                    'post_count' => '0',
                    'post_mime_type' => $file_ext,
                    'post_comment' => '1',
                    'date' => date('Y-m-d H:i:s'),
                    'status' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'deleted_at' => date('Y-m-d H:i:s')
                ]);
            }
        }
        if ($request->hasfile('upload_icon')) {
            foreach ($request->file('upload_icon') as $image) {
                $original_name = $image->getClientOriginalName();
                $file_name = ext_name($original_name);
                $file_ext = ext_filename($original_name);

                $image->move(public_path() . '/upload/', strip_i_slug($file_name) . '.' . $file_ext);

                Post::insert([
                    'user_id' => Auth::user()->id,
                    'post_type' => 'cat_icon',
                    'post_up' => $id,
                    'post_slug' =>  strip_i_slug($file_name),
                    'post_title' => $file_name,
                    'post_content' => '',
                    'post_summary' => '',
                    'post_price' => '0',
                    'post_unit' => '0',
                    'post_link' => '',
                    'post_video' => '',
                    'post_key' => '',
                    'post_desc' => '',
                    'post_count' => '0',
                    'post_mime_type' => $file_ext,
                    'post_comment' => '1',
                    'date' => date('Y-m-d H:i:s'),
                    'status' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'deleted_at' => date('Y-m-d H:i:s')
                ]);
            }
        }
        if ($request->hasfile('upload_thumb')) {
            foreach ($request->file('upload_thumb') as $image) {
                $original_name = $image->getClientOriginalName();
                $file_name = ext_name($original_name);
                $file_ext = ext_filename($original_name);

                $image->move(public_path() . '/upload/', strip_i_slug($file_name) . '.' . $file_ext);

                Post::insert([
                    'user_id' => Auth::user()->id,
                    'post_type' => 'cat_thumb',
                    'post_up' => $id,
                    'post_slug' =>  strip_i_slug($file_name),
                    'post_title' => $file_name,
                    'post_content' => '',
                    'post_summary' => '',
                    'post_price' => '0',
                    'post_unit' => '0',
                    'post_link' => '',
                    'post_video' => '',
                    'post_key' => '',
                    'post_desc' => '',
                    'post_count' => '0',
                    'post_mime_type' => $file_ext,
                    'post_comment' => '1',
                    'date' => date('Y-m-d H:i:s'),
                    'status' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'deleted_at' => date('Y-m-d H:i:s')
                ]);
            }
        }
        if ($request->hasfile('upload_background')) {
            foreach ($request->file('upload_background') as $image) {
                $original_name = $image->getClientOriginalName();
                $file_name = ext_name($original_name);
                $file_ext = ext_filename($original_name);

                $image->move(public_path() . '/upload/', strip_i_slug($file_name) . '.' . $file_ext);

                Post::insert([
                    'user_id' => Auth::user()->id,
                    'post_type' => 'cat_background',
                    'post_up' => $id,
                    'post_slug' =>  strip_i_slug($file_name),
                    'post_title' => $file_name,
                    'post_content' => '',
                    'post_summary' => '',
                    'post_price' => '0',
                    'post_unit' => '0',
                    'post_link' => '',
                    'post_video' => '',
                    'post_key' => '',
                    'post_desc' => '',
                    'post_count' => '0',
                    'post_mime_type' => $file_ext,
                    'post_comment' => '1',
                    'date' => date('Y-m-d H:i:s'),
                    'status' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'deleted_at' => date('Y-m-d H:i:s')
                ]);
            }
        }
        if ($request->hasfile('upload_pdf')) {
            foreach ($request->file('upload_pdf') as $image) {
                $original_name = $image->getClientOriginalName();
                $file_name = ext_name($original_name);
                $file_ext = ext_filename($original_name);

                $image->move(public_path() . '/upload/files/', strip_i_slug($file_name) . '.' . $file_ext);

                Post::insert([
                    'user_id' => Auth::user()->id,
                    'post_type' => 'cat_pdf',
                    'post_up' => $id,
                    'post_slug' =>  strip_i_slug($file_name),
                    'post_title' => $file_name,
                    'post_content' => '',
                    'post_summary' => '',
                    'post_price' => '0',
                    'post_unit' => '0',
                    'post_link' => '',
                    'post_video' => '',
                    'post_key' => '',
                    'post_desc' => '',
                    'post_count' => '0',
                    'post_mime_type' => $file_ext,
                    'post_comment' => '1',
                    'date' => date('Y-m-d H:i:s'),
                    'status' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'deleted_at' => date('Y-m-d H:i:s')
                ]);
            }
        }
        if ($request->hasfile('upload_files')) {
            foreach ($request->file('upload_files') as $image) {
                $original_name = $image->getClientOriginalName();
                $file_name = ext_name($original_name);
                $file_ext = ext_filename($original_name);

                $image->move(public_path() . '/upload/files/', strip_i_slug($file_name) . '.' . $file_ext);

                Post::insert([
                    'user_id' => Auth::user()->id,
                    'post_type' => 'cat_files',
                    'post_up' => $id,
                    'post_slug' =>  strip_i_slug($file_name),
                    'post_title' => $file_name,
                    'post_content' => '',
                    'post_summary' => '',
                    'post_price' => '0',
                    'post_unit' => '0',
                    'post_link' => '',
                    'post_video' => '',
                    'post_key' => '',
                    'post_desc' => '',
                    'post_count' => '0',
                    'post_mime_type' => $file_ext,
                    'post_comment' => '1',
                    'date' => date('Y-m-d H:i:s'),
                    'status' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'deleted_at' => date('Y-m-d H:i:s')
                ]);
            }
        }
        return redirect('/categories/edit/' . $id.'?type='. $post_data->cat_type)->with('success', 'Category updated!');
    }

}
