<html lang="en" moznomarginboxes mozdisallowselectionprint>

<head>
    <title>Faktur Pembelian Barang</title>
    <meta charset="utf-8">
    <style>
        @font-face {
            font-family: 'Dot Matrix';
            src: url('{{ url('/') }}/themes/default/fonts/DotMatrix/DotMatrixBold.woff2') format('woff2'),
                url('{{ url('/') }}/themes/default/fonts/DotMatrix/DotMatrixBold.woff') format('woff');
            font-weight: bold;
            font-style: normal;
        }

        @font-face {
            font-family: 'Dot Matrix';
            src: url('{{ url('/') }}/themes/default/fonts/DotMatrix/DotMatrix.woff2') format('woff2'),
                url('{{ url('/') }}/themes/default/fonts/DotMatrix/DotMatrix.woff') format('woff');
            font-weight: normal;
            font-style: normal;
        }
        body{
            font-family: 'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif;
        }

        @media print {
            @page {
                margin: 0;
            }

            body {
                margin: 5px;
                font-family: 'Dot Matrix';
                font-weight: normal;
                font-style: normal;
            }
            .break{
            page-break-after: always;
            }
        }
    </style>
</head>
<!--<body onload="window.print()">-->

<body onload="window.print()">
    @foreach ($post_list as $p)
    <div id="laporan">
        <table align="center" style="width:700px; border-bottom:3px double;border-top:none;border-right:none;border-left:none;margin-top:5px;margin-bottom:20px;">
            <!--<tr>
    <td><img src="<?php // echo base_url().'assets/img/kop_surat.png'
                    ?>"/></td>
</tr>-->
        </table>

        <table border="0" align="center" style="width:700px; border:none;margin-top:5px;margin-bottom:0px;">
            <tr>

            </tr>

        </table>
        <table border="0" align="center" style="width:700px;border:none; line-height:15px;">
            <tr>
                <th style="text-align:left;">PT. Semar Tidar Medika</th>
                <th style="text-align:left;">No Faktur</th>
                <th style="text-align:left;">: {{$id}}</th>
            </tr>
            <tr>
                <th style="text-align:left;">Jl. Blabak Sawangan Km 1.1, Tapen, Pagersari, Blabak, Magelang</th>
                <th style="text-align:left;">Tanggal</th>
                <th style="text-align:left;">: {{$p->beli_tanggal}}</th>
            </tr>
            <tr>
                <th style="text-align:left;">(0293) 7184563</th>
                <th style="text-align:left;"></th>
                <th style="text-align:left;"></th>
            </tr>
        </table>
        <div style="max-width:700px; margin:10px auto 5px; border-top:1px dotted #000;"></div>
        <table border="0" align="center" style="width:700px;margin-bottom:20px; line-height:15px;">
            <thead>

                <tr>
                    <th style="width:50px; border-bottom:1px dotted #000; padding-bottom:5px;">No</th>
                    <th style="border-bottom:1px dotted #000; padding-bottom:5px;">Nama Barang</th>
                    <th style="border-bottom:1px dotted #000; padding-bottom:5px;">Satuan</th>
                    <th style="border-bottom:1px dotted #000; padding-bottom:5px;">Harga Jual</th>
                    <th style="border-bottom:1px dotted #000; padding-bottom:5px;">Qty</th>
                    <th style="border-bottom:1px dotted #000; padding-bottom:5px;">SubTotal</th>
                </tr>
            </thead>
            <tbody>
                @php
                $total = array();
                @endphp
                @foreach (App\Buyingdetail::where('d_beli_nofak','=',$p->beli_nofak)->leftjoin('items', 'items.barang_id', '=', 'buy_details.d_beli_barang_id')->get() as $value => $key)
                    <tr>
                        <td style="text-align:center; padding: 10px 0 5px;">{{$value+1}}</td>
                        <td style="text-align:left; padding: 10px 0 5px">{{$key->barang_nama}}</td>
                        <td style="text-align:center; padding: 10px 0 5px">{{$key->barang_satuan}}</td>
                        <td style="text-align:right; padding: 10px 0 5px">{{currency($key->d_beli_harga)}}</td>
                        <td style="text-align:center; padding: 10px 0 5px">{{$key->d_beli_jumlah}}</td>
                        <td style="text-align:right; padding: 10px 0 5px">{{currency($key->d_beli_total)}}</td>
                    </tr>
                    @php
                    $diskon[]   = $key->d_beli_diskon;
                    $total[]    = $key->d_beli_total;
                    @endphp
                @endforeach
            </tbody>

                @if(App\Buyingdetail::where('beli_id','=',$p->beli_id)->count()>=10)
                </table>
                    <div class="break"></div>
                <table border="0" align="center" style="width:700px;margin-bottom:20px; line-height:15px;">
                @endif
            <tfoot>

                <tr>
                    <td colspan="5" style="text-align:right; padding-bottom:5px;"><b>Total</b></td>
                    <td style="text-align:right; padding-bottom:5px;"><b>Rp. {{currency(array_sum($total))}}</b></td>
                </tr>
            </tfoot>
        </table>

        <table align="center" style="width:100%; border:none;margin-top:5px;margin-bottom:20px;">
            <tr>
                <td></td>
        </table>
        <table style="width:700px; margin-left:auto; margin-right:auto; border:none;margin-top:5px;margin-bottom:20px;">
            <tr>
                <td>
                    <table align="center" width="100%">
                        <tr>
                            <td align="right"></td>
                        </tr>
                        <tr>
                            <td align="right"></td>
                        </tr>

                        <tr>
                            <td><br /><br /><br /><br /></td>
                        </tr>
                        <tr>
                            <td align="right"></td>
                        </tr>
                        <tr>
                            <td align="center"></td>
                        </tr>
                    </table>
                </td>
                <td style="text-align: right; width:40%;">
                    <table align="center" width="100%">
                        <tr>
                            <td align="right">Parakan, {{date('d-M-Y')}}</td>
                        </tr>
                        <tr>
                            <td align="right"></td>
                        </tr>

                        <tr>
                            <td><br /><br /><br /><br /></td>
                        </tr>
                        <tr>
                            <td align="right">( {{$p->first_name.' '.$p->last_name}} )</td>
                        </tr>
                        <tr>
                            <td align="center"></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

        <table align="center" style="width:700px; border:none;margin-top:5px;margin-bottom:20px;">
            <tr>
                <th><br /><br /></th>
            </tr>
            <tr>
                <th align="left"></th>
            </tr>
        </table>
    </div>
    @endforeach
</body>

</html>
