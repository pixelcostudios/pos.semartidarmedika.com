<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login CMS</title>
    <link href="{{ url('themes') }}/default/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ url('themes') }}/default/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="{{ url('themes') }}/default/css/animate.css" rel="stylesheet">
    <link href="{{ url('themes') }}/default/css/style.css" rel="stylesheet">
    <style type="text/css">
        .margin-bottom-20 {
            margin-bottom: 20px;
        }

        .margin-auto {
            margin: 0 auto;
            float: none;
        }

        .middle-box h1 {
            margin-top: 0;
        }

        body {
            background-image: url({{ url('themes') }}/default/img/bg_blue.jpg);
            background-repeat: no-repeat;
            background-position: left;
            height: auto;
        }

        .loginscreen.middle-box {
            width: 100%;
            margin: 18% 0 0 0;
            max-width: inherit;
            background: rgba(255, 255, 255, 0.3)
        }

        .ibox-content {
            background: none;
            border: 0;
            padding: 5px 20px 25px 20px;
        }

        .ibox-content h3 {
            color: #fff;
            font-size: 25px;
        }

        .ibox-content .username {
            margin-top: 24px;
            text-align: right;
            display: block;
            color: #fff;
        }

        .ibox-content .password {
            margin-top: 28px;
            text-align: right;
            display: block;
            color: #fff;
        }

        .ibox-content .logo {
            margin: 15px 0 0;
        }

        .ibox-content p {
            color: #fff;
        }

        .btn-primary {
            background: #007bff;
        }

        .btn-primary:hover {
            background: none;
            border: 1px solid #007bff;
        }

        @media only screen and (max-width:480px) {
            .ibox-content .username {
                display: none;
            }

            .ibox-content .password {
                display: none;
            }
        }
    </style>
</head>

<body class="gray-bg">
    @yield('content')
    <!-- Mainly scripts -->
    <script src="{{ url('themes') }}/default/js/jquery-3.1.1.min.js"></script>
    <script src="{{ url('themes') }}/default/js/bootstrap.min.js"></script>

</body>

</html>
