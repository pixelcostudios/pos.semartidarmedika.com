<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
            <form role="search" method="get" class="navbar-form-custom" action="{{ url('/post/search/') }}">
                <div class="form-group">
                    <input type="text" placeholder="Search for something..." class="form-control" name="query" id="top-search">
                </div>
            </form>
        </div>
        <ul class="nav navbar-top-links navbar-right">
            <li>
                <span class="m-r-sm text-muted welcome-message">Welcome to Pixel Interface v6.</span>
            </li>
            <li><a href="{{ url('/') }}" target="_blank"><i class="fa fa-external-link"></i> Visit Site</a></li>
                                    <li class="dropdown">
                <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                    <i class="fa fa-bell"></i>  <span class="label label-primary">612</span>
                </a>
                <ul class="dropdown-menu dropdown-alerts">
                    <li>
                        <a href="notification.html" class="dropdown-item">
                            <div>
                                <i class="fa fa-bell"></i> You have 612 notification this day
                                <span class="float-right text-muted small"></span>
                            </div>
                        </a>
                    </li>
                    <li class="dropdown-divider"></li>
                                        <li>
                        <a href="profile.html" class="dropdown-item">
                            <div>
                                <i class="fa fa-bell"></i> administrator - Logout                                <span class="float-right text-muted small">12 minutes ago</span>
                            </div>
                        </a>
                    </li>
                    <li class="dropdown-divider"></li>
                                        <li>
                        <a href="profile.html" class="dropdown-item">
                            <div>
                                <i class="fa fa-bell"></i> administrator - Logout                                <span class="float-right text-muted small">12 minutes ago</span>
                            </div>
                        </a>
                    </li>
                    <li class="dropdown-divider"></li>
                                        <li>
                        <a href="profile.html" class="dropdown-item">
                            <div>
                                <i class="fa fa-bell"></i> administrator - Login                                <span class="float-right text-muted small">12 minutes ago</span>
                            </div>
                        </a>
                    </li>
                    <li class="dropdown-divider"></li>
                                        <li>
                        <a href="profile.html" class="dropdown-item">
                            <div>
                                <i class="fa fa-bell"></i> administrator - Login                                <span class="float-right text-muted small">12 minutes ago</span>
                            </div>
                        </a>
                    </li>
                    <li class="dropdown-divider"></li>
                                        <li>
                        <a href="profile.html" class="dropdown-item">
                            <div>
                                <i class="fa fa-bell"></i> administrator - Login                                <span class="float-right text-muted small">12 minutes ago</span>
                            </div>
                        </a>
                    </li>
                    <li class="dropdown-divider"></li>
                                        <li>
                        <div class="text-center link-block">
                            <a href="{{ url('/') }}panel/notification/manage.html" class="dropdown-item">
                                <strong>See All Alerts</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </div>
                    </li>
                </ul>
            </li>

            <li>
                <a href="{{ url('logout') }}" onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                    <i class="fa fa-sign-out"></i> Log out
                </a>
            </li>
        </ul>

    </nav>
</div>
