<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link href="{{ url('/') }}/themes/default/css/bootstrap.min.css" rel="stylesheet">
	<link href="{{ url('/') }}/themes/default/font-awesome/css/font-awesome.css" rel="stylesheet">
	<link href="{{ url('/') }}/themes/default/js/plugins/datetime/bootstrap-datetimepicker.css" rel="stylesheet" />
	<link href="{{ url('/') }}/themes/default/css/plugins/select2/select2.min.css" rel="stylesheet">
	<link href="{{ url('/') }}/themes/default/css/animate.css" rel="stylesheet">
	<link href="{{ url('/') }}/themes/default/css/style.css" rel="stylesheet">
	<link href="{{ url('/') }}/themes/default/css/custom.css" rel="stylesheet">

</head>

<body class="top-navigation pace-done">
    <div id="wrapper">

        <div id="page-wrapper" class="gray-bg">
            @yield('content')
        </div>
    </div>

 <!-- Mainly scripts -->
<script src="{{ url('/') }}/themes/default/js/jquery-3.1.1.min.js"></script>
<script src="{{ url('/') }}/themes/default/js/popper.min.js"></script>
<script src="{{ url('/') }}/themes/default/js/bootstrap.js"></script>
<script src="{{ url('/') }}/themes/default/js/plugins/metismenu/jquery.metismenu.js"></script>
<script src="{{ url('/') }}/themes/default/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="{{ url('/') }}/themes/default/js/inspinia.js"></script>
<script src="{{ url('/') }}/themes/default/js/plugins/pace/pace.min.js"></script>

<!--Files Scripts-->
<script src="{{ url('/') }}/themes/default/js/plugins/filestyle/filestyle.js"></script>
<script src="{{ url('/') }}/themes/default/js/plugins/filestyle/aplication.js"></script>

<!-- Check -->
<script src="{{ url('/') }}/themes/default/js/check.js"></script>

<!-- Select2 -->
<script src="{{ url('/') }}/themes/default/js/plugins/select2/select2.full.min.js"></script>

<!--Bootstrap Date Picker-->
<script src="{{ url('/') }}/themes/default/js/plugins/datetime/bootstrap-datetimepicker.min.js"></script>

<script>
    $(document).ready(function() {
    $("#{{request()->segment(1)}}MainNav").addClass('active');
    $("#{{request()->segment(1)}}MainNav .collapse").addClass('in');
    });
	$(".date-picker").datetimepicker({
		format: 'yyyy-mm-dd hh:ii:ss',
		autoclose: true,
	});
	$(document).ready(function () {
		$(".select2").select2();
		$('#example-multiple-selected').multiselect();
	});
</script>
</body>
</html>
