@extends('layouts.setting.setting')

@section('content')
@include('layouts.navtop')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Meta</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ url('home') }}">Home</a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{ url('setting/meta') }}">Meta</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>All Meta</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">

    <form name="myform" method="post" enctype="multipart/form-data" class="form" data-async>
        @csrf
        <div class="row">
            <div class="col-lg-12 padding-none">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Meta Editor</h5>
                        <button type="submit" name="Pixel_Save" value="Submit" class="btn btn-primary float-right">Save Setting</button>
                    </div>
                    <div class="ibox-content">
                        @foreach($setting_list as $p)
                        <div class="form-group">
                            <label for="title">{{$p->setting_explain}} </label>
                            <input type="text" class="form-control" name="{{$p->setting_name}}" value="{{$p->setting_value}}">
                        </div>
                        @endforeach

                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>

    </form>

</div>
<div class="footer">
    <div class="float-right">
        <strong>
            </strong>
    </div>
    <div>
        <strong>Copyright</strong> {{$copyright}}
    </div>
</div>
@endsection
