<?php

namespace App\Http\Controllers\API;

use DB;
use App\Categories;
use App\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input as input;

class SlideshowController extends Controller
{
    public $successStatus = 200;

    public function index()
    {
        $post_list    = Post::where('post_type', 'slideshow')->where('status', '1')->orderBy('date', 'DESC')->get();

        $data_array = array();
        foreach ($post_list as $key)
        {
            $images_array = array();
            foreach (Post::where('post_type', 'images')->where([['post_up', '=', $key->post_id]])->skip(0)->take(1)->get() as $p2)
            {
                $images_array[] = url('/').$p2->post_slug . '.' . $p2->post_mime_type;
            }
            $data_array[] = array('title'=>$key->post_title,'images'=>$images_array);
        }

        return response()->json(['status' => 200, 'message' => 'List Slideshow', 'data' => $data_array], $this->successStatus);
    }
}
