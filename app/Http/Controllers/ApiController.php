<?php
namespace App\Http\Controllers;

use DB;

use App\Inflation;
use App\Setting;
use App\Month;
use App\District;
use App\Commodity;
use App\Market;
use App\Member;
use App\Helpers\Base;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input as input;

use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function districts()
    {
        if (Input::get('search') != '') {
            $search = Input::get('search');
        } else {
            $search = '';
        }
        $data_array = array();
        $a_array = array('Kabupaten');
        $b_array = array('Kab');
        foreach (District::where('ka_kabupaten', 'like', '%' . $search . '%')->get() as $key) {
            $data_array[] = array('id' => $key->ka_id, 'text' => str_replace($a_array, $b_array,$key->ka_kabupaten));
        }
        $data_array_all = array('id' => 'all', 'text' => 'Semua Kabupaten / Kota');
        return response()->json(array_merge(array($data_array_all), $data_array), 200);
    }
    public function districts_price()
    {
        if (Input::get('search') != '') {
            $search = Input::get('search');
        } else {
            $search = '';
        }
        if (Input::get('commoditys') != '') {
            $commoditys = Input::get('commoditys');
        } else {
            $commoditys = 15;
        }
        if (Input::get('date') != '') {
            $date = Input::get('date');
        } else {
            $date = '2020-01-10';//date('Y-m-d');
        }
        $prices = DB::table('prices')->selectRaw('ka_id,avg(ha_harga) as price')->where('ko_id', '=',  $commoditys)->groupBy('ka_id')->where('ha_date', '=',  $date)->orderby('ka_id', 'ASC')->get();
        $data_price_array= array();
        foreach ($prices as $key) {
            $data_price_array[] = array($key->price => $key->ka_id);
            // $data_price_name_array[] = array('price' => $key->ka_id);
        }
        $data_array = array();
        $a_array = array('Kabupaten');
        $b_array = array('Kab');
        foreach (District::orderby('districts.ka_id','ASC')->get() as $key) {
            //selectRaw('*,avg(ha_harga) as price')->leftjoin('prices', 'prices.ka_id', '=', 'districts.ka_id')->where('ko_id', '=',  $commoditys)->groupBy('districts.ka_id')->where('ha_date', '=',  $date)->orderby('districts.ka_id', 'ASC')->
            // $price = DB::table('prices')->selectRAW('avg(ha_harga) as price')->where('prices.ka_id', '=', $key->ka_id)->where('prices.ko_id', '=', $commoditys)->where('ha_date', '=',  $date)->value('price');
            $id= $key->ka_id;
            // if (in_array($key->ka_id, array_column($data_price_array, 'id')) > 0)
            // if (!empty($data_price_array[$id]))
            $ids = array_search_id($id, $data_price_array, array(''));
            // print($search_path);
            if ($ids!=null)
            {
                $price_value = $ids;
            }
            else
            {
                $price_value = 0;
            }
            $data_array[] = array('id' => $key->ka_id, 'text' => str_replace($a_array, $b_array, $key->ka_kabupaten), 'price' => currency($price_value));
        }
        // print_r(str_replace($data_price_array, $data_price_array,$data_array));
        $data_array_all = array('id' => 'all', 'text' => 'Semua Kabupaten / Kota', 'price'=>'');
        return response()->json(array_merge(array($data_array_all), $data_array), 200);
    }
    public function districts_price2()
    {
        if (Input::get('search') != '') {
            $search = Input::get('search');
        } else {
            $search = '';
        }
        if (Input::get('commoditys') != '') {
            $commoditys = Input::get('commoditys');
        } else {
            $commoditys = 15;
        }
        if (Input::get('date') != '') {
            $date = Input::get('date');
        } else {
            $date = '2020-01-10'; //date('Y-m-d');
        }
        $prices = DB::table('prices')->selectRaw('*,avg(ha_harga) as price')->where('ko_id', '=',  $commoditys)->groupBy('ka_id')->where('ha_date', '=',  $date)->orderby('ka_id', 'ASC')->get();
        $data_price_array = array();
        foreach ($prices as $key) {
            $data_price_array[] = array('id' => $key->ka_id, 'text' => '', 'price' => $key->ha_harga);
        }
        $data_array = array();
        $a_array = array('Kabupaten');
        $b_array = array('Kab');
        foreach (District::orderby('districts.ka_id', 'ASC')->get() as $key) {
            //selectRaw('*,avg(ha_harga) as price')->leftjoin('prices', 'prices.ka_id', '=', 'districts.ka_id')->where('ko_id', '=',  $commoditys)->groupBy('districts.ka_id')->where('ha_date', '=',  $date)->orderby('districts.ka_id', 'ASC')->
            // $price = DB::table('prices')->selectRAW('avg(ha_harga) as price')->where('prices.ka_id', '=', $key->ka_id)->where('prices.ko_id', '=', $commoditys)->where('ha_date', '=',  $date)->value('price');
            $id = $key->ka_id - 1;
            if (array_search($key->ka_id, array_column($data_price_array, 'id')) > 0) {
                $price_value = (!empty($data_price_array[$id])) ? $data_price_array[$id]['price'] : 0;
            } else {
                $price_value = 0;
            }
            $data_array[] = array('id' => $key->ka_id, 'text' => str_replace($a_array, $b_array, $key->ka_kabupaten), 'price' => $price_value);
        }
        $data_array_all = array('id' => 'all', 'text' => 'Semua Kabupaten / Kota', 'price' => '');
        // return response()->json($data_price_array, 200);
        print_r($data_price_array);
    }
    public function maps()
    {
        if (Input::get('commoditys') != '') {
            $commoditys = Input::get('commoditys');
        } else {
            $commoditys = 1;
        }
        // $date_price = DB::table('prices')->selectRAW('ha_date')->where('ka_id', '=', $ka_id)->orderby('ha_date', 'DESC')->limit(1)->value('ha_date');

        $date = '2020-01-10';//date('Y-m', strtotime($date_price)); //date('Y-m');
        $date_last = '2020-01-10';//date('Y-m', strtotime($date_price . " -1 month"));

        $data_array = array();
        foreach (District::get() as $key) {
            //selectRaw('*, (select ha_harga from prices where districts.ka_id=prices.ka_id and ko_id='. $commoditys.' and ha_date='. $date.' limit 1) as price')->
            $price = DB::table('prices')->selectRAW('avg(ha_harga) as price, ha_date')->where('ka_id', '=', $key->ka_id)->where('ko_id', '=', $commoditys)->where('ha_date', '=',  $date)->orderby('ha_harga','DESC')->get();
            $price_array = array();
            $date_array = array();
            foreach ($price as $key2)
            {
                $price_array[]= $key2->price;
                $date_array[] = $key2->ha_date;
            }

            // $price_last = DB::table('prices')->selectRAW('ha_harga, ha_date')->where('ka_id', '=', $key->ka_id)->where('ko_id', '=', $commoditys)->where('ha_date', 'like',  $date_last . '%')->orderby('ha_harga', 'DESC')->get();
            // $price_last_array = array();
            // $date_last_array = array();
            // foreach ($price_last as $key2) {
            //     $price_last_array[] = $key2->ha_harga;
            //     $date_last_array[] = $key2->ha_date;
            // }

            // $last_month = ($price_last_array) ? array_sum($price_last_array) / count($price_last_array) : 0;
            $this_month = ($price_array) ? array_sum($price_array) / count($price_array) : 0;

            // $price = DB::table('prices')->selectRAW('avg(ha_harga) as price')->where('prices.ka_id', '=', $key->ka_id)->where('prices.ko_id', '=', $commoditys)->value('price');
            $data_array[] = array('id' => $key->ka_id, 'color' => 'red', 'price' => $this_month, 'sum' => '', 'deviasi' => '10.000', 'districts' => $key->ka_kabupaten);
        }
        return response()->json($data_array, 200);
    }
    public function maps_districts()
    {
        if (Input::get('districts') != '') {
            $districts = Input::get('districts');
            $ka_kabupaten = District::where('ka_id', '=', $districts)->limit('1')->value('ka_kabupaten');
        } else {
            $districts = null;
            $ka_kabupaten = '';
        }
        if (Input::get('commoditys') != '') {
            $commoditys = Input::get('commoditys');
            $ko_komoditi = Commodity::where('ko_id', '=', $commoditys)->limit('1')->value('ko_komoditi');
        } else {
            $commoditys = 15;
            $ko_komoditi = '';
        }
        if (Input::get('date') != '') {
            $date = Input::get('date');
        } else {
            $date = date('Y-m-d');
        }

        $price_average = DB::table('prices')->selectRAW('avg(ha_harga) as price, ha_date')->where('ko_id', '=', $commoditys)->where('ha_date', '=',  $date)->orderby('ha_harga', 'DESC')->get();
        $price_average_array = array();
        $date_average_array = array();
        foreach ($price_average as $key2) {
            $price_average_array[] = $key2->price;
            $date_average_array[] = $key2->ha_date;
        }

        $price_range = DB::table('prices')->selectRAW('avg(ha_harga) as price, ha_date')->where('ka_id', '=', $districts)->where('ko_id', '=', $commoditys)->where('ha_date', '>=', date('Y-m-d', strtotime($date . " -7 days")))->where('ha_date', '<=', $date)->groupBy('ha_date')->orderby('ha_date', 'ASC')->get();
        $price_range_array = array();
        $date_range_array = array();
        foreach ($price_range as $key2) {
            $price_range_array[] = round($key2->price);
            $date_range_array[] = date('d',strtotime($key2->ha_date));
        }

        $price = DB::table('prices')->selectRAW('avg(ha_harga) as price, ha_date')->where('ka_id', '=', $districts)->where('ko_id', '=', $commoditys)->where('ha_date', '=',  $date)->orderby('ha_harga', 'DESC')->get();
        $price_array = array();
        $date_array = array();
        foreach ($price as $key2) {
            $price_array[] = $key2->price;
            $date_array[] = $key2->ha_date;
        }

        $price_last = DB::table('prices')->selectRAW('avg(ha_harga) as price, ha_date')->where('ka_id', '=', $districts)->where('ko_id', '=', $commoditys)->where('ha_date', '=',  $date)->orderby('ha_harga', 'DESC')->get();
        $price_last_array = array();
        $date_last_array = array();
        foreach ($price_last as $key2) {
            $price_last_array[] = $key2->price;
            $date_last_array[] = $key2->ha_date;
        }

        $average = ($price_average_array) ? array_sum($price_average_array) / count($price_average_array) : 0;
        $last_day = ($price_last_array) ? array_sum($price_last_array) / count($price_last_array) : 0;
        $this_day = ($price_array) ? array_sum($price_array) / count($price_array) : 0;

        if($this_day== $last_day)
        {
            $price_status = '<span class="badge badge-success"><i class="fa fa-pause fa-rotate-90"></i></span>';
        }
        elseif($this_day> $last_day)
        {
            $price_status = '<span class="badge badge-danger"><i class="fa fa-play fa-rotate-270"></i></span>';
        }
        elseif($this_day< $last_day)
        {
            $price_status = '<span class="badge badge-warning"><i class="fa fa-play fa-rotate-90"></i></span>';
        }

        $average_up = $average + ($average * 5 / 100);
        $average_down = $average - ($average * 5 / 100);

        if ($this_day >= $average_down AND $this_day <= $average_up) {
            $color = 'l2';
        } elseif ($this_day > $average_up) {
            $color = 'l3';
        } elseif ($this_day < $average_down) {
            $color = 'l1';
        }
        else {
            $color = 'l1';
        }

        $data_array = array('id' => $districts, 'districts' => $ka_kabupaten, 'commoditys' => $ko_komoditi, 'color'=> $color, 'date' => date('d M Y',strtotime($date)), 'price' => currency($this_day), 'price_status' => $price_status, 'graph'=> array('price'=>$price_range_array,'date'=> $date_range_array));

        return response()->json($data_array, 200);
    }
    public function maps_districts2()
    {
        if (Input::get('districts') != '') {
            $districts = Input::get('districts');
            $ka_kabupaten = District::where('ka_id', '=', $districts)->limit('1')->value('ka_kabupaten');
        } else {
            $districts = null;
            $ka_kabupaten = '';
        }
        if (Input::get('commoditys') != '') {
            $commoditys = Input::get('commoditys');
            $ko_komoditi = Commodity::where('ko_id', '=', $commoditys)->limit('1')->value('ko_komoditi');
        } else {
            $commoditys = 1;
            $ko_komoditi = '';
        }
        if (Input::get('date') != '') {
            $date = Input::get('date');
        } else {
            $date = date('Y-m-d');
        }
        // $date_price = DB::table('prices')->selectRAW('ha_date')->where('ka_id', '=', $ka_id)->orderby('ha_date', 'DESC')->limit(1)->value('ha_date');

        // $date = '2020-01-10'; //date('Y-m', strtotime($date_price)); //date('Y-m');
        // $date_last = '2020-01-10'; //date('Y-m', strtotime($date_price . " -1 month"));

        //selectRaw('*, (select ha_harga from prices where districts.ka_id=prices.ka_id and ko_id='. $commoditys.' and ha_date='. $date.' limit 1) as price')->
        $price_month = DB::table('prices')->selectRAW('ha_harga, ha_date')->where('ko_id', '=', $commoditys)->where('ha_date', '=',  $date)->orderby('ha_harga', 'DESC')->get();
        $price_month_array = array();
        $date_month_array = array();
        foreach ($price_month as $key2) {
            $price_month_array[] = $key2->ha_harga;
            $date_month_array[] = $key2->ha_date;
        }

        $price_range = DB::table('prices')->selectRAW('ha_harga, ha_date')->where('ka_id', '=', $districts)->where('ko_id', '=', $commoditys)->where('ha_date', '>=', date('Y-m-d', strtotime($date . " -7 days")))->where('ha_date', '<=', $date)->orderby('ha_harga', 'DESC')->get();
        $price_range_array = array();
        $date_range_array = array();
        foreach ($price_range as $key2) {
            $price_range_array[] = $key2->ha_harga;
            $date_range_array[] = date('d', strtotime($key2->ha_date));
        }

        $price = DB::table('prices')->selectRAW('ha_harga, ha_date')->where('ka_id', '=', $districts)->where('ko_id', '=', $commoditys)->where('ha_date', '=',  $date)->orderby('ha_harga', 'DESC')->get();
        $price_array = array();
        $date_array = array();
        foreach ($price as $key2) {
            $price_array[] = $key2->ha_harga;
            $date_array[] = $key2->ha_date;
        }

        $price_last = DB::table('prices')->selectRAW('ha_harga, ha_date')->where('ka_id', '=', $districts)->where('ko_id', '=', $commoditys)->where('ha_date', '=',  $date)->orderby('ha_harga', 'DESC')->get();
        $price_last_array = array();
        $date_last_array = array();
        foreach ($price_last as $key2) {
            $price_last_array[] = $key2->ha_harga;
            $date_last_array[] = $key2->ha_date;
        }

        $average = ($price_month_array) ? array_sum($price_month_array) / count($price_month_array) : 0;
        $deviation = ($price_month_array) ? Stand_Deviation($price_month_array) : 0;
        $last_day = ($price_last_array) ? array_sum($price_last_array) / count($price_last_array) : 0;
        $this_day = ($price_array) ? array_sum($price_array) / count($price_array) : 0;

        if ($this_day == $last_day) {
            $price_status = '<span class="badge badge-success"><i class="fa fa-pause fa-rotate-90"></i></span>';
        } elseif ($this_day > $last_day) {
            $price_status = '<span class="badge badge-danger"><i class="fa fa-play fa-rotate-270"></i></span>';
        } elseif ($this_day < $last_day) {
            $price_status = '<span class="badge badge-warning"><i class="fa fa-play fa-rotate-90"></i></span>';
        }

        $average_up = $average + ($average * 5 / 100);
        $average_down = $average - ($average * 5 / 100);

        if ($this_day < $average_up and $this_day < $average_down) {
            $color = 'green';
        } elseif ($this_day > $average_up) {
            $color = 'red';
        } elseif ($this_day < $average_down) {
            $color = 'green';
        } else {
            $color = '';
        }

        // $price = DB::table('prices')->selectRAW('avg(ha_harga) as price')->where('prices.ka_id', '=', $key->ka_id)->where('prices.ko_id', '=', $commoditys)->value('price');
        $data_array = array('id' => $districts, 'districts' => $ka_kabupaten, 'commoditys' => $ko_komoditi, 'date' => date('d M Y', strtotime($date)), 'color' => $color, 'price' => currency($this_day), 'price_status' => $price_status, 'average' => round($average), 'average_up' => round($average_up), 'average_down' => round($average_down), 'deviasi' => round($deviation), 'graph' => array('price' => $price_range_array, 'date' => $date_range_array));

        return response()->json($data_array, 200);
    }
    public function commoditys()
    {
        if (Input::get('search') != '') {
            $search = Input::get('search');
        } else {
            $search = '';
        }
        $data_array = array();
        foreach (Commodity::where('ko_type', '=', 'harga')->where('ko_komoditi', 'like', '%' . $search . '%')->orderBy('ko_komoditi', 'DESC')->get() as $key) {
            $data_array[] = array('id' => $key->ko_id, 'text' => $key->ko_komoditi);
        }
        return response()->json($data_array, 200);
    }
    public function prices()
    {
        if (Input::get('ka_id') != '') {
            $ka_id = Input::get('ka_id');
            $ka_kabupaten = District::where('ka_id', '=', $ka_id)->limit('1')->value('ka_kabupaten');
        } else {
            $ka_id = null;
            $ka_kabupaten = '';
        }
        if (Input::get('ko_id') != '') {
            $ko_id = Input::get('ko_id');
            $ko_komoditi = Commodity::where('ko_id', '=', $ko_id)->limit('1')->value('ko_komoditi');
        } else {
            $ko_id = null;
            $ko_komoditi = '';
        }
        if (Input::get('date') != '') {
            $date = Input::get('date');
        } else {
            $date = '';
        }
        $data_array = array();
        foreach (DB::table('markets')->where('ka_id', '=', $ka_id)->orderBy('pa_pasar', 'DESC')->get() as $key) {
            $price = DB::table('prices')->where('pa_id', '=', $key->pa_id)->where('ko_id', '=', $ko_id)->where('ha_date', '=', $date)->limit('1')->first();
            $price_old = DB::table('prices')->where('pa_id', '=', $key->pa_id)->where('ko_id', '=', $ko_id)->where('ha_date', '<=', date('Y-m-d', strtotime($date . "-1 days")))->limit('1')->value('ha_harga');
            if ($price) {
                $change_value = ($price and $price_old > 0) ? ($price->ha_harga - $price_old) / $price_old * 100 : 0;
                $price_value = $price->ha_harga;
            } else {
                $change_value = 0;
                $price_value = 0;
            }
            $data_array[] = array('id' => $key->pa_id, 'text' => $key->pa_pasar, 'price' => $price_value, 'change' => round($change_value) . ' %', 'date' => $date, 'date_old' => date('Y-m-d', strtotime($date . "-1 days")));
        }
        return response()->json(array('districts' => $ka_kabupaten, 'commoditys' => $ko_komoditi, 'date' => date('D, d M Y', strtotime($date)), 'markets' => $data_array), 200);
    }
    public function prices_districts()
    {
        if (Input::get('ko_id') != '') {
            $ko_id = Input::get('ko_id');
            $ko_komoditi = Commodity::where('ko_id', '=', $ko_id)->limit('1')->value('ko_komoditi');
        } else {
            $ko_id = null;
            $ko_komoditi = '';
        }
        if (Input::get('date') != '') {
            $date = Input::get('date');
        } else {
            $date = '';
        }
        $data_array = array();
        foreach (District::orderBy('ka_kabupaten', 'ASC')->get() as $key) {
            $price = DB::table('prices')->selectRAW('avg(ha_harga) as total')->where('ka_id', '=', $key->ka_id)->where('ko_id', '=', $ko_id)->where('ha_date', '=', $date)->value('total');
            $price_old = DB::table('prices')->selectRAW('avg(ha_harga) as total')->where('ka_id', '=', $key->ka_id)->where('ko_id', '=', $ko_id)->where('ha_date', '=', date('Y-m-d', strtotime($date . "-1 days")))->value('total');
            if ($price) {
                $change_value = ($price and $price_old > 0) ? ($price - $price_old) / $price_old * 100 : 0;
                $price_value = $price;
            } else {
                $change_value = 0;
                $price_value = 0;
            }
            $data_array[] = array('id' => $key->ka_id, 'text' => $key->ka_kabupaten, 'price' => round($price_value), 'change' => round($change_value) . ' %', 'date' => $date, 'date_old' => date('Y-m-d', strtotime($date . "-1 days")));
        }
        return response()->json(array('districts' => 'Sumatera Utara', 'commoditys' => $ko_komoditi, 'date' => date('D, d M Y', strtotime($date)), 'markets' => $data_array), 200);
    }
    public function inflation()
    {
        if (Input::get('year') != '') {
            $year = Input::get('year');
        } else {
            $year = date('Y');
        }
        $month = Month::get();
        $table_array = array();
        // $graph_array = array();
        foreach ($month as $key) {
            $count = Inflation::where('inflation_month', '=', $year . '-' . $key->month)->first();
            $table_array[] = array('month' => $key->month_name, 'local' => ($count) ? $count->inflation_local : '0', 'national' => ($count) ? $count->inflation_national : '0');
        }
        // foreach ($month as $key) {
        //     $count = Inflation::where('inflation_month', '=', $year . '-' . $key->month)->first();
        //     $graph_array[] = array('month' => $key->month_name, 'local' => ($count) ? $count->inflation_local : '0', 'national' => ($count) ? $count->inflation_national : '0');
        // }
        return response()->json($table_array, 200);
    }
    public function profile()
    {
        if (Input::get('ka_id') != '') {
            $ka_id = Input::get('ka_id');
            $ka_kabupaten = District::where('ka_id', '=', $ka_id)->limit('1')->value('ka_kabupaten');
        } else {
            $ka_id = 1;
            $ka_kabupaten = District::where('ka_id', '=', $ka_id)->limit('1')->value('ka_kabupaten');
        }
        $commodity = Commodity::whereIn('ko_id', [2,10,14,15,19])->get();

        $date_price = DB::table('prices')->selectRAW('ha_date')->where('ka_id', '=', $ka_id)->orderby('ha_date','DESC')->limit(1)->value('ha_date');

        $date = $price = date('Y-m', strtotime($date_price));//date('Y-m');
        $date_last = date('Y-m', strtotime($date_price." -1 month"));
        // $price_max_value = DB::table('prices')->selectRAW('ha_harga,ha_date')->where('ka_id', '=', $ka_id)->where('ko_id', '=', '2')->where('ha_date', 'like',  $date . '%')->orderby('ha_harga', 'DESC')->limit(1)->first();

        $table_array = array();
        foreach ($commodity as $key) {
            // $price = DB::table('prices')->selectRAW('avg(ha_harga) as total')->where('ka_id', '=', $ka_id)->where('ko_id', '=', $key->ko_id)->where('ha_date', 'like',  $date . '%')->limit(1)->value('total');

            $price = DB::table('prices')->selectRAW('ha_harga, ha_date')->where('ka_id', '=', $ka_id)->where('ko_id', '=', $key->ko_id)->where('ha_date', 'like',  $date . '%')->orderby('ha_harga','DESC')->get();
            $price_array = array();
            $date_array = array();
            foreach ($price as $key2)
            {
                $price_array[]= $key2->ha_harga;
                $date_array[] = $key2->ha_date;
            }

            $price_last = DB::table('prices')->selectRAW('ha_harga, ha_date')->where('ka_id', '=', $ka_id)->where('ko_id', '=', $key->ko_id)->where('ha_date', 'like',  $date_last . '%')->orderby('ha_harga', 'DESC')->get();
            $price_last_array = array();
            $date_last_array = array();
            foreach ($price_last as $key2) {
                $price_last_array[] = $key2->ha_harga;
                $date_last_array[] = $key2->ha_date;
            }

            $last_month = ($price_last_array) ? array_sum($price_last_array) / count($price_last_array) : 0;
            $this_month = ($price_array) ? array_sum($price_array) / count($price_array) : 0;

            $last_month_array[] = $last_month;
            $this_month_array[] = $this_month;

            // $last_max_month[] = ($price_last_array) ? $price_last_array[0] : 0;
            // $this_max_month[] = ($price_array) ? $price_array[0] : 0;

            $table_array[] = array('comodity' => $key->ko_komoditi, 'last_month' => round($last_month), 'this_month' => round($this_month), 'highest' => ($date_array) ? date('d M Y',strtotime($date_array[0])) : '');
        }

        $market=  Market::where('ka_id','=', $ka_id)->count();
        $officer=  Member::where('ka_id','=', $ka_id)->get();
        $officer_array = array();
        foreach ($officer as $key) {
            $officer_array[] = array('avatar'=>$key->avatar, 'name'=>$key->first_name.' '. $key->last_name);
        }

        $price_last_month = ($last_month_array) ? array_sum($last_month_array) / count($last_month_array) : 0;
        $price_this_month = ($this_month_array) ? array_sum($this_month_array) / count($this_month_array) : 0;

        $price_max = ($price_this_month) ? round((($price_this_month-$price_last_month)*100)/ $price_this_month) : 0;
        if($price_this_month==$price_last_month)
        {
            $price_status = '<h1 class="text-navy"> '.$price_max.' % <i class="fa fa-pause fa-rotate-90"></i> Sama</h1>';
        }
        elseif($price_this_month>$price_last_month)
        {
            $price_status = '<h1 class="text-danger"> '.$price_max.' % <i class="fa fa-play fa-rotate-270"></i> Naik</h1>';
        }
        elseif($price_this_month<$price_last_month)
        {
            $price_status = '<h1 class="text-info"> '.$price_max.' % <i class="fa fa-play fa-rotate-90"></i> Turun</h1>';
        }

        return response()->json(array('districts' => $ka_kabupaten, 'table' => $table_array, 'price_status'=> $price_status, 'date' => date('D, d M Y', strtotime($date_price)), 'date_max' => ($date_array) ? date('d M Y', strtotime($date_array[0])) : '', 'officer' => $officer_array, 'market' => $market), 200);
    }
    public function year()
    {
        if (Input::get('year') != '') {
            $year = Input::get('year');
        } else {
            $year = date('Y');
        }
        foreach (range('2016', date('Y')) as $key) {
            $data_array[] = array('id' => $key, 'text' => 'Tahun ' . $key);
        }
        return response()->json($data_array, 200);
    }
}
