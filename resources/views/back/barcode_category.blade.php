@extends('layouts.post.post')

@section('content')
@include('layouts.menu')
<div class="row wrapper border-bottom white-bg page-heading">
<div class="col-lg-10">
<h2>Barcode Category</h2>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="{{url('/')}}">Home</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{url('barcode/category/')}}">Barcode Category</a>
    </li>
    <li class="breadcrumb-item active">
        <strong>All Barcode Category</strong>
    </li>
</ol>
</div>
<div class="col-lg-2">

</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
<div class="row">
<div class="col-lg-12">
    <div class="ibox ">

            <div class="ibox-title">
                <h5>Manage Barcode Category </h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-wrench"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="col-sm-3">
                        <form method="get" action="{{ url('barcode/category_search') }}">
                    @csrf
                        <div class="input-group">
                            <input type="text" name="query" placeholder="Search" class="input-sm form-control">
                            <span class="input-group-btn">
                                <button type="submit" name="Pixel_Search" class="btn btn-sm btn-primary"> Go!</button> </span></div>
                        </form>
                    </div>
                    <div class="col-sm-6 m-b-xs">
                    </div>
                    <div class="col-sm-3 m-b-xs">

                        <select id="select-show" onchange="window.location = jQuery('#select-show option:selected').val();" class="input-sm form-control input-s-sm inline float-right category-show">
                            <option  value="{{url('/')}}/barcode/category/?show=10" @if($show=='10')selected @endif>10</option>
                            <option  value="{{url('/')}}/barcode/category/?show=25" @if($show=='25')selected @endif>25</option>
                            <option  value="{{url('/')}}/barcode/category/?show=50" @if($show=='50')selected @endif>50</option>
                            <option  value="{{url('/')}}/barcode/category/?show=100" @if($show=='100')selected @endif>100</option>
                        </select>
                    </div>
                </div>
                <div class="table-responsive">
                    <form method="post">
                    @csrf
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Title </th>
                                <th width="7%">Barcode</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($categories_list as $p)
                            <tr>
                                <td>{{$p->cat_title}}</td>

                                <td class="text-center">
                                    <a href="category_barcode/{{  $p->cat_id }}"><i class="fa fa-search"></i></a>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                        @if (count($categories_list) > 0)
                        <tfoot>
                            <tr>
                                <td colspan="7" class="footable-visible">

                                    @if ($categories_list->lastPage() > 1)
                                            <ul class="pagination pull-right">
                                                <li class="page-item"><a href="#" class="page-link">{{ $categories_list->total() }} Post</a></li>
                                                <li class="{{ ($categories_list->currentPage() == 1) ? ' disabled' : '' }} page-item">
                                                    <a class=" page-link " href="{{ $categories_list->url(1) }}&show={{$show}}" aria-label="Previous">
                                                        <span aria-hidden="true">&laquo;</span>
                                                        <span class="sr-only">Previous</span>
                                                    </a>
                                                </li>
                                                @if ( $categories_list->currentPage() > 5 )
                                                <li class="page-item">
                                                    <a class="page-link" tabindex="-1">...</a>
                                                </li>
                                                @endif

                                                @for ($i = 1; $i <= $categories_list->lastPage(); $i++)
                                                    @if ( ($i > ($categories_list->currentPage() - 5)) && ($i < ($categories_list->currentPage() + 5)) )
                                                    <li class="{{ ($categories_list->currentPage() == $i) ? ' active' : '' }} page-item">
                                                        <a class=" page-link " href="{{ $categories_list->url($i) }}&show={{$show}}">{{ $i }}</a>
                                                    </li>
                                                    @endif
                                                @endfor
                                                @if ( $categories_list->currentPage() < ($categories_list->lastPage() - 4) )
                                                <li class="page-item">
                                                    <a class="page-link" tabindex="-1">...</a>
                                                </li>
                                                @endif

                                                <li class="{{ ($categories_list->currentPage() == $categories_list->lastPage()) ? ' disabled' : '' }} page-item">
                                                    <a href="{{ $categories_list->url($categories_list->currentPage()+1) }}&show={{$show}}" class="page-link" aria-label="Next">
                                                        <span aria-hidden="true">&raquo;</span>
                                                        <span class="sr-only">Next</span>
                                                    </a>
                                                </li>
                                                <li class="page-item {{($categories_list->currentPage()==$categories_list->lastPage())?'disabled':''}}">
                                                    <a class="page-link" href="{{ $categories_list->url($categories_list->lastPage()) }}">
                                                        Last
                                                    </a>
                                                </li>
                                            </ul>
                                    @endif

                                </td>
                            </tr>
                        </tfoot>
                        @endif
                       </table>

            <div class="modal fade" id="modal_delete">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title">Are you sure ?</h4>

                        </div>
                        <div class="modal-body">
                            <p>Do you want to delete this Categories.</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" name="post-delete" value="delete" class="btn btn-danger">Delete</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="modal_deleted">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title">Are you sure ?</h4>

                        </div>
                        <div class="modal-body">
                            <p>Do you want to delete this Categories.</p>
                            <input type="hidden" name="post_id" id="post_id" value="" />
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" name="post-deleted" value="delete" class="btn btn-danger">Delete</button>
                        </div>
                    </div>
                </div>
            </div>


        </form>
    </div>
</div>
</div>
</div>

</div>
</div>
<div class="footer">
<div class="float-right">
<strong>
    </strong>
</div>
<div>
<strong>Copyright</strong> {{$copyright}}
</div>
</div>
@endsection
