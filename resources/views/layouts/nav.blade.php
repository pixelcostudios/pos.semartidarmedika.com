<nav class="navbar-default navbar-static-side" role="navigation">
<div class="sidebar-collapse">
<ul class="nav metismenu" id="side-menu">
    <li class="nav-header">
        <div class="dropdown profile-element"> <span>
                <img alt="image" class="img-circle" src="{{ url('/') }}/themes/default/img/logo.png" />
            </span>
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">{{Auth::user()->first_name.' '.Auth::user()->last_name}}</strong>
                    </span> <span class="text-muted text-xs block">{{Auth::user()->profession}} <b class="caret"></b></span> </span> </a>
            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                <li><a href="{{ url('profile') }}">Profile</a></li>
                <li class="divider"></li>
                <li><a href="{{ url('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                    <form id="logout-form" action="{{ url('logout') }}" method="get" style="display: none;">
                        @csrf
                    </form>
            </li>
            </ul>
        </div>
        <div class="logo-element">
            PX+
        </div>
    </li>
    <li id="homeMainNav"><a href="{{ url('home') }}"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboard</span></a></li>
    <li id="sellingMainNav">
        <a href="#"><i class="fa fa-shopping-cart"></i> <span class="nav-label">Transaksi</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse">
            <li><a href="{{ url('selling/retail') }}">Penjualan (Eceran) </a> </li>
            <li><a href="{{ url('selling/distributor') }}">Penjualan (Grosir) </a></li>
            <li><a href="{{ url('selling/bill') }}">Penjualan (Bon) </a></li>
        </ul>
    </li>
    <li id="returMainNav">
        <a href="#"><i class="fa fa-circle"></i> <span class="nav-label">Retur</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse">
            <li><a href="{{ url('retur/selling') }}">Penjualan</a> </li>
            <li><a href="{{ url('retur/buying') }}">Pembelian</a></li>
        </ul>
    </li>
    <li id="graphMainNav">
        <a href="#"><i class="fa fa-line-chart"></i> <span class="nav-label">Grafik</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse">
            <li><a href="{{ url('graph/stock') }}">Stok Barang</a> </li>
            <li><a href="{{ url('graph/selling_month') }}">Penjualan PerBulan</a></li>
            <li><a href="{{ url('graph/selling_year') }}">Penjualan PerTahun</a></li>
        </ul>
    </li>
    <li id="reportMainNav">
        <a href="#"><i class="fa fa-bar-chart"></i> <span class="nav-label">Laporan</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse">
            <li><a href="{{ url('report/item') }}">Data Barang</a> </li>
            <li><a href="{{ url('report/item_stock') }}">Stok Barang</a></li>
            <li><a href="{{ url('report/selling') }}">Penjualan Eceran</a></li>
            <li><a href="{{ url('report/selling_distributor') }}">Penjualan Grosir</a></li>
            <li><a href="{{ url('report/selling_bill') }}">Penjualan Bon</a></li>
            <li><a href="{{ url('report/retur') }}">Retur Penjualan</a></li>
            <li><a href="{{ url('report/retur_selling') }}">Retur Pembelian</a></li>
            <li><a href="{{ url('report/profit_loss') }}">Laba/Rugi</a></li>
        </ul>
    </li>
    <li id="barcodeMainNav">
        <a href="#"><i class="fa fa-barcode"></i> <span class="nav-label">Barcode</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse">
            <li><a href="{{ url('barcode/category') }}">By Kategori</a> </li>
            <li><a href="{{ url('barcode/all') }}">Semua Barang</a></li>
        </ul>
    </li>
    <li id="itemMainNav">
        <a href="#"><i class="fa fa-product-hunt"></i> <span class="nav-label">Barang</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse">
            <li><a href="{{ url('item/manage') }}">List Barang</a> </li>
            <li><a href="{{ url('item/unit') }}">Unit</a></li>
        </ul>
    </li>
    <li id="categoriesMainNav">
        <a href="#"><i class="fa fa-archive"></i> <span class="nav-label">Categories</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse">
            <li><a href="{{ url('categories') }}">Manage Categories </a></li>
        </ul>
    </li>
    <li id="memberMainNav">
        <a href="#"><i class="fa fa-users"></i> <span class="nav-label">Member</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse">
            <li><a href="{{ url('member') }}">Manage Member </a></li>
            <li><a href="{{ url('member/role') }}">Manage Roles </a></li>
        </ul>
    </li>
    <li id="settingMainNav">
        <a href="#"><i class="fa fa-cog"></i> <span class="nav-label">Setting</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse">
            <li>
                <a href="{{ url('/setting/meta/') }}">
                    <span class="menu-text">Setting Meta</span>
                </a>
            </li>
            <li>
                <a href="{{ url('/setting/contact/') }}">
                    <span class="menu-text">Setting Contact</span>
                </a>
            </li>
            <li>
                <a href="{{ url('/setting/social_network/') }}">
                    <span class="menu-text">Setting Social Network</span>
                </a>
            </li>
            <li>
                <a href="{{ url('/setting/robots/') }}">
                    <span class="menu-text">Setting Robots</span>
                </a>
            </li>
            <li>
                <a href="{{ url('/setting/verified/') }}">
                    <span class="menu-text">Setting Verified</span>
                </a>
            </li>
            <li>
                <a href="{{ url('/setting/visitor/') }}">
                    <span class="menu-text">Setting Visitor</span>
                </a>
            </li>
        </ul>
    </li>
    <li>
        <a href="#"><i class="fa fa-bell"></i> <span class="nav-label">Notification</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse">
            <li>
                <a href="{{ url('/notification/') }}">
                    <span class="menu-text">Manage Notification</span>
                </a>
            </li>
        </ul>
    </li>
</ul>
</div>
</nav>
